FROM atlas/analysisbase:latest
ADD . /MiniAnalysis/source
WORKDIR /MiniAnalysis/build

RUN sudo chown -R atlas /MiniAnalysis && \
	source ~/release_setup.sh && \
	cmake ../source && \
	make -j4
