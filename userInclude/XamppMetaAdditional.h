#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

#include <string>
#include <vector>

#include "AnaTree.h"

class XamppMetaAdditional : public AnaTree {
public:
    XamppMetaAdditional(TChain* chain) : AnaTree(chain) { Init(this->fChain); }

    // Declaration of leaf types
    Bool_t isData;
    UInt_t mcChannelNumber;
    UInt_t runNumber;
    UInt_t ProcessID;
    Double_t xSection;
    Double_t kFactor;
    Double_t FilterEfficiency;
    Long64_t TotalEvents;
    Double_t TotalSumW;
    Double_t TotalSumW2;
    Long64_t ProcessedEvents;
    Double_t prwLuminosity;
    std::string* ProcessName;
    // Char_t *ProcessName[3];

    // List of branches
    TBranch* b_isData;           //!
    TBranch* b_mcChannelNumber;  //!
    TBranch* b_runNumber;        //!
    TBranch* b_ProcessID;        //!
    TBranch* b_xSection;         //!
    TBranch* b_kFactor;          //!
    TBranch* b_FilterEfficiency; //!
    TBranch* b_TotalEvents;      //!
    TBranch* b_TotalSumW;        //!
    TBranch* b_TotalSumW2;       //!
    TBranch* b_ProcessedEvents;  //!
    TBranch* b_prwLuminosity;    //!
    TBranch* b_ProcessName;      //!

    void Init(TChain* tree) override {
        std::lock_guard<std::mutex> lock(_mutex);
        // The Init() function is called when the selector needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).

        ProcessName = 0;
        // Set branch addresses and branch pointers
        if (!tree) return;
        fChain = tree;
        fChain->SetMakeClass(1);

        fChain->SetBranchAddress("isData", &isData, &b_isData);
        fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
        fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
        fChain->SetBranchAddress("ProcessID", &ProcessID, &b_ProcessID);
        fChain->SetBranchAddress("xSection", &xSection, &b_xSection);
        fChain->SetBranchAddress("kFactor", &kFactor, &b_kFactor);
        fChain->SetBranchAddress("FilterEfficiency", &FilterEfficiency, &b_FilterEfficiency);
        fChain->SetBranchAddress("TotalEvents", &TotalEvents, &b_TotalEvents);
        fChain->SetBranchAddress("TotalSumW", &TotalSumW, &b_TotalSumW);
        fChain->SetBranchAddress("TotalSumW2", &TotalSumW2, &b_TotalSumW2);
        fChain->SetBranchAddress("ProcessedEvents", &ProcessedEvents, &b_ProcessedEvents);
        fChain->SetBranchAddress("prwLuminosity", &prwLuminosity, &b_prwLuminosity);
        fChain->SetBranchAddress("ProcessName", &ProcessName, &b_ProcessName);
    }
};
