#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

#include <vector>

#include "AnaObjs.h"
#include "AnaTree.h"

class XamppTree : public AnaTree {
public:
    XamppTree(TChain *chain) : AnaTree(chain) { Init(this->fChain); }
    AnaObjs getElectrons(bool truth_id = false, bool sig_isol = false) {
        AnaObjs eleVec;
        for (unsigned int i = 0; i < this->electrons_pt->size(); i++) {
            int eleID(0);
            if (this->electrons_signal->at(i) == 1) {
                eleID |= EIsSignal;
            }
            if (this->electrons_isol->at(i) == 1) {
                eleID |= EIso;
            }
            if (sig_isol) {
                if (this->electrons_isol_FCHighPtCaloOnly->at(i) == 1) {
                    eleID |= EIsoFCHighPtCaloOnly;
                }
                if (this->electrons_isol_FCLoose->at(i) == 1) {
                    eleID |= EIsoFCLoose;
                }
                if (this->electrons_isol_FCTight->at(i) == 1) {
                    eleID |= EIsoFCTight;
                }
                if (this->electrons_isol_Gradient->at(i) == 1) {
                    eleID |= EIsoGradient;
                }
            }
            if (truth_id) {
                if (this->mcChannelNumber != 0) {
                    if (this->electrons_truthType->at(i) == 2) {
                        eleID |= EIsTruthE;
                    } else {
                        eleID |= EIsNotTruthE;
                    }
                } else {
                    eleID |= EIsNotTruthE;
                }
            }
            AnaObj ele(ELECTRON, (this->electrons_pt->at(i) / 1000), this->electrons_eta->at(i), this->electrons_phi->at(i),
                       (this->electrons_e->at(i) / 1000), this->electrons_charge->at(i), eleID, (this->electrons_MT->at(i) / 1000));
            eleVec.emplace_back(ele);
        }
        eleVec.sortByPt();
        return eleVec;
    }

    AnaObjs getMuons(bool truth_id = false, bool sig_isol = false) {
        AnaObjs muonVec;
        for (unsigned int i = 0; i < this->muons_pt->size(); i++) {
            int muonID(0);
            if (this->muons_signal->at(i) == 1) {
                muonID |= MuIsSignal;
            }
            if (this->muons_isol->at(i) == 1) {
                muonID |= MuIso;
            }

            if (sig_isol) {
                if (this->muons_isol_HighPtTrackOnly->at(i) == 1) {
                    muonID |= MuIsoHighPtTrackOnly;
                }
                if (this->muons_isol_Loose_FixedRad->at(i) == 1) {
                    muonID |= MuIsoLoose_FixedRad;
                }
                if (this->muons_isol_Loose_VarRad->at(i) == 1) {
                    muonID |= MuIsoLoose_VarRad;
                }
                if (this->muons_isol_PflowLoose_FixedRad->at(i) == 1) {
                    muonID |= MuIsoPflowLoose_FixedRad;
                }
                if (this->muons_isol_PflowLoose_VarRad->at(i) == 1) {
                    muonID |= MuIsoPflowLoose_VarRad;
                }
                if (this->muons_isol_PflowTight_FixedRad->at(i) == 1) {
                    muonID |= MuIsoPflowTight_FixedRad;
                }
                if (this->muons_isol_PflowTight_VarRad->at(i) == 1) {
                    muonID |= MuIsoPflowTight_VarRad;
                }
                if (this->muons_isol_TightTrackOnly_FixedRad->at(i) == 1) {
                    muonID |= MuIsoTightTrackOnly_FixedRad;
                }
                if (this->muons_isol_TightTrackOnly_VarRad->at(i) == 1) {
                    muonID |= MuIsoTightTrackOnly_VarRad;
                }
                if (this->muons_isol_Tight_FixedRad->at(i) == 1) {
                    muonID |= MuIsoTight_FixedRad;
                }
                if (this->muons_isol_Tight_VarRad->at(i) == 1) {
                    muonID |= MuIsoTight_VarRad;
                }
            }
            if (truth_id) {
                if (this->mcChannelNumber != 0) {
                    if (this->muons_truthType->at(i) == 6) {
                        muonID |= MuIsTruthMu;
                    } else {
                        muonID |= MuIsNotTruthMu;
                    }
                } else {
                    muonID |= MuIsNotTruthMu;
                }
            }
            AnaObj muon(MUON, (this->muons_pt->at(i) / 1000), this->muons_eta->at(i), this->muons_phi->at(i), (this->muons_e->at(i) / 1000),
                        this->muons_charge->at(i), muonID, (this->muons_MT->at(i) / 1000));
            muonVec.emplace_back(muon);
        }
        muonVec.sortByPt();
        return muonVec;
    }

    AnaObjs getTaus(bool truth_id = false) {
        AnaObjs tauVec;
        for (unsigned int i = 0; i < this->taus_pt->size(); i++) {
            int tauID(0);
            switch (this->taus_Quality->at(i)) {
                case 0:
                    tauID |= TauVeryLoose;
                    break;
                case 1:
                    tauID |= TauVeryLoose | TauLoose;
                    break;
                case 2:
                    tauID |= TauVeryLoose | TauLoose | TauMedium;
                    break;
                case 3:
                    tauID |= TauVeryLoose | TauLoose | TauMedium | TauTight;
            }
            if (this->taus_NTrks->at(i) == 1) {
                tauID |= TauOneProng;
            } else {
                tauID |= TauThreeProng;
            }

            if (truth_id) {
                if (this->mcChannelNumber != 0) {
                    if (this->taus_truthType->at(i) == 10) {
                        tauID |= TauIsTruthTau;
                    } else {
                        tauID |= TauIsNotTruthTau;
                    }
                } else {
                    tauID |= TauIsNotTruthTau;
                }
            }
            AnaObj tau(TAU, (this->taus_pt->at(i) / 1000), this->taus_eta->at(i), this->taus_phi->at(i), (this->taus_e->at(i) / 1000),
                       this->taus_charge->at(i), tauID, (this->taus_MT->at(i) / 1000));
            tauVec.emplace_back(tau);
        }
        tauVec.setProperties("RNNScore", *(this->taus_RNNJetScore));
        tauVec.sortByPt();
        return tauVec;
    }

    AnaObjs getJets() {
        AnaObjs jetVec;
        for (unsigned int i = 0; i < this->jets_pt->size(); i++) {
            int jetID(0);
            if (this->jets_bjet->at(i)) {
                jetID |= GoodBJet;
            }
            if (this->jets_Jvt->at(i)) {
                jetID |= JetVertexTag;
            }
            // Now the jets_signal in ntuple are all 1 so it's useless
            // if(this->jets_signal->at(i)){
            //	jetID |= JetIsSignal;
            //}

            AnaObj jet(JET, 0, jetID);
            jet.SetPtEtaPhiM(this->jets_pt->at(i) / 1000, this->jets_eta->at(i), this->jets_phi->at(i), this->jets_m->at(i) / 1000);
            jetVec.emplace_back(jet);
        }
        jetVec.setProperties("BTagScore", *(this->jets_BTagScore));
        jetVec.sortByPt();
        return jetVec;
    }

    std::vector<SUSYP> getBSM() {
        std::vector<SUSYP> BSMvec;
        // std::cout<<this->TruthBSM_status->size()<<","<<this->TruthBSM_pdgId->size()<<","<<this->TruthBSM_classifierParticleType->size()<<","<<this->TruthBSM_classifierParticleOrigin->size()<<","<<this->TruthBSM_classifierParticleOutCome->size()<<std::endl;
        for (unsigned int i = 0; i < this->TruthBSM_status->size(); i++) {
            auto bsm = SUSYP(this->TruthBSM_pdgId->at(i), this->TruthBSM_status->at(i), int(this->TruthBSM_classifierParticleType->at(i)),
                             int(this->TruthBSM_classifierParticleOrigin->at(i)), int(this->TruthBSM_classifierParticleOutCome->at(i)));
            BSMvec.emplace_back(bsm);
        }
        return BSMvec;
    }

    // Declaration of leaf types
    Int_t mcChannelNumber;
    Float_t Aplanarity;
    Float_t Circularity;
    Float_t ColinearMTauTau;
    Float_t CosChi1;
    Float_t CosChi2;
    Float_t ElMu_MT2_max;
    Float_t ElMu_MT2_min;
    Double_t EleWeight;
    Double_t EleWeightId;
    Double_t EleWeightIso;
    Double_t EleWeightReco;
    Double_t EleWeightTrig_e17_lhmedium_nod0_L1EM15HI_OR_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI;
    Double_t
        EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0;
    Float_t GenFiltHT;
    Float_t GenFiltMET;
    Double_t GenWeight;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEW;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEW;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEW;
    Double_t GenWeight_LHE_AUX_bare_not_for_analyses;
    Double_t GenWeight_LHE_MUR0p5_MUF0p5_PDF260000;
    Double_t GenWeight_LHE_MUR0p5_MUF1p0_PDF260000;
    Double_t GenWeight_LHE_MUR0p5_MUF2p0_PDF260000;
    Double_t GenWeight_LHE_MUR1p0_MUF0p5_PDF260000;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF13100;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF25200;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260000;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260001;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260002;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260003;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260004;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260005;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260006;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260007;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260008;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260009;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260010;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260011;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260012;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260013;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260014;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260015;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260016;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260017;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260018;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260019;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260020;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260021;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260022;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260023;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260024;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260025;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260026;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260027;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260028;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260029;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260030;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260031;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260032;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260033;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260034;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260035;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260036;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260037;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260038;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260039;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260040;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260041;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260042;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260043;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260044;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260045;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260046;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260047;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260048;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260049;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260050;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260051;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260052;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260053;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260054;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260055;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260056;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260057;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260058;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260059;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260060;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260061;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260062;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260063;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260064;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260065;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260066;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260067;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260068;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260069;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260070;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260071;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260072;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260073;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260074;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260075;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260076;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260077;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260078;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260079;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260080;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260081;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260082;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260083;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260084;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260085;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260086;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260087;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260088;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260089;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260090;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260091;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260092;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260093;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260094;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260095;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260096;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260097;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260098;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260099;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF260100;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF265000;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF266000;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90400;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90401;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90402;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90403;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90404;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90405;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90406;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90407;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90408;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90409;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90410;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90411;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90412;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90413;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90414;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90415;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90416;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90417;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90418;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90419;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90420;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90421;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90422;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90423;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90424;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90425;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90426;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90427;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90428;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90429;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90430;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90431;
    Double_t GenWeight_LHE_MUR1p0_MUF1p0_PDF90432;
    Double_t GenWeight_LHE_MUR1p0_MUF2p0_PDF260000;
    Double_t GenWeight_LHE_MUR2p0_MUF0p5_PDF260000;
    Double_t GenWeight_LHE_MUR2p0_MUF1p0_PDF260000;
    Double_t GenWeight_LHE_MUR2p0_MUF2p0_PDF260000;
    Float_t Ht_Jet;
    Float_t Ht_Lep;
    Double_t JetWeight;
    Int_t LQ_ncl;
    Float_t Lin_Aplanarity;
    Float_t Lin_Circularity;
    Float_t Lin_Planarity;
    Float_t Lin_Sphericity;
    Float_t Lin_Sphericity_C;
    Float_t Lin_Sphericity_D;
    Bool_t Lin_Sphericity_IsValid;
    Float_t MCT;
    Bool_t MET_BiSect;
    Float_t MET_Centrality;
    Float_t MET_CosMinDeltaPhi;
    Float_t MET_LepTau_DeltaPhi;
    Float_t MET_LepTau_LeadingJet_VecSumPt;
    Float_t MET_LepTau_VecSumPhi;
    Float_t MET_LepTau_VecSumPt;
    Float_t MET_SumCosDeltaPhi;
    Float_t MT2_max;
    Float_t MT2_min;
    Float_t Meff;
    Float_t MetTST_met;
    Float_t MetTST_phi;
    Float_t MetTST_sumet;
    Float_t MetTST_OverSqrtHT;
    Float_t MetTST_OverSqrtSumET;
    Float_t MetTST_Significance;
    Float_t MetTST_Significance_Rho;
    Float_t MetTST_Significance_VarL;
    Float_t MetTST_Significance_noPUJets_noSoftTerm;
    Float_t MetTST_Significance_noPUJets_noSoftTerm_Rho;
    Float_t MetTST_Significance_noPUJets_noSoftTerm_VarL;
    Float_t MetTST_Significance_phireso_noPUJets;
    Float_t MetTST_Significance_phireso_noPUJets_Rho;
    Float_t MetTST_Significance_phireso_noPUJets_VarL;
    Float_t MetTrack_met;
    Float_t MetTrack_phi;
    Float_t MetTrack_sumet;
    Double_t MuoWeight;
    Double_t MuoWeightIsol;
    Double_t MuoWeightReco;
    Double_t MuoWeightTTVA;
    Double_t MuoWeightTrigHLT_mu14;
    Double_t MuoWeightTrigHLT_mu14_ivarloose;
    Double_t MuoWeightTrigHLT_mu20_iloose_L1MU15;
    Double_t MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu50;
    Double_t MuoWeightTrigHLT_mu26_ivarmedium;
    Double_t MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;
    Double_t MuoWeightTrigHLT_mu50;
    Char_t OS_BaseTauEle;
    Char_t OS_BaseTauMuo;
    Char_t OS_EleEle;
    Char_t OS_MuoMuo;
    Char_t OS_TauEle;
    Char_t OS_TauMuo;
    Char_t OS_TauTau;
    Float_t Oblateness;
    Float_t PTt;
    Float_t Planarity;
    Float_t RJW_CosThetaStarW;
    Float_t RJW_GammaBetaW;
    std::vector<float> *RJW_JigSawCandidates_eta;
    std::vector<unsigned int> *RJW_JigSawCandidates_frame;
    std::vector<float> *RJW_JigSawCandidates_m;
    std::vector<int> *RJW_JigSawCandidates_pdgId;
    std::vector<float> *RJW_JigSawCandidates_phi;
    std::vector<float> *RJW_JigSawCandidates_pt;
    Float_t RJW_dPhiDecayPlaneW;
    Float_t RJZ_BalMetObj_Z;
    Float_t RJZ_CMS_Mass;
    Float_t RJZ_H01_Tau1;
    Float_t RJZ_H01_Z;
    Float_t RJZ_H11_Tau1;
    Float_t RJZ_H11_Z;
    Float_t RJZ_H21_Tau1;
    Float_t RJZ_H21_Z;
    Float_t RJZ_Ht01_Tau1;
    Float_t RJZ_Ht01_Z;
    Float_t RJZ_Ht11_Tau1;
    Float_t RJZ_Ht11_Z;
    Float_t RJZ_Ht21_Tau1;
    Float_t RJZ_Ht21_Z;
    std::vector<float> *RJZ_JigSawCandidates_CosThetaStar;
    std::vector<float> *RJZ_JigSawCandidates_dPhiDecayPlane;
    std::vector<float> *RJZ_JigSawCandidates_eta;
    std::vector<unsigned int> *RJZ_JigSawCandidates_frame;
    std::vector<float> *RJZ_JigSawCandidates_m;
    std::vector<int> *RJZ_JigSawCandidates_pdgId;
    std::vector<float> *RJZ_JigSawCandidates_phi;
    std::vector<float> *RJZ_JigSawCandidates_pt;
    Float_t RJZ_R_BoostZ;
    Float_t RJZ_cosTheta_Tau1;
    Float_t RJZ_cosTheta_Tau2;
    Float_t RJZ_cosTheta_Z;
    Float_t RJZ_dPhiDecayPlane_Tau1;
    Float_t RJZ_dPhiDecayPlane_Tau2;
    Float_t RJZ_dPhiDecayPlane_Z;
    UInt_t RandomLumiBlockNumber;
    UInt_t RandomRunNumber;
    Int_t SUSYFinalState;
    Float_t Sphericity;
    Bool_t Sphericity_IsValid;
    Float_t Spherocity;
    Bool_t Spherocity_IsValid;
    Double_t TauWeight;
    Double_t TauWeightTrigHLT_tau125_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Double_t TauWeightTrigHLT_tau160_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau25_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau25_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau25_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Double_t TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Double_t TauWeightTrigHLT_tau35_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau35_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau35_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Double_t TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau60_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau60_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Double_t TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Float_t Thrust;
    Float_t ThrustMajor;
    Float_t ThrustMinor;
    Bool_t Thrust_IsValid;
    Char_t TrigHLT_e120_lhloose;
    Char_t TrigHLT_e140_lhloose_nod0;
    Char_t TrigHLT_e17_lhmedium_nod0;
    Char_t TrigHLT_e17_lhmedium_nod0_ivarloose;
    Char_t TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;
    Char_t TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;
    Char_t TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12;
    Char_t TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;
    Char_t TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo;
    Char_t TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;
    Char_t TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;
    Char_t TrigHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo;
    Char_t TrigHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50;
    Char_t TrigHLT_e24_lhmedium_L1EM20VH;
    Char_t TrigHLT_e24_lhmedium_nod0_L1EM20VH;
    Char_t TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_e24_lhtight_nod0_ivarloose;
    Char_t TrigHLT_e26_lhtight_nod0_ivarloose;
    Char_t TrigHLT_e60_lhmedium;
    Char_t TrigHLT_e60_lhmedium_nod0;
    Char_t TrigHLT_mu14;
    Char_t TrigHLT_mu14_iloose_tau25_medium1_tracktwo_xe50;
    Char_t TrigHLT_mu14_ivarloose;
    Char_t TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo;
    Char_t TrigHLT_mu14_ivarloose_tau25_medium1_tracktwoEF;
    Char_t TrigHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12;
    Char_t TrigHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;
    Char_t TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo;
    Char_t TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;
    Char_t TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_mu14_tau25_medium1_tracktwo;
    Char_t TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigHLT_mu14_tau25_medium1_tracktwo_xe50;
    Char_t TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;
    Char_t TrigHLT_mu14_tau35_medium1_tracktwo;
    Char_t TrigHLT_mu20_iloose_L1MU15;
    Char_t TrigHLT_mu20_iloose_L1MU15_OR_HLT_mu50;
    Char_t TrigHLT_mu26_ivarmedium;
    Char_t TrigHLT_mu26_ivarmedium_OR_HLT_mu50;
    Char_t TrigHLT_mu50;
    Char_t TrigHLT_noalg_L1J400;
    Char_t TrigHLT_tau125_medium1_tracktwo;
    Char_t TrigHLT_tau160_medium1_tracktwo;
    Char_t TrigHLT_tau160_medium1_tracktwoEF;
    Char_t TrigHLT_tau160_medium1_tracktwoEF_L1TAU100;
    Char_t TrigHLT_tau160_medium1_tracktwo_L1TAU100;
    Char_t TrigHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;
    Char_t TrigHLT_tau25_medium1_tracktwo;
    Char_t TrigHLT_tau25_medium1_tracktwoEF;
    Char_t TrigHLT_tau25_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_tau35_medium1_tracktwo;
    Char_t TrigHLT_tau35_medium1_tracktwoEF;
    Char_t TrigHLT_tau35_medium1_tracktwoEF_L1TAU12IM;
    Char_t TrigHLT_tau35_medium1_tracktwoEF_xe70_L1XE45;
    Char_t TrigHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50;
    Char_t TrigHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45;
    Char_t TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
    Char_t TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
    Char_t TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    Char_t TrigHLT_tau35_medium1_tracktwo_xe70_L1XE45;
    Char_t TrigHLT_tau35_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;
    Char_t TrigHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45;
    Char_t TrigHLT_tau50_medium1_tracktwo_L1TAU12;
    Char_t TrigHLT_tau60_medium1_tracktwo;
    Char_t TrigHLT_tau60_medium1_tracktwoEF;
    Char_t TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    Char_t TrigHLT_tau60_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;
    Char_t TrigHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12;
    Char_t TrigHLT_tau80_medium1_tracktwoEF;
    Char_t TrigHLT_tau80_medium1_tracktwoEF_L1TAU60;
    Char_t TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
    Char_t TrigHLT_tau80_medium1_tracktwo_L1TAU60;
    Char_t TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;
    Char_t TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
    Char_t TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
    Char_t TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;
    Char_t TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
    Char_t TrigHLT_xe100_pufit_L1XE55;
    Char_t TrigHLT_xe110_mht_L1XE50;
    Char_t TrigHLT_xe110_pufit_L1XE50;
    Char_t TrigHLT_xe110_pufit_L1XE55;
    Char_t TrigHLT_xe110_pufit_xe65_L1XE50;
    Char_t TrigHLT_xe110_pufit_xe70_L1XE50;
    Char_t TrigHLT_xe120_pufit_L1XE50;
    Char_t TrigHLT_xe70;
    Char_t TrigHLT_xe70_mht;
    Char_t TrigHLT_xe70_tc_lcw;
    Char_t TrigHLT_xe90_mht_L1XE50;
    Char_t TrigHLT_xe90_pufit_L1XE50;
    Char_t TrigMatchHLT_e120_lhloose;
    Char_t TrigMatchHLT_e140_lhloose_nod0;
    Char_t TrigMatchHLT_e17_lhmedium_nod0;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_ivarloose;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;
    Char_t TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo;
    Char_t TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50;
    Char_t TrigMatchHLT_e24_lhmedium_L1EM20VH;
    Char_t TrigMatchHLT_e24_lhmedium_nod0_L1EM20VH;
    Char_t TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA;
    Char_t TrigMatchHLT_e24_lhtight_nod0_ivarloose;
    Char_t TrigMatchHLT_e26_lhtight_nod0_ivarloose;
    Char_t TrigMatchHLT_e60_lhmedium;
    Char_t TrigMatchHLT_e60_lhmedium_nod0;
    Char_t TrigMatchHLT_mu14;
    Char_t TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50;
    Char_t TrigMatchHLT_mu14_ivarloose;
    Char_t TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;
    Char_t TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF;
    Char_t TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12;
    Char_t TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA;
    Char_t TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;
    Char_t TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;
    Char_t TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;
    Char_t TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;
    Char_t TrigMatchHLT_mu14_tau25_medium1_tracktwo;
    Char_t TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;
    Char_t TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;
    Char_t TrigMatchHLT_mu14_tau35_medium1_tracktwo;
    Char_t TrigMatchHLT_mu20_iloose_L1MU15;
    Char_t TrigMatchHLT_mu20_iloose_L1MU15_OR_HLT_mu50;
    Char_t TrigMatchHLT_mu26_ivarmedium;
    Char_t TrigMatchHLT_mu26_ivarmedium_OR_HLT_mu50;
    Char_t TrigMatchHLT_mu50;
    Char_t TrigMatchHLT_tau125_medium1_tracktwo;
    Char_t TrigMatchHLT_tau160_medium1_tracktwo;
    Char_t TrigMatchHLT_tau160_medium1_tracktwoEF;
    Char_t TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100;
    Char_t TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;
    Char_t TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;
    Char_t TrigMatchHLT_tau25_medium1_tracktwo;
    Char_t TrigMatchHLT_tau25_medium1_tracktwoEF;
    Char_t TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo;
    Char_t TrigMatchHLT_tau35_medium1_tracktwoEF;
    Char_t TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM;
    Char_t TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45;
    Char_t TrigMatchHLT_tau35_mediumRNN_tracktwoMVA;
    Char_t TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;
    Char_t TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45;
    Char_t TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;
    Char_t TrigMatchHLT_tau60_medium1_tracktwo;
    Char_t TrigMatchHLT_tau60_medium1_tracktwoEF;
    Char_t TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    Char_t TrigMatchHLT_tau60_mediumRNN_tracktwoMVA;
    Char_t TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;
    Char_t TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12;
    Char_t TrigMatchHLT_tau80_medium1_tracktwoEF;
    Char_t TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;
    Char_t TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
    Char_t TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;
    Char_t TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;
    Char_t TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
    Char_t TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
    Char_t TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;
    Char_t TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
    Char_t TrigMatching;
    Float_t TruthMET_met;
    Float_t TruthMET_phi;
    Float_t TruthMET_sumet;
    Float_t UnNormed_WolframMoment_0;
    Float_t UnNormed_WolframMoment_1;
    Float_t UnNormed_WolframMoment_10;
    Float_t UnNormed_WolframMoment_11;
    Float_t UnNormed_WolframMoment_2;
    Float_t UnNormed_WolframMoment_3;
    Float_t UnNormed_WolframMoment_4;
    Float_t UnNormed_WolframMoment_5;
    Float_t UnNormed_WolframMoment_6;
    Float_t UnNormed_WolframMoment_7;
    Float_t UnNormed_WolframMoment_8;
    Float_t UnNormed_WolframMoment_9;
    Bool_t UnNormed_WolframMoments_AreValid;
    Float_t VecSumPt_LepTau;
    Int_t Vtx_n;
    Float_t WolframMoment_0;
    Float_t WolframMoment_1;
    Float_t WolframMoment_10;
    Float_t WolframMoment_11;
    Float_t WolframMoment_2;
    Float_t WolframMoment_3;
    Float_t WolframMoment_4;
    Float_t WolframMoment_5;
    Float_t WolframMoment_6;
    Float_t WolframMoment_7;
    Float_t WolframMoment_8;
    Float_t WolframMoment_9;
    Bool_t WolframMoments_AreValid;
    Float_t actualInteractionsPerCrossing;
    Float_t averageInteractionsPerCrossing;
    UInt_t backgroundFlags;
    UInt_t bcid;
    UInt_t coreFlags;
    Float_t corr_avgIntPerX;
    Float_t dPhiMetTrackMetTST;
    std::vector<float> *dilepton_charge;
    std::vector<float> *dilepton_eta;
    std::vector<float> *dilepton_m;
    std::vector<int> *dilepton_pdgId;
    std::vector<float> *dilepton_phi;
    std::vector<float> *dilepton_pt;
    std::vector<int> *electrons_IFFClassType;
    std::vector<float> *electrons_MT;
    std::vector<float> *electrons_charge;
    std::vector<float> *electrons_d0sig;
    std::vector<float> *electrons_e;
    std::vector<float> *electrons_eta;
    std::vector<char> *electrons_isol;
    std::vector<char> *electrons_isol_FCHighPtCaloOnly;
    std::vector<char> *electrons_isol_FCLoose;
    std::vector<char> *electrons_isol_FCTight;
    std::vector<char> *electrons_isol_Gradient;
    std::vector<float> *electrons_phi;
    std::vector<float> *electrons_pt;
    std::vector<char> *electrons_signal;
    std::vector<int> *electrons_truthOrigin;
    std::vector<int> *electrons_truthType;
    std::vector<float> *electrons_z0sinTheta;
    ULong64_t eventNumber;
    UInt_t forwardDetFlags;
    std::vector<double> *jets_BTagScore;
    std::vector<float> *jets_Jvt;
    std::vector<int> *jets_NTrks;
    std::vector<char> *jets_bjet;
    std::vector<float> *jets_eta;
    std::vector<char> *jets_isBadTight;
    std::vector<float> *jets_m;
    std::vector<float> *jets_phi;
    std::vector<float> *jets_pt;
    std::vector<char> *jets_signal;
    std::vector<int> *TruthBSM_status;
    std::vector<int> *TruthBSM_pdgId;
    std::vector<unsigned int> *TruthBSM_classifierParticleOrigin;
    std::vector<unsigned int> *TruthBSM_classifierParticleType;
    std::vector<unsigned int> *TruthBSM_classifierParticleOutCome;
    UInt_t larFlags;
    UInt_t lumiBlock;
    UInt_t lumiFlags;
    Double_t muWeight;
    Float_t mu_density;
    UInt_t muonFlags;
    std::vector<int> *muons_IFFClassType;
    std::vector<float> *muons_MT;
    std::vector<float> *muons_charge;
    std::vector<float> *muons_d0sig;
    std::vector<float> *muons_e;
    std::vector<float> *muons_eta;
    std::vector<char> *muons_isol;
    std::vector<char> *muons_isol_HighPtTrackOnly;
    std::vector<char> *muons_isol_Loose_FixedRad;
    std::vector<char> *muons_isol_Loose_VarRad;
    std::vector<char> *muons_isol_PflowLoose_FixedRad;
    std::vector<char> *muons_isol_PflowLoose_VarRad;
    std::vector<char> *muons_isol_PflowTight_FixedRad;
    std::vector<char> *muons_isol_PflowTight_VarRad;
    std::vector<char> *muons_isol_TightTrackOnly_FixedRad;
    std::vector<char> *muons_isol_TightTrackOnly_VarRad;
    std::vector<char> *muons_isol_Tight_FixedRad;
    std::vector<char> *muons_isol_Tight_VarRad;
    std::vector<float> *muons_phi;
    std::vector<float> *muons_pt;
    std::vector<char> *muons_signal;
    std::vector<int> *muons_truthOrigin;
    std::vector<int> *muons_truthType;
    std::vector<float> *muons_z0sinTheta;
    Int_t n_BJets;
    Int_t n_BaseElec;
    Int_t n_BaseJets;
    Int_t n_BaseMuon;
    Int_t n_BaseTau;
    Int_t n_SignalElec;
    Int_t n_SignalJets;
    Int_t n_SignalMuon;
    Int_t n_SignalTau;
    Int_t n_TruthJets;
    UInt_t pixelFlags;
    UInt_t runNumber;
    UInt_t sctFlags;
    std::vector<float> *taus_BDTEleScore;
    std::vector<float> *taus_BDTEleScoreSigTrans;
    std::vector<int> *taus_ConeTruthLabelID;
    std::vector<float> *taus_MT;
    std::vector<int> *taus_NTrks;
    std::vector<int> *taus_NTrksJet;
    std::vector<int> *taus_PartonTruthLabelID;
    std::vector<int> *taus_Quality;
    std::vector<float> *taus_RNNJetScore;
    std::vector<float> *taus_RNNJetScoreSigTrans;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50;
    std::vector<char> *taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA;
    std::vector<char> *taus_TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50;
    std::vector<char> *taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50;
    std::vector<char> *taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12;
    std::vector<char> *taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA;
    std::vector<char> *taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;
    std::vector<char> *taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;
    std::vector<char> *taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;
    std::vector<char> *taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;
    std::vector<char> *taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;
    std::vector<char> *taus_TrigMatchHLT_mu14_tau35_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau125_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau160_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau160_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100;
    std::vector<char> *taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;
    std::vector<char> *taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;
    std::vector<char> *taus_TrigMatchHLT_tau25_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau25_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45;
    std::vector<char> *taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA;
    std::vector<char> *taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;
    std::vector<char> *taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45;
    std::vector<char> *taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;
    std::vector<char> *taus_TrigMatchHLT_tau60_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau60_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
    std::vector<char> *taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    std::vector<char> *taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA;
    std::vector<char> *taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
    std::vector<char> *taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;
    std::vector<char> *taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
    std::vector<float> *taus_TrkJetWidth;
    std::vector<float> *taus_Width;
    std::vector<float> *taus_charge;
    std::vector<float> *taus_d0;
    std::vector<float> *taus_d0sig;
    std::vector<float> *taus_e;
    std::vector<float> *taus_eta;
    std::vector<float> *taus_phi;
    std::vector<float> *taus_pt;
    std::vector<char> *taus_signalID;
    std::vector<std::vector<float> > *taus_trks_d0;
    std::vector<std::vector<float> > *taus_trks_d0sig;
    std::vector<std::vector<float> > *taus_trks_z0sinTheta;
    std::vector<int> *taus_truthOrigin;
    std::vector<int> *taus_truthType;
    std::vector<float> *taus_z0sinTheta;
    UInt_t tileFlags;
    UInt_t trtFlags;
    // Extra branches
    Float_t ML_score;
    Int_t mergedRunNumber;
    Int_t mergedlumiBlock;
    Double_t tau1Pt;
    Double_t tau2Pt;
    Double_t tau1Mt;
    Double_t tau2Mt;
    Double_t dRtt;
    Double_t dPhitt;
    Double_t lepPt;
    Double_t mT2;
    Double_t MT2; // yuanj
    Double_t MET_sig;
    Double_t MET;
    Double_t Mtt;
    Double_t Mtt_12; // yuanj
    Double_t lepMt;
    Double_t meff;
    Double_t mct;
    Int_t Njet;
    Int_t NTightTau;
    Int_t topTagger;
    Double_t Weight_mc;
    Double_t totalWeight;
    Double_t MT12;
    Double_t MTsum;
    Double_t Ptjet1;
    Double_t Ptjet2;
    Double_t Ptlep1;
    Double_t Pttau1;
    Double_t Mtjet1;
    Double_t MT_jet;
    Double_t MT_tau;
    Double_t MT_lep;
    Double_t dRlt;
    Double_t dPhile;
    Double_t dPhilj;
    Double_t dPhitj;
    Double_t dPhite;
    Double_t dPhilt;
    Double_t dEtalt;
    Double_t M_inv_tl;
    Double_t Mditau;
    Double_t MT2_tl_100;
    Double_t MT2_tl_50;
    Double_t R_ISR;
    Double_t METmeff;
    Double_t TwotauPt;
    Double_t dRLepTwoTau;
    Double_t dPhiLepTwoTau;
    Double_t dRt1MET;
    Double_t dRt2MET;
    Double_t dRlepMET;
    Double_t dR2TauMET;
    Double_t dPhit1MET;
    Double_t dPhit2MET;
    Double_t dPhilepMET;
    Double_t dPhi2TauMET;
    Double_t MCTtt;
    Double_t METOHTj;
    Double_t Minvtt;
    Double_t MT2tt_50;
    Double_t nS_tau;
    Double_t R_ISR1;
    Int_t nJets;
    Double_t HTj;
    Double_t MTtau1met;

    // List of branches
    TBranch *b_mcChannelNumber;                                                                              //!
    TBranch *b_Aplanarity;                                                                                   //!
    TBranch *b_Circularity;                                                                                  //!
    TBranch *b_ColinearMTauTau;                                                                              //!
    TBranch *b_CosChi1;                                                                                      //!
    TBranch *b_CosChi2;                                                                                      //!
    TBranch *b_ElMu_MT2_max;                                                                                 //!
    TBranch *b_ElMu_MT2_min;                                                                                 //!
    TBranch *b_EleWeight;                                                                                    //!
    TBranch *b_EleWeightId;                                                                                  //!
    TBranch *b_EleWeightIso;                                                                                 //!
    TBranch *b_EleWeightReco;                                                                                //!
    TBranch *b_EleWeightTrig_e17_lhmedium_nod0_L1EM15HI_OR_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI; //!
    TBranch *
        b_EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0; //!
    TBranch *b_GenFiltHT;                                                                                                 //!
    TBranch *b_GenFiltMET;                                                                                                //!
    TBranch *b_GenWeight;                                                                                                 //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200;                                                                         //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEW;                                                                   //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEW;                                                                //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEW;                                                              //!
    TBranch *b_GenWeight_LHE_AUX_bare_not_for_analyses;                                                                   //!
    TBranch *b_GenWeight_LHE_MUR0p5_MUF0p5_PDF260000;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR0p5_MUF1p0_PDF260000;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR0p5_MUF2p0_PDF260000;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF0p5_PDF260000;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF13100;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF25200;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260000;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260001;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260002;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260003;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260004;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260005;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260006;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260007;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260008;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260009;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260010;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260011;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260012;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260013;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260014;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260015;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260016;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260017;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260018;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260019;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260020;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260021;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260022;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260023;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260024;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260025;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260026;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260027;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260028;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260029;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260030;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260031;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260032;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260033;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260034;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260035;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260036;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260037;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260038;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260039;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260040;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260041;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260042;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260043;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260044;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260045;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260046;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260047;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260048;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260049;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260050;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260051;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260052;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260053;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260054;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260055;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260056;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260057;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260058;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260059;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260060;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260061;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260062;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260063;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260064;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260065;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260066;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260067;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260068;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260069;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260070;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260071;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260072;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260073;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260074;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260075;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260076;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260077;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260078;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260079;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260080;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260081;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260082;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260083;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260084;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260085;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260086;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260087;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260088;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260089;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260090;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260091;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260092;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260093;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260094;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260095;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260096;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260097;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260098;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260099;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260100;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF265000;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF266000;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90400;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90401;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90402;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90403;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90404;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90405;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90406;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90407;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90408;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90409;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90410;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90411;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90412;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90413;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90414;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90415;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90416;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90417;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90418;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90419;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90420;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90421;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90422;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90423;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90424;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90425;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90426;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90427;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90428;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90429;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90430;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90431;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90432;                                                                      //!
    TBranch *b_GenWeight_LHE_MUR1p0_MUF2p0_PDF260000;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR2p0_MUF0p5_PDF260000;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR2p0_MUF1p0_PDF260000;                                                                     //!
    TBranch *b_GenWeight_LHE_MUR2p0_MUF2p0_PDF260000;                                                                     //!
    TBranch *b_Ht_Jet;                                                                                                    //!
    TBranch *b_Ht_Lep;                                                                                                    //!
    TBranch *b_JetWeight;                                                                                                 //!
    TBranch *b_LQ_ncl;                                                                                                    //!
    TBranch *b_Lin_Aplanarity;                                                                                            //!
    TBranch *b_Lin_Circularity;                                                                                           //!
    TBranch *b_Lin_Planarity;                                                                                             //!
    TBranch *b_Lin_Sphericity;                                                                                            //!
    TBranch *b_Lin_Sphericity_C;                                                                                          //!
    TBranch *b_Lin_Sphericity_D;                                                                                          //!
    TBranch *b_Lin_Sphericity_IsValid;                                                                                    //!
    TBranch *b_MCT;                                                                                                       //!
    TBranch *b_MET_BiSect;                                                                                                //!
    TBranch *b_MET_Centrality;                                                                                            //!
    TBranch *b_MET_CosMinDeltaPhi;                                                                                        //!
    TBranch *b_MET_LepTau_DeltaPhi;                                                                                       //!
    TBranch *b_MET_LepTau_LeadingJet_VecSumPt;                                                                            //!
    TBranch *b_MET_LepTau_VecSumPhi;                                                                                      //!
    TBranch *b_MET_LepTau_VecSumPt;                                                                                       //!
    TBranch *b_MET_SumCosDeltaPhi;                                                                                        //!
    TBranch *b_MT2_max;                                                                                                   //!
    TBranch *b_MT2_min;                                                                                                   //!
    TBranch *b_Meff;                                                                                                      //!
    TBranch *b_MetTST_met;                                                                                                //!
    TBranch *b_MetTST_phi;                                                                                                //!
    TBranch *b_MetTST_sumet;                                                                                              //!
    TBranch *b_MetTST_OverSqrtHT;                                                                                         //!
    TBranch *b_MetTST_OverSqrtSumET;                                                                                      //!
    TBranch *b_MetTST_Significance;                                                                                       //!
    TBranch *b_MetTST_Significance_Rho;                                                                                   //!
    TBranch *b_MetTST_Significance_VarL;                                                                                  //!
    TBranch *b_MetTST_Significance_noPUJets_noSoftTerm;                                                                   //!
    TBranch *b_MetTST_Significance_noPUJets_noSoftTerm_Rho;                                                               //!
    TBranch *b_MetTST_Significance_noPUJets_noSoftTerm_VarL;                                                              //!
    TBranch *b_MetTST_Significance_phireso_noPUJets;                                                                      //!
    TBranch *b_MetTST_Significance_phireso_noPUJets_Rho;                                                                  //!
    TBranch *b_MetTST_Significance_phireso_noPUJets_VarL;                                                                 //!
    TBranch *b_MetTrack_met;                                                                                              //!
    TBranch *b_MetTrack_phi;                                                                                              //!
    TBranch *b_MetTrack_sumet;                                                                                            //!
    TBranch *b_MuoWeight;                                                                                                 //!
    TBranch *b_MuoWeightIsol;                                                                                             //!
    TBranch *b_MuoWeightReco;                                                                                             //!
    TBranch *b_MuoWeightTTVA;                                                                                             //!
    TBranch *b_MuoWeightTrigHLT_mu14;                                                                                     //!
    TBranch *b_MuoWeightTrigHLT_mu14_ivarloose;                                                                           //!
    TBranch *b_MuoWeightTrigHLT_mu20_iloose_L1MU15;                                                                       //!
    TBranch *b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu50;                                                           //!
    TBranch *b_MuoWeightTrigHLT_mu26_ivarmedium;                                                                          //!
    TBranch *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;                                                              //!
    TBranch *b_MuoWeightTrigHLT_mu50;                                                                                     //!
    TBranch *b_OS_BaseTauEle;                                                                                             //!
    TBranch *b_OS_BaseTauMuo;                                                                                             //!
    TBranch *b_OS_EleEle;                                                                                                 //!
    TBranch *b_OS_MuoMuo;                                                                                                 //!
    TBranch *b_OS_TauEle;                                                                                                 //!
    TBranch *b_OS_TauMuo;                                                                                                 //!
    TBranch *b_OS_TauTau;                                                                                                 //!
    TBranch *b_Oblateness;                                                                                                //!
    TBranch *b_PTt;                                                                                                       //!
    TBranch *b_Planarity;                                                                                                 //!
    TBranch *b_RJW_CosThetaStarW;                                                                                         //!
    TBranch *b_RJW_GammaBetaW;                                                                                            //!
    TBranch *b_RJW_JigSawCandidates_eta;                                                                                  //!
    TBranch *b_RJW_JigSawCandidates_frame;                                                                                //!
    TBranch *b_RJW_JigSawCandidates_m;                                                                                    //!
    TBranch *b_RJW_JigSawCandidates_pdgId;                                                                                //!
    TBranch *b_RJW_JigSawCandidates_phi;                                                                                  //!
    TBranch *b_RJW_JigSawCandidates_pt;                                                                                   //!
    TBranch *b_RJW_dPhiDecayPlaneW;                                                                                       //!
    TBranch *b_RJZ_BalMetObj_Z;                                                                                           //!
    TBranch *b_RJZ_CMS_Mass;                                                                                              //!
    TBranch *b_RJZ_H01_Tau1;                                                                                              //!
    TBranch *b_RJZ_H01_Z;                                                                                                 //!
    TBranch *b_RJZ_H11_Tau1;                                                                                              //!
    TBranch *b_RJZ_H11_Z;                                                                                                 //!
    TBranch *b_RJZ_H21_Tau1;                                                                                              //!
    TBranch *b_RJZ_H21_Z;                                                                                                 //!
    TBranch *b_RJZ_Ht01_Tau1;                                                                                             //!
    TBranch *b_RJZ_Ht01_Z;                                                                                                //!
    TBranch *b_RJZ_Ht11_Tau1;                                                                                             //!
    TBranch *b_RJZ_Ht11_Z;                                                                                                //!
    TBranch *b_RJZ_Ht21_Tau1;                                                                                             //!
    TBranch *b_RJZ_Ht21_Z;                                                                                                //!
    TBranch *b_RJZ_JigSawCandidates_CosThetaStar;                                                                         //!
    TBranch *b_RJZ_JigSawCandidates_dPhiDecayPlane;                                                                       //!
    TBranch *b_RJZ_JigSawCandidates_eta;                                                                                  //!
    TBranch *b_RJZ_JigSawCandidates_frame;                                                                                //!
    TBranch *b_RJZ_JigSawCandidates_m;                                                                                    //!
    TBranch *b_RJZ_JigSawCandidates_pdgId;                                                                                //!
    TBranch *b_RJZ_JigSawCandidates_phi;                                                                                  //!
    TBranch *b_RJZ_JigSawCandidates_pt;                                                                                   //!
    TBranch *b_RJZ_R_BoostZ;                                                                                              //!
    TBranch *b_RJZ_cosTheta_Tau1;                                                                                         //!
    TBranch *b_RJZ_cosTheta_Tau2;                                                                                         //!
    TBranch *b_RJZ_cosTheta_Z;                                                                                            //!
    TBranch *b_RJZ_dPhiDecayPlane_Tau1;                                                                                   //!
    TBranch *b_RJZ_dPhiDecayPlane_Tau2;                                                                                   //!
    TBranch *b_RJZ_dPhiDecayPlane_Z;                                                                                      //!
    TBranch *b_RandomLumiBlockNumber;                                                                                     //!
    TBranch *b_RandomRunNumber;                                                                                           //!
    TBranch *b_SUSYFinalState;                                                                                            //!
    TBranch *b_Sphericity;                                                                                                //!
    TBranch *b_Sphericity_IsValid;                                                                                        //!
    TBranch *b_Spherocity;                                                                                                //!
    TBranch *b_Spherocity_IsValid;                                                                                        //!
    TBranch *b_TauWeight;                                                                                                 //!
    TBranch *b_TauWeightTrigHLT_tau125_medium1_tracktwo;                                                                  //!
    TBranch *b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwo;                                                          //!
    TBranch *b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF;                                                        //!
    TBranch *b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                               //!
    TBranch *b_TauWeightTrigHLT_tau160_medium1_tracktwo;                                                                  //!
    TBranch *b_TauWeightTrigHLT_tau25_medium1_tracktwo;                                                                   //!
    TBranch *b_TauWeightTrigHLT_tau25_medium1_tracktwoEF;                                                                 //!
    TBranch *b_TauWeightTrigHLT_tau25_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                                        //!
    TBranch *b_TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF;                                                        //!
    TBranch *b_TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                               //!
    TBranch *b_TauWeightTrigHLT_tau35_medium1_tracktwo;                                                                   //!
    TBranch *b_TauWeightTrigHLT_tau35_medium1_tracktwoEF;                                                                 //!
    TBranch *b_TauWeightTrigHLT_tau35_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                                        //!
    TBranch *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;                                                            //!
    TBranch *b_TauWeightTrigHLT_tau60_medium1_tracktwo;                                                                   //!
    TBranch *b_TauWeightTrigHLT_tau60_medium1_tracktwoEF;                                                                 //!
    TBranch *b_TauWeightTrigHLT_tau60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                                        //!
    TBranch *b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo;                                                            //!
    TBranch *b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF;                                                          //!
    TBranch *b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                                 //!
    TBranch *b_Thrust;                                                                                                    //!
    TBranch *b_ThrustMajor;                                                                                               //!
    TBranch *b_ThrustMinor;                                                                                               //!
    TBranch *b_Thrust_IsValid;                                                                                            //!
    TBranch *b_TrigHLT_e120_lhloose;                                                                                      //!
    TBranch *b_TrigHLT_e140_lhloose_nod0;                                                                                 //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0;                                                                                 //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_ivarloose;                                                                       //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;                                                //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;                                              //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50;                                         //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12;                        //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;                                           //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;                                      //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo;                                                          //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;                                                   //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;                                                     //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;                                                //!
    TBranch *b_TrigHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo;                                                          //!
    TBranch *b_TrigHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50;                                                          //!
    TBranch *b_TrigHLT_e24_lhmedium_L1EM20VH;                                                                             //!
    TBranch *b_TrigHLT_e24_lhmedium_nod0_L1EM20VH;                                                                        //!
    TBranch *b_TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA;                                           //!
    TBranch *b_TrigHLT_e24_lhtight_nod0_ivarloose;                                                                        //!
    TBranch *b_TrigHLT_e26_lhtight_nod0_ivarloose;                                                                        //!
    TBranch *b_TrigHLT_e60_lhmedium;                                                                                      //!
    TBranch *b_TrigHLT_e60_lhmedium_nod0;                                                                                 //!
    TBranch *b_TrigHLT_mu14;                                                                                              //!
    TBranch *b_TrigHLT_mu14_iloose_tau25_medium1_tracktwo_xe50;                                                           //!
    TBranch *b_TrigHLT_mu14_ivarloose;                                                                                    //!
    TBranch *b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo;                                                             //!
    TBranch *b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwoEF;                                                           //!
    TBranch *b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50;                                                      //!
    TBranch *b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12;                                         //!
    TBranch *b_TrigHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA;                                                        //!
    TBranch *b_TrigHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;                                                   //!
    TBranch *b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo;                                                             //!
    TBranch *b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;                                                           //!
    TBranch *b_TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;                                                        //!
    TBranch *b_TrigHLT_mu14_tau25_medium1_tracktwo;                                                                       //!
    TBranch *b_TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50;                                                                //!
    TBranch *b_TrigHLT_mu14_tau25_medium1_tracktwo_xe50;                                                                  //!
    TBranch *b_TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;                                                             //!
    TBranch *b_TrigHLT_mu14_tau35_medium1_tracktwo;                                                                       //!
    TBranch *b_TrigHLT_mu20_iloose_L1MU15;                                                                                //!
    TBranch *b_TrigHLT_mu20_iloose_L1MU15_OR_HLT_mu50;                                                                    //!
    TBranch *b_TrigHLT_mu26_ivarmedium;                                                                                   //!
    TBranch *b_TrigHLT_mu26_ivarmedium_OR_HLT_mu50;                                                                       //!
    TBranch *b_TrigHLT_mu50;                                                                                              //!
    TBranch *b_TrigHLT_noalg_L1J400;                                                                                      //!
    TBranch *b_TrigHLT_tau125_medium1_tracktwo;                                                                           //!
    TBranch *b_TrigHLT_tau160_medium1_tracktwo;                                                                           //!
    TBranch *b_TrigHLT_tau160_medium1_tracktwoEF;                                                                         //!
    TBranch *b_TrigHLT_tau160_medium1_tracktwoEF_L1TAU100;                                                                //!
    TBranch *b_TrigHLT_tau160_medium1_tracktwo_L1TAU100;                                                                  //!
    TBranch *b_TrigHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;                                                             //!
    TBranch *b_TrigHLT_tau25_medium1_tracktwo;                                                                            //!
    TBranch *b_TrigHLT_tau25_medium1_tracktwoEF;                                                                          //!
    TBranch *b_TrigHLT_tau25_mediumRNN_tracktwoMVA;                                                                       //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo;                                                                            //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwoEF;                                                                          //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwoEF_L1TAU12IM;                                                                //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwoEF_xe70_L1XE45;                                                              //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50;                                //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45;                                                        //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;                                                     //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;                                  //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                                //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo_xe70_L1XE45;                                                                //!
    TBranch *b_TrigHLT_tau35_mediumRNN_tracktwoMVA;                                                                       //!
    TBranch *b_TrigHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;                                                             //!
    TBranch *b_TrigHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45;                                                           //!
    TBranch *b_TrigHLT_tau50_medium1_tracktwo_L1TAU12;                                                                    //!
    TBranch *b_TrigHLT_tau60_medium1_tracktwo;                                                                            //!
    TBranch *b_TrigHLT_tau60_medium1_tracktwoEF;                                                                          //!
    TBranch *b_TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;                                            //!
    TBranch *b_TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                                //!
    TBranch *b_TrigHLT_tau60_mediumRNN_tracktwoMVA;                                                                       //!
    TBranch *b_TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;                                      //!
    TBranch *b_TrigHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12;                                                         //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwoEF;                                                                          //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60;                                                                  //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;                                 //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60;                                                                    //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;           //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;                                     //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;                                     //!
    TBranch *b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;                                                               //!
    TBranch *b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;                           //!
    TBranch *b_TrigHLT_xe100_pufit_L1XE55;                                                                                //!
    TBranch *b_TrigHLT_xe110_mht_L1XE50;                                                                                  //!
    TBranch *b_TrigHLT_xe110_pufit_L1XE50;                                                                                //!
    TBranch *b_TrigHLT_xe110_pufit_L1XE55;                                                                                //!
    TBranch *b_TrigHLT_xe110_pufit_xe65_L1XE50;                                                                           //!
    TBranch *b_TrigHLT_xe110_pufit_xe70_L1XE50;                                                                           //!
    TBranch *b_TrigHLT_xe120_pufit_L1XE50;                                                                                //!
    TBranch *b_TrigHLT_xe70;                                                                                              //!
    TBranch *b_TrigHLT_xe70_mht;                                                                                          //!
    TBranch *b_TrigHLT_xe70_tc_lcw;                                                                                       //!
    TBranch *b_TrigHLT_xe90_mht_L1XE50;                                                                                   //!
    TBranch *b_TrigHLT_xe90_pufit_L1XE50;                                                                                 //!
    TBranch *b_TrigMatchHLT_e120_lhloose;                                                                                 //!
    TBranch *b_TrigMatchHLT_e140_lhloose_nod0;                                                                            //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0;                                                                            //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose;                                                                  //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;                                           //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;                                         //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50;                                    //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12;                   //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;                                      //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;                                 //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo;                                                     //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;                                              //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;                                                //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;                                           //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo;                                                     //!
    TBranch *b_TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50;                                                     //!
    TBranch *b_TrigMatchHLT_e24_lhmedium_L1EM20VH;                                                                        //!
    TBranch *b_TrigMatchHLT_e24_lhmedium_nod0_L1EM20VH;                                                                   //!
    TBranch *b_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA;                                      //!
    TBranch *b_TrigMatchHLT_e24_lhtight_nod0_ivarloose;                                                                   //!
    TBranch *b_TrigMatchHLT_e26_lhtight_nod0_ivarloose;                                                                   //!
    TBranch *b_TrigMatchHLT_e60_lhmedium;                                                                                 //!
    TBranch *b_TrigMatchHLT_e60_lhmedium_nod0;                                                                            //!
    TBranch *b_TrigMatchHLT_mu14;                                                                                         //!
    TBranch *b_TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50;                                                      //!
    TBranch *b_TrigMatchHLT_mu14_ivarloose;                                                                               //!
    TBranch *b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;                                                        //!
    TBranch *b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF;                                                      //!
    TBranch *b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50;                                                 //!
    TBranch *b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12;                                    //!
    TBranch *b_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA;                                                   //!
    TBranch *b_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;                                              //!
    TBranch *b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;                                                        //!
    TBranch *b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;                                                      //!
    TBranch *b_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;                                                   //!
    TBranch *b_TrigMatchHLT_mu14_tau25_medium1_tracktwo;                                                                  //!
    TBranch *b_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;                                                           //!
    TBranch *b_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;                                                             //!
    TBranch *b_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;                                                        //!
    TBranch *b_TrigMatchHLT_mu14_tau35_medium1_tracktwo;                                                                  //!
    TBranch *b_TrigMatchHLT_mu20_iloose_L1MU15;                                                                           //!
    TBranch *b_TrigMatchHLT_mu20_iloose_L1MU15_OR_HLT_mu50;                                                               //!
    TBranch *b_TrigMatchHLT_mu26_ivarmedium;                                                                              //!
    TBranch *b_TrigMatchHLT_mu26_ivarmedium_OR_HLT_mu50;                                                                  //!
    TBranch *b_TrigMatchHLT_mu50;                                                                                         //!
    TBranch *b_TrigMatchHLT_tau125_medium1_tracktwo;                                                                      //!
    TBranch *b_TrigMatchHLT_tau160_medium1_tracktwo;                                                                      //!
    TBranch *b_TrigMatchHLT_tau160_medium1_tracktwoEF;                                                                    //!
    TBranch *b_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100;                                                           //!
    TBranch *b_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;                                                             //!
    TBranch *b_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;                                                        //!
    TBranch *b_TrigMatchHLT_tau25_medium1_tracktwo;                                                                       //!
    TBranch *b_TrigMatchHLT_tau25_medium1_tracktwoEF;                                                                     //!
    TBranch *b_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;                                                                  //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo;                                                                       //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwoEF;                                                                     //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM;                                                           //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45;                                                         //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50;                           //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45;                                                   //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;                                                //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;                             //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                           //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45;                                                           //!
    TBranch *b_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA;                                                                  //!
    TBranch *b_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;                                                        //!
    TBranch *b_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45;                                                      //!
    TBranch *b_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;                                                               //!
    TBranch *b_TrigMatchHLT_tau60_medium1_tracktwo;                                                                       //!
    TBranch *b_TrigMatchHLT_tau60_medium1_tracktwoEF;                                                                     //!
    TBranch *b_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;                                       //!
    TBranch *b_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                           //!
    TBranch *b_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA;                                                                  //!
    TBranch *b_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;                                 //!
    TBranch *b_TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12;                                                    //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwoEF;                                                                     //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;                                                             //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;                            //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;                                                               //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;      //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;                                //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;                                //!
    TBranch *b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;                                                          //!
    TBranch *b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;                      //!
    TBranch *b_TrigMatching;                                                                                              //!
    TBranch *b_TruthMET_met;                                                                                              //!
    TBranch *b_TruthMET_phi;                                                                                              //!
    TBranch *b_TruthMET_sumet;                                                                                            //!
    TBranch *b_UnNormed_WolframMoment_0;                                                                                  //!
    TBranch *b_UnNormed_WolframMoment_1;                                                                                  //!
    TBranch *b_UnNormed_WolframMoment_10;                                                                                 //!
    TBranch *b_UnNormed_WolframMoment_11;                                                                                 //!
    TBranch *b_UnNormed_WolframMoment_2;                                                                                  //!
    TBranch *b_UnNormed_WolframMoment_3;                                                                                  //!
    TBranch *b_UnNormed_WolframMoment_4;                                                                                  //!
    TBranch *b_UnNormed_WolframMoment_5;                                                                                  //!
    TBranch *b_UnNormed_WolframMoment_6;                                                                                  //!
    TBranch *b_UnNormed_WolframMoment_7;                                                                                  //!
    TBranch *b_UnNormed_WolframMoment_8;                                                                                  //!
    TBranch *b_UnNormed_WolframMoment_9;                                                                                  //!
    TBranch *b_UnNormed_WolframMoments_AreValid;                                                                          //!
    TBranch *b_VecSumPt_LepTau;                                                                                           //!
    TBranch *b_Vtx_n;                                                                                                     //!
    TBranch *b_WolframMoment_0;                                                                                           //!
    TBranch *b_WolframMoment_1;                                                                                           //!
    TBranch *b_WolframMoment_10;                                                                                          //!
    TBranch *b_WolframMoment_11;                                                                                          //!
    TBranch *b_WolframMoment_2;                                                                                           //!
    TBranch *b_WolframMoment_3;                                                                                           //!
    TBranch *b_WolframMoment_4;                                                                                           //!
    TBranch *b_WolframMoment_5;                                                                                           //!
    TBranch *b_WolframMoment_6;                                                                                           //!
    TBranch *b_WolframMoment_7;                                                                                           //!
    TBranch *b_WolframMoment_8;                                                                                           //!
    TBranch *b_WolframMoment_9;                                                                                           //!
    TBranch *b_WolframMoments_AreValid;                                                                                   //!
    TBranch *b_actualInteractionsPerCrossing;                                                                             //!
    TBranch *b_averageInteractionsPerCrossing;                                                                            //!
    TBranch *b_backgroundFlags;                                                                                           //!
    TBranch *b_bcid;                                                                                                      //!
    TBranch *b_coreFlags;                                                                                                 //!
    TBranch *b_corr_avgIntPerX;                                                                                           //!
    TBranch *b_dPhiMetTrackMetTST;                                                                                        //!
    TBranch *b_dilepton_charge;                                                                                           //!
    TBranch *b_dilepton_eta;                                                                                              //!
    TBranch *b_dilepton_m;                                                                                                //!
    TBranch *b_dilepton_pdgId;                                                                                            //!
    TBranch *b_dilepton_phi;                                                                                              //!
    TBranch *b_dilepton_pt;                                                                                               //!
    TBranch *b_electrons_IFFClassType;                                                                                    //!
    TBranch *b_electrons_MT;                                                                                              //!
    TBranch *b_electrons_charge;                                                                                          //!
    TBranch *b_electrons_d0sig;                                                                                           //!
    TBranch *b_electrons_e;                                                                                               //!
    TBranch *b_electrons_eta;                                                                                             //!
    TBranch *b_electrons_isol;                                                                                            //!
    TBranch *b_electrons_isol_FCHighPtCaloOnly;                                                                           //!
    TBranch *b_electrons_isol_FCLoose;                                                                                    //!
    TBranch *b_electrons_isol_FCTight;                                                                                    //!
    TBranch *b_electrons_isol_Gradient;                                                                                   //!
    TBranch *b_electrons_phi;                                                                                             //!
    TBranch *b_electrons_pt;                                                                                              //!
    TBranch *b_electrons_signal;                                                                                          //!
    TBranch *b_electrons_truthOrigin;                                                                                     //!
    TBranch *b_electrons_truthType;                                                                                       //!
    TBranch *b_electrons_z0sinTheta;                                                                                      //!
    TBranch *b_eventNumber;                                                                                               //!
    TBranch *b_forwardDetFlags;                                                                                           //!
    TBranch *b_jets_BTagScore;                                                                                            //!
    TBranch *b_jets_Jvt;                                                                                                  //!
    TBranch *b_jets_NTrks;                                                                                                //!
    TBranch *b_jets_bjet;                                                                                                 //!
    TBranch *b_jets_eta;                                                                                                  //!
    TBranch *b_jets_isBadTight;                                                                                           //!
    TBranch *b_jets_m;                                                                                                    //!
    TBranch *b_jets_phi;                                                                                                  //!
    TBranch *b_jets_pt;                                                                                                   //!
    TBranch *b_jets_signal;                                                                                               //!
    TBranch *b_TruthBSM_status;                                                                                           //!
    TBranch *b_TruthBSM_pdgId;                                                                                            //!
    TBranch *b_TruthBSM_classifierParticleOrigin;                                                                         //!
    TBranch *b_TruthBSM_classifierParticleType;                                                                           //!
    TBranch *b_TruthBSM_classifierParticleOutCome;                                                                        //!
    TBranch *b_larFlags;                                                                                                  //!
    TBranch *b_lumiBlock;                                                                                                 //!
    TBranch *b_lumiFlags;                                                                                                 //!
    TBranch *b_muWeight;                                                                                                  //!
    TBranch *b_mu_density;                                                                                                //!
    TBranch *b_muonFlags;                                                                                                 //!
    TBranch *b_muons_IFFClassType;                                                                                        //!
    TBranch *b_muons_MT;                                                                                                  //!
    TBranch *b_muons_charge;                                                                                              //!
    TBranch *b_muons_d0sig;                                                                                               //!
    TBranch *b_muons_e;                                                                                                   //!
    TBranch *b_muons_eta;                                                                                                 //!
    TBranch *b_muons_isol;                                                                                                //!
    TBranch *b_muons_isol_HighPtTrackOnly;                                                                                //!
    TBranch *b_muons_isol_Loose_FixedRad;                                                                                 //!
    TBranch *b_muons_isol_Loose_VarRad;                                                                                   //!
    TBranch *b_muons_isol_PflowLoose_FixedRad;                                                                            //!
    TBranch *b_muons_isol_PflowLoose_VarRad;                                                                              //!
    TBranch *b_muons_isol_PflowTight_FixedRad;                                                                            //!
    TBranch *b_muons_isol_PflowTight_VarRad;                                                                              //!
    TBranch *b_muons_isol_TightTrackOnly_FixedRad;                                                                        //!
    TBranch *b_muons_isol_TightTrackOnly_VarRad;                                                                          //!
    TBranch *b_muons_isol_Tight_FixedRad;                                                                                 //!
    TBranch *b_muons_isol_Tight_VarRad;                                                                                   //!
    TBranch *b_muons_phi;                                                                                                 //!
    TBranch *b_muons_pt;                                                                                                  //!
    TBranch *b_muons_signal;                                                                                              //!
    TBranch *b_muons_truthOrigin;                                                                                         //!
    TBranch *b_muons_truthType;                                                                                           //!
    TBranch *b_muons_z0sinTheta;                                                                                          //!
    TBranch *b_n_BJets;                                                                                                   //!
    TBranch *b_n_BaseElec;                                                                                                //!
    TBranch *b_n_BaseJets;                                                                                                //!
    TBranch *b_n_BaseMuon;                                                                                                //!
    TBranch *b_n_BaseTau;                                                                                                 //!
    TBranch *b_n_SignalElec;                                                                                              //!
    TBranch *b_n_SignalJets;                                                                                              //!
    TBranch *b_n_SignalMuon;                                                                                              //!
    TBranch *b_n_SignalTau;                                                                                               //!
    TBranch *b_n_TruthJets;                                                                                               //!
    TBranch *b_pixelFlags;                                                                                                //!
    TBranch *b_runNumber;                                                                                                 //!
    TBranch *b_sctFlags;                                                                                                  //!
    TBranch *b_taus_BDTEleScore;                                                                                          //!
    TBranch *b_taus_BDTEleScoreSigTrans;                                                                                  //!
    TBranch *b_taus_ConeTruthLabelID;                                                                                     //!
    TBranch *b_taus_MT;                                                                                                   //!
    TBranch *b_taus_NTrks;                                                                                                //!
    TBranch *b_taus_NTrksJet;                                                                                             //!
    TBranch *b_taus_PartonTruthLabelID;                                                                                   //!
    TBranch *b_taus_Quality;                                                                                              //!
    TBranch *b_taus_RNNJetScore;                                                                                          //!
    TBranch *b_taus_RNNJetScoreSigTrans;                                                                                  //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;                                      //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;                                    //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50;                               //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12;              //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;                                 //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;                            //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo;                                                //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;                                         //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;                                           //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;                                      //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo;                                                //!
    TBranch *b_taus_TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50;                                                //!
    TBranch *b_taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA;                                 //!
    TBranch *b_taus_TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50;                                                 //!
    TBranch *b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;                                                   //!
    TBranch *b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF;                                                 //!
    TBranch *b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50;                                            //!
    TBranch *b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12;                               //!
    TBranch *b_taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA;                                              //!
    TBranch *b_taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50;                                         //!
    TBranch *b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;                                                   //!
    TBranch *b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;                                                 //!
    TBranch *b_taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;                                              //!
    TBranch *b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo;                                                             //!
    TBranch *b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;                                                      //!
    TBranch *b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;                                                        //!
    TBranch *b_taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;                                                   //!
    TBranch *b_taus_TrigMatchHLT_mu14_tau35_medium1_tracktwo;                                                             //!
    TBranch *b_taus_TrigMatchHLT_tau125_medium1_tracktwo;                                                                 //!
    TBranch *b_taus_TrigMatchHLT_tau160_medium1_tracktwo;                                                                 //!
    TBranch *b_taus_TrigMatchHLT_tau160_medium1_tracktwoEF;                                                               //!
    TBranch *b_taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100;                                                      //!
    TBranch *b_taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;                                                        //!
    TBranch *b_taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;                                                   //!
    TBranch *b_taus_TrigMatchHLT_tau25_medium1_tracktwo;                                                                  //!
    TBranch *b_taus_TrigMatchHLT_tau25_medium1_tracktwoEF;                                                                //!
    TBranch *b_taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;                                                             //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo;                                                                  //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF;                                                                //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM;                                                      //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45;                                                    //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50;                      //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45;                                              //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;                                           //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;                        //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                      //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45;                                                      //!
    TBranch *b_taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA;                                                             //!
    TBranch *b_taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;                                                   //!
    TBranch *b_taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45;                                                 //!
    TBranch *b_taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;                                                          //!
    TBranch *b_taus_TrigMatchHLT_tau60_medium1_tracktwo;                                                                  //!
    TBranch *b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF;                                                                //!
    TBranch *b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;                                  //!
    TBranch *b_taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                      //!
    TBranch *b_taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA;                                                             //!
    TBranch *b_taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;                            //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12;                                               //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF;                                                                //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;                                                        //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;                       //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;                                                          //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I; //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;                           //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;                           //!
    TBranch *b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;                                                     //!
    TBranch *b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;                 //!
    TBranch *b_taus_TrkJetWidth;                                                                                          //!
    TBranch *b_taus_Width;                                                                                                //!
    TBranch *b_taus_charge;                                                                                               //!
    TBranch *b_taus_d0;                                                                                                   //!
    TBranch *b_taus_d0sig;                                                                                                //!
    TBranch *b_taus_e;                                                                                                    //!
    TBranch *b_taus_eta;                                                                                                  //!
    TBranch *b_taus_phi;                                                                                                  //!
    TBranch *b_taus_pt;                                                                                                   //!
    TBranch *b_taus_signalID;                                                                                             //!
    TBranch *b_taus_trks_d0;                                                                                              //!
    TBranch *b_taus_trks_d0sig;                                                                                           //!
    TBranch *b_taus_trks_z0sinTheta;                                                                                      //!
    TBranch *b_taus_truthOrigin;                                                                                          //!
    TBranch *b_taus_truthType;                                                                                            //!
    TBranch *b_taus_z0sinTheta;                                                                                           //!
    TBranch *b_tileFlags;                                                                                                 //!
    TBranch *b_trtFlags;                                                                                                  //!
    TBranch *b_MCTtt;
    TBranch *b_METOHTj;
    TBranch *b_Minvtt;
    TBranch *b_MT2tt_50;
    TBranch *b_nS_tau;
    TBranch *b_R_ISR1;
    TBranch *b_nJets;
    TBranch *b_HTj;
    TBranch *b_MTtau1met;

    TBranch *b_ML_score;        //!
    TBranch *b_mergedRunNumber; //!
    TBranch *b_mergedlumiBlock; //!
    TBranch *b_tau1Pt;          //!
    TBranch *b_tau2Pt;          //!
    TBranch *b_tau1Mt;          //!
    TBranch *b_tau2Mt;          //!
    TBranch *b_dRtt;            //!
    TBranch *b_dPhitt;          //!
    TBranch *b_lepPt;           //!
    TBranch *b_mT2;             //!
    TBranch *b_MT2;             //! yuanj
    TBranch *b_MET_sig;         //!
    TBranch *b_MET;             //!
    TBranch *b_Mtt;             //!
    TBranch *b_Mtt_12;          //! yuanj
    TBranch *b_lepMt;           //!
    TBranch *b_meff;            //!
    TBranch *b_mct;             //!
    TBranch *b_Njet;            //!
    TBranch *b_NTightTau;       //!
    TBranch *b_topTagger;       //!
    TBranch *b_Weight_mc;       //!
    TBranch *b_totalWeight;     //!
    TBranch *b_MT12;            //!
    TBranch *b_MTsum;           //!
    TBranch *b_Ptjet1;          //!
    TBranch *b_Ptjet2;          //!
    TBranch *b_Ptlep1;          //!
    TBranch *b_Pttau1;          //!
    TBranch *b_Mtjet1;          //!
    TBranch *b_MT_jet;          //!
    TBranch *b_MT_tau;          //!
    TBranch *b_MT_lep;          //!
    TBranch *b_dRlt;            //!
    TBranch *b_dPhile;          //!
    TBranch *b_dPhilj;          //!
    TBranch *b_dPhitj;          //!
    TBranch *b_dPhite;          //!
    TBranch *b_dPhilt;          //!
    TBranch *b_dEtalt;          //!
    TBranch *b_M_inv_tl;        //!
    TBranch *b_Mditau;          //!;
    TBranch *b_MT2_tl_100;      //!;
    TBranch *b_MT2_tl_50;       //!;
    TBranch *b_R_ISR;           //!
    TBranch *b_METmeff;         //!
    TBranch *b_TwotauPt;        //!
    TBranch *b_dRLepTwoTau;     //!
    TBranch *b_dPhiLepTwoTau;   //!
    TBranch *b_dRt1MET;         //!
    TBranch *b_dRt2MET;         //!
    TBranch *b_dRlepMET;        //!
    TBranch *b_dR2TauMET;       //!
    TBranch *b_dPhit1MET;       //!
    TBranch *b_dPhit2MET;       //!
    TBranch *b_dPhilepMET;      //!
    TBranch *b_dPhi2TauMET;     //!

    void Init(TChain *tree) override {
        std::lock_guard<std::mutex> lock(_mutex);
        // The Init() function is called when the selector needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).

        // Set object pointer
        mcChannelNumber = 0;
        RJW_JigSawCandidates_eta = 0;
        RJW_JigSawCandidates_frame = 0;
        RJW_JigSawCandidates_m = 0;
        RJW_JigSawCandidates_pdgId = 0;
        RJW_JigSawCandidates_phi = 0;
        RJW_JigSawCandidates_pt = 0;
        RJZ_JigSawCandidates_CosThetaStar = 0;
        RJZ_JigSawCandidates_dPhiDecayPlane = 0;
        RJZ_JigSawCandidates_eta = 0;
        RJZ_JigSawCandidates_frame = 0;
        RJZ_JigSawCandidates_m = 0;
        RJZ_JigSawCandidates_pdgId = 0;
        RJZ_JigSawCandidates_phi = 0;
        RJZ_JigSawCandidates_pt = 0;
        dilepton_charge = 0;
        dilepton_eta = 0;
        dilepton_m = 0;
        dilepton_pdgId = 0;
        dilepton_phi = 0;
        dilepton_pt = 0;
        electrons_IFFClassType = 0;
        electrons_MT = 0;
        electrons_charge = 0;
        electrons_d0sig = 0;
        electrons_e = 0;
        electrons_eta = 0;
        electrons_isol = 0;
        electrons_isol_FCHighPtCaloOnly = 0;
        electrons_isol_FCLoose = 0;
        electrons_isol_FCTight = 0;
        electrons_isol_Gradient = 0;
        electrons_phi = 0;
        electrons_pt = 0;
        electrons_signal = 0;
        electrons_truthOrigin = 0;
        electrons_truthType = 0;
        electrons_z0sinTheta = 0;
        jets_BTagScore = 0;
        jets_Jvt = 0;
        jets_NTrks = 0;
        jets_bjet = 0;
        jets_eta = 0;
        jets_isBadTight = 0;
        jets_m = 0;
        jets_phi = 0;
        jets_pt = 0;
        jets_signal = 0;
        TruthBSM_status = 0;
        TruthBSM_pdgId = 0;
        TruthBSM_classifierParticleOrigin = 0;
        TruthBSM_classifierParticleType = 0;
        TruthBSM_classifierParticleOutCome = 0;
        muons_IFFClassType = 0;
        muons_MT = 0;
        muons_charge = 0;
        muons_d0sig = 0;
        muons_e = 0;
        muons_eta = 0;
        muons_isol = 0;
        muons_isol_HighPtTrackOnly = 0;
        muons_isol_Loose_FixedRad = 0;
        muons_isol_Loose_VarRad = 0;
        muons_isol_PflowLoose_FixedRad = 0;
        muons_isol_PflowLoose_VarRad = 0;
        muons_isol_PflowTight_FixedRad = 0;
        muons_isol_PflowTight_VarRad = 0;
        muons_isol_TightTrackOnly_FixedRad = 0;
        muons_isol_TightTrackOnly_VarRad = 0;
        muons_isol_Tight_FixedRad = 0;
        muons_isol_Tight_VarRad = 0;
        muons_phi = 0;
        muons_pt = 0;
        muons_signal = 0;
        muons_truthOrigin = 0;
        muons_truthType = 0;
        muons_z0sinTheta = 0;
        taus_BDTEleScore = 0;
        taus_BDTEleScoreSigTrans = 0;
        taus_ConeTruthLabelID = 0;
        taus_MT = 0;
        taus_NTrks = 0;
        taus_NTrksJet = 0;
        taus_PartonTruthLabelID = 0;
        taus_Quality = 0;
        taus_RNNJetScore = 0;
        taus_RNNJetScoreSigTrans = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50 = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12 = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50 = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50 = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
        taus_TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo = 0;
        taus_TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50 = 0;
        taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA = 0;
        taus_TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50 = 0;
        taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo = 0;
        taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50 = 0;
        taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12 = 0;
        taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA = 0;
        taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
        taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo = 0;
        taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA = 0;
        taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo = 0;
        taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50 = 0;
        taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50 = 0;
        taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
        taus_TrigMatchHLT_mu14_tau35_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau125_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau160_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau160_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100 = 0;
        taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100 = 0;
        taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100 = 0;
        taus_TrigMatchHLT_tau25_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau25_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45 = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50 = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45 = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50 = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45 = 0;
        taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA = 0;
        taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM = 0;
        taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45 = 0;
        taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12 = 0;
        taus_TrigMatchHLT_tau60_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau60_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50 = 0;
        taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50 = 0;
        taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA = 0;
        taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
        taus_TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12 = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60 = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40 = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60 = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12 = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40 = 0;
        taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60 = 0;
        taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40 = 0;
        taus_TrkJetWidth = 0;
        taus_Width = 0;
        taus_charge = 0;
        taus_d0 = 0;
        taus_d0sig = 0;
        taus_e = 0;
        taus_eta = 0;
        taus_phi = 0;
        taus_pt = 0;
        taus_signalID = 0;
        taus_trks_d0 = 0;
        taus_trks_d0sig = 0;
        taus_trks_z0sinTheta = 0;
        taus_truthOrigin = 0;
        taus_truthType = 0;
        taus_z0sinTheta = 0;

        fChain = tree;

        fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
        fChain->SetBranchAddress("Aplanarity", &Aplanarity, &b_Aplanarity);
        fChain->SetBranchAddress("Circularity", &Circularity, &b_Circularity);
        fChain->SetBranchAddress("ColinearMTauTau", &ColinearMTauTau, &b_ColinearMTauTau);
        fChain->SetBranchAddress("CosChi1", &CosChi1, &b_CosChi1);
        fChain->SetBranchAddress("CosChi2", &CosChi2, &b_CosChi2);
        fChain->SetBranchAddress("ElMu_MT2_max", &ElMu_MT2_max, &b_ElMu_MT2_max);
        fChain->SetBranchAddress("ElMu_MT2_min", &ElMu_MT2_min, &b_ElMu_MT2_min);
        fChain->SetBranchAddress("EleWeight", &EleWeight, &b_EleWeight);
        fChain->SetBranchAddress("EleWeightId", &EleWeightId, &b_EleWeightId);
        fChain->SetBranchAddress("EleWeightIso", &EleWeightIso, &b_EleWeightIso);
        fChain->SetBranchAddress("EleWeightReco", &EleWeightReco, &b_EleWeightReco);
        fChain->SetBranchAddress("EleWeightTrig_e17_lhmedium_nod0_L1EM15HI_OR_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI",
                                 &EleWeightTrig_e17_lhmedium_nod0_L1EM15HI_OR_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI,
                                 &b_EleWeightTrig_e17_lhmedium_nod0_L1EM15HI_OR_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI);
        fChain->SetBranchAddress(
            "EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_"
            "nod0",
            &EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,
            &b_EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0);
        fChain->SetBranchAddress("GenFiltHT", &GenFiltHT, &b_GenFiltHT);
        fChain->SetBranchAddress("GenFiltMET", &GenFiltMET, &b_GenFiltMET);
        fChain->SetBranchAddress("GenWeight", &GenWeight, &b_GenWeight);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200", &GenWeight_LHE_MUR1_MUF1_PDF303200, &b_GenWeight_LHE_MUR1_MUF1_PDF303200);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEW", &GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEW,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEW);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEW", &GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEW,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEW);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEW", &GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEW,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEW);
        fChain->SetBranchAddress("GenWeight_LHE_AUX_bare_not_for_analyses", &GenWeight_LHE_AUX_bare_not_for_analyses,
                                 &b_GenWeight_LHE_AUX_bare_not_for_analyses);
        fChain->SetBranchAddress("GenWeight_LHE_MUR0p5_MUF0p5_PDF260000", &GenWeight_LHE_MUR0p5_MUF0p5_PDF260000,
                                 &b_GenWeight_LHE_MUR0p5_MUF0p5_PDF260000);
        fChain->SetBranchAddress("GenWeight_LHE_MUR0p5_MUF1p0_PDF260000", &GenWeight_LHE_MUR0p5_MUF1p0_PDF260000,
                                 &b_GenWeight_LHE_MUR0p5_MUF1p0_PDF260000);
        fChain->SetBranchAddress("GenWeight_LHE_MUR0p5_MUF2p0_PDF260000", &GenWeight_LHE_MUR0p5_MUF2p0_PDF260000,
                                 &b_GenWeight_LHE_MUR0p5_MUF2p0_PDF260000);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF0p5_PDF260000", &GenWeight_LHE_MUR1p0_MUF0p5_PDF260000,
                                 &b_GenWeight_LHE_MUR1p0_MUF0p5_PDF260000);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF13100", &GenWeight_LHE_MUR1p0_MUF1p0_PDF13100,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF13100);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF25200", &GenWeight_LHE_MUR1p0_MUF1p0_PDF25200,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF25200);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260000", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260000,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260000);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260001", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260001,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260001);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260002", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260002,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260002);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260003", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260003,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260003);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260004", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260004,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260004);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260005", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260005,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260005);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260006", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260006,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260006);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260007", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260007,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260007);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260008", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260008,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260008);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260009", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260009,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260009);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260010", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260010,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260010);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260011", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260011,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260011);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260012", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260012,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260012);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260013", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260013,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260013);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260014", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260014,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260014);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260015", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260015,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260015);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260016", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260016,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260016);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260017", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260017,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260017);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260018", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260018,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260018);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260019", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260019,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260019);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260020", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260020,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260020);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260021", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260021,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260021);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260022", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260022,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260022);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260023", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260023,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260023);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260024", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260024,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260024);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260025", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260025,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260025);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260026", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260026,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260026);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260027", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260027,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260027);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260028", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260028,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260028);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260029", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260029,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260029);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260030", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260030,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260030);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260031", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260031,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260031);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260032", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260032,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260032);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260033", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260033,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260033);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260034", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260034,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260034);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260035", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260035,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260035);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260036", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260036,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260036);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260037", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260037,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260037);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260038", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260038,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260038);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260039", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260039,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260039);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260040", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260040,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260040);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260041", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260041,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260041);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260042", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260042,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260042);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260043", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260043,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260043);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260044", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260044,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260044);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260045", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260045,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260045);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260046", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260046,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260046);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260047", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260047,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260047);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260048", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260048,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260048);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260049", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260049,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260049);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260050", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260050,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260050);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260051", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260051,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260051);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260052", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260052,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260052);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260053", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260053,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260053);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260054", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260054,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260054);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260055", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260055,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260055);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260056", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260056,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260056);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260057", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260057,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260057);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260058", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260058,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260058);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260059", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260059,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260059);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260060", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260060,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260060);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260061", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260061,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260061);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260062", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260062,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260062);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260063", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260063,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260063);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260064", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260064,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260064);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260065", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260065,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260065);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260066", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260066,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260066);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260067", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260067,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260067);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260068", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260068,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260068);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260069", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260069,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260069);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260070", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260070,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260070);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260071", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260071,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260071);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260072", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260072,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260072);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260073", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260073,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260073);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260074", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260074,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260074);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260075", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260075,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260075);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260076", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260076,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260076);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260077", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260077,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260077);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260078", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260078,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260078);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260079", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260079,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260079);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260080", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260080,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260080);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260081", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260081,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260081);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260082", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260082,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260082);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260083", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260083,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260083);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260084", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260084,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260084);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260085", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260085,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260085);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260086", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260086,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260086);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260087", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260087,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260087);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260088", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260088,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260088);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260089", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260089,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260089);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260090", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260090,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260090);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260091", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260091,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260091);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260092", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260092,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260092);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260093", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260093,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260093);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260094", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260094,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260094);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260095", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260095,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260095);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260096", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260096,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260096);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260097", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260097,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260097);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260098", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260098,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260098);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260099", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260099,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260099);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF260100", &GenWeight_LHE_MUR1p0_MUF1p0_PDF260100,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF260100);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF265000", &GenWeight_LHE_MUR1p0_MUF1p0_PDF265000,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF265000);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF266000", &GenWeight_LHE_MUR1p0_MUF1p0_PDF266000,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF266000);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90400", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90400,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90400);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90401", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90401,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90401);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90402", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90402,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90402);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90403", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90403,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90403);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90404", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90404,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90404);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90405", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90405,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90405);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90406", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90406,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90406);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90407", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90407,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90407);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90408", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90408,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90408);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90409", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90409,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90409);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90410", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90410,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90410);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90411", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90411,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90411);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90412", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90412,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90412);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90413", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90413,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90413);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90414", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90414,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90414);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90415", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90415,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90415);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90416", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90416,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90416);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90417", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90417,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90417);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90418", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90418,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90418);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90419", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90419,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90419);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90420", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90420,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90420);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90421", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90421,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90421);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90422", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90422,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90422);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90423", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90423,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90423);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90424", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90424,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90424);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90425", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90425,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90425);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90426", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90426,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90426);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90427", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90427,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90427);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90428", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90428,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90428);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90429", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90429,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90429);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90430", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90430,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90430);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90431", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90431,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90431);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF1p0_PDF90432", &GenWeight_LHE_MUR1p0_MUF1p0_PDF90432,
                                 &b_GenWeight_LHE_MUR1p0_MUF1p0_PDF90432);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1p0_MUF2p0_PDF260000", &GenWeight_LHE_MUR1p0_MUF2p0_PDF260000,
                                 &b_GenWeight_LHE_MUR1p0_MUF2p0_PDF260000);
        fChain->SetBranchAddress("GenWeight_LHE_MUR2p0_MUF0p5_PDF260000", &GenWeight_LHE_MUR2p0_MUF0p5_PDF260000,
                                 &b_GenWeight_LHE_MUR2p0_MUF0p5_PDF260000);
        fChain->SetBranchAddress("GenWeight_LHE_MUR2p0_MUF1p0_PDF260000", &GenWeight_LHE_MUR2p0_MUF1p0_PDF260000,
                                 &b_GenWeight_LHE_MUR2p0_MUF1p0_PDF260000);
        fChain->SetBranchAddress("GenWeight_LHE_MUR2p0_MUF2p0_PDF260000", &GenWeight_LHE_MUR2p0_MUF2p0_PDF260000,
                                 &b_GenWeight_LHE_MUR2p0_MUF2p0_PDF260000);
        fChain->SetBranchAddress("Ht_Jet", &Ht_Jet, &b_Ht_Jet);
        fChain->SetBranchAddress("Ht_Lep", &Ht_Lep, &b_Ht_Lep);
        fChain->SetBranchAddress("JetWeight", &JetWeight, &b_JetWeight);
        fChain->SetBranchAddress("LQ_ncl", &LQ_ncl, &b_LQ_ncl);
        fChain->SetBranchAddress("Lin_Aplanarity", &Lin_Aplanarity, &b_Lin_Aplanarity);
        fChain->SetBranchAddress("Lin_Circularity", &Lin_Circularity, &b_Lin_Circularity);
        fChain->SetBranchAddress("Lin_Planarity", &Lin_Planarity, &b_Lin_Planarity);
        fChain->SetBranchAddress("Lin_Sphericity", &Lin_Sphericity, &b_Lin_Sphericity);
        fChain->SetBranchAddress("Lin_Sphericity_C", &Lin_Sphericity_C, &b_Lin_Sphericity_C);
        fChain->SetBranchAddress("Lin_Sphericity_D", &Lin_Sphericity_D, &b_Lin_Sphericity_D);
        fChain->SetBranchAddress("Lin_Sphericity_IsValid", &Lin_Sphericity_IsValid, &b_Lin_Sphericity_IsValid);
        fChain->SetBranchAddress("MCT", &MCT, &b_MCT);
        fChain->SetBranchAddress("MET_BiSect", &MET_BiSect, &b_MET_BiSect);
        fChain->SetBranchAddress("MET_Centrality", &MET_Centrality, &b_MET_Centrality);
        fChain->SetBranchAddress("MET_CosMinDeltaPhi", &MET_CosMinDeltaPhi, &b_MET_CosMinDeltaPhi);
        fChain->SetBranchAddress("MET_LepTau_DeltaPhi", &MET_LepTau_DeltaPhi, &b_MET_LepTau_DeltaPhi);
        fChain->SetBranchAddress("MET_LepTau_LeadingJet_VecSumPt", &MET_LepTau_LeadingJet_VecSumPt, &b_MET_LepTau_LeadingJet_VecSumPt);
        fChain->SetBranchAddress("MET_LepTau_VecSumPhi", &MET_LepTau_VecSumPhi, &b_MET_LepTau_VecSumPhi);
        fChain->SetBranchAddress("MET_LepTau_VecSumPt", &MET_LepTau_VecSumPt, &b_MET_LepTau_VecSumPt);
        fChain->SetBranchAddress("MET_SumCosDeltaPhi", &MET_SumCosDeltaPhi, &b_MET_SumCosDeltaPhi);
        fChain->SetBranchAddress("MT2_max", &MT2_max, &b_MT2_max);
        fChain->SetBranchAddress("MT2_min", &MT2_min, &b_MT2_min);
        fChain->SetBranchAddress("Meff", &Meff, &b_Meff);
        fChain->SetBranchAddress("MetTST_met", &MetTST_met, &b_MetTST_met);
        fChain->SetBranchAddress("MetTST_phi", &MetTST_phi, &b_MetTST_phi);
        fChain->SetBranchAddress("MetTST_sumet", &MetTST_sumet, &b_MetTST_sumet);
        fChain->SetBranchAddress("MetTST_OverSqrtHT", &MetTST_OverSqrtHT, &b_MetTST_OverSqrtHT);
        fChain->SetBranchAddress("MetTST_OverSqrtSumET", &MetTST_OverSqrtSumET, &b_MetTST_OverSqrtSumET);
        fChain->SetBranchAddress("MetTST_Significance", &MetTST_Significance, &b_MetTST_Significance);
        fChain->SetBranchAddress("MetTST_Significance_Rho", &MetTST_Significance_Rho, &b_MetTST_Significance_Rho);
        fChain->SetBranchAddress("MetTST_Significance_VarL", &MetTST_Significance_VarL, &b_MetTST_Significance_VarL);
        fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm", &MetTST_Significance_noPUJets_noSoftTerm,
                                 &b_MetTST_Significance_noPUJets_noSoftTerm);
        fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm_Rho", &MetTST_Significance_noPUJets_noSoftTerm_Rho,
                                 &b_MetTST_Significance_noPUJets_noSoftTerm_Rho);
        fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm_VarL", &MetTST_Significance_noPUJets_noSoftTerm_VarL,
                                 &b_MetTST_Significance_noPUJets_noSoftTerm_VarL);
        fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets", &MetTST_Significance_phireso_noPUJets,
                                 &b_MetTST_Significance_phireso_noPUJets);
        fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets_Rho", &MetTST_Significance_phireso_noPUJets_Rho,
                                 &b_MetTST_Significance_phireso_noPUJets_Rho);
        fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets_VarL", &MetTST_Significance_phireso_noPUJets_VarL,
                                 &b_MetTST_Significance_phireso_noPUJets_VarL);
        fChain->SetBranchAddress("MetTrack_met", &MetTrack_met, &b_MetTrack_met);
        fChain->SetBranchAddress("MetTrack_phi", &MetTrack_phi, &b_MetTrack_phi);
        fChain->SetBranchAddress("MetTrack_sumet", &MetTrack_sumet, &b_MetTrack_sumet);
        fChain->SetBranchAddress("MuoWeight", &MuoWeight, &b_MuoWeight);
        fChain->SetBranchAddress("MuoWeightIsol", &MuoWeightIsol, &b_MuoWeightIsol);
        fChain->SetBranchAddress("MuoWeightReco", &MuoWeightReco, &b_MuoWeightReco);
        fChain->SetBranchAddress("MuoWeightTTVA", &MuoWeightTTVA, &b_MuoWeightTTVA);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu14", &MuoWeightTrigHLT_mu14, &b_MuoWeightTrigHLT_mu14);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu14_ivarloose", &MuoWeightTrigHLT_mu14_ivarloose, &b_MuoWeightTrigHLT_mu14_ivarloose);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu20_iloose_L1MU15", &MuoWeightTrigHLT_mu20_iloose_L1MU15, &b_MuoWeightTrigHLT_mu20_iloose_L1MU15);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu50", &MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu50,
                                 &b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu50);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmedium", &MuoWeightTrigHLT_mu26_ivarmedium, &b_MuoWeightTrigHLT_mu26_ivarmedium);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50,
                                 &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu50", &MuoWeightTrigHLT_mu50, &b_MuoWeightTrigHLT_mu50);
        fChain->SetBranchAddress("OS_BaseTauEle", &OS_BaseTauEle, &b_OS_BaseTauEle);
        fChain->SetBranchAddress("OS_BaseTauMuo", &OS_BaseTauMuo, &b_OS_BaseTauMuo);
        fChain->SetBranchAddress("OS_EleEle", &OS_EleEle, &b_OS_EleEle);
        fChain->SetBranchAddress("OS_MuoMuo", &OS_MuoMuo, &b_OS_MuoMuo);
        fChain->SetBranchAddress("OS_TauEle", &OS_TauEle, &b_OS_TauEle);
        fChain->SetBranchAddress("OS_TauMuo", &OS_TauMuo, &b_OS_TauMuo);
        fChain->SetBranchAddress("OS_TauTau", &OS_TauTau, &b_OS_TauTau);
        fChain->SetBranchAddress("Oblateness", &Oblateness, &b_Oblateness);
        fChain->SetBranchAddress("PTt", &PTt, &b_PTt);
        fChain->SetBranchAddress("Planarity", &Planarity, &b_Planarity);
        fChain->SetBranchAddress("RJW_CosThetaStarW", &RJW_CosThetaStarW, &b_RJW_CosThetaStarW);
        fChain->SetBranchAddress("RJW_GammaBetaW", &RJW_GammaBetaW, &b_RJW_GammaBetaW);
        fChain->SetBranchAddress("RJW_JigSawCandidates_eta", &RJW_JigSawCandidates_eta, &b_RJW_JigSawCandidates_eta);
        fChain->SetBranchAddress("RJW_JigSawCandidates_frame", &RJW_JigSawCandidates_frame, &b_RJW_JigSawCandidates_frame);
        fChain->SetBranchAddress("RJW_JigSawCandidates_m", &RJW_JigSawCandidates_m, &b_RJW_JigSawCandidates_m);
        fChain->SetBranchAddress("RJW_JigSawCandidates_pdgId", &RJW_JigSawCandidates_pdgId, &b_RJW_JigSawCandidates_pdgId);
        fChain->SetBranchAddress("RJW_JigSawCandidates_phi", &RJW_JigSawCandidates_phi, &b_RJW_JigSawCandidates_phi);
        fChain->SetBranchAddress("RJW_JigSawCandidates_pt", &RJW_JigSawCandidates_pt, &b_RJW_JigSawCandidates_pt);
        fChain->SetBranchAddress("RJW_dPhiDecayPlaneW", &RJW_dPhiDecayPlaneW, &b_RJW_dPhiDecayPlaneW);
        fChain->SetBranchAddress("RJZ_BalMetObj_Z", &RJZ_BalMetObj_Z, &b_RJZ_BalMetObj_Z);
        fChain->SetBranchAddress("RJZ_CMS_Mass", &RJZ_CMS_Mass, &b_RJZ_CMS_Mass);
        fChain->SetBranchAddress("RJZ_H01_Tau1", &RJZ_H01_Tau1, &b_RJZ_H01_Tau1);
        fChain->SetBranchAddress("RJZ_H01_Z", &RJZ_H01_Z, &b_RJZ_H01_Z);
        fChain->SetBranchAddress("RJZ_H11_Tau1", &RJZ_H11_Tau1, &b_RJZ_H11_Tau1);
        fChain->SetBranchAddress("RJZ_H11_Z", &RJZ_H11_Z, &b_RJZ_H11_Z);
        fChain->SetBranchAddress("RJZ_H21_Tau1", &RJZ_H21_Tau1, &b_RJZ_H21_Tau1);
        fChain->SetBranchAddress("RJZ_H21_Z", &RJZ_H21_Z, &b_RJZ_H21_Z);
        fChain->SetBranchAddress("RJZ_Ht01_Tau1", &RJZ_Ht01_Tau1, &b_RJZ_Ht01_Tau1);
        fChain->SetBranchAddress("RJZ_Ht01_Z", &RJZ_Ht01_Z, &b_RJZ_Ht01_Z);
        fChain->SetBranchAddress("RJZ_Ht11_Tau1", &RJZ_Ht11_Tau1, &b_RJZ_Ht11_Tau1);
        fChain->SetBranchAddress("RJZ_Ht11_Z", &RJZ_Ht11_Z, &b_RJZ_Ht11_Z);
        fChain->SetBranchAddress("RJZ_Ht21_Tau1", &RJZ_Ht21_Tau1, &b_RJZ_Ht21_Tau1);
        fChain->SetBranchAddress("RJZ_Ht21_Z", &RJZ_Ht21_Z, &b_RJZ_Ht21_Z);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_CosThetaStar", &RJZ_JigSawCandidates_CosThetaStar, &b_RJZ_JigSawCandidates_CosThetaStar);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_dPhiDecayPlane", &RJZ_JigSawCandidates_dPhiDecayPlane, &b_RJZ_JigSawCandidates_dPhiDecayPlane);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_eta", &RJZ_JigSawCandidates_eta, &b_RJZ_JigSawCandidates_eta);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_frame", &RJZ_JigSawCandidates_frame, &b_RJZ_JigSawCandidates_frame);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_m", &RJZ_JigSawCandidates_m, &b_RJZ_JigSawCandidates_m);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_pdgId", &RJZ_JigSawCandidates_pdgId, &b_RJZ_JigSawCandidates_pdgId);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_phi", &RJZ_JigSawCandidates_phi, &b_RJZ_JigSawCandidates_phi);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_pt", &RJZ_JigSawCandidates_pt, &b_RJZ_JigSawCandidates_pt);
        fChain->SetBranchAddress("RJZ_R_BoostZ", &RJZ_R_BoostZ, &b_RJZ_R_BoostZ);
        fChain->SetBranchAddress("RJZ_cosTheta_Tau1", &RJZ_cosTheta_Tau1, &b_RJZ_cosTheta_Tau1);
        fChain->SetBranchAddress("RJZ_cosTheta_Tau2", &RJZ_cosTheta_Tau2, &b_RJZ_cosTheta_Tau2);
        fChain->SetBranchAddress("RJZ_cosTheta_Z", &RJZ_cosTheta_Z, &b_RJZ_cosTheta_Z);
        fChain->SetBranchAddress("RJZ_dPhiDecayPlane_Tau1", &RJZ_dPhiDecayPlane_Tau1, &b_RJZ_dPhiDecayPlane_Tau1);
        fChain->SetBranchAddress("RJZ_dPhiDecayPlane_Tau2", &RJZ_dPhiDecayPlane_Tau2, &b_RJZ_dPhiDecayPlane_Tau2);
        fChain->SetBranchAddress("RJZ_dPhiDecayPlane_Z", &RJZ_dPhiDecayPlane_Z, &b_RJZ_dPhiDecayPlane_Z);
        fChain->SetBranchAddress("RandomLumiBlockNumber", &RandomLumiBlockNumber, &b_RandomLumiBlockNumber);
        fChain->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
        fChain->SetBranchAddress("SUSYFinalState", &SUSYFinalState, &b_SUSYFinalState);
        fChain->SetBranchAddress("Sphericity", &Sphericity, &b_Sphericity);
        fChain->SetBranchAddress("Sphericity_IsValid", &Sphericity_IsValid, &b_Sphericity_IsValid);
        fChain->SetBranchAddress("Spherocity", &Spherocity, &b_Spherocity);
        fChain->SetBranchAddress("Spherocity_IsValid", &Spherocity_IsValid, &b_Spherocity_IsValid);
        fChain->SetBranchAddress("TauWeight", &TauWeight, &b_TauWeight);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo", &TauWeightTrigHLT_tau125_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau125_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwo", &TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF", &TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo", &TauWeightTrigHLT_tau160_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau160_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo", &TauWeightTrigHLT_tau25_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwoEF", &TauWeightTrigHLT_tau25_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau25_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau25_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF", &TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo", &TauWeightTrigHLT_tau35_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwoEF", &TauWeightTrigHLT_tau35_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau35_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau35_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau60_medium1_tracktwo", &TauWeightTrigHLT_tau60_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau60_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau60_medium1_tracktwoEF", &TauWeightTrigHLT_tau60_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau60_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo", &TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF", &TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("Thrust", &Thrust, &b_Thrust);
        fChain->SetBranchAddress("ThrustMajor", &ThrustMajor, &b_ThrustMajor);
        fChain->SetBranchAddress("ThrustMinor", &ThrustMinor, &b_ThrustMinor);
        fChain->SetBranchAddress("Thrust_IsValid", &Thrust_IsValid, &b_Thrust_IsValid);
        fChain->SetBranchAddress("TrigHLT_e120_lhloose", &TrigHLT_e120_lhloose, &b_TrigHLT_e120_lhloose);
        fChain->SetBranchAddress("TrigHLT_e140_lhloose_nod0", &TrigHLT_e140_lhloose_nod0, &b_TrigHLT_e140_lhloose_nod0);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0", &TrigHLT_e17_lhmedium_nod0, &b_TrigHLT_e17_lhmedium_nod0);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose", &TrigHLT_e17_lhmedium_nod0_ivarloose, &b_TrigHLT_e17_lhmedium_nod0_ivarloose);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo",
                                 &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo,
                                 &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF",
                                 &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF,
                                 &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50",
                                 &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12",
                                 &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12,
                                 &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA",
                                 &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA,
                                 &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo", &TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo,
                                 &b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50", &TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50", &TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50,
                                 &b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo", &TrigHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo,
                                 &b_TrigHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50", &TrigHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50,
                                 &b_TrigHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigHLT_e24_lhmedium_L1EM20VH", &TrigHLT_e24_lhmedium_L1EM20VH, &b_TrigHLT_e24_lhmedium_L1EM20VH);
        fChain->SetBranchAddress("TrigHLT_e24_lhmedium_nod0_L1EM20VH", &TrigHLT_e24_lhmedium_nod0_L1EM20VH, &b_TrigHLT_e24_lhmedium_nod0_L1EM20VH);
        fChain->SetBranchAddress("TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA",
                                 &TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA,
                                 &b_TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_e24_lhtight_nod0_ivarloose", &TrigHLT_e24_lhtight_nod0_ivarloose, &b_TrigHLT_e24_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("TrigHLT_e26_lhtight_nod0_ivarloose", &TrigHLT_e26_lhtight_nod0_ivarloose, &b_TrigHLT_e26_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("TrigHLT_e60_lhmedium", &TrigHLT_e60_lhmedium, &b_TrigHLT_e60_lhmedium);
        fChain->SetBranchAddress("TrigHLT_e60_lhmedium_nod0", &TrigHLT_e60_lhmedium_nod0, &b_TrigHLT_e60_lhmedium_nod0);
        fChain->SetBranchAddress("TrigHLT_mu14", &TrigHLT_mu14, &b_TrigHLT_mu14);
        fChain->SetBranchAddress("TrigHLT_mu14_iloose_tau25_medium1_tracktwo_xe50", &TrigHLT_mu14_iloose_tau25_medium1_tracktwo_xe50,
                                 &b_TrigHLT_mu14_iloose_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigHLT_mu14_ivarloose", &TrigHLT_mu14_ivarloose, &b_TrigHLT_mu14_ivarloose);
        fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo", &TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo,
                                 &b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau25_medium1_tracktwoEF", &TrigHLT_mu14_ivarloose_tau25_medium1_tracktwoEF,
                                 &b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50", &TrigHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12",
                                 &TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12,
                                 &b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12);
        fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA", &TrigHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA,
                                 &b_TrigHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50", &TrigHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_TrigHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo", &TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo,
                                 &b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF", &TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF,
                                 &b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA", &TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA,
                                 &b_TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_mu14_tau25_medium1_tracktwo", &TrigHLT_mu14_tau25_medium1_tracktwo, &b_TrigHLT_mu14_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50", &TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigHLT_mu14_tau25_medium1_tracktwo_xe50", &TrigHLT_mu14_tau25_medium1_tracktwo_xe50,
                                 &b_TrigHLT_mu14_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50", &TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("TrigHLT_mu14_tau35_medium1_tracktwo", &TrigHLT_mu14_tau35_medium1_tracktwo, &b_TrigHLT_mu14_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_mu20_iloose_L1MU15", &TrigHLT_mu20_iloose_L1MU15, &b_TrigHLT_mu20_iloose_L1MU15);
        fChain->SetBranchAddress("TrigHLT_mu20_iloose_L1MU15_OR_HLT_mu50", &TrigHLT_mu20_iloose_L1MU15_OR_HLT_mu50,
                                 &b_TrigHLT_mu20_iloose_L1MU15_OR_HLT_mu50);
        fChain->SetBranchAddress("TrigHLT_mu26_ivarmedium", &TrigHLT_mu26_ivarmedium, &b_TrigHLT_mu26_ivarmedium);
        fChain->SetBranchAddress("TrigHLT_mu26_ivarmedium_OR_HLT_mu50", &TrigHLT_mu26_ivarmedium_OR_HLT_mu50, &b_TrigHLT_mu26_ivarmedium_OR_HLT_mu50);
        fChain->SetBranchAddress("TrigHLT_mu50", &TrigHLT_mu50, &b_TrigHLT_mu50);
        fChain->SetBranchAddress("TrigHLT_noalg_L1J400", &TrigHLT_noalg_L1J400, &b_TrigHLT_noalg_L1J400);
        fChain->SetBranchAddress("TrigHLT_tau125_medium1_tracktwo", &TrigHLT_tau125_medium1_tracktwo, &b_TrigHLT_tau125_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwo", &TrigHLT_tau160_medium1_tracktwo, &b_TrigHLT_tau160_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwoEF", &TrigHLT_tau160_medium1_tracktwoEF, &b_TrigHLT_tau160_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwoEF_L1TAU100", &TrigHLT_tau160_medium1_tracktwoEF_L1TAU100,
                                 &b_TrigHLT_tau160_medium1_tracktwoEF_L1TAU100);
        fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwo_L1TAU100", &TrigHLT_tau160_medium1_tracktwo_L1TAU100,
                                 &b_TrigHLT_tau160_medium1_tracktwo_L1TAU100);
        fChain->SetBranchAddress("TrigHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100", &TrigHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100,
                                 &b_TrigHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100);
        fChain->SetBranchAddress("TrigHLT_tau25_medium1_tracktwo", &TrigHLT_tau25_medium1_tracktwo, &b_TrigHLT_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau25_medium1_tracktwoEF", &TrigHLT_tau25_medium1_tracktwoEF, &b_TrigHLT_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_tau25_mediumRNN_tracktwoMVA", &TrigHLT_tau25_mediumRNN_tracktwoMVA, &b_TrigHLT_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo", &TrigHLT_tau35_medium1_tracktwo, &b_TrigHLT_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwoEF", &TrigHLT_tau35_medium1_tracktwoEF, &b_TrigHLT_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwoEF_L1TAU12IM", &TrigHLT_tau35_medium1_tracktwoEF_L1TAU12IM,
                                 &b_TrigHLT_tau35_medium1_tracktwoEF_L1TAU12IM);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwoEF_xe70_L1XE45", &TrigHLT_tau35_medium1_tracktwoEF_xe70_L1XE45,
                                 &b_TrigHLT_tau35_medium1_tracktwoEF_xe70_L1XE45);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50",
                                 &TrigHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50,
                                 &b_TrigHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45", &TrigHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45,
                                 &b_TrigHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo", &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo,
                                 &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
                                 &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM,
                                 &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_xe70_L1XE45", &TrigHLT_tau35_medium1_tracktwo_xe70_L1XE45,
                                 &b_TrigHLT_tau35_medium1_tracktwo_xe70_L1XE45);
        fChain->SetBranchAddress("TrigHLT_tau35_mediumRNN_tracktwoMVA", &TrigHLT_tau35_mediumRNN_tracktwoMVA, &b_TrigHLT_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM", &TrigHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM,
                                 &b_TrigHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM);
        fChain->SetBranchAddress("TrigHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45", &TrigHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45,
                                 &b_TrigHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45);
        fChain->SetBranchAddress("TrigHLT_tau50_medium1_tracktwo_L1TAU12", &TrigHLT_tau50_medium1_tracktwo_L1TAU12,
                                 &b_TrigHLT_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwo", &TrigHLT_tau60_medium1_tracktwo, &b_TrigHLT_tau60_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwoEF", &TrigHLT_tau60_medium1_tracktwoEF, &b_TrigHLT_tau60_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
                                 &TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigHLT_tau60_mediumRNN_tracktwoMVA", &TrigHLT_tau60_mediumRNN_tracktwoMVA, &b_TrigHLT_tau60_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12", &TrigHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12,
                                 &b_TrigHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwoEF", &TrigHLT_tau80_medium1_tracktwoEF, &b_TrigHLT_tau80_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwoEF_L1TAU60", &TrigHLT_tau80_medium1_tracktwoEF_L1TAU60,
                                 &b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
                                 &TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40,
                                 &b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60", &TrigHLT_tau80_medium1_tracktwo_L1TAU60,
                                 &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
                                 &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I,
                                 &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
                                 &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12,
                                 &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
                                 &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40,
                                 &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
        fChain->SetBranchAddress("TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60", &TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60,
                                 &b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60);
        fChain->SetBranchAddress("TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
                                 &TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40,
                                 &b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
        fChain->SetBranchAddress("TrigHLT_xe100_pufit_L1XE55", &TrigHLT_xe100_pufit_L1XE55, &b_TrigHLT_xe100_pufit_L1XE55);
        fChain->SetBranchAddress("TrigHLT_xe110_mht_L1XE50", &TrigHLT_xe110_mht_L1XE50, &b_TrigHLT_xe110_mht_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe110_pufit_L1XE50", &TrigHLT_xe110_pufit_L1XE50, &b_TrigHLT_xe110_pufit_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe110_pufit_L1XE55", &TrigHLT_xe110_pufit_L1XE55, &b_TrigHLT_xe110_pufit_L1XE55);
        fChain->SetBranchAddress("TrigHLT_xe110_pufit_xe65_L1XE50", &TrigHLT_xe110_pufit_xe65_L1XE50, &b_TrigHLT_xe110_pufit_xe65_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe110_pufit_xe70_L1XE50", &TrigHLT_xe110_pufit_xe70_L1XE50, &b_TrigHLT_xe110_pufit_xe70_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe120_pufit_L1XE50", &TrigHLT_xe120_pufit_L1XE50, &b_TrigHLT_xe120_pufit_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe70", &TrigHLT_xe70, &b_TrigHLT_xe70);
        fChain->SetBranchAddress("TrigHLT_xe70_mht", &TrigHLT_xe70_mht, &b_TrigHLT_xe70_mht);
        fChain->SetBranchAddress("TrigHLT_xe70_tc_lcw", &TrigHLT_xe70_tc_lcw, &b_TrigHLT_xe70_tc_lcw);
        fChain->SetBranchAddress("TrigHLT_xe90_mht_L1XE50", &TrigHLT_xe90_mht_L1XE50, &b_TrigHLT_xe90_mht_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe90_pufit_L1XE50", &TrigHLT_xe90_pufit_L1XE50, &b_TrigHLT_xe90_pufit_L1XE50);
        fChain->SetBranchAddress("TrigMatchHLT_e120_lhloose", &TrigMatchHLT_e120_lhloose, &b_TrigMatchHLT_e120_lhloose);
        fChain->SetBranchAddress("TrigMatchHLT_e140_lhloose_nod0", &TrigMatchHLT_e140_lhloose_nod0, &b_TrigMatchHLT_e140_lhloose_nod0);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0", &TrigMatchHLT_e17_lhmedium_nod0, &b_TrigMatchHLT_e17_lhmedium_nod0);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose", &TrigMatchHLT_e17_lhmedium_nod0_ivarloose,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo",
                                 &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF",
                                 &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50",
                                 &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12",
                                 &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA",
                                 &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo", &TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50",
                                 &TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50",
                                 &TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo", &TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo,
                                 &b_TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50,
                                 &b_TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_e24_lhmedium_L1EM20VH", &TrigMatchHLT_e24_lhmedium_L1EM20VH, &b_TrigMatchHLT_e24_lhmedium_L1EM20VH);
        fChain->SetBranchAddress("TrigMatchHLT_e24_lhmedium_nod0_L1EM20VH", &TrigMatchHLT_e24_lhmedium_nod0_L1EM20VH,
                                 &b_TrigMatchHLT_e24_lhmedium_nod0_L1EM20VH);
        fChain->SetBranchAddress("TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA",
                                 &TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA,
                                 &b_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigMatchHLT_e24_lhtight_nod0_ivarloose", &TrigMatchHLT_e24_lhtight_nod0_ivarloose,
                                 &b_TrigMatchHLT_e24_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("TrigMatchHLT_e26_lhtight_nod0_ivarloose", &TrigMatchHLT_e26_lhtight_nod0_ivarloose,
                                 &b_TrigMatchHLT_e26_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("TrigMatchHLT_e60_lhmedium", &TrigMatchHLT_e60_lhmedium, &b_TrigMatchHLT_e60_lhmedium);
        fChain->SetBranchAddress("TrigMatchHLT_e60_lhmedium_nod0", &TrigMatchHLT_e60_lhmedium_nod0, &b_TrigMatchHLT_e60_lhmedium_nod0);
        fChain->SetBranchAddress("TrigMatchHLT_mu14", &TrigMatchHLT_mu14, &b_TrigMatchHLT_mu14);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50,
                                 &b_TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose", &TrigMatchHLT_mu14_ivarloose, &b_TrigMatchHLT_mu14_ivarloose);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo", &TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo,
                                 &b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF", &TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50",
                                 &TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12",
                                 &TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12,
                                 &b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA", &TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA,
                                 &b_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo", &TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo,
                                 &b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF", &TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA", &TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA,
                                 &b_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_tau25_medium1_tracktwo", &TrigMatchHLT_mu14_tau25_medium1_tracktwo,
                                 &b_TrigMatchHLT_mu14_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50", &TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50,
                                 &b_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50", &TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_mu14_tau35_medium1_tracktwo", &TrigMatchHLT_mu14_tau35_medium1_tracktwo,
                                 &b_TrigMatchHLT_mu14_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_mu20_iloose_L1MU15", &TrigMatchHLT_mu20_iloose_L1MU15, &b_TrigMatchHLT_mu20_iloose_L1MU15);
        fChain->SetBranchAddress("TrigMatchHLT_mu20_iloose_L1MU15_OR_HLT_mu50", &TrigMatchHLT_mu20_iloose_L1MU15_OR_HLT_mu50,
                                 &b_TrigMatchHLT_mu20_iloose_L1MU15_OR_HLT_mu50);
        fChain->SetBranchAddress("TrigMatchHLT_mu26_ivarmedium", &TrigMatchHLT_mu26_ivarmedium, &b_TrigMatchHLT_mu26_ivarmedium);
        fChain->SetBranchAddress("TrigMatchHLT_mu26_ivarmedium_OR_HLT_mu50", &TrigMatchHLT_mu26_ivarmedium_OR_HLT_mu50,
                                 &b_TrigMatchHLT_mu26_ivarmedium_OR_HLT_mu50);
        fChain->SetBranchAddress("TrigMatchHLT_mu50", &TrigMatchHLT_mu50, &b_TrigMatchHLT_mu50);
        fChain->SetBranchAddress("TrigMatchHLT_tau125_medium1_tracktwo", &TrigMatchHLT_tau125_medium1_tracktwo,
                                 &b_TrigMatchHLT_tau125_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwo", &TrigMatchHLT_tau160_medium1_tracktwo,
                                 &b_TrigMatchHLT_tau160_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwoEF", &TrigMatchHLT_tau160_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_tau160_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100", &TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100,
                                 &b_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100);
        fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100", &TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100,
                                 &b_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100);
        fChain->SetBranchAddress("TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100", &TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100,
                                 &b_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100);
        fChain->SetBranchAddress("TrigMatchHLT_tau25_medium1_tracktwo", &TrigMatchHLT_tau25_medium1_tracktwo, &b_TrigMatchHLT_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau25_medium1_tracktwoEF", &TrigMatchHLT_tau25_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_tau25_mediumRNN_tracktwoMVA", &TrigMatchHLT_tau25_mediumRNN_tracktwoMVA,
                                 &b_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo", &TrigMatchHLT_tau35_medium1_tracktwo, &b_TrigMatchHLT_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwoEF", &TrigMatchHLT_tau35_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM", &TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45", &TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50",
                                 &TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45", &TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo",
                                 &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
                                 &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45", &TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_mediumRNN_tracktwoMVA", &TrigMatchHLT_tau35_mediumRNN_tracktwoMVA,
                                 &b_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM", &TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM,
                                 &b_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45", &TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45,
                                 &b_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45);
        fChain->SetBranchAddress("TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12", &TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12,
                                 &b_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwo", &TrigMatchHLT_tau60_medium1_tracktwo, &b_TrigMatchHLT_tau60_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwoEF", &TrigMatchHLT_tau60_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_tau60_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
                                 &TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_mediumRNN_tracktwoMVA", &TrigMatchHLT_tau60_mediumRNN_tracktwoMVA,
                                 &b_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12", &TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12,
                                 &b_TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwoEF", &TrigMatchHLT_tau80_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60", &TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
                                 &TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60", &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
                                 &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
                                 &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
                                 &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60", &TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60,
                                 &b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
                                 &TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40,
                                 &b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
        fChain->SetBranchAddress("TrigMatching", &TrigMatching, &b_TrigMatching);
        fChain->SetBranchAddress("TruthMET_met", &TruthMET_met, &b_TruthMET_met);
        fChain->SetBranchAddress("TruthMET_phi", &TruthMET_phi, &b_TruthMET_phi);
        fChain->SetBranchAddress("TruthMET_sumet", &TruthMET_sumet, &b_TruthMET_sumet);
        fChain->SetBranchAddress("UnNormed_WolframMoment_0", &UnNormed_WolframMoment_0, &b_UnNormed_WolframMoment_0);
        fChain->SetBranchAddress("UnNormed_WolframMoment_1", &UnNormed_WolframMoment_1, &b_UnNormed_WolframMoment_1);
        fChain->SetBranchAddress("UnNormed_WolframMoment_10", &UnNormed_WolframMoment_10, &b_UnNormed_WolframMoment_10);
        fChain->SetBranchAddress("UnNormed_WolframMoment_11", &UnNormed_WolframMoment_11, &b_UnNormed_WolframMoment_11);
        fChain->SetBranchAddress("UnNormed_WolframMoment_2", &UnNormed_WolframMoment_2, &b_UnNormed_WolframMoment_2);
        fChain->SetBranchAddress("UnNormed_WolframMoment_3", &UnNormed_WolframMoment_3, &b_UnNormed_WolframMoment_3);
        fChain->SetBranchAddress("UnNormed_WolframMoment_4", &UnNormed_WolframMoment_4, &b_UnNormed_WolframMoment_4);
        fChain->SetBranchAddress("UnNormed_WolframMoment_5", &UnNormed_WolframMoment_5, &b_UnNormed_WolframMoment_5);
        fChain->SetBranchAddress("UnNormed_WolframMoment_6", &UnNormed_WolframMoment_6, &b_UnNormed_WolframMoment_6);
        fChain->SetBranchAddress("UnNormed_WolframMoment_7", &UnNormed_WolframMoment_7, &b_UnNormed_WolframMoment_7);
        fChain->SetBranchAddress("UnNormed_WolframMoment_8", &UnNormed_WolframMoment_8, &b_UnNormed_WolframMoment_8);
        fChain->SetBranchAddress("UnNormed_WolframMoment_9", &UnNormed_WolframMoment_9, &b_UnNormed_WolframMoment_9);
        fChain->SetBranchAddress("UnNormed_WolframMoments_AreValid", &UnNormed_WolframMoments_AreValid, &b_UnNormed_WolframMoments_AreValid);
        fChain->SetBranchAddress("VecSumPt_LepTau", &VecSumPt_LepTau, &b_VecSumPt_LepTau);
        fChain->SetBranchAddress("Vtx_n", &Vtx_n, &b_Vtx_n);
        fChain->SetBranchAddress("WolframMoment_0", &WolframMoment_0, &b_WolframMoment_0);
        fChain->SetBranchAddress("WolframMoment_1", &WolframMoment_1, &b_WolframMoment_1);
        fChain->SetBranchAddress("WolframMoment_10", &WolframMoment_10, &b_WolframMoment_10);
        fChain->SetBranchAddress("WolframMoment_11", &WolframMoment_11, &b_WolframMoment_11);
        fChain->SetBranchAddress("WolframMoment_2", &WolframMoment_2, &b_WolframMoment_2);
        fChain->SetBranchAddress("WolframMoment_3", &WolframMoment_3, &b_WolframMoment_3);
        fChain->SetBranchAddress("WolframMoment_4", &WolframMoment_4, &b_WolframMoment_4);
        fChain->SetBranchAddress("WolframMoment_5", &WolframMoment_5, &b_WolframMoment_5);
        fChain->SetBranchAddress("WolframMoment_6", &WolframMoment_6, &b_WolframMoment_6);
        fChain->SetBranchAddress("WolframMoment_7", &WolframMoment_7, &b_WolframMoment_7);
        fChain->SetBranchAddress("WolframMoment_8", &WolframMoment_8, &b_WolframMoment_8);
        fChain->SetBranchAddress("WolframMoment_9", &WolframMoment_9, &b_WolframMoment_9);
        fChain->SetBranchAddress("WolframMoments_AreValid", &WolframMoments_AreValid, &b_WolframMoments_AreValid);
        fChain->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
        fChain->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
        fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
        fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
        fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
        fChain->SetBranchAddress("corr_avgIntPerX", &corr_avgIntPerX, &b_corr_avgIntPerX);
        fChain->SetBranchAddress("dPhiMetTrackMetTST", &dPhiMetTrackMetTST, &b_dPhiMetTrackMetTST);
        fChain->SetBranchAddress("dilepton_charge", &dilepton_charge, &b_dilepton_charge);
        fChain->SetBranchAddress("dilepton_eta", &dilepton_eta, &b_dilepton_eta);
        fChain->SetBranchAddress("dilepton_m", &dilepton_m, &b_dilepton_m);
        fChain->SetBranchAddress("dilepton_pdgId", &dilepton_pdgId, &b_dilepton_pdgId);
        fChain->SetBranchAddress("dilepton_phi", &dilepton_phi, &b_dilepton_phi);
        fChain->SetBranchAddress("dilepton_pt", &dilepton_pt, &b_dilepton_pt);
        fChain->SetBranchAddress("electrons_IFFClassType", &electrons_IFFClassType, &b_electrons_IFFClassType);
        fChain->SetBranchAddress("electrons_MT", &electrons_MT, &b_electrons_MT);
        fChain->SetBranchAddress("electrons_charge", &electrons_charge, &b_electrons_charge);
        fChain->SetBranchAddress("electrons_d0sig", &electrons_d0sig, &b_electrons_d0sig);
        fChain->SetBranchAddress("electrons_e", &electrons_e, &b_electrons_e);
        fChain->SetBranchAddress("electrons_eta", &electrons_eta, &b_electrons_eta);
        fChain->SetBranchAddress("electrons_isol", &electrons_isol, &b_electrons_isol);
        fChain->SetBranchAddress("electrons_isol_FCHighPtCaloOnly", &electrons_isol_FCHighPtCaloOnly, &b_electrons_isol_FCHighPtCaloOnly);
        fChain->SetBranchAddress("electrons_isol_FCLoose", &electrons_isol_FCLoose, &b_electrons_isol_FCLoose);
        fChain->SetBranchAddress("electrons_isol_FCTight", &electrons_isol_FCTight, &b_electrons_isol_FCTight);
        fChain->SetBranchAddress("electrons_isol_Gradient", &electrons_isol_Gradient, &b_electrons_isol_Gradient);
        fChain->SetBranchAddress("electrons_phi", &electrons_phi, &b_electrons_phi);
        fChain->SetBranchAddress("electrons_pt", &electrons_pt, &b_electrons_pt);
        fChain->SetBranchAddress("electrons_signal", &electrons_signal, &b_electrons_signal);
        fChain->SetBranchAddress("electrons_truthOrigin", &electrons_truthOrigin, &b_electrons_truthOrigin);
        fChain->SetBranchAddress("electrons_truthType", &electrons_truthType, &b_electrons_truthType);
        fChain->SetBranchAddress("electrons_z0sinTheta", &electrons_z0sinTheta, &b_electrons_z0sinTheta);
        fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
        fChain->SetBranchAddress("forwardDetFlags", &forwardDetFlags, &b_forwardDetFlags);
        fChain->SetBranchAddress("jets_BTagScore", &jets_BTagScore, &b_jets_BTagScore);
        fChain->SetBranchAddress("jets_Jvt", &jets_Jvt, &b_jets_Jvt);
        fChain->SetBranchAddress("jets_NTrks", &jets_NTrks, &b_jets_NTrks);
        fChain->SetBranchAddress("jets_bjet", &jets_bjet, &b_jets_bjet);
        fChain->SetBranchAddress("jets_eta", &jets_eta, &b_jets_eta);
        fChain->SetBranchAddress("jets_isBadTight", &jets_isBadTight, &b_jets_isBadTight);
        fChain->SetBranchAddress("jets_m", &jets_m, &b_jets_m);
        fChain->SetBranchAddress("jets_phi", &jets_phi, &b_jets_phi);
        fChain->SetBranchAddress("jets_pt", &jets_pt, &b_jets_pt);
        fChain->SetBranchAddress("jets_signal", &jets_signal, &b_jets_signal);
        fChain->SetBranchAddress("TruthBSM_status", &TruthBSM_status, &b_TruthBSM_status);
        fChain->SetBranchAddress("TruthBSM_pdgId", &TruthBSM_pdgId, &b_TruthBSM_pdgId);
        fChain->SetBranchAddress("TruthBSM_classifierParticleOrigin", &TruthBSM_classifierParticleOrigin, &b_TruthBSM_classifierParticleOrigin);
        fChain->SetBranchAddress("TruthBSM_classifierParticleType", &TruthBSM_classifierParticleType, &b_TruthBSM_classifierParticleType);
        fChain->SetBranchAddress("TruthBSM_classifierParticleOutCome", &TruthBSM_classifierParticleOutCome, &b_TruthBSM_classifierParticleOutCome);
        fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
        fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
        fChain->SetBranchAddress("lumiFlags", &lumiFlags, &b_lumiFlags);
        fChain->SetBranchAddress("muWeight", &muWeight, &b_muWeight);
        fChain->SetBranchAddress("mu_density", &mu_density, &b_mu_density);
        fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
        fChain->SetBranchAddress("muons_IFFClassType", &muons_IFFClassType, &b_muons_IFFClassType);
        fChain->SetBranchAddress("muons_MT", &muons_MT, &b_muons_MT);
        fChain->SetBranchAddress("muons_charge", &muons_charge, &b_muons_charge);
        fChain->SetBranchAddress("muons_d0sig", &muons_d0sig, &b_muons_d0sig);
        fChain->SetBranchAddress("muons_e", &muons_e, &b_muons_e);
        fChain->SetBranchAddress("muons_eta", &muons_eta, &b_muons_eta);
        fChain->SetBranchAddress("muons_isol", &muons_isol, &b_muons_isol);
        fChain->SetBranchAddress("muons_isol_HighPtTrackOnly", &muons_isol_HighPtTrackOnly, &b_muons_isol_HighPtTrackOnly);
        fChain->SetBranchAddress("muons_isol_Loose_FixedRad", &muons_isol_Loose_FixedRad, &b_muons_isol_Loose_FixedRad);
        fChain->SetBranchAddress("muons_isol_Loose_VarRad", &muons_isol_Loose_VarRad, &b_muons_isol_Loose_VarRad);
        fChain->SetBranchAddress("muons_isol_PflowLoose_FixedRad", &muons_isol_PflowLoose_FixedRad, &b_muons_isol_PflowLoose_FixedRad);
        fChain->SetBranchAddress("muons_isol_PflowLoose_VarRad", &muons_isol_PflowLoose_VarRad, &b_muons_isol_PflowLoose_VarRad);
        fChain->SetBranchAddress("muons_isol_PflowTight_FixedRad", &muons_isol_PflowTight_FixedRad, &b_muons_isol_PflowTight_FixedRad);
        fChain->SetBranchAddress("muons_isol_PflowTight_VarRad", &muons_isol_PflowTight_VarRad, &b_muons_isol_PflowTight_VarRad);
        fChain->SetBranchAddress("muons_isol_TightTrackOnly_FixedRad", &muons_isol_TightTrackOnly_FixedRad, &b_muons_isol_TightTrackOnly_FixedRad);
        fChain->SetBranchAddress("muons_isol_TightTrackOnly_VarRad", &muons_isol_TightTrackOnly_VarRad, &b_muons_isol_TightTrackOnly_VarRad);
        fChain->SetBranchAddress("muons_isol_Tight_FixedRad", &muons_isol_Tight_FixedRad, &b_muons_isol_Tight_FixedRad);
        fChain->SetBranchAddress("muons_isol_Tight_VarRad", &muons_isol_Tight_VarRad, &b_muons_isol_Tight_VarRad);
        fChain->SetBranchAddress("muons_phi", &muons_phi, &b_muons_phi);
        fChain->SetBranchAddress("muons_pt", &muons_pt, &b_muons_pt);
        fChain->SetBranchAddress("muons_signal", &muons_signal, &b_muons_signal);
        fChain->SetBranchAddress("muons_truthOrigin", &muons_truthOrigin, &b_muons_truthOrigin);
        fChain->SetBranchAddress("muons_truthType", &muons_truthType, &b_muons_truthType);
        fChain->SetBranchAddress("muons_z0sinTheta", &muons_z0sinTheta, &b_muons_z0sinTheta);
        fChain->SetBranchAddress("n_BJets", &n_BJets, &b_n_BJets);
        fChain->SetBranchAddress("n_BaseElec", &n_BaseElec, &b_n_BaseElec);
        fChain->SetBranchAddress("n_BaseJets", &n_BaseJets, &b_n_BaseJets);
        fChain->SetBranchAddress("n_BaseMuon", &n_BaseMuon, &b_n_BaseMuon);
        fChain->SetBranchAddress("n_BaseTau", &n_BaseTau, &b_n_BaseTau);
        fChain->SetBranchAddress("n_SignalElec", &n_SignalElec, &b_n_SignalElec);
        fChain->SetBranchAddress("n_SignalJets", &n_SignalJets, &b_n_SignalJets);
        fChain->SetBranchAddress("n_SignalMuon", &n_SignalMuon, &b_n_SignalMuon);
        fChain->SetBranchAddress("n_SignalTau", &n_SignalTau, &b_n_SignalTau);
        fChain->SetBranchAddress("n_TruthJets", &n_TruthJets, &b_n_TruthJets);
        fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
        fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
        fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
        fChain->SetBranchAddress("taus_BDTEleScore", &taus_BDTEleScore, &b_taus_BDTEleScore);
        fChain->SetBranchAddress("taus_BDTEleScoreSigTrans", &taus_BDTEleScoreSigTrans, &b_taus_BDTEleScoreSigTrans);
        fChain->SetBranchAddress("taus_ConeTruthLabelID", &taus_ConeTruthLabelID, &b_taus_ConeTruthLabelID);
        fChain->SetBranchAddress("taus_MT", &taus_MT, &b_taus_MT);
        fChain->SetBranchAddress("taus_NTrks", &taus_NTrks, &b_taus_NTrks);
        fChain->SetBranchAddress("taus_NTrksJet", &taus_NTrksJet, &b_taus_NTrksJet);
        fChain->SetBranchAddress("taus_PartonTruthLabelID", &taus_PartonTruthLabelID, &b_taus_PartonTruthLabelID);
        fChain->SetBranchAddress("taus_Quality", &taus_Quality, &b_taus_Quality);
        fChain->SetBranchAddress("taus_RNNJetScore", &taus_RNNJetScore, &b_taus_RNNJetScore);
        fChain->SetBranchAddress("taus_RNNJetScoreSigTrans", &taus_RNNJetScoreSigTrans, &b_taus_RNNJetScoreSigTrans);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo",
                                 &taus_TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau80_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50",
                                 &taus_TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50,
                                 &b_taus_TrigMatchHLT_e17_lhmedium_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA",
                                 &taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA,
                                 &b_taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50",
                                 &taus_TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50,
                                 &b_taus_TrigMatchHLT_mu14_iloose_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo", &taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF",
                                 &taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50",
                                 &taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50,
                                 &b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12",
                                 &taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12,
                                 &b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA",
                                 &taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA,
                                 &b_taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_taus_TrigMatchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo", &taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF",
                                 &taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA",
                                 &taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA,
                                 &b_taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo", &taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50", &taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50,
                                 &b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50", &taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50,
                                 &b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50", &taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau35_medium1_tracktwo", &taus_TrigMatchHLT_mu14_tau35_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_mu14_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau125_medium1_tracktwo", &taus_TrigMatchHLT_tau125_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau125_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwo", &taus_TrigMatchHLT_tau160_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau160_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwoEF", &taus_TrigMatchHLT_tau160_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_tau160_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100", &taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100,
                                 &b_taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100", &taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100,
                                 &b_taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100", &taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100,
                                 &b_taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_medium1_tracktwo", &taus_TrigMatchHLT_tau25_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_medium1_tracktwoEF", &taus_TrigMatchHLT_tau25_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA,
                                 &b_taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo", &taus_TrigMatchHLT_tau35_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwoEF", &taus_TrigMatchHLT_tau35_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM", &taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45", &taus_TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF_xe70_L1XE45);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50",
                                 &taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_tau25_medium1_tracktwo_L1TAU12_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45",
                                 &taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_L1TAU20_xe70_L1XE45);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo",
                                 &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
                                 &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45", &taus_TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_xe70_L1XE45);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA,
                                 &b_taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM", &taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM,
                                 &b_taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45",
                                 &taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45,
                                 &b_taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_xe70_L1XE45);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12", &taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12,
                                 &b_taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwo", &taus_TrigMatchHLT_tau60_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau60_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwoEF", &taus_TrigMatchHLT_tau60_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
                                 &taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50,
                                 &b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA,
                                 &b_taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50",
                                 &taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50,
                                 &b_taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12",
                                 &taus_TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12,
                                 &b_taus_TrigMatchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwoEF", &taus_TrigMatchHLT_tau80_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60", &taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
                                 &taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60", &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
                                 &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
                                 &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
                                 &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60", &taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60,
                                 &b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
                                 &taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40,
                                 &b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
        fChain->SetBranchAddress("taus_TrkJetWidth", &taus_TrkJetWidth, &b_taus_TrkJetWidth);
        fChain->SetBranchAddress("taus_Width", &taus_Width, &b_taus_Width);
        fChain->SetBranchAddress("taus_charge", &taus_charge, &b_taus_charge);
        fChain->SetBranchAddress("taus_d0", &taus_d0, &b_taus_d0);
        fChain->SetBranchAddress("taus_d0sig", &taus_d0sig, &b_taus_d0sig);
        fChain->SetBranchAddress("taus_e", &taus_e, &b_taus_e);
        fChain->SetBranchAddress("taus_eta", &taus_eta, &b_taus_eta);
        fChain->SetBranchAddress("taus_phi", &taus_phi, &b_taus_phi);
        fChain->SetBranchAddress("taus_pt", &taus_pt, &b_taus_pt);
        fChain->SetBranchAddress("taus_signalID", &taus_signalID, &b_taus_signalID);
        fChain->SetBranchAddress("taus_trks_d0", &taus_trks_d0, &b_taus_trks_d0);
        fChain->SetBranchAddress("taus_trks_d0sig", &taus_trks_d0sig, &b_taus_trks_d0sig);
        fChain->SetBranchAddress("taus_trks_z0sinTheta", &taus_trks_z0sinTheta, &b_taus_trks_z0sinTheta);
        fChain->SetBranchAddress("taus_truthOrigin", &taus_truthOrigin, &b_taus_truthOrigin);
        fChain->SetBranchAddress("taus_truthType", &taus_truthType, &b_taus_truthType);
        fChain->SetBranchAddress("taus_z0sinTheta", &taus_z0sinTheta, &b_taus_z0sinTheta);
        fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
        fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);

        // extra branches
        fChain->SetBranchAddress("ML_score", &ML_score, &b_ML_score);
        fChain->SetBranchAddress("mergedRunNumber", &mergedRunNumber, &b_mergedRunNumber);
        fChain->SetBranchAddress("mergedlumiBlock", &mergedlumiBlock, &b_mergedlumiBlock);
        fChain->SetBranchAddress("tau1Pt", &tau1Pt, &b_tau1Pt);
        fChain->SetBranchAddress("tau2Pt", &tau2Pt, &b_tau2Pt);
        fChain->SetBranchAddress("tau1Mt", &tau1Mt, &b_tau1Mt);
        fChain->SetBranchAddress("tau2Mt", &tau2Mt, &b_tau2Mt);
        fChain->SetBranchAddress("dRtt", &dRtt, &b_dRtt);
        fChain->SetBranchAddress("dPhitt", &dPhitt, &b_dPhitt);
        fChain->SetBranchAddress("lepPt", &lepPt, &b_lepPt);
        fChain->SetBranchAddress("mT2", &mT2, &b_mT2);
        fChain->SetBranchAddress("MT2", &MT2, &b_MT2); // yuanj
        fChain->SetBranchAddress("MET_sig", &MET_sig, &b_MET_sig);
        fChain->SetBranchAddress("MET", &MET, &b_MET);
        fChain->SetBranchAddress("Mtt", &Mtt, &b_Mtt);
        fChain->SetBranchAddress("Mtt_12", &Mtt_12, &b_Mtt_12); // yuanj
        fChain->SetBranchAddress("lepMt", &lepMt, &b_lepMt);
        fChain->SetBranchAddress("tau2Mt", &tau2Mt, &b_tau2Mt);
        fChain->SetBranchAddress("meff", &meff, &b_meff);
        fChain->SetBranchAddress("mct", &mct, &b_mct);
        fChain->SetBranchAddress("Njet", &Njet, &b_Njet);
        fChain->SetBranchAddress("NTightTau", &NTightTau, &b_NTightTau);
        fChain->SetBranchAddress("topTagger", &topTagger, &b_topTagger);
        fChain->SetBranchAddress("Weight_mc", &Weight_mc, &b_Weight_mc);
        fChain->SetBranchAddress("totalWeight", &totalWeight, &b_totalWeight);
        fChain->SetBranchAddress("MT12", &MT12, &b_MT12);
        fChain->SetBranchAddress("MTsum", &MTsum, &b_MTsum);
        fChain->SetBranchAddress("Ptjet1", &Ptjet1, &b_Ptjet1);
        fChain->SetBranchAddress("Ptjet2", &Ptjet2, &b_Ptjet2);
        fChain->SetBranchAddress("Ptlep1", &Ptlep1, &b_Ptlep1);
        fChain->SetBranchAddress("Pttau1", &Pttau1, &b_Pttau1);
        fChain->SetBranchAddress("Mtjet1", &Mtjet1, &b_Mtjet1);
        fChain->SetBranchAddress("MT_jet", &MT_jet, &b_MT_jet);
        fChain->SetBranchAddress("MT_tau", &MT_tau, &b_MT_tau);
        fChain->SetBranchAddress("MT_lep", &MT_lep, &b_MT_lep);
        fChain->SetBranchAddress("dRlt", &dRlt, &b_dRlt);
        fChain->SetBranchAddress("dPhile", &dPhile, &b_dPhile);
        fChain->SetBranchAddress("dPhilj", &dPhilj, &b_dPhilj);
        fChain->SetBranchAddress("dPhitj", &dPhitj, &b_dPhitj);
        fChain->SetBranchAddress("dPhite", &dPhite, &b_dPhite);
        fChain->SetBranchAddress("dPhilt", &dPhilt, &b_dPhilt);
        fChain->SetBranchAddress("dEtalt", &dEtalt, &b_dEtalt);
        fChain->SetBranchAddress("M_inv_tl", &M_inv_tl, &b_M_inv_tl);
        fChain->SetBranchAddress("Mditau", &Mditau, &b_Mditau);
        fChain->SetBranchAddress("MT2_tl_100", &MT2_tl_100, &b_MT2_tl_100);
        fChain->SetBranchAddress("MT2_tl_50", &MT2_tl_50, &b_MT2_tl_50);
        fChain->SetBranchAddress("R_ISR", &R_ISR, &b_R_ISR);
        fChain->SetBranchAddress("METmeff", &METmeff, &b_METmeff);
        fChain->SetBranchAddress("TwotauPt", &TwotauPt, &b_TwotauPt);
        fChain->SetBranchAddress("dRLepTwoTau", &dRLepTwoTau, &b_dRLepTwoTau);
        fChain->SetBranchAddress("dPhiLepTwoTau", &dPhiLepTwoTau, &b_dPhiLepTwoTau);
        fChain->SetBranchAddress("dRt1MET", &dRt1MET, &b_dRt1MET);
        fChain->SetBranchAddress("dRt2MET", &dRt2MET, &b_dRt2MET);
        fChain->SetBranchAddress("dRlepMET", &dRlepMET, &b_dRlepMET);
        fChain->SetBranchAddress("dR2TauMET", &dR2TauMET, &b_dR2TauMET);
        fChain->SetBranchAddress("dPhit1MET", &dPhit1MET, &b_dPhit1MET);
        fChain->SetBranchAddress("dPhit2MET", &dPhit2MET, &b_dPhit2MET);
        fChain->SetBranchAddress("dPhilepMET", &dPhilepMET, &b_dPhilepMET);
        fChain->SetBranchAddress("dPhi2TauMET", &dPhi2TauMET, &b_dPhi2TauMET);
        fChain->SetBranchAddress("MCTtt", &MCTtt, &b_MCTtt);
        fChain->SetBranchAddress("METOHTj", &METOHTj, &b_METOHTj);
        fChain->SetBranchAddress("Minvtt", &Minvtt, &b_Minvtt);
        fChain->SetBranchAddress("MT2tt_50", &MT2tt_50, &b_MT2tt_50);
        fChain->SetBranchAddress("nS_tau", &nS_tau, &b_nS_tau);
        fChain->SetBranchAddress("R_ISR1", &R_ISR1, &b_R_ISR1);
        fChain->SetBranchAddress("nJets", &nJets, &b_nJets);
        fChain->SetBranchAddress("HTj", &HTj, &b_HTj);
        fChain->SetBranchAddress("MTtau1met", &MTtau1met, &b_MTtau1met);

        //   newTree = fChain->CloneTree(0);
        //   newTree->SetBranchStatus("*",0);
        //   newTree->SetBranchStatus("*up*",1);
        //   newTree->SetBranchStatus("*down*",1);
        //   newTree->SetBranchStatus("*Weight*",1);
        //   newTree->SetBranchStatus("*weight*",1);
    }
};
