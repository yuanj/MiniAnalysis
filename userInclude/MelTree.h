#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

#include <vector>

#include "AnaObjs.h"
#include "AnaTree.h"
#include "TLorentzVector.h"

class MelTree : public AnaTree {
public:
    MelTree(TChain *chain) : AnaTree(chain) { Init(this->fChain); }
    // todo: add truth id or properties, add scale factors
    AnaObjs getElectrons() {
        AnaObjs electronVec;
        for (unsigned int i = 0; i < this->electron_pt_Nominal->size(); i++) {
            int eleID(0);

            if (this->electron_isSignal_Nominal->at(i)) eleID |= EIsSignal;
            if (this->electron_isMedium_Nominal->at(i)) eleID |= EMediumLH;
            if (this->electron_isTight_Nominal->at(i)) eleID |= ETightLH;

            if (this->electron_isol_Loose_VarRad_Nominal->at(i)) eleID |= EIsoLoose_VarRad;
            if (this->electron_isol_Tight_VarRad_Nominal->at(i)) eleID |= EIsoTight_VarRad;
            if (this->electron_isol_TightTrk_VarRad_Nominal->at(i)) eleID |= EIsoTightTrackOnly_VarRad;
            if (this->electron_isol_TightTrk_FixedRad_Nominal->at(i)) eleID |= EIsoTightTrackOnly_FixedRad;
            if (this->electron_isol_HighPt_Nominal->at(i)) eleID |= EIsoFCHighPtCaloOnly;

            if (this->electron_HLT_e24_lhmedium_L1EM20VH_match_Nominal->at(i)) eleID |= E24_lhmedium_L1EM20VH;
            if (this->electron_HLT_e60_lhmedium_match_Nominal->at(i)) eleID |= E60_lhmedium;
            if (this->electron_HLT_e120_lhloose_match_Nominal->at(i)) eleID |= E120_lhloose;
            if (this->electron_HLT_e26_lhtight_nod0_ivarloose_match_Nominal->at(i)) eleID |= E26_lhtight_nod0_ivarloose;
            if (this->electron_HLT_e60_lhmedium_nod0_match_Nominal->at(i)) eleID |= E60_lhmedium_nod0;
            if (this->electron_HLT_e140_lhloose_nod0_match_Nominal->at(i)) eleID |= E140_lhloose_nod0;
            if (this->electron_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal->at(i)) eleID |= E26_lhtight_ivarloose_L1EM22VHI;
            if (this->electron_HLT_e60_lhmedium_L1EM22VHI_match_Nominal->at(i)) eleID |= E60_lhmedium_L1EM22VHI;
            if (this->electron_HLT_e140_lhloose_L1EM22VHI_match_Nominal->at(i)) eleID |= E140_lhloose_L1EM22VHI;

            AnaObj ele(ELECTRON, (this->electron_pt_Nominal->at(i) / 1000), this->electron_eta_Nominal->at(i), this->electron_phi_Nominal->at(i),
                       (this->electron_e_Nominal->at(i) / 1000), this->electron_charge_Nominal->at(i), eleID);
            electronVec.emplace_back(ele);
        }
        //if(tree->mcChannel != 0){
        //    electronVec.setProperties("Origin",*(this->electron_Origin_Nominal));
        //    electronVec.setProperties("Type",*(this->electron_Type_Nominal));
        //    electronVec.setProperties("IFFType",*(this->electron_IFFType_Nominal));
        //}
        electronVec.setProperties("d0sig", *(this->electron_d0sig_Nominal));
        electronVec.setProperties("z0sinTheta", *(this->electron_z0sinTheta_Nominal));
        electronVec.setProperties("passOR", *(this->electron_passOR_Nominal));
        electronVec = electronVec.filterObjects(0, "passOR");
        electronVec.sortByPt();
        return electronVec;
    }

    AnaObjs getMuons() {
        AnaObjs muonVec;
        for (unsigned int i = 0; i < this->muon_pt_Nominal->size(); i++) {
            int muonID(0);
            if (this->muon_isSignal_Nominal->at(i)) muonID |= MuIsSignal;
            if (this->muon_isol_PfLoose_VarRad_Nominal->at(i)) muonID |= MuIsoPflowLoose_VarRad;
            if (this->muon_isol_PfTight_VarRad_Nominal->at(i)) muonID |= MuIsoPflowTight_VarRad;
            if (this->muon_isol_Loose_VarRad_Nominal->at(i)) muonID |= MuIsoLoose_VarRad;
            if (this->muon_isol_Tight_VarRad_Nominal->at(i)) muonID |= MuIsoTight_VarRad;

            if (this->muon_HLT_mu20_iloose_L1MU15_match_Nominal->at(i)) muonID |= Mu20_iloose_L1MU15;
            if (this->muon_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal->at(i)) muonID |= Mu24_ivarmedium_L1MU14FCH;
            if (this->muon_HLT_mu26_ivarmedium_match_Nominal->at(i)) muonID |= Mu26_ivarmedium;
            if (this->muon_HLT_mu50_match_Nominal->at(i)) muonID |= Mu50;
            if (this->muon_HLT_mu50_L1MU14FCH_match_Nominal->at(i)) muonID |= Mu50_L1MU14FCH;

            AnaObj muon(MUON, (this->muon_pt_Nominal->at(i) / 1000), this->muon_eta_Nominal->at(i), this->muon_phi_Nominal->at(i),
                        (this->muon_e_Nominal->at(i) / 1000), this->muon_charg_Nominal->at(i), muonID);
            muonVec.emplace_back(muon);
        }
        //if(tree->mcChannel != 0){
        //    muonVec.setProperties("Origin",*(this->muon_Origin_Nominal));
        //    muonVec.setProperties("Type",*(this->muon_Type_Nominal));
        //    muonVec.setProperties("IFFType",*(this->muon_IFFType_Nominal));
        //}
        muonVec.setProperties("d0sig", *(this->muon_d0sig_Nominal));
        muonVec.setProperties("z0sinTheta", *(this->muon_z0sinTheta_Nominal));
        muonVec.setProperties("passOR", *(this->muon_passOR_Nominal));
        muonVec = muonVec.filterObjects(0, "passOR");
        muonVec.sortByPt();
        return muonVec;
    }
    AnaObjs getTaus() {
        AnaObjs tauVec;
        for (unsigned int i = 0; i < this->tau_pt_Nominal->size(); i++) {
            int tauID(0);
            switch ((int)this->tau_quality_Nominal->at(i)) {
                case 0:
                    tauID |= TauVeryLoose;
                    break;
                case 1:
                    tauID |= TauVeryLoose | TauLoose;
                    break;
                case 2:
                    tauID |= TauVeryLoose | TauLoose | TauMedium;
                    break;
                case 3:
                    tauID |= TauVeryLoose | TauLoose | TauMedium | TauTight;
            }
            switch ((int)this->tau_eveto_Nominal->at(i)) {
                case 1:
                    tauID |= TauEvetoLoose;
                    break;
                case 2:
                    tauID |= TauEvetoLoose | TauEvetoMedium;
                    break;
                case 3:
                    tauID |= TauEvetoLoose | TauEvetoMedium | TauEvetoTight;
            }
            if (this->tau_ntrks_Nominal->at(i) == 1) {
                tauID |= TauOneProng;
            } else {
                tauID |= TauThreeProng;
            }
            AnaObj tau(TAU, (this->tau_pt_Nominal->at(i) / 1000), this->tau_eta_Nominal->at(i), this->tau_phi_Nominal->at(i),
                       (this->tau_E_Nominal->at(i) / 1000), this->tau_charg_Nominal->at(i), tauID);
            tauVec.emplace_back(tau);
        }
        // tauVec.setProperties("Origin", *(this->tau_Origin_Nominal));
        // tauVec.setProperties("Type", *(this->tau_Type_Nominal));
        // tauVec.setProperties("IsTruthMatched", *(this->tau_IsTruthMatched_Nominal));
        tauVec.setProperties("passOR", *(this->tau_passOR_Nominal));
        tauVec.setProperties("RNNScore", *(this->tau_RNNJetScore_Nominal));
        tauVec = tauVec.filterObjects(0, "passOR");
        tauVec.sortByPt();
        return tauVec;
    }
    AnaObjs getJets() {
        AnaObjs jetVec;
        for (unsigned int i = 0; i < this->ptjets_Nominal->size(); i++) {
            int jetID(0);
            if (this->isBjets_Nominal->at(i)) jetID |= GoodBJet;
            // TODO: fix jvt,fjvt,nnjvt score/pass issue
            //if (fabs(this->etajets_Nominal->at(i)) < 2.4 && this->ptjets_Nominal->at(i) / 1000 > 20 && this->ptjets_Nominal->at(i) / 1000 < 60){
            //    if (this->JVTjets_Nominal->at(i)>0.5) jetID |= JetVertexTag;
            //}
            AnaObj jet(JET, 0, jetID);
            jet.SetPtEtaPhiM(this->ptjets_Nominal->at(i) / 1000, this->etajets_Nominal->at(i), this->phijets_Nominal->at(i),
                             (this->massjets_Nominal->at(i) / 1000));
            jetVec.emplace_back(jet);
        }
        jetVec.setProperties("JVTscore", *(this->JVTjets_Nominal));
        jetVec.setProperties("passOR", *(this->passORjet_Nominal));
        jetVec = jetVec.filterObjects(0, "passOR");
        jetVec.sortByPt();
        return jetVec;
    }
    int bNumber() {
        int bNum(0);
        for (unsigned int i = 0; i < this->isBjets_Nominal->size(); i++) {
            if (this->isBjets_Nominal->at(i) && this->passORjet_Nominal->at(i)) bNum++;
        }
        return bNum;
    }
    TLorentzVector getEtMiss() {
        TLorentzVector METVec;
        METVec.SetPtEtaPhiE(this->EtMiss_tst_Nominal / 1000, 0, this->EtMiss_tstPhi_Nominal, this->EtMiss_tst_Nominal / 1000);
        return METVec;
    }
    int treatAsYear(int runNumber) {
        if (RunNumber < 290000) {
            return 15;
        } else if (RunNumber < 320000) {
            return 16;
        } else if (RunNumber < 342000) {
            return 17;
        } else if (RunNumber < 400000) {
            return 18;
        } else if (RunNumber < 450000) {
            return 22;
        } else if (RunNumber < 460000) {
            return 23;
        } else {
            return -1;
        }
    }
    int getYear() { return this->treatAsYear(this->RunNumber); }

    // single lepton trigger
    AnaObjs getElectrons_Pass_1leptrig() {
        AnaObjs eleVec = this->getElectrons();
        AnaObjs outVec;
        int year = this->getYear();
        if (year < 16) {
            if (this->HLT_e24_lhmedium_L1EM20VH || this->HLT_e60_lhmedium || this->HLT_e120_lhloose)
                outVec = eleVec.filterObjects(25, 100, E24_lhmedium_L1EM20VH) + eleVec.filterObjects(61, 100, E60_lhmedium) +
                         eleVec.filterObjects(121, 100, E120_lhloose);
        } else if (year < 22) {
            if (this->HLT_e26_lhtight_nod0_ivarloose || this->HLT_e60_lhmedium_nod0 || this->HLT_e140_lhloose_nod0)
                outVec = eleVec.filterObjects(27, 100, E26_lhtight_nod0_ivarloose) + eleVec.filterObjects(61, 100, E60_lhmedium_nod0) +
                         eleVec.filterObjects(141, 100, E140_lhloose_nod0);
        } else {
            if (this->HLT_e26_lhtight_ivarloose_L1EM22VHI || this->HLT_e60_lhmedium_L1EM22VHI || this->HLT_e140_lhloose_L1EM22VHI)
                outVec = eleVec.filterObjects(27, 100, E26_lhtight_ivarloose_L1EM22VHI) + eleVec.filterObjects(61, 100, E60_lhmedium_L1EM22VHI) +
                         eleVec.filterObjects(141, 100, E140_lhloose_L1EM22VHI);
        }
        return outVec;
    }
    AnaObjs getMuons_Pass_1leptrig() {
        AnaObjs muonVec = this->getMuons();
        AnaObjs outVec;
        int year = this->getYear();
        if (year < 16) {
            if (this->HLT_mu20_iloose_L1MU15 || this->HLT_mu50)
                outVec = muonVec.filterObjects(20 * 1.05, 100, Mu20_iloose_L1MU15) + muonVec.filterObjects(50 * 1.05, 100, Mu50);
        } else if (year < 22) {
            if (this->HLT_mu26_ivarmedium || this->HLT_mu50)
                outVec = muonVec.filterObjects(26 * 1.05, 100, Mu26_ivarmedium) + muonVec.filterObjects(50 * 1.05, 100, Mu50);
        } else {
            if (this->HLT_mu24_ivarmedium_L1MU14FCH || this->HLT_mu50_L1MU14FCH)
                outVec = muonVec.filterObjects(24 * 1.05, 100, Mu24_ivarmedium_L1MU14FCH) + muonVec.filterObjects(50 * 1.05, 100, Mu50_L1MU14FCH);
        }
        return outVec;
    }
    Float_t SingleLeptonTrigger_SF(AnaObjs lepVec) {
        if (lepVec.size() == 0) return 1;
        lepVec.sortByPt();
        if (lepVec[0].type() == ELECTRON) {
            return this->WeightEvents_trigger_ele_single_Nominal;
        } else {
            return this->WeightEvents_trigger_mu_single_Nominal;
        }
    }

    // Declaration of leaf types
    ULong64_t EventNumber;
    UInt_t RunNumber;
    UInt_t LumiBlockNumber;
    Float_t GenFiltMET;
    Float_t GenFiltHT;
    Double_t Xsec;
    Double_t XsecEff;
    Double_t kFact;
    Double_t FilterEfficiency;
    Int_t subprocess;
    Int_t mcChannel;
    Double_t Weight_mc;
    Double_t fb_nBaseJet;
    Double_t fb_Mtt_reco;
    Double_t fb_dEtatt;
    Double_t fb_dPhit1j;
    Double_t fb_pt_lep;
    Double_t fb_pt_tau;
    Double_t fb_dRtt;
    Float_t WeightEvents;
    Float_t WeightEventsPU_Nominal;
    Float_t WeightEventselSF_Nominal;
    Float_t WeightEventsmuSF_Nominal;
    Float_t WeightEventstauSF_Nominal;
    Float_t WeightEventsSF_global_Nominal;
    Float_t WeightEvents_trigger_ele_single_Nominal;
    Float_t WeightEvents_trigger_mu_single_Nominal;
    Float_t WeightEvents_trigger_global_Nominal;
    Float_t WeightEventsbTag_Nominal;
    Float_t WeightEventsJVT_Nominal;
    Float_t WeightEventsfJVT_Nominal;
    Float_t AverageInteractionsPerCrossing;
    Float_t corrAverageInteractionsPerCrossing;
    Float_t corrActualInteractionsPerCrossing;
    Int_t NumPrimaryVertices;
    Bool_t passMETtrig;
    Int_t TruthPDGID1;
    Int_t TruthPDGID2;
    Int_t TruthPDFID1;
    Int_t TruthPDFID2;
    Float_t TruthX1;
    Float_t TruthX2;
    Float_t TruthXF1;
    Float_t TruthXF2;
    Float_t TruthQ;
    std::vector<int> *labeljets_reco_Nominal;
    std::vector<float> *ptjets_Nominal;
    std::vector<float> *etajets_Nominal;
    std::vector<float> *phijets_Nominal;
    std::vector<float> *massjets_Nominal;
    std::vector<int> *isBjets_Nominal;
    std::vector<float> *btag_weight_Nominal;
    std::vector<float> *JVTjets_Nominal;
    std::vector<bool> *passORjet_Nominal;
    std::vector<float> *tau_pt_Nominal;
    std::vector<float> *tau_eta_Nominal;
    std::vector<float> *tau_phi_Nominal;
    std::vector<float> *tau_E_Nominal;
    std::vector<float> *tau_m_Nominal;
    std::vector<bool> *tau_passOR_Nominal;
    std::vector<float> *tau_ntrks_Nominal;
    std::vector<float> *tau_nisotrks_Nominal;
    std::vector<float> *tau_charg_Nominal;
    std::vector<bool> *tau_isSignal_Nominal;
    std::vector<float> *tau_ipZ0SinThetaSigLeadTrk_Nominal;
    std::vector<float> *tau_leadTrkPt_Nominal;
    std::vector<float> *tau_ipSigLeadTrk_Nominal;
    std::vector<float> *tau_massTrkSys_Nominal;
    std::vector<float> *tau_trFlightPathSig_Nominal;
    std::vector<float> *tau_dRmax_Nominal;
    std::vector<float> *tau_quality_Nominal;
    std::vector<float> *tau_eveto_Nominal;
    std::vector<float> *tau_RNNJetScore_Nominal;
    std::vector<float> *tau_RNNJetScoreTrans_Nominal;
    std::vector<float> *tau_RNNEleScore_Nominal;
    std::vector<float> *tau_RNNEleScoreTrans_Nominal;
    std::vector<bool> *tau_IsTruthMatched_Nominal;
    std::vector<int> *tau_Origin_Nominal;
    std::vector<int> *tau_Type_Nominal;
    std::vector<float> *tau_effsf_Nominal;
    std::vector<float> *ptleptons_Nominal;
    std::vector<float> *etaleptons_Nominal;
    std::vector<float> *phileptons_Nominal;
    std::vector<float> *massleptons_Nominal;
    std::vector<float> *electron_pt_Nominal;
    std::vector<float> *electron_eta_Nominal;
    std::vector<float> *electron_phi_Nominal;
    std::vector<float> *electron_m_Nominal;
    std::vector<float> *electron_e_Nominal;
    std::vector<int> *electron_charge_Nominal;
    std::vector<bool> *electron_isMedium_Nominal;
    std::vector<bool> *electron_isTight_Nominal;
    std::vector<float> *electron_effsf_Nominal;
    std::vector<bool> *electron_isol_Loose_VarRad_Nominal;
    std::vector<bool> *electron_isol_Tight_VarRad_Nominal;
    std::vector<bool> *electron_isol_TightTrk_VarRad_Nominal;
    std::vector<bool> *electron_isol_TightTrk_FixedRad_Nominal;
    std::vector<bool> *electron_isol_HighPt_Nominal;
    std::vector<float> *muon_pt_Nominal;
    std::vector<float> *muon_eta_Nominal;
    std::vector<float> *muon_phi_Nominal;
    std::vector<float> *muon_m_Nominal;
    std::vector<float> *muon_e_Nominal;
    std::vector<int> *muon_charg_Nominal;
    std::vector<float> *muon_effsf_Nominal;
    std::vector<bool> *muon_isol_PfLoose_VarRad_Nominal;
    std::vector<bool> *muon_isol_PfTight_VarRad_Nominal;
    std::vector<bool> *muon_isol_Loose_VarRad_Nominal;
    std::vector<bool> *muon_isol_Tight_VarRad_Nominal;
    std::vector<int> *flavlep_Nominal;
    std::vector<int> *Originlep_Nominal;
    std::vector<int> *Typelep_Nominal;
    std::vector<int> *IFFTypelep_Nominal;
    std::vector<int> *electron_Origin_Nominal;
    std::vector<int> *electron_Type_Nominal;
    std::vector<int> *electron_IFFType_Nominal;
    std::vector<int> *muon_Origin_Nominal;
    std::vector<int> *muon_Type_Nominal;
    std::vector<int> *muon_IFFType_Nominal;
    std::vector<bool> *passORlep_Nominal;
    std::vector<bool> *isSignallep_Nominal;
    std::vector<bool> *electron_passOR_Nominal;
    std::vector<bool> *electron_isSignal_Nominal;
    std::vector<bool> *muon_passOR_Nominal;
    std::vector<bool> *muon_isSignal_Nominal;
    std::vector<float> *d0sig_Nominal;
    std::vector<float> *z0sinTheta_Nominal;
    std::vector<float> *electron_d0sig_Nominal;
    std::vector<float> *electron_z0sinTheta_Nominal;
    std::vector<float> *muon_d0sig_Nominal;
    std::vector<float> *muon_z0sinTheta_Nominal;
    Bool_t cleaningVeto_Nominal;
    Float_t EtMiss_tstPhi_Nominal;
    Float_t EtMiss_tst_Nominal;
    Float_t EtMiss_SigObj_Nominal;
    Float_t Etmiss_PVSoftTrkPhi_Nominal;
    Float_t Etmiss_PVSoftTrk_Nominal;
    Float_t Etmiss_JetPhi_Nominal;
    Float_t Etmiss_Jet_Nominal;
    Float_t Etmiss_ElePhi_Nominal;
    Float_t Etmiss_Ele_Nominal;
    Float_t Etmiss_MuonPhi_Nominal;
    Float_t Etmiss_Muon_Nominal;
    Float_t Etmiss_TauPhi_Nominal;
    Float_t Etmiss_Tau_Nominal;
    Bool_t HLT_e120_lhloose;
    std::vector<bool> *HLT_e120_lhloose_match_Nominal;
    std::vector<bool> *jet_HLT_e120_lhloose_match_Nominal;
    std::vector<bool> *electron_HLT_e120_lhloose_match_Nominal;
    std::vector<bool> *muon_HLT_e120_lhloose_match_Nominal;
    std::vector<bool> *tau_HLT_e120_lhloose_match_Nominal;
    std::vector<bool> *gamma_HLT_e120_lhloose_match_Nominal;
    Bool_t HLT_e140_lhloose_L1EM22VHI;
    std::vector<bool> *HLT_e140_lhloose_L1EM22VHI_match_Nominal;
    std::vector<bool> *jet_HLT_e140_lhloose_L1EM22VHI_match_Nominal;
    std::vector<bool> *electron_HLT_e140_lhloose_L1EM22VHI_match_Nominal;
    std::vector<bool> *muon_HLT_e140_lhloose_L1EM22VHI_match_Nominal;
    std::vector<bool> *tau_HLT_e140_lhloose_L1EM22VHI_match_Nominal;
    std::vector<bool> *gamma_HLT_e140_lhloose_L1EM22VHI_match_Nominal;
    Bool_t HLT_e140_lhloose_nod0;
    std::vector<bool> *HLT_e140_lhloose_nod0_match_Nominal;
    std::vector<bool> *jet_HLT_e140_lhloose_nod0_match_Nominal;
    std::vector<bool> *electron_HLT_e140_lhloose_nod0_match_Nominal;
    std::vector<bool> *muon_HLT_e140_lhloose_nod0_match_Nominal;
    std::vector<bool> *tau_HLT_e140_lhloose_nod0_match_Nominal;
    std::vector<bool> *gamma_HLT_e140_lhloose_nod0_match_Nominal;
    Bool_t HLT_e24_lhmedium_L1EM20VH;
    std::vector<bool> *HLT_e24_lhmedium_L1EM20VH_match_Nominal;
    std::vector<bool> *jet_HLT_e24_lhmedium_L1EM20VH_match_Nominal;
    std::vector<bool> *electron_HLT_e24_lhmedium_L1EM20VH_match_Nominal;
    std::vector<bool> *muon_HLT_e24_lhmedium_L1EM20VH_match_Nominal;
    std::vector<bool> *tau_HLT_e24_lhmedium_L1EM20VH_match_Nominal;
    std::vector<bool> *gamma_HLT_e24_lhmedium_L1EM20VH_match_Nominal;
    Bool_t HLT_e26_lhtight_ivarloose_L1EM22VHI;
    std::vector<bool> *HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;
    std::vector<bool> *jet_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;
    std::vector<bool> *electron_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;
    std::vector<bool> *muon_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;
    std::vector<bool> *tau_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;
    std::vector<bool> *gamma_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;
    Bool_t HLT_e26_lhtight_nod0_ivarloose;
    std::vector<bool> *HLT_e26_lhtight_nod0_ivarloose_match_Nominal;
    std::vector<bool> *jet_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;
    std::vector<bool> *electron_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;
    std::vector<bool> *muon_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;
    std::vector<bool> *tau_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;
    std::vector<bool> *gamma_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;
    Bool_t HLT_e60_lhmedium;
    std::vector<bool> *HLT_e60_lhmedium_match_Nominal;
    std::vector<bool> *jet_HLT_e60_lhmedium_match_Nominal;
    std::vector<bool> *electron_HLT_e60_lhmedium_match_Nominal;
    std::vector<bool> *muon_HLT_e60_lhmedium_match_Nominal;
    std::vector<bool> *tau_HLT_e60_lhmedium_match_Nominal;
    std::vector<bool> *gamma_HLT_e60_lhmedium_match_Nominal;
    Bool_t HLT_e60_lhmedium_L1EM22VHI;
    std::vector<bool> *HLT_e60_lhmedium_L1EM22VHI_match_Nominal;
    std::vector<bool> *jet_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;
    std::vector<bool> *electron_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;
    std::vector<bool> *muon_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;
    std::vector<bool> *tau_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;
    std::vector<bool> *gamma_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;
    Bool_t HLT_e60_lhmedium_nod0;
    std::vector<bool> *HLT_e60_lhmedium_nod0_match_Nominal;
    std::vector<bool> *jet_HLT_e60_lhmedium_nod0_match_Nominal;
    std::vector<bool> *electron_HLT_e60_lhmedium_nod0_match_Nominal;
    std::vector<bool> *muon_HLT_e60_lhmedium_nod0_match_Nominal;
    std::vector<bool> *tau_HLT_e60_lhmedium_nod0_match_Nominal;
    std::vector<bool> *gamma_HLT_e60_lhmedium_nod0_match_Nominal;
    Bool_t HLT_mu20_iloose_L1MU15;
    std::vector<bool> *HLT_mu20_iloose_L1MU15_match_Nominal;
    std::vector<bool> *jet_HLT_mu20_iloose_L1MU15_match_Nominal;
    std::vector<bool> *electron_HLT_mu20_iloose_L1MU15_match_Nominal;
    std::vector<bool> *muon_HLT_mu20_iloose_L1MU15_match_Nominal;
    std::vector<bool> *tau_HLT_mu20_iloose_L1MU15_match_Nominal;
    std::vector<bool> *gamma_HLT_mu20_iloose_L1MU15_match_Nominal;
    Bool_t HLT_mu24_ivarmedium_L1MU14FCH;
    std::vector<bool> *HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;
    std::vector<bool> *jet_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;
    std::vector<bool> *electron_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;
    std::vector<bool> *muon_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;
    std::vector<bool> *tau_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;
    std::vector<bool> *gamma_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;
    Bool_t HLT_mu26_ivarmedium;
    std::vector<bool> *HLT_mu26_ivarmedium_match_Nominal;
    std::vector<bool> *jet_HLT_mu26_ivarmedium_match_Nominal;
    std::vector<bool> *electron_HLT_mu26_ivarmedium_match_Nominal;
    std::vector<bool> *muon_HLT_mu26_ivarmedium_match_Nominal;
    std::vector<bool> *tau_HLT_mu26_ivarmedium_match_Nominal;
    std::vector<bool> *gamma_HLT_mu26_ivarmedium_match_Nominal;
    Bool_t HLT_mu50;
    std::vector<bool> *HLT_mu50_match_Nominal;
    std::vector<bool> *jet_HLT_mu50_match_Nominal;
    std::vector<bool> *electron_HLT_mu50_match_Nominal;
    std::vector<bool> *muon_HLT_mu50_match_Nominal;
    std::vector<bool> *tau_HLT_mu50_match_Nominal;
    std::vector<bool> *gamma_HLT_mu50_match_Nominal;
    Bool_t HLT_mu50_L1MU14FCH;
    std::vector<bool> *HLT_mu50_L1MU14FCH_match_Nominal;
    std::vector<bool> *jet_HLT_mu50_L1MU14FCH_match_Nominal;
    std::vector<bool> *electron_HLT_mu50_L1MU14FCH_match_Nominal;
    std::vector<bool> *muon_HLT_mu50_L1MU14FCH_match_Nominal;
    std::vector<bool> *tau_HLT_mu50_L1MU14FCH_match_Nominal;
    std::vector<bool> *gamma_HLT_mu50_L1MU14FCH_match_Nominal;

    // List of branches
    TBranch *b_EventNumber;                                                //!
    TBranch *b_RunNumber;                                                  //!
    TBranch *b_LumiBlock;                                                  //!
    TBranch *b_GenFiltMET;                                                 //!
    TBranch *b_GenFiltHT;                                                  //!
    TBranch *b_Xsec;                                                       //!
    TBranch *b_XsecEff;                                                    //!
    TBranch *b_kFact;                                                      //!
    TBranch *b_FilterEfficiency;                                           //!
    TBranch *b_subprocess;                                                 //!
    TBranch *b_mcChannel;                                                  //!
    TBranch *b_Weight_mc;                                                  //!
    TBranch *b_fb_nBaseJet;                                                //!
    TBranch *b_fb_Mtt_reco;                                               //!
    TBranch *b_fb_dEtatt;                                                 //!
    TBranch *b_fb_dPhit1j;                                                //!
    TBranch *b_fb_pt_lep;                                                 //!
    TBranch *b_fb_pt_tau;                                                 //!
    TBranch *b_fb_dRtt;                                                   //!
    TBranch *b_WeightEvents;                                               //!
    TBranch *b_WeightEventsPU_Nominal;                                     //!
    TBranch *b_WeightEventselSF_Nominal;                                   //!
    TBranch *b_WeightEventsmuSF_Nominal;                                   //!
    TBranch *b_WeightEventstauSF_Nominal;                                  //!
    TBranch *b_WeightEventsSF_global_Nominal;                              //!
    TBranch *b_WeightEvents_trigger_ele_single_Nominal;                    //!
    TBranch *b_WeightEvents_trigger_mu_single_Nominal;                     //!
    TBranch *b_WeightEvents_trigger_global_Nominal;                        //!
    TBranch *b_WeightEventsbTag_Nominal;                                   //!
    TBranch *b_WeightEventsJVT_Nominal;                                    //!
    TBranch *b_WeightEventsfJVT_Nominal;                                   //!
    TBranch *b_AverageInteractionsPerCrossing;                             //!
    TBranch *b_corrAverageInteractionsPerCrossing;                         //!
    TBranch *b_corrActualInteractionsPerCrossing;                          //!
    TBranch *b_NumPrimaryVertices;                                         //!
    TBranch *b_passMETtrig;                                                //!
    TBranch *b_TruthPDGID1;                                                //!
    TBranch *b_TruthPDGID2;                                                //!
    TBranch *b_TruthPDFID1;                                                //!
    TBranch *b_TruthPDFID2;                                                //!
    TBranch *b_TruthX1;                                                    //!
    TBranch *b_TruthX2;                                                    //!
    TBranch *b_TruthXF1;                                                   //!
    TBranch *b_TruthXF2;                                                   //!
    TBranch *b_TruthQ;                                                     //!
    TBranch *b_labeljets_reco_Nominal;                                     //!
    TBranch *b_ptjets_Nominal;                                             //!
    TBranch *b_etajets_Nominal;                                            //!
    TBranch *b_phijets_Nominal;                                            //!
    TBranch *b_massjets_Nominal;                                           //!
    TBranch *b_isBjets_Nominal;                                            //!
    TBranch *b_btag_weight_Nominal;                                        //!
    TBranch *b_JVTjets_Nominal;                                            //!
    TBranch *b_passORjet_Nominal;                                          //!
    TBranch *b_tau_pt_Nominal;                                             //!
    TBranch *b_tau_eta_Nominal;                                            //!
    TBranch *b_tau_phi_Nominal;                                            //!
    TBranch *b_tau_E_Nominal;                                              //!
    TBranch *b_tau_m_Nominal;                                              //!
    TBranch *b_tau_passOR_Nominal;                                         //!
    TBranch *b_tau_ntrks_Nominal;                                          //!
    TBranch *b_tau_nisotrks_Nominal;                                       //!
    TBranch *b_tau_charg_Nominal;                                          //!
    TBranch *b_tau_isSignal_Nominal;                                       //!
    TBranch *b_tau_ipZ0SinThetaSigLeadTrk_Nominal;                         //!
    TBranch *b_tau_leadTrkPt_Nominal;                                      //!
    TBranch *b_tau_ipSigLeadTrk_Nominal;                                   //!
    TBranch *b_tau_massTrkSys_Nominal;                                     //!
    TBranch *b_tau_trFlightPathSig_Nominal;                                //!
    TBranch *b_tau_dRmax_Nominal;                                          //!
    TBranch *b_tau_quality_Nominal;                                        //!
    TBranch *b_tau_eveto_Nominal;                                          //!
    TBranch *b_tau_RNNJetScore_Nominal;                                    //!
    TBranch *b_tau_RNNJetScoreTrans_Nominal;                               //!
    TBranch *b_tau_RNNEleScore_Nominal;                                    //!
    TBranch *b_tau_RNNEleScoreTrans_Nominal;                               //!
    TBranch *b_tau_IsTruthMatched_Nominal;                                 //!
    TBranch *b_tau_Origin_Nominal;                                         //!
    TBranch *b_tau_Type_Nominal;                                           //!
    TBranch *b_tau_effsf_Nominal;                                          //!
    TBranch *b_ptleptons_Nominal;                                          //!
    TBranch *b_etaleptons_Nominal;                                         //!
    TBranch *b_phileptons_Nominal;                                         //!
    TBranch *b_massleptons_Nominal;                                        //!
    TBranch *b_electron_pt_Nominal;                                        //!
    TBranch *b_electron_eta_Nominal;                                       //!
    TBranch *b_electron_phi_Nominal;                                       //!
    TBranch *b_electron_m_Nominal;                                         //!
    TBranch *b_electron_e_Nominal;                                         //!
    TBranch *b_electron_charge_Nominal;                                    //!
    TBranch *b_electron_isMedium_Nominal;                                  //!
    TBranch *b_electron_isTight_Nominal;                                   //!
    TBranch *b_electron_effsf_Nominal;                                     //!
    TBranch *b_electron_isol_Loose_VarRad_Nominal;                         //!
    TBranch *b_electron_isol_Tight_VarRad_Nominal;                         //!
    TBranch *b_electron_isol_TightTrk_VarRad_Nominal;                      //!
    TBranch *b_electron_isol_TightTrk_FixedRad_Nominal;                    //!
    TBranch *b_electron_isol_HighPt_Nominal;                               //!
    TBranch *b_muon_pt_Nominal;                                            //!
    TBranch *b_muon_eta_Nominal;                                           //!
    TBranch *b_muon_phi_Nominal;                                           //!
    TBranch *b_muon_m_Nominal;                                             //!
    TBranch *b_muon_e_Nominal;                                             //!
    TBranch *b_muon_charg_Nominal;                                         //!
    TBranch *b_muon_effsf_Nominal;                                         //!
    TBranch *b_muon_isol_PfLoose_VarRad_Nominal;                           //!
    TBranch *b_muon_isol_PfTight_VarRad_Nominal;                           //!
    TBranch *b_muon_isol_Loose_VarRad_Nominal;                             //!
    TBranch *b_muon_isol_Tight_VarRad_Nominal;                             //!
    TBranch *b_flavlep_Nominal;                                            //!
    TBranch *b_Originlep_Nominal;                                          //!
    TBranch *b_Typelep_Nominal;                                            //!
    TBranch *b_IFFTypelep_Nominal;                                         //!
    TBranch *b_electron_Origin_Nominal;                                    //!
    TBranch *b_electron_Type_Nominal;                                      //!
    TBranch *b_electron_IFFType_Nominal;                                   //!
    TBranch *b_muon_Origin_Nominal;                                        //!
    TBranch *b_muon_Type_Nominal;                                          //!
    TBranch *b_muon_IFFType_Nominal;                                       //!
    TBranch *b_passORlep_Nominal;                                          //!
    TBranch *b_isSignallep_Nominal;                                        //!
    TBranch *b_electron_passOR_Nominal;                                    //!
    TBranch *b_electron_isSignal_Nominal;                                  //!
    TBranch *b_muon_passOR_Nominal;                                        //!
    TBranch *b_muon_isSignal_Nominal;                                      //!
    TBranch *b_d0sig_Nominal;                                              //!
    TBranch *b_z0sinTheta_Nominal;                                         //!
    TBranch *b_electron_d0sig_Nominal;                                     //!
    TBranch *b_electron_z0sinTheta_Nominal;                                //!
    TBranch *b_muon_d0sig_Nominal;                                         //!
    TBranch *b_muon_z0sinTheta_Nominal;                                    //!
    TBranch *b_cleaningVeto_Nominal;                                       //!
    TBranch *b_EtMiss_tstPhi_Nominal;                                      //!
    TBranch *b_EtMiss_tst_Nominal;                                         //!
    TBranch *b_EtMiss_SigObj_Nominal;                                      //!
    TBranch *b_Etmiss_PVSoftTrkPhi_Nominal;                                //!
    TBranch *b_Etmiss_PVSoftTrk_Nominal;                                   //!
    TBranch *b_Etmiss_JetPhi_Nominal;                                      //!
    TBranch *b_Etmiss_Jet_Nominal;                                         //!
    TBranch *b_Etmiss_ElePhi_Nominal;                                      //!
    TBranch *b_Etmiss_Ele_Nominal;                                         //!
    TBranch *b_Etmiss_MuonPhi_Nominal;                                     //!
    TBranch *b_Etmiss_Muon_Nominal;                                        //!
    TBranch *b_Etmiss_TauPhi_Nominal;                                      //!
    TBranch *b_Etmiss_Tau_Nominal;                                         //!
    TBranch *b_HLT_e120_lhloose;                                           //!
    TBranch *b_HLT_e120_lhloose_match_Nominal;                             //!
    TBranch *b_jet_HLT_e120_lhloose_match_Nominal;                         //!
    TBranch *b_electron_HLT_e120_lhloose_match_Nominal;                    //!
    TBranch *b_muon_HLT_e120_lhloose_match_Nominal;                        //!
    TBranch *b_tau_HLT_e120_lhloose_match_Nominal;                         //!
    TBranch *b_gamma_HLT_e120_lhloose_match_Nominal;                       //!
    TBranch *b_HLT_e140_lhloose_L1EM22VHI;                                 //!
    TBranch *b_HLT_e140_lhloose_L1EM22VHI_match_Nominal;                   //!
    TBranch *b_jet_HLT_e140_lhloose_L1EM22VHI_match_Nominal;               //!
    TBranch *b_electron_HLT_e140_lhloose_L1EM22VHI_match_Nominal;          //!
    TBranch *b_muon_HLT_e140_lhloose_L1EM22VHI_match_Nominal;              //!
    TBranch *b_tau_HLT_e140_lhloose_L1EM22VHI_match_Nominal;               //!
    TBranch *b_gamma_HLT_e140_lhloose_L1EM22VHI_match_Nominal;             //!
    TBranch *b_HLT_e140_lhloose_nod0;                                      //!
    TBranch *b_HLT_e140_lhloose_nod0_match_Nominal;                        //!
    TBranch *b_jet_HLT_e140_lhloose_nod0_match_Nominal;                    //!
    TBranch *b_electron_HLT_e140_lhloose_nod0_match_Nominal;               //!
    TBranch *b_muon_HLT_e140_lhloose_nod0_match_Nominal;                   //!
    TBranch *b_tau_HLT_e140_lhloose_nod0_match_Nominal;                    //!
    TBranch *b_gamma_HLT_e140_lhloose_nod0_match_Nominal;                  //!
    TBranch *b_HLT_e24_lhmedium_L1EM20VH;                                  //!
    TBranch *b_HLT_e24_lhmedium_L1EM20VH_match_Nominal;                    //!
    TBranch *b_jet_HLT_e24_lhmedium_L1EM20VH_match_Nominal;                //!
    TBranch *b_electron_HLT_e24_lhmedium_L1EM20VH_match_Nominal;           //!
    TBranch *b_muon_HLT_e24_lhmedium_L1EM20VH_match_Nominal;               //!
    TBranch *b_tau_HLT_e24_lhmedium_L1EM20VH_match_Nominal;                //!
    TBranch *b_gamma_HLT_e24_lhmedium_L1EM20VH_match_Nominal;              //!
    TBranch *b_HLT_e26_lhtight_ivarloose_L1EM22VHI;                        //!
    TBranch *b_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;          //!
    TBranch *b_jet_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;      //!
    TBranch *b_electron_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal; //!
    TBranch *b_muon_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;     //!
    TBranch *b_tau_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;      //!
    TBranch *b_gamma_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;    //!
    TBranch *b_HLT_e26_lhtight_nod0_ivarloose;                             //!
    TBranch *b_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;               //!
    TBranch *b_jet_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;           //!
    TBranch *b_electron_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;      //!
    TBranch *b_muon_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;          //!
    TBranch *b_tau_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;           //!
    TBranch *b_gamma_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;         //!
    TBranch *b_HLT_e60_lhmedium;                                           //!
    TBranch *b_HLT_e60_lhmedium_match_Nominal;                             //!
    TBranch *b_jet_HLT_e60_lhmedium_match_Nominal;                         //!
    TBranch *b_electron_HLT_e60_lhmedium_match_Nominal;                    //!
    TBranch *b_muon_HLT_e60_lhmedium_match_Nominal;                        //!
    TBranch *b_tau_HLT_e60_lhmedium_match_Nominal;                         //!
    TBranch *b_gamma_HLT_e60_lhmedium_match_Nominal;                       //!
    TBranch *b_HLT_e60_lhmedium_L1EM22VHI;                                 //!
    TBranch *b_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;                   //!
    TBranch *b_jet_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;               //!
    TBranch *b_electron_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;          //!
    TBranch *b_muon_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;              //!
    TBranch *b_tau_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;               //!
    TBranch *b_gamma_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;             //!
    TBranch *b_HLT_e60_lhmedium_nod0;                                      //!
    TBranch *b_HLT_e60_lhmedium_nod0_match_Nominal;                        //!
    TBranch *b_jet_HLT_e60_lhmedium_nod0_match_Nominal;                    //!
    TBranch *b_electron_HLT_e60_lhmedium_nod0_match_Nominal;               //!
    TBranch *b_muon_HLT_e60_lhmedium_nod0_match_Nominal;                   //!
    TBranch *b_tau_HLT_e60_lhmedium_nod0_match_Nominal;                    //!
    TBranch *b_gamma_HLT_e60_lhmedium_nod0_match_Nominal;                  //!
    TBranch *b_HLT_mu20_iloose_L1MU15;                                     //!
    TBranch *b_HLT_mu20_iloose_L1MU15_match_Nominal;                       //!
    TBranch *b_jet_HLT_mu20_iloose_L1MU15_match_Nominal;                   //!
    TBranch *b_electron_HLT_mu20_iloose_L1MU15_match_Nominal;              //!
    TBranch *b_muon_HLT_mu20_iloose_L1MU15_match_Nominal;                  //!
    TBranch *b_tau_HLT_mu20_iloose_L1MU15_match_Nominal;                   //!
    TBranch *b_gamma_HLT_mu20_iloose_L1MU15_match_Nominal;                 //!
    TBranch *b_HLT_mu24_ivarmedium_L1MU14FCH;                              //!
    TBranch *b_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;                //!
    TBranch *b_jet_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;            //!
    TBranch *b_electron_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;       //!
    TBranch *b_muon_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;           //!
    TBranch *b_tau_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;            //!
    TBranch *b_gamma_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;          //!
    TBranch *b_HLT_mu26_ivarmedium;                                        //!
    TBranch *b_HLT_mu26_ivarmedium_match_Nominal;                          //!
    TBranch *b_jet_HLT_mu26_ivarmedium_match_Nominal;                      //!
    TBranch *b_electron_HLT_mu26_ivarmedium_match_Nominal;                 //!
    TBranch *b_muon_HLT_mu26_ivarmedium_match_Nominal;                     //!
    TBranch *b_tau_HLT_mu26_ivarmedium_match_Nominal;                      //!
    TBranch *b_gamma_HLT_mu26_ivarmedium_match_Nominal;                    //!
    TBranch *b_HLT_mu50;                                                   //!
    TBranch *b_HLT_mu50_match_Nominal;                                     //!
    TBranch *b_jet_HLT_mu50_match_Nominal;                                 //!
    TBranch *b_electron_HLT_mu50_match_Nominal;                            //!
    TBranch *b_muon_HLT_mu50_match_Nominal;                                //!
    TBranch *b_tau_HLT_mu50_match_Nominal;                                 //!
    TBranch *b_gamma_HLT_mu50_match_Nominal;                               //!
    TBranch *b_HLT_mu50_L1MU14FCH;                                         //!
    TBranch *b_HLT_mu50_L1MU14FCH_match_Nominal;                           //!
    TBranch *b_jet_HLT_mu50_L1MU14FCH_match_Nominal;                       //!
    TBranch *b_electron_HLT_mu50_L1MU14FCH_match_Nominal;                  //!
    TBranch *b_muon_HLT_mu50_L1MU14FCH_match_Nominal;                      //!
    TBranch *b_tau_HLT_mu50_L1MU14FCH_match_Nominal;                       //!
    TBranch *b_gamma_HLT_mu50_L1MU14FCH_match_Nominal;                     //!

    void Init(TChain *tree) override {
        std::lock_guard<std::mutex> lock(_mutex);
        // The Init() function is called when the selector needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).

        // Set object pointer
        labeljets_reco_Nominal = 0;
        ptjets_Nominal = 0;
        etajets_Nominal = 0;
        phijets_Nominal = 0;
        massjets_Nominal = 0;
        isBjets_Nominal = 0;
        btag_weight_Nominal = 0;
        JVTjets_Nominal = 0;
        passORjet_Nominal = 0;
        tau_pt_Nominal = 0;
        tau_eta_Nominal = 0;
        tau_phi_Nominal = 0;
        tau_E_Nominal = 0;
        tau_m_Nominal = 0;
        tau_passOR_Nominal = 0;
        tau_ntrks_Nominal = 0;
        tau_nisotrks_Nominal = 0;
        tau_charg_Nominal = 0;
        tau_isSignal_Nominal = 0;
        tau_ipZ0SinThetaSigLeadTrk_Nominal = 0;
        tau_leadTrkPt_Nominal = 0;
        tau_ipSigLeadTrk_Nominal = 0;
        tau_massTrkSys_Nominal = 0;
        tau_trFlightPathSig_Nominal = 0;
        tau_dRmax_Nominal = 0;
        tau_quality_Nominal = 0;
        tau_eveto_Nominal = 0;
        tau_RNNJetScore_Nominal = 0;
        tau_RNNJetScoreTrans_Nominal = 0;
        tau_RNNEleScore_Nominal = 0;
        tau_RNNEleScoreTrans_Nominal = 0;
        tau_IsTruthMatched_Nominal = 0;
        tau_Origin_Nominal = 0;
        tau_Type_Nominal = 0;
        tau_effsf_Nominal = 0;
        ptleptons_Nominal = 0;
        etaleptons_Nominal = 0;
        phileptons_Nominal = 0;
        massleptons_Nominal = 0;
        electron_pt_Nominal = 0;
        electron_eta_Nominal = 0;
        electron_phi_Nominal = 0;
        electron_m_Nominal = 0;
        electron_e_Nominal = 0;
        electron_charge_Nominal = 0;
        electron_isMedium_Nominal = 0;
        electron_isTight_Nominal = 0;
        electron_effsf_Nominal = 0;
        electron_isol_Loose_VarRad_Nominal = 0;
        electron_isol_Tight_VarRad_Nominal = 0;
        electron_isol_TightTrk_VarRad_Nominal = 0;
        electron_isol_TightTrk_FixedRad_Nominal = 0;
        electron_isol_HighPt_Nominal = 0;
        muon_pt_Nominal = 0;
        muon_eta_Nominal = 0;
        muon_phi_Nominal = 0;
        muon_m_Nominal = 0;
        muon_e_Nominal = 0;
        muon_charg_Nominal = 0;
        muon_effsf_Nominal = 0;
        muon_isol_PfLoose_VarRad_Nominal = 0;
        muon_isol_PfTight_VarRad_Nominal = 0;
        muon_isol_Loose_VarRad_Nominal = 0;
        muon_isol_Tight_VarRad_Nominal = 0;
        flavlep_Nominal = 0;
        Originlep_Nominal = 0;
        Typelep_Nominal = 0;
        IFFTypelep_Nominal = 0;
        electron_Origin_Nominal = 0;
        electron_Type_Nominal = 0;
        electron_IFFType_Nominal = 0;
        muon_Origin_Nominal = 0;
        muon_Type_Nominal = 0;
        muon_IFFType_Nominal = 0;
        passORlep_Nominal = 0;
        isSignallep_Nominal = 0;
        electron_passOR_Nominal = 0;
        electron_isSignal_Nominal = 0;
        muon_passOR_Nominal = 0;
        muon_isSignal_Nominal = 0;
        d0sig_Nominal = 0;
        z0sinTheta_Nominal = 0;
        electron_d0sig_Nominal = 0;
        electron_z0sinTheta_Nominal = 0;
        muon_d0sig_Nominal = 0;
        muon_z0sinTheta_Nominal = 0;
        HLT_e120_lhloose_match_Nominal = 0;
        jet_HLT_e120_lhloose_match_Nominal = 0;
        electron_HLT_e120_lhloose_match_Nominal = 0;
        muon_HLT_e120_lhloose_match_Nominal = 0;
        tau_HLT_e120_lhloose_match_Nominal = 0;
        gamma_HLT_e120_lhloose_match_Nominal = 0;
        HLT_e140_lhloose_L1EM22VHI_match_Nominal = 0;
        jet_HLT_e140_lhloose_L1EM22VHI_match_Nominal = 0;
        electron_HLT_e140_lhloose_L1EM22VHI_match_Nominal = 0;
        muon_HLT_e140_lhloose_L1EM22VHI_match_Nominal = 0;
        tau_HLT_e140_lhloose_L1EM22VHI_match_Nominal = 0;
        gamma_HLT_e140_lhloose_L1EM22VHI_match_Nominal = 0;
        HLT_e140_lhloose_nod0_match_Nominal = 0;
        jet_HLT_e140_lhloose_nod0_match_Nominal = 0;
        electron_HLT_e140_lhloose_nod0_match_Nominal = 0;
        muon_HLT_e140_lhloose_nod0_match_Nominal = 0;
        tau_HLT_e140_lhloose_nod0_match_Nominal = 0;
        gamma_HLT_e140_lhloose_nod0_match_Nominal = 0;
        HLT_e24_lhmedium_L1EM20VH_match_Nominal = 0;
        jet_HLT_e24_lhmedium_L1EM20VH_match_Nominal = 0;
        electron_HLT_e24_lhmedium_L1EM20VH_match_Nominal = 0;
        muon_HLT_e24_lhmedium_L1EM20VH_match_Nominal = 0;
        tau_HLT_e24_lhmedium_L1EM20VH_match_Nominal = 0;
        gamma_HLT_e24_lhmedium_L1EM20VH_match_Nominal = 0;
        HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal = 0;
        jet_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal = 0;
        electron_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal = 0;
        muon_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal = 0;
        tau_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal = 0;
        gamma_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal = 0;
        HLT_e26_lhtight_nod0_ivarloose_match_Nominal = 0;
        jet_HLT_e26_lhtight_nod0_ivarloose_match_Nominal = 0;
        electron_HLT_e26_lhtight_nod0_ivarloose_match_Nominal = 0;
        muon_HLT_e26_lhtight_nod0_ivarloose_match_Nominal = 0;
        tau_HLT_e26_lhtight_nod0_ivarloose_match_Nominal = 0;
        gamma_HLT_e26_lhtight_nod0_ivarloose_match_Nominal = 0;
        HLT_e60_lhmedium_match_Nominal = 0;
        jet_HLT_e60_lhmedium_match_Nominal = 0;
        electron_HLT_e60_lhmedium_match_Nominal = 0;
        muon_HLT_e60_lhmedium_match_Nominal = 0;
        tau_HLT_e60_lhmedium_match_Nominal = 0;
        gamma_HLT_e60_lhmedium_match_Nominal = 0;
        HLT_e60_lhmedium_L1EM22VHI_match_Nominal = 0;
        jet_HLT_e60_lhmedium_L1EM22VHI_match_Nominal = 0;
        electron_HLT_e60_lhmedium_L1EM22VHI_match_Nominal = 0;
        muon_HLT_e60_lhmedium_L1EM22VHI_match_Nominal = 0;
        tau_HLT_e60_lhmedium_L1EM22VHI_match_Nominal = 0;
        gamma_HLT_e60_lhmedium_L1EM22VHI_match_Nominal = 0;
        HLT_e60_lhmedium_nod0_match_Nominal = 0;
        jet_HLT_e60_lhmedium_nod0_match_Nominal = 0;
        electron_HLT_e60_lhmedium_nod0_match_Nominal = 0;
        muon_HLT_e60_lhmedium_nod0_match_Nominal = 0;
        tau_HLT_e60_lhmedium_nod0_match_Nominal = 0;
        gamma_HLT_e60_lhmedium_nod0_match_Nominal = 0;
        HLT_mu20_iloose_L1MU15_match_Nominal = 0;
        jet_HLT_mu20_iloose_L1MU15_match_Nominal = 0;
        electron_HLT_mu20_iloose_L1MU15_match_Nominal = 0;
        muon_HLT_mu20_iloose_L1MU15_match_Nominal = 0;
        tau_HLT_mu20_iloose_L1MU15_match_Nominal = 0;
        gamma_HLT_mu20_iloose_L1MU15_match_Nominal = 0;
        HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal = 0;
        jet_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal = 0;
        electron_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal = 0;
        muon_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal = 0;
        tau_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal = 0;
        gamma_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal = 0;
        HLT_mu26_ivarmedium_match_Nominal = 0;
        jet_HLT_mu26_ivarmedium_match_Nominal = 0;
        electron_HLT_mu26_ivarmedium_match_Nominal = 0;
        muon_HLT_mu26_ivarmedium_match_Nominal = 0;
        tau_HLT_mu26_ivarmedium_match_Nominal = 0;
        gamma_HLT_mu26_ivarmedium_match_Nominal = 0;
        HLT_mu50_match_Nominal = 0;
        jet_HLT_mu50_match_Nominal = 0;
        electron_HLT_mu50_match_Nominal = 0;
        muon_HLT_mu50_match_Nominal = 0;
        tau_HLT_mu50_match_Nominal = 0;
        gamma_HLT_mu50_match_Nominal = 0;
        HLT_mu50_L1MU14FCH_match_Nominal = 0;
        jet_HLT_mu50_L1MU14FCH_match_Nominal = 0;
        electron_HLT_mu50_L1MU14FCH_match_Nominal = 0;
        muon_HLT_mu50_L1MU14FCH_match_Nominal = 0;
        tau_HLT_mu50_L1MU14FCH_match_Nominal = 0;
        gamma_HLT_mu50_L1MU14FCH_match_Nominal = 0;
        // Set branch addresses and branch pointers
        if (!tree) return;
        fChain = tree;
        // fCurrent = -1;
        // fChain->SetMakeClass(1);

        fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
        fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
        fChain->SetBranchAddress("LumiBlockNumber", &LumiBlockNumber, &b_LumiBlock);
        fChain->SetBranchAddress("GenFiltMET", &GenFiltMET, &b_GenFiltMET);
        fChain->SetBranchAddress("GenFiltHT", &GenFiltHT, &b_GenFiltHT);
        fChain->SetBranchAddress("Xsec", &Xsec, &b_Xsec);
        fChain->SetBranchAddress("XsecEff", &XsecEff, &b_XsecEff);
        fChain->SetBranchAddress("kFact", &kFact, &b_kFact);
        fChain->SetBranchAddress("FilterEfficiency", &FilterEfficiency, &b_FilterEfficiency);
        fChain->SetBranchAddress("subprocess", &subprocess, &b_subprocess);
        fChain->SetBranchAddress("mcChannel", &mcChannel, &b_mcChannel);
        fChain->SetBranchAddress("WeightEvents", &WeightEvents, &b_WeightEvents);
        fChain->SetBranchAddress("Weight_mc", &Weight_mc, &b_Weight_mc);
        fChain->SetBranchAddress("fb_nBaseJet", &fb_nBaseJet, &b_fb_nBaseJet);
        fChain->SetBranchAddress("fb_Mtt_reco", &fb_Mtt_reco, &b_fb_Mtt_reco);
        fChain->SetBranchAddress("fb_dEtatt", &fb_dEtatt, &b_fb_dEtatt);
        fChain->SetBranchAddress("fb_dPhit1j", &fb_dPhit1j, &b_fb_dPhit1j);
        fChain->SetBranchAddress("fb_pt_lep", &fb_pt_lep, &b_fb_pt_lep);
        fChain->SetBranchAddress("fb_pt_tau", &fb_pt_tau, &b_fb_pt_tau);
        fChain->SetBranchAddress("fb_dRtt", &fb_dRtt, &b_fb_dRtt);
        fChain->SetBranchAddress("WeightEventsPU_Nominal", &WeightEventsPU_Nominal, &b_WeightEventsPU_Nominal);
        fChain->SetBranchAddress("WeightEventselSF_Nominal", &WeightEventselSF_Nominal, &b_WeightEventselSF_Nominal);
        fChain->SetBranchAddress("WeightEventsmuSF_Nominal", &WeightEventsmuSF_Nominal, &b_WeightEventsmuSF_Nominal);
        fChain->SetBranchAddress("WeightEventstauSF_Nominal", &WeightEventstauSF_Nominal, &b_WeightEventstauSF_Nominal);
        fChain->SetBranchAddress("WeightEventsSF_global_Nominal", &WeightEventsSF_global_Nominal, &b_WeightEventsSF_global_Nominal);
        fChain->SetBranchAddress("WeightEvents_trigger_ele_single_Nominal", &WeightEvents_trigger_ele_single_Nominal,
                                 &b_WeightEvents_trigger_ele_single_Nominal);
        fChain->SetBranchAddress("WeightEvents_trigger_mu_single_Nominal", &WeightEvents_trigger_mu_single_Nominal,
                                 &b_WeightEvents_trigger_mu_single_Nominal);
        fChain->SetBranchAddress("WeightEvents_trigger_global_Nominal", &WeightEvents_trigger_global_Nominal, &b_WeightEvents_trigger_global_Nominal);
        fChain->SetBranchAddress("WeightEventsbTag_Nominal", &WeightEventsbTag_Nominal, &b_WeightEventsbTag_Nominal);
        fChain->SetBranchAddress("WeightEventsJVT_Nominal", &WeightEventsJVT_Nominal, &b_WeightEventsJVT_Nominal);
        fChain->SetBranchAddress("WeightEventsfJVT_Nominal", &WeightEventsfJVT_Nominal, &b_WeightEventsfJVT_Nominal);
        fChain->SetBranchAddress("AverageInteractionsPerCrossing", &AverageInteractionsPerCrossing, &b_AverageInteractionsPerCrossing);
        fChain->SetBranchAddress("corrAverageInteractionsPerCrossing", &corrAverageInteractionsPerCrossing, &b_corrAverageInteractionsPerCrossing);
        fChain->SetBranchAddress("corrActualInteractionsPerCrossing", &corrActualInteractionsPerCrossing, &b_corrActualInteractionsPerCrossing);
        fChain->SetBranchAddress("NumPrimaryVertices", &NumPrimaryVertices, &b_NumPrimaryVertices);
        fChain->SetBranchAddress("passMETtrig", &passMETtrig, &b_passMETtrig);
        fChain->SetBranchAddress("TruthPDGID1", &TruthPDGID1, &b_TruthPDGID1);
        fChain->SetBranchAddress("TruthPDGID2", &TruthPDGID2, &b_TruthPDGID2);
        fChain->SetBranchAddress("TruthPDFID1", &TruthPDFID1, &b_TruthPDFID1);
        fChain->SetBranchAddress("TruthPDFID2", &TruthPDFID2, &b_TruthPDFID2);
        fChain->SetBranchAddress("TruthX1", &TruthX1, &b_TruthX1);
        fChain->SetBranchAddress("TruthX2", &TruthX2, &b_TruthX2);
        fChain->SetBranchAddress("TruthXF1", &TruthXF1, &b_TruthXF1);
        fChain->SetBranchAddress("TruthXF2", &TruthXF2, &b_TruthXF2);
        fChain->SetBranchAddress("TruthQ", &TruthQ, &b_TruthQ);
        fChain->SetBranchAddress("labeljets_reco_Nominal", &labeljets_reco_Nominal, &b_labeljets_reco_Nominal);
        fChain->SetBranchAddress("ptjets_Nominal", &ptjets_Nominal, &b_ptjets_Nominal);
        fChain->SetBranchAddress("etajets_Nominal", &etajets_Nominal, &b_etajets_Nominal);
        fChain->SetBranchAddress("phijets_Nominal", &phijets_Nominal, &b_phijets_Nominal);
        fChain->SetBranchAddress("massjets_Nominal", &massjets_Nominal, &b_massjets_Nominal);
        fChain->SetBranchAddress("isBjets_Nominal", &isBjets_Nominal, &b_isBjets_Nominal);
        fChain->SetBranchAddress("btag_weight_Nominal", &btag_weight_Nominal, &b_btag_weight_Nominal);
        fChain->SetBranchAddress("JVTjets_Nominal", &JVTjets_Nominal, &b_JVTjets_Nominal);
        fChain->SetBranchAddress("passORjet_Nominal", &passORjet_Nominal, &b_passORjet_Nominal);
        fChain->SetBranchAddress("tau_pt_Nominal", &tau_pt_Nominal, &b_tau_pt_Nominal);
        fChain->SetBranchAddress("tau_eta_Nominal", &tau_eta_Nominal, &b_tau_eta_Nominal);
        fChain->SetBranchAddress("tau_phi_Nominal", &tau_phi_Nominal, &b_tau_phi_Nominal);
        fChain->SetBranchAddress("tau_E_Nominal", &tau_E_Nominal, &b_tau_E_Nominal);
        fChain->SetBranchAddress("tau_m_Nominal", &tau_m_Nominal, &b_tau_m_Nominal);
        fChain->SetBranchAddress("tau_passOR_Nominal", &tau_passOR_Nominal, &b_tau_passOR_Nominal);
        fChain->SetBranchAddress("tau_ntrks_Nominal", &tau_ntrks_Nominal, &b_tau_ntrks_Nominal);
        fChain->SetBranchAddress("tau_nisotrks_Nominal", &tau_nisotrks_Nominal, &b_tau_nisotrks_Nominal);
        fChain->SetBranchAddress("tau_charg_Nominal", &tau_charg_Nominal, &b_tau_charg_Nominal);
        fChain->SetBranchAddress("tau_isSignal_Nominal", &tau_isSignal_Nominal, &b_tau_isSignal_Nominal);
        fChain->SetBranchAddress("tau_ipZ0SinThetaSigLeadTrk_Nominal", &tau_ipZ0SinThetaSigLeadTrk_Nominal, &b_tau_ipZ0SinThetaSigLeadTrk_Nominal);
        fChain->SetBranchAddress("tau_leadTrkPt_Nominal", &tau_leadTrkPt_Nominal, &b_tau_leadTrkPt_Nominal);
        fChain->SetBranchAddress("tau_ipSigLeadTrk_Nominal", &tau_ipSigLeadTrk_Nominal, &b_tau_ipSigLeadTrk_Nominal);
        fChain->SetBranchAddress("tau_massTrkSys_Nominal", &tau_massTrkSys_Nominal, &b_tau_massTrkSys_Nominal);
        fChain->SetBranchAddress("tau_trFlightPathSig_Nominal", &tau_trFlightPathSig_Nominal, &b_tau_trFlightPathSig_Nominal);
        fChain->SetBranchAddress("tau_dRmax_Nominal", &tau_dRmax_Nominal, &b_tau_dRmax_Nominal);
        fChain->SetBranchAddress("tau_quality_Nominal", &tau_quality_Nominal, &b_tau_quality_Nominal);
        fChain->SetBranchAddress("tau_eveto_Nominal", &tau_eveto_Nominal, &b_tau_eveto_Nominal);
        fChain->SetBranchAddress("tau_RNNJetScore_Nominal", &tau_RNNJetScore_Nominal, &b_tau_RNNJetScore_Nominal);
        fChain->SetBranchAddress("tau_RNNJetScoreTrans_Nominal", &tau_RNNJetScoreTrans_Nominal, &b_tau_RNNJetScoreTrans_Nominal);
        fChain->SetBranchAddress("tau_RNNEleScore_Nominal", &tau_RNNEleScore_Nominal, &b_tau_RNNEleScore_Nominal);
        fChain->SetBranchAddress("tau_RNNEleScoreTrans_Nominal", &tau_RNNEleScoreTrans_Nominal, &b_tau_RNNEleScoreTrans_Nominal);
        fChain->SetBranchAddress("tau_IsTruthMatched_Nominal", &tau_IsTruthMatched_Nominal, &b_tau_IsTruthMatched_Nominal);
        fChain->SetBranchAddress("tau_Origin_Nominal", &tau_Origin_Nominal, &b_tau_Origin_Nominal);
        fChain->SetBranchAddress("tau_Type_Nominal", &tau_Type_Nominal, &b_tau_Type_Nominal);
        fChain->SetBranchAddress("tau_effsf_Nominal", &tau_effsf_Nominal, &b_tau_effsf_Nominal);
        fChain->SetBranchAddress("ptleptons_Nominal", &ptleptons_Nominal, &b_ptleptons_Nominal);
        fChain->SetBranchAddress("etaleptons_Nominal", &etaleptons_Nominal, &b_etaleptons_Nominal);
        fChain->SetBranchAddress("phileptons_Nominal", &phileptons_Nominal, &b_phileptons_Nominal);
        fChain->SetBranchAddress("massleptons_Nominal", &massleptons_Nominal, &b_massleptons_Nominal);
        fChain->SetBranchAddress("electron_pt_Nominal", &electron_pt_Nominal, &b_electron_pt_Nominal);
        fChain->SetBranchAddress("electron_eta_Nominal", &electron_eta_Nominal, &b_electron_eta_Nominal);
        fChain->SetBranchAddress("electron_phi_Nominal", &electron_phi_Nominal, &b_electron_phi_Nominal);
        fChain->SetBranchAddress("electron_m_Nominal", &electron_m_Nominal, &b_electron_m_Nominal);
        fChain->SetBranchAddress("electron_e_Nominal", &electron_e_Nominal, &b_electron_e_Nominal);
        fChain->SetBranchAddress("electron_charge_Nominal", &electron_charge_Nominal, &b_electron_charge_Nominal);
        fChain->SetBranchAddress("electron_isMedium_Nominal", &electron_isMedium_Nominal, &b_electron_isMedium_Nominal);
        fChain->SetBranchAddress("electron_isTight_Nominal", &electron_isTight_Nominal, &b_electron_isTight_Nominal);
        fChain->SetBranchAddress("electron_effsf_Nominal", &electron_effsf_Nominal, &b_electron_effsf_Nominal);
        fChain->SetBranchAddress("electron_isol_Loose_VarRad_Nominal", &electron_isol_Loose_VarRad_Nominal, &b_electron_isol_Loose_VarRad_Nominal);
        fChain->SetBranchAddress("electron_isol_Tight_VarRad_Nominal", &electron_isol_Tight_VarRad_Nominal, &b_electron_isol_Tight_VarRad_Nominal);
        fChain->SetBranchAddress("electron_isol_TightTrk_VarRad_Nominal", &electron_isol_TightTrk_VarRad_Nominal,
                                 &b_electron_isol_TightTrk_VarRad_Nominal);
        fChain->SetBranchAddress("electron_isol_TightTrk_FixedRad_Nominal", &electron_isol_TightTrk_FixedRad_Nominal,
                                 &b_electron_isol_TightTrk_FixedRad_Nominal);
        fChain->SetBranchAddress("electron_isol_HighPt_Nominal", &electron_isol_HighPt_Nominal, &b_electron_isol_HighPt_Nominal);
        fChain->SetBranchAddress("muon_pt_Nominal", &muon_pt_Nominal, &b_muon_pt_Nominal);
        fChain->SetBranchAddress("muon_eta_Nominal", &muon_eta_Nominal, &b_muon_eta_Nominal);
        fChain->SetBranchAddress("muon_phi_Nominal", &muon_phi_Nominal, &b_muon_phi_Nominal);
        fChain->SetBranchAddress("muon_m_Nominal", &muon_m_Nominal, &b_muon_m_Nominal);
        fChain->SetBranchAddress("muon_e_Nominal", &muon_e_Nominal, &b_muon_e_Nominal);
        fChain->SetBranchAddress("muon_charg_Nominal", &muon_charg_Nominal, &b_muon_charg_Nominal);
        fChain->SetBranchAddress("muon_effsf_Nominal", &muon_effsf_Nominal, &b_muon_effsf_Nominal);
        fChain->SetBranchAddress("muon_isol_PfLoose_VarRad_Nominal", &muon_isol_PfLoose_VarRad_Nominal, &b_muon_isol_PfLoose_VarRad_Nominal);
        fChain->SetBranchAddress("muon_isol_PfTight_VarRad_Nominal", &muon_isol_PfTight_VarRad_Nominal, &b_muon_isol_PfTight_VarRad_Nominal);
        fChain->SetBranchAddress("muon_isol_Loose_VarRad_Nominal", &muon_isol_Loose_VarRad_Nominal, &b_muon_isol_Loose_VarRad_Nominal);
        fChain->SetBranchAddress("muon_isol_Tight_VarRad_Nominal", &muon_isol_Tight_VarRad_Nominal, &b_muon_isol_Tight_VarRad_Nominal);
        fChain->SetBranchAddress("flavlep_Nominal", &flavlep_Nominal, &b_flavlep_Nominal);
        fChain->SetBranchAddress("Originlep_Nominal", &Originlep_Nominal, &b_Originlep_Nominal);
        fChain->SetBranchAddress("Typelep_Nominal", &Typelep_Nominal, &b_Typelep_Nominal);
        fChain->SetBranchAddress("IFFTypelep_Nominal", &IFFTypelep_Nominal, &b_IFFTypelep_Nominal);
        fChain->SetBranchAddress("electron_Origin_Nominal", &electron_Origin_Nominal, &b_electron_Origin_Nominal);
        fChain->SetBranchAddress("electron_Type_Nominal", &electron_Type_Nominal, &b_electron_Type_Nominal);
        fChain->SetBranchAddress("electron_IFFType_Nominal", &electron_IFFType_Nominal, &b_electron_IFFType_Nominal);
        fChain->SetBranchAddress("muon_Origin_Nominal", &muon_Origin_Nominal, &b_muon_Origin_Nominal);
        fChain->SetBranchAddress("muon_Type_Nominal", &muon_Type_Nominal, &b_muon_Type_Nominal);
        fChain->SetBranchAddress("muon_IFFType_Nominal", &muon_IFFType_Nominal, &b_muon_IFFType_Nominal);
        fChain->SetBranchAddress("passORlep_Nominal", &passORlep_Nominal, &b_passORlep_Nominal);
        fChain->SetBranchAddress("isSignallep_Nominal", &isSignallep_Nominal, &b_isSignallep_Nominal);
        fChain->SetBranchAddress("electron_passOR_Nominal", &electron_passOR_Nominal, &b_electron_passOR_Nominal);
        fChain->SetBranchAddress("electron_isSignal_Nominal", &electron_isSignal_Nominal, &b_electron_isSignal_Nominal);
        fChain->SetBranchAddress("muon_passOR_Nominal", &muon_passOR_Nominal, &b_muon_passOR_Nominal);
        fChain->SetBranchAddress("muon_isSignal_Nominal", &muon_isSignal_Nominal, &b_muon_isSignal_Nominal);
        fChain->SetBranchAddress("d0sig_Nominal", &d0sig_Nominal, &b_d0sig_Nominal);
        fChain->SetBranchAddress("z0sinTheta_Nominal", &z0sinTheta_Nominal, &b_z0sinTheta_Nominal);
        fChain->SetBranchAddress("electron_d0sig_Nominal", &electron_d0sig_Nominal, &b_electron_d0sig_Nominal);
        fChain->SetBranchAddress("electron_z0sinTheta_Nominal", &electron_z0sinTheta_Nominal, &b_electron_z0sinTheta_Nominal);
        fChain->SetBranchAddress("muon_d0sig_Nominal", &muon_d0sig_Nominal, &b_muon_d0sig_Nominal);
        fChain->SetBranchAddress("muon_z0sinTheta_Nominal", &muon_z0sinTheta_Nominal, &b_muon_z0sinTheta_Nominal);
        fChain->SetBranchAddress("cleaningVeto_Nominal", &cleaningVeto_Nominal, &b_cleaningVeto_Nominal);
        fChain->SetBranchAddress("EtMiss_tstPhi_Nominal", &EtMiss_tstPhi_Nominal, &b_EtMiss_tstPhi_Nominal);
        fChain->SetBranchAddress("EtMiss_tst_Nominal", &EtMiss_tst_Nominal, &b_EtMiss_tst_Nominal);
        fChain->SetBranchAddress("EtMiss_SigObj_Nominal", &EtMiss_SigObj_Nominal, &b_EtMiss_SigObj_Nominal);
        fChain->SetBranchAddress("Etmiss_PVSoftTrkPhi_Nominal", &Etmiss_PVSoftTrkPhi_Nominal, &b_Etmiss_PVSoftTrkPhi_Nominal);
        fChain->SetBranchAddress("Etmiss_PVSoftTrk_Nominal", &Etmiss_PVSoftTrk_Nominal, &b_Etmiss_PVSoftTrk_Nominal);
        fChain->SetBranchAddress("Etmiss_JetPhi_Nominal", &Etmiss_JetPhi_Nominal, &b_Etmiss_JetPhi_Nominal);
        fChain->SetBranchAddress("Etmiss_Jet_Nominal", &Etmiss_Jet_Nominal, &b_Etmiss_Jet_Nominal);
        fChain->SetBranchAddress("Etmiss_ElePhi_Nominal", &Etmiss_ElePhi_Nominal, &b_Etmiss_ElePhi_Nominal);
        fChain->SetBranchAddress("Etmiss_Ele_Nominal", &Etmiss_Ele_Nominal, &b_Etmiss_Ele_Nominal);
        fChain->SetBranchAddress("Etmiss_MuonPhi_Nominal", &Etmiss_MuonPhi_Nominal, &b_Etmiss_MuonPhi_Nominal);
        fChain->SetBranchAddress("Etmiss_Muon_Nominal", &Etmiss_Muon_Nominal, &b_Etmiss_Muon_Nominal);
        fChain->SetBranchAddress("Etmiss_TauPhi_Nominal", &Etmiss_TauPhi_Nominal, &b_Etmiss_TauPhi_Nominal);
        fChain->SetBranchAddress("Etmiss_Tau_Nominal", &Etmiss_Tau_Nominal, &b_Etmiss_Tau_Nominal);
        fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
        fChain->SetBranchAddress("HLT_e120_lhloose_match_Nominal", &HLT_e120_lhloose_match_Nominal, &b_HLT_e120_lhloose_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_e120_lhloose_match_Nominal", &jet_HLT_e120_lhloose_match_Nominal, &b_jet_HLT_e120_lhloose_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_e120_lhloose_match_Nominal", &electron_HLT_e120_lhloose_match_Nominal,
                                 &b_electron_HLT_e120_lhloose_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_e120_lhloose_match_Nominal", &muon_HLT_e120_lhloose_match_Nominal, &b_muon_HLT_e120_lhloose_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_e120_lhloose_match_Nominal", &tau_HLT_e120_lhloose_match_Nominal, &b_tau_HLT_e120_lhloose_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_e120_lhloose_match_Nominal", &gamma_HLT_e120_lhloose_match_Nominal,
                                 &b_gamma_HLT_e120_lhloose_match_Nominal);
        fChain->SetBranchAddress("HLT_e140_lhloose_L1EM22VHI", &HLT_e140_lhloose_L1EM22VHI, &b_HLT_e140_lhloose_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e140_lhloose_L1EM22VHI_match_Nominal", &HLT_e140_lhloose_L1EM22VHI_match_Nominal,
                                 &b_HLT_e140_lhloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_e140_lhloose_L1EM22VHI_match_Nominal", &jet_HLT_e140_lhloose_L1EM22VHI_match_Nominal,
                                 &b_jet_HLT_e140_lhloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_e140_lhloose_L1EM22VHI_match_Nominal", &electron_HLT_e140_lhloose_L1EM22VHI_match_Nominal,
                                 &b_electron_HLT_e140_lhloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_e140_lhloose_L1EM22VHI_match_Nominal", &muon_HLT_e140_lhloose_L1EM22VHI_match_Nominal,
                                 &b_muon_HLT_e140_lhloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_e140_lhloose_L1EM22VHI_match_Nominal", &tau_HLT_e140_lhloose_L1EM22VHI_match_Nominal,
                                 &b_tau_HLT_e140_lhloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_e140_lhloose_L1EM22VHI_match_Nominal", &gamma_HLT_e140_lhloose_L1EM22VHI_match_Nominal,
                                 &b_gamma_HLT_e140_lhloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
        fChain->SetBranchAddress("HLT_e140_lhloose_nod0_match_Nominal", &HLT_e140_lhloose_nod0_match_Nominal, &b_HLT_e140_lhloose_nod0_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_e140_lhloose_nod0_match_Nominal", &jet_HLT_e140_lhloose_nod0_match_Nominal,
                                 &b_jet_HLT_e140_lhloose_nod0_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_e140_lhloose_nod0_match_Nominal", &electron_HLT_e140_lhloose_nod0_match_Nominal,
                                 &b_electron_HLT_e140_lhloose_nod0_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_e140_lhloose_nod0_match_Nominal", &muon_HLT_e140_lhloose_nod0_match_Nominal,
                                 &b_muon_HLT_e140_lhloose_nod0_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_e140_lhloose_nod0_match_Nominal", &tau_HLT_e140_lhloose_nod0_match_Nominal,
                                 &b_tau_HLT_e140_lhloose_nod0_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_e140_lhloose_nod0_match_Nominal", &gamma_HLT_e140_lhloose_nod0_match_Nominal,
                                 &b_gamma_HLT_e140_lhloose_nod0_match_Nominal);
        fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
        fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH_match_Nominal", &HLT_e24_lhmedium_L1EM20VH_match_Nominal,
                                 &b_HLT_e24_lhmedium_L1EM20VH_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_e24_lhmedium_L1EM20VH_match_Nominal", &jet_HLT_e24_lhmedium_L1EM20VH_match_Nominal,
                                 &b_jet_HLT_e24_lhmedium_L1EM20VH_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_e24_lhmedium_L1EM20VH_match_Nominal", &electron_HLT_e24_lhmedium_L1EM20VH_match_Nominal,
                                 &b_electron_HLT_e24_lhmedium_L1EM20VH_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_e24_lhmedium_L1EM20VH_match_Nominal", &muon_HLT_e24_lhmedium_L1EM20VH_match_Nominal,
                                 &b_muon_HLT_e24_lhmedium_L1EM20VH_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_e24_lhmedium_L1EM20VH_match_Nominal", &tau_HLT_e24_lhmedium_L1EM20VH_match_Nominal,
                                 &b_tau_HLT_e24_lhmedium_L1EM20VH_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_e24_lhmedium_L1EM20VH_match_Nominal", &gamma_HLT_e24_lhmedium_L1EM20VH_match_Nominal,
                                 &b_gamma_HLT_e24_lhmedium_L1EM20VH_match_Nominal);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_L1EM22VHI", &HLT_e26_lhtight_ivarloose_L1EM22VHI, &b_HLT_e26_lhtight_ivarloose_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal", &HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal,
                                 &b_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal", &jet_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal,
                                 &b_jet_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal",
                                 &electron_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal,
                                 &b_electron_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal", &muon_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal,
                                 &b_muon_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal", &tau_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal,
                                 &b_tau_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal", &gamma_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal,
                                 &b_gamma_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose_match_Nominal", &HLT_e26_lhtight_nod0_ivarloose_match_Nominal,
                                 &b_HLT_e26_lhtight_nod0_ivarloose_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_e26_lhtight_nod0_ivarloose_match_Nominal", &jet_HLT_e26_lhtight_nod0_ivarloose_match_Nominal,
                                 &b_jet_HLT_e26_lhtight_nod0_ivarloose_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_e26_lhtight_nod0_ivarloose_match_Nominal", &electron_HLT_e26_lhtight_nod0_ivarloose_match_Nominal,
                                 &b_electron_HLT_e26_lhtight_nod0_ivarloose_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_e26_lhtight_nod0_ivarloose_match_Nominal", &muon_HLT_e26_lhtight_nod0_ivarloose_match_Nominal,
                                 &b_muon_HLT_e26_lhtight_nod0_ivarloose_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_e26_lhtight_nod0_ivarloose_match_Nominal", &tau_HLT_e26_lhtight_nod0_ivarloose_match_Nominal,
                                 &b_tau_HLT_e26_lhtight_nod0_ivarloose_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_e26_lhtight_nod0_ivarloose_match_Nominal", &gamma_HLT_e26_lhtight_nod0_ivarloose_match_Nominal,
                                 &b_gamma_HLT_e26_lhtight_nod0_ivarloose_match_Nominal);
        fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
        fChain->SetBranchAddress("HLT_e60_lhmedium_match_Nominal", &HLT_e60_lhmedium_match_Nominal, &b_HLT_e60_lhmedium_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_e60_lhmedium_match_Nominal", &jet_HLT_e60_lhmedium_match_Nominal, &b_jet_HLT_e60_lhmedium_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_e60_lhmedium_match_Nominal", &electron_HLT_e60_lhmedium_match_Nominal,
                                 &b_electron_HLT_e60_lhmedium_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_e60_lhmedium_match_Nominal", &muon_HLT_e60_lhmedium_match_Nominal, &b_muon_HLT_e60_lhmedium_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_e60_lhmedium_match_Nominal", &tau_HLT_e60_lhmedium_match_Nominal, &b_tau_HLT_e60_lhmedium_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_e60_lhmedium_match_Nominal", &gamma_HLT_e60_lhmedium_match_Nominal,
                                 &b_gamma_HLT_e60_lhmedium_match_Nominal);
        fChain->SetBranchAddress("HLT_e60_lhmedium_L1EM22VHI", &HLT_e60_lhmedium_L1EM22VHI, &b_HLT_e60_lhmedium_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e60_lhmedium_L1EM22VHI_match_Nominal", &HLT_e60_lhmedium_L1EM22VHI_match_Nominal,
                                 &b_HLT_e60_lhmedium_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_e60_lhmedium_L1EM22VHI_match_Nominal", &jet_HLT_e60_lhmedium_L1EM22VHI_match_Nominal,
                                 &b_jet_HLT_e60_lhmedium_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_e60_lhmedium_L1EM22VHI_match_Nominal", &electron_HLT_e60_lhmedium_L1EM22VHI_match_Nominal,
                                 &b_electron_HLT_e60_lhmedium_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_e60_lhmedium_L1EM22VHI_match_Nominal", &muon_HLT_e60_lhmedium_L1EM22VHI_match_Nominal,
                                 &b_muon_HLT_e60_lhmedium_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_e60_lhmedium_L1EM22VHI_match_Nominal", &tau_HLT_e60_lhmedium_L1EM22VHI_match_Nominal,
                                 &b_tau_HLT_e60_lhmedium_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_e60_lhmedium_L1EM22VHI_match_Nominal", &gamma_HLT_e60_lhmedium_L1EM22VHI_match_Nominal,
                                 &b_gamma_HLT_e60_lhmedium_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
        fChain->SetBranchAddress("HLT_e60_lhmedium_nod0_match_Nominal", &HLT_e60_lhmedium_nod0_match_Nominal, &b_HLT_e60_lhmedium_nod0_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_e60_lhmedium_nod0_match_Nominal", &jet_HLT_e60_lhmedium_nod0_match_Nominal,
                                 &b_jet_HLT_e60_lhmedium_nod0_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_e60_lhmedium_nod0_match_Nominal", &electron_HLT_e60_lhmedium_nod0_match_Nominal,
                                 &b_electron_HLT_e60_lhmedium_nod0_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_e60_lhmedium_nod0_match_Nominal", &muon_HLT_e60_lhmedium_nod0_match_Nominal,
                                 &b_muon_HLT_e60_lhmedium_nod0_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_e60_lhmedium_nod0_match_Nominal", &tau_HLT_e60_lhmedium_nod0_match_Nominal,
                                 &b_tau_HLT_e60_lhmedium_nod0_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_e60_lhmedium_nod0_match_Nominal", &gamma_HLT_e60_lhmedium_nod0_match_Nominal,
                                 &b_gamma_HLT_e60_lhmedium_nod0_match_Nominal);
        fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
        fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15_match_Nominal", &HLT_mu20_iloose_L1MU15_match_Nominal,
                                 &b_HLT_mu20_iloose_L1MU15_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_mu20_iloose_L1MU15_match_Nominal", &jet_HLT_mu20_iloose_L1MU15_match_Nominal,
                                 &b_jet_HLT_mu20_iloose_L1MU15_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_mu20_iloose_L1MU15_match_Nominal", &electron_HLT_mu20_iloose_L1MU15_match_Nominal,
                                 &b_electron_HLT_mu20_iloose_L1MU15_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_mu20_iloose_L1MU15_match_Nominal", &muon_HLT_mu20_iloose_L1MU15_match_Nominal,
                                 &b_muon_HLT_mu20_iloose_L1MU15_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_mu20_iloose_L1MU15_match_Nominal", &tau_HLT_mu20_iloose_L1MU15_match_Nominal,
                                 &b_tau_HLT_mu20_iloose_L1MU15_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_mu20_iloose_L1MU15_match_Nominal", &gamma_HLT_mu20_iloose_L1MU15_match_Nominal,
                                 &b_gamma_HLT_mu20_iloose_L1MU15_match_Nominal);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_L1MU14FCH", &HLT_mu24_ivarmedium_L1MU14FCH, &b_HLT_mu24_ivarmedium_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal", &HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal,
                                 &b_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal", &jet_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal,
                                 &b_jet_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal", &electron_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal,
                                 &b_electron_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal", &muon_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal,
                                 &b_muon_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal", &tau_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal,
                                 &b_tau_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal", &gamma_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal,
                                 &b_gamma_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
        fChain->SetBranchAddress("HLT_mu26_ivarmedium_match_Nominal", &HLT_mu26_ivarmedium_match_Nominal, &b_HLT_mu26_ivarmedium_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_mu26_ivarmedium_match_Nominal", &jet_HLT_mu26_ivarmedium_match_Nominal,
                                 &b_jet_HLT_mu26_ivarmedium_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_mu26_ivarmedium_match_Nominal", &electron_HLT_mu26_ivarmedium_match_Nominal,
                                 &b_electron_HLT_mu26_ivarmedium_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_mu26_ivarmedium_match_Nominal", &muon_HLT_mu26_ivarmedium_match_Nominal,
                                 &b_muon_HLT_mu26_ivarmedium_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_mu26_ivarmedium_match_Nominal", &tau_HLT_mu26_ivarmedium_match_Nominal,
                                 &b_tau_HLT_mu26_ivarmedium_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_mu26_ivarmedium_match_Nominal", &gamma_HLT_mu26_ivarmedium_match_Nominal,
                                 &b_gamma_HLT_mu26_ivarmedium_match_Nominal);
        fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
        fChain->SetBranchAddress("HLT_mu50_match_Nominal", &HLT_mu50_match_Nominal, &b_HLT_mu50_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_mu50_match_Nominal", &jet_HLT_mu50_match_Nominal, &b_jet_HLT_mu50_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_mu50_match_Nominal", &electron_HLT_mu50_match_Nominal, &b_electron_HLT_mu50_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_mu50_match_Nominal", &muon_HLT_mu50_match_Nominal, &b_muon_HLT_mu50_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_mu50_match_Nominal", &tau_HLT_mu50_match_Nominal, &b_tau_HLT_mu50_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_mu50_match_Nominal", &gamma_HLT_mu50_match_Nominal, &b_gamma_HLT_mu50_match_Nominal);
        fChain->SetBranchAddress("HLT_mu50_L1MU14FCH", &HLT_mu50_L1MU14FCH, &b_HLT_mu50_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu50_L1MU14FCH_match_Nominal", &HLT_mu50_L1MU14FCH_match_Nominal, &b_HLT_mu50_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("jet_HLT_mu50_L1MU14FCH_match_Nominal", &jet_HLT_mu50_L1MU14FCH_match_Nominal,
                                 &b_jet_HLT_mu50_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("electron_HLT_mu50_L1MU14FCH_match_Nominal", &electron_HLT_mu50_L1MU14FCH_match_Nominal,
                                 &b_electron_HLT_mu50_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("muon_HLT_mu50_L1MU14FCH_match_Nominal", &muon_HLT_mu50_L1MU14FCH_match_Nominal,
                                 &b_muon_HLT_mu50_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("tau_HLT_mu50_L1MU14FCH_match_Nominal", &tau_HLT_mu50_L1MU14FCH_match_Nominal,
                                 &b_tau_HLT_mu50_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("gamma_HLT_mu50_L1MU14FCH_match_Nominal", &gamma_HLT_mu50_L1MU14FCH_match_Nominal,
                                 &b_gamma_HLT_mu50_L1MU14FCH_match_Nominal);
    }
};
