#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

#include <vector>

#include "AnaObjs.h"
#include "AnaTree.h"
#include "TLorentzVector.h"



class MelTree_v1 : public AnaTree {
public:
    MelTree_v1(TChain *chain) : AnaTree(chain) { Init(this->fChain); }
    bool safeAccess(const std::vector<bool>* vec, size_t index) {
        if (vec && index < vec->size()) {
            return vec->at(index);
        }
        return false; // Default to false if out of range or null
    }
    AnaObjs getElectrons() {
        AnaObjs electronVec;
        std::vector<bool> isEle;
        for (unsigned int i = 0; i < this->ptleptons_Nominal->size(); i++) {
            isEle.emplace_back(fabs(this->flavlep_Nominal->at(i)) == 11);

            int eleID(0);
            if (this->isSignallep_Nominal->at(i)) eleID |= EIsSignal;
            if (this->isMediumlep_Nominal->at(i)) eleID |= EMediumLH;
            if (this->isTightlep_Nominal->at(i)) eleID |= ETightLH;
            if (this->safeAccess(HLT_e24_lhmedium_L1EM20VH_match_Nominal, i)) eleID |= E24_lhmedium_L1EM20VH;
            if (this->safeAccess(HLT_e60_lhmedium_match_Nominal, i)) eleID |= E60_lhmedium;
            if (this->safeAccess(HLT_e120_lhloose_match_Nominal, i)) eleID |= E120_lhloose;
            if (this->safeAccess(this->HLT_e26_lhtight_nod0_ivarloose_match_Nominal, i)) eleID |= E26_lhtight_nod0_ivarloose;
            if (this->safeAccess(this->HLT_e60_lhmedium_nod0_match_Nominal, i)) eleID |= E60_lhmedium_nod0;
            if (this->safeAccess(this->HLT_e140_lhloose_nod0_match_Nominal, i)) eleID |= E140_lhloose_nod0;
            if (this->safeAccess(this->HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal, i)) eleID |= E26_lhtight_ivarloose_L1EM22VHI;
            if (this->safeAccess(this->HLT_e60_lhmedium_L1EM22VHI_match_Nominal, i)) eleID |= E60_lhmedium_L1EM22VHI;
            if (this->safeAccess(this->HLT_e140_lhloose_L1EM22VHI_match_Nominal, i)) eleID |= E140_lhloose_L1EM22VHI;

            TLorentzVector p4;
            p4.SetPtEtaPhiM((this->ptleptons_Nominal->at(i) / 1000), this->etaleptons_Nominal->at(i), this->phileptons_Nominal->at(i),(this->massleptons_Nominal->at(i) / 1000));
            AnaObj ele(ELECTRON, p4, this->flavlep_Nominal->at(i)/fabs(this->flavlep_Nominal->at(i)), eleID);
            electronVec.emplace_back(ele);
        }
        if (this->Originlep_Nominal) {
            electronVec.setProperties("Origin", *(this->Originlep_Nominal));
            electronVec.setProperties("Type", *(this->Typelep_Nominal));
            electronVec.setProperties("IFFType", *(this->IFFTypelep_Nominal));
        }
        electronVec.setProperties("d0sig", *(this->d0sig_Nominal));
        electronVec.setProperties("z0sinTheta", *(this->z0sinTheta_Nominal));
        electronVec.setProperties("isEle", isEle);
        electronVec.setProperties("passOR", *(this->passORlep_Nominal));
        electronVec = electronVec.filterObjects(0, "passOR");
        electronVec = electronVec.filterObjects(0, "isEle");
        electronVec.sortByPt();
        return electronVec;
    }


    AnaObjs getMuons() {
        AnaObjs muonVec;
        std::vector<bool> isMuon;
        for (unsigned int i = 0; i < this->ptleptons_Nominal->size(); i++) {
            isMuon.emplace_back(fabs(this->flavlep_Nominal->at(i)) == 13);
            
            int muonID(0);
            if (this->isSignallep_Nominal->at(i)) muonID |= MuIsSignal;
            if (this->isMediumlep_Nominal->at(i)) muonID |= MuMedium;
            if (this->isTightlep_Nominal->at(i)) muonID |= MuTight;
            if (this->safeAccess(HLT_mu20_iloose_L1MU15_match_Nominal, i)) muonID |= Mu20_iloose_L1MU15;
            if (this->safeAccess(HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal, i)) muonID |= Mu24_ivarmedium_L1MU14FCH;
            if (this->safeAccess(HLT_mu26_ivarmedium_match_Nominal, i)) muonID |= Mu26_ivarmedium;
            if (this->safeAccess(HLT_mu50_match_Nominal, i)) muonID |= Mu50;
            if (this->safeAccess(HLT_mu50_L1MU14FCH_match_Nominal, i)) muonID |= Mu50_L1MU14FCH;

            TLorentzVector p4;
            p4.SetPtEtaPhiM((this->ptleptons_Nominal->at(i) / 1000), this->etaleptons_Nominal->at(i), this->phileptons_Nominal->at(i),(this->massleptons_Nominal->at(i) / 1000));
            AnaObj muon(MUON, p4, this->flavlep_Nominal->at(i)/fabs(this->flavlep_Nominal->at(i)), muonID);
            muonVec.emplace_back(muon);
        }
        if (this->Originlep_Nominal) {
            muonVec.setProperties("Origin", *(this->Originlep_Nominal));
            muonVec.setProperties("Type", *(this->Typelep_Nominal));
            muonVec.setProperties("IFFType", *(this->IFFTypelep_Nominal));
        }
        muonVec.setProperties("d0sig", *(this->d0sig_Nominal));
        muonVec.setProperties("z0sinTheta", *(this->z0sinTheta_Nominal));
        muonVec.setProperties("isMuon", isMuon);
        muonVec.setProperties("passOR", *(this->passORlep_Nominal));
        muonVec = muonVec.filterObjects(0, "passOR");
        muonVec = muonVec.filterObjects(0, "isMuon");
        muonVec.sortByPt();
        return muonVec;
    }

    AnaObjs getTaus() {
        AnaObjs tauVec;
        for (unsigned int i = 0; i < this->pttau_Nominal->size(); i++) {
            int tauID(0);
            switch ((int)this->qualitytau_Nominal->at(i)) {
                case 0:
                    tauID |= TauVeryLoose;
                    break;
                case 1:
                    tauID |= TauVeryLoose | TauLoose;
                    break;
                case 2:
                    tauID |= TauVeryLoose | TauLoose | TauMedium;
                    break;
                case 3:
                    tauID |= TauVeryLoose | TauLoose | TauMedium | TauTight;
            }
            switch ((int)this->evetotau_Nominal->at(i)) {
                case 1:
                    tauID |= TauEvetoLoose;
                    break;
                case 2:
                    tauID |= TauEvetoLoose | TauEvetoMedium;
                    break;
                case 3:
                    tauID |= TauEvetoLoose | TauEvetoMedium | TauEvetoTight;
            }
            if (this->ntrkstau_Nominal->at(i) == 1) {
                tauID |= TauOneProng;
            } else {
                tauID |= TauThreeProng;
            }
            if (this->signaltau_Nominal->at(i)) tauID |= TauIsSignal;
            if (this->safeAccess(IsTruthMatchedtau_Nominal, i)) tauID |= TauIsTruthMatched;
            if (this->safeAccess(IsHadronictau_Nominal, i)) tauID |= TauIsTruthHad;

            AnaObj tau(TAU, (this->pttau_Nominal->at(i) / 1000), this->etatau_Nominal->at(i), this->phitau_Nominal->at(i),
                (this->Etau_Nominal->at(i) / 1000), this->chargtau_Nominal->at(i), tauID);
            tauVec.emplace_back(tau);
        }
        if (this->Origintau_Nominal && this->Origintau_Nominal->size()>0) {
            tauVec.setProperties("Origin", *(this->Origintau_Nominal));
            tauVec.setProperties("Type", *(this->Typetau_Nominal));
        }
        tauVec.setProperties("RNNScore", *(this->RNNJetScoretau_Nominal));
        tauVec.setProperties("RNNScoreSigTrans", *(this->RNNJetScoreSigTranstau_Nominal));
        tauVec.setProperties("RNNEleScore", *(this->RNNEleScoretau_Nominal));
        tauVec.setProperties("passOR", *(this->passORtau_Nominal));
        tauVec = tauVec.filterObjects(0, "passOR");
        tauVec.sortByPt();
        return tauVec;
    }

    AnaObjs getJets() {
        AnaObjs jetVec;
        for (unsigned int i = 0; i < this->ptjets_Nominal->size(); i++) {
            int jetID(0);
            if (this->isBjets_Nominal->at(i)>3) jetID |= GoodBJet;// 77%
            else jetID |= LightJet;
            TLorentzVector p4;
            p4.SetPtEtaPhiM((this->ptjets_Nominal->at(i) / 1000), this->etajets_Nominal->at(i), this->phijets_Nominal->at(i),(this->massjets_Nominal->at(i) / 1000));
            AnaObj jet(JET, p4, 0, jetID);
            jetVec.emplace_back(jet);
        }
        jetVec.setProperties("JVTscore", *(this->JVTjets_Nominal));
        jetVec.setProperties("passOR", *(this->passORjet_Nominal));
        jetVec = jetVec.filterObjects(0, "passOR");
        jetVec.sortByPt();
        // labeljets_reco_Nominal //HadronConeExclExtendedTruthLabelID
        return jetVec;
    }

    int bNumber() {
        int bNum(0);
        for (unsigned int i = 0; i < this->isBjets_Nominal->size(); i++) {
            if ((this->isBjets_Nominal->at(i)>3) && this->passORjet_Nominal->at(i)) bNum++;
        }
        return bNum;
    }
    TLorentzVector getEtMiss() {
        TLorentzVector METVec;
        METVec.SetPtEtaPhiE(this->EtMiss_tst_Nominal / 1000, 0, this->EtMiss_tstPhi_Nominal, this->EtMiss_tst_Nominal / 1000);
        return METVec;
    }
    int treatAsYear(int runNumber) {
        if (RunNumber < 290000) {
            return 15;
        } else if (RunNumber < 320000) {
            return 16;
        } else if (RunNumber < 342000) {
            return 17;
        } else if (RunNumber < 400000) {
            return 18;
        } else if (RunNumber < 450000) {
            return 22;
        } else if (RunNumber < 460000) {
            return 23;
        } else {
            return -1;
        }
    }
    int getYear() { return this->treatAsYear(this->RunNumber); }

    // single lepton trigger
    AnaObjs getElectrons_Pass_1leptrig() {
        AnaObjs eleVec = this->getElectrons();
        AnaObjs outVec;
        int year = this->getYear();
        if (year < 16) {
            if (this->HLT_e24_lhmedium_L1EM20VH || this->HLT_e60_lhmedium || this->HLT_e120_lhloose)
                outVec = eleVec.filterObjects(25, 100, E24_lhmedium_L1EM20VH) + eleVec.filterObjects(61, 100, E60_lhmedium) +
                         eleVec.filterObjects(121, 100, E120_lhloose);
        } else if (year < 22) {
            if (this->HLT_e26_lhtight_nod0_ivarloose || this->HLT_e60_lhmedium_nod0 || this->HLT_e140_lhloose_nod0)
                outVec = eleVec.filterObjects(27, 100, E26_lhtight_nod0_ivarloose) + eleVec.filterObjects(61, 100, E60_lhmedium_nod0) +
                         eleVec.filterObjects(141, 100, E140_lhloose_nod0);
        } else {
            if (this->HLT_e26_lhtight_ivarloose_L1EM22VHI || this->HLT_e60_lhmedium_L1EM22VHI || this->HLT_e140_lhloose_L1EM22VHI)
                outVec = eleVec.filterObjects(27, 100, E26_lhtight_ivarloose_L1EM22VHI) + eleVec.filterObjects(61, 100, E60_lhmedium_L1EM22VHI) +
                         eleVec.filterObjects(141, 100, E140_lhloose_L1EM22VHI);
        }
        return outVec;
    }
    AnaObjs getMuons_Pass_1leptrig() {
        AnaObjs muonVec = this->getMuons();
        AnaObjs outVec;
        int year = this->getYear();
        if (year < 16) {
            if (this->HLT_mu20_iloose_L1MU15 || this->HLT_mu50)
                outVec = muonVec.filterObjects(20 * 1.05, 100, Mu20_iloose_L1MU15) + muonVec.filterObjects(50 * 1.05, 100, Mu50);
        } else if (year < 22) {
            if (this->HLT_mu26_ivarmedium || this->HLT_mu50)
                outVec = muonVec.filterObjects(26 * 1.05, 100, Mu26_ivarmedium) + muonVec.filterObjects(50 * 1.05, 100, Mu50);
        } else {
            if (this->HLT_mu24_ivarmedium_L1MU14FCH || this->HLT_mu50_L1MU14FCH)
                outVec = muonVec.filterObjects(24 * 1.05, 100, Mu24_ivarmedium_L1MU14FCH) + muonVec.filterObjects(50 * 1.05, 100, Mu50_L1MU14FCH);
        }
        return outVec;
    }
    Float_t SingleLeptonTrigger_SF(AnaObjs lepVec) {
        if (lepVec.size() == 0) return 1;
        lepVec.sortByPt();
        if (lepVec[0].type() == ELECTRON) {
            return this->WeightEvents_trigger_ele_single_Nominal;
        } else {
            return this->WeightEvents_trigger_mu_single_Nominal;
        }
    }

    // Declaration of leaf types
    ULong64_t       EventNumber;
    UInt_t          RunNumber;
    UInt_t          LumiBlockNumber;
    Float_t         GenFiltMET;
    Float_t         GenFiltHTinclNu;
    Float_t         GenFiltPTZ;
    Float_t         GenFiltFatJ;
    Float_t         GenFiltHT;
    Double_t        xsec;
    Int_t           subprocess;
    Double_t        mcChannel;
    Double_t        Weight_mc;
    Double_t        fb_nBaseJet;
    Double_t        fb_Mtt_reco;
    Double_t        fb_dEtatt;
    Float_t         WeightEvents;
    Float_t         WeightEventsPU_Nominal;
    Float_t         WeightEventselSF_Nominal;
    Float_t         WeightEventsmuSF_Nominal;
    Float_t         WeightEventstauSF_Nominal;
    Float_t         WeightEventsSF_global_Nominal;
    Float_t         WeightEvents_trigger_ele_single_Nominal;
    Float_t         WeightEvents_trigger_mu_single_Nominal;
    Float_t         WeightEvents_trigger_global_Nominal;
    Float_t         WeightEventsbTag_Nominal;
    Float_t         WeightEventsJVT_Nominal;
    Float_t         WeightEventsfJVT_Nominal;
    Float_t         AverageInteractionsPerCrossing;
    Float_t         corrAverageInteractionsPerCrossing;
    Float_t         corrActualInteractionsPerCrossing;
    Int_t           NumPrimaryVertices;
    Bool_t          passMETtrig;
    Int_t           TruthPDGID1;
    Int_t           TruthPDGID2;
    Int_t           TruthPDFID1;
    Int_t           TruthPDFID2;
    Float_t         TruthX1;
    Float_t         TruthX2;
    Float_t         TruthXF1;
    Float_t         TruthXF2;
    Float_t         TruthQ;
    std::vector<int>     *labeljets_reco_Nominal;
    std::vector<float>   *ptjets_Nominal;
    std::vector<float>   *etajets_Nominal;
    std::vector<float>   *phijets_Nominal;
    std::vector<float>   *massjets_Nominal;
    std::vector<int>     *isBjets_Nominal;
    std::vector<float>   *btag_weight_Nominal;
    std::vector<float>   *btag_pu_Nominal;
    std::vector<float>   *btag_pb_Nominal;
    std::vector<float>   *btag_pc_Nominal;
    std::vector<float>   *btag_ptau_Nominal;
    std::vector<float>   *JVTjets_Nominal;
    std::vector<bool>    *passORjet_Nominal;
    std::vector<float>   *pttau_Nominal;
    std::vector<float>   *etatau_Nominal;
    std::vector<float>   *phitau_Nominal;
    std::vector<float>   *Etau_Nominal;
    std::vector<bool>    *passORtau_Nominal;
    std::vector<bool>    *signaltau_Nominal;
    std::vector<float>   *ntrkstau_Nominal;
    std::vector<float>   *chargtau_Nominal;
    std::vector<float>   *RNNJetScoretau_Nominal;
    std::vector<float>   *RNNJetScoreSigTranstau_Nominal;
    std::vector<int>     *qualitytau_Nominal;
    std::vector<int>     *evetotau_Nominal;
    std::vector<float>   *RNNEleScoretau_Nominal;
    std::vector<bool>    *IsTruthMatchedtau_Nominal;
    std::vector<int>     *Origintau_Nominal;
    std::vector<int>     *Typetau_Nominal;
    std::vector<bool>    *IsHadronictau_Nominal;
    std::vector<float>   *ptleptons_Nominal;
    std::vector<float>   *etaleptons_Nominal;
    std::vector<float>   *phileptons_Nominal;
    std::vector<float>   *massleptons_Nominal;
    std::vector<int>     *flavlep_Nominal;
    std::vector<int>     *Originlep_Nominal;
    std::vector<int>     *Typelep_Nominal;
    std::vector<int>     *IFFTypelep_Nominal;
    std::vector<bool>    *passORlep_Nominal;
    std::vector<bool>    *isSignallep_Nominal;
    std::vector<bool>    *isMediumlep_Nominal;
    std::vector<bool>    *isTightlep_Nominal;
    std::vector<float>   *d0sig_Nominal;
    std::vector<float>   *z0sinTheta_Nominal;
    Bool_t          cleaningVeto_Nominal;
    Float_t         EtMiss_tstPhi_Nominal;
    Float_t         EtMiss_tst_Nominal;
    Float_t         EtMiss_SigObj_Nominal;
    Float_t         Etmiss_PVSoftTrkPhi_Nominal;
    Float_t         Etmiss_PVSoftTrk_Nominal;
    Float_t         Etmiss_JetPhi_Nominal;
    Float_t         Etmiss_Jet_Nominal;
    Float_t         Etmiss_ElePhi_Nominal;
    Float_t         Etmiss_Ele_Nominal;
    Float_t         Etmiss_MuonPhi_Nominal;
    Float_t         Etmiss_Muon_Nominal;
    Float_t         Etmiss_TauPhi_Nominal;
    Float_t         Etmiss_Tau_Nominal;
    Bool_t          HLT_e120_lhloose;
    std::vector<bool>    *HLT_e120_lhloose_match_Nominal;
    Bool_t          HLT_e24_lhmedium_L1EM20VH;
    std::vector<bool>    *HLT_e24_lhmedium_L1EM20VH_match_Nominal;
    Bool_t          HLT_e60_lhmedium;
    std::vector<bool>    *HLT_e60_lhmedium_match_Nominal;
    Bool_t          HLT_mu20_iloose_L1MU15;
    std::vector<bool>    *HLT_mu20_iloose_L1MU15_match_Nominal;
    Bool_t          HLT_e140_lhloose_nod0;
    std::vector<bool>    *HLT_e140_lhloose_nod0_match_Nominal;
    Bool_t          HLT_e26_lhtight_nod0_ivarloose;
    std::vector<bool>    *HLT_e26_lhtight_nod0_ivarloose_match_Nominal;
    Bool_t          HLT_e60_lhmedium_nod0;
    std::vector<bool>    *HLT_e60_lhmedium_nod0_match_Nominal;
    Bool_t          HLT_mu26_ivarmedium;
    std::vector<bool>    *HLT_mu26_ivarmedium_match_Nominal;
    Bool_t          HLT_mu50;
    std::vector<bool>    *HLT_mu50_match_Nominal;
    Bool_t          HLT_e140_lhloose_L1EM22VHI;
    std::vector<bool>    *HLT_e140_lhloose_L1EM22VHI_match_Nominal;
    Bool_t          HLT_e26_lhtight_ivarloose_L1EM22VHI;
    std::vector<bool>    *HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;
    Bool_t          HLT_e60_lhmedium_L1EM22VHI;
    std::vector<bool>    *HLT_e60_lhmedium_L1EM22VHI_match_Nominal;
    Bool_t          HLT_mu24_ivarmedium_L1MU14FCH;
    std::vector<bool>    *HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;
    Bool_t          HLT_mu50_L1MU14FCH;
    std::vector<bool>    *HLT_mu50_L1MU14FCH_match_Nominal;

    // List of branches
    TBranch        *b_EventNumber;   //!
    TBranch        *b_RunNumber;   //!
    TBranch        *b_LumiBlock;   //!
    TBranch        *b_GenFiltMET;   //!
    TBranch        *b_GenFiltHTinclNu;   //!
    TBranch        *b_GenFiltPTZ;   //!
    TBranch        *b_GenFiltFatJ;   //!
    TBranch        *b_GenFiltHT;   //!
    TBranch        *b_xsec;   //!
    TBranch        *b_subprocess;   //!
    TBranch *b_mcChannel;                                                  //!
    TBranch *b_Weight_mc;                                                  //!
    TBranch *b_fb_nBaseJet;                                                //! 
    TBranch *b_fb_Mtt_reco;                                                //! 
    TBranch *b_fb_dEtatt;                                                //! 
    TBranch        *b_WeightEvents;   //!
    TBranch        *b_WeightEventsPU_Nominal;   //!
    TBranch        *b_WeightEventselSF_Nominal;   //!
    TBranch        *b_WeightEventsmuSF_Nominal;   //!
    TBranch        *b_WeightEventstauSF_Nominal;   //!
    TBranch        *b_WeightEventsSF_global_Nominal;   //!
    TBranch        *b_WeightEvents_trigger_ele_single_Nominal;   //!
    TBranch        *b_WeightEvents_trigger_mu_single_Nominal;   //!
    TBranch        *b_WeightEvents_trigger_global_Nominal;   //!
    TBranch        *b_WeightEventsbTag_Nominal;   //!
    TBranch        *b_WeightEventsJVT_Nominal;   //!
    TBranch        *b_WeightEventsfJVT_Nominal;   //!
    TBranch        *b_AverageInteractionsPerCrossing;   //!
    TBranch        *b_corrAverageInteractionsPerCrossing;   //!
    TBranch        *b_corrActualInteractionsPerCrossing;   //!
    TBranch        *b_NumPrimaryVertices;   //!
    TBranch        *b_passMETtrig;   //!
    TBranch        *b_TruthPDGID1;   //!
    TBranch        *b_TruthPDGID2;   //!
    TBranch        *b_TruthPDFID1;   //!
    TBranch        *b_TruthPDFID2;   //!
    TBranch        *b_TruthX1;   //!
    TBranch        *b_TruthX2;   //!
    TBranch        *b_TruthXF1;   //!
    TBranch        *b_TruthXF2;   //!
    TBranch        *b_TruthQ;   //!
    TBranch        *b_labeljets_reco_Nominal;   //!
    TBranch        *b_ptjets_Nominal;   //!
    TBranch        *b_etajets_Nominal;   //!
    TBranch        *b_phijets_Nominal;   //!
    TBranch        *b_massjets_Nominal;   //!
    TBranch        *b_isBjets_Nominal;   //!
    TBranch        *b_btag_weight_Nominal;   //!
    TBranch        *b_btag_pu_Nominal;   //!
    TBranch        *b_btag_pb_Nominal;   //!
    TBranch        *b_btag_pc_Nominal;   //!
    TBranch        *b_btag_ptau_Nominal;   //!
    TBranch        *b_JVTjets_Nominal;   //!
    TBranch        *b_passORjet_Nominal;   //!
    TBranch        *b_pttau_Nominal;   //!
    TBranch        *b_etatau_Nominal;   //!
    TBranch        *b_phitau_Nominal;   //!
    TBranch        *b_Etau_Nominal;   //!
    TBranch        *b_passORtau_Nominal;   //!
    TBranch        *b_signaltau_Nominal;   //!
    TBranch        *b_ntrkstau_Nominal;   //!
    TBranch        *b_chargtau_Nominal;   //!
    TBranch        *b_RNNJetScoretau_Nominal;   //!
    TBranch        *b_RNNJetScoreSigTranstau_Nominal;   //!
    TBranch        *b_qualitytau_Nominal;   //!
    TBranch        *b_evetotau_Nominal;   //!
    TBranch        *b_RNNEleScoretau_Nominal;   //!
    TBranch        *b_IsTruthMatchedtau_Nominal;   //!
    TBranch        *b_Origintau_Nominal;   //!
    TBranch        *b_Typetau_Nominal;   //!
    TBranch        *b_IsHadronictau_Nominal;   //!
    TBranch        *b_ptleptons_Nominal;   //!
    TBranch        *b_etaleptons_Nominal;   //!
    TBranch        *b_phileptons_Nominal;   //!
    TBranch        *b_massleptons_Nominal;   //!
    TBranch        *b_flavlep_Nominal;   //!
    TBranch        *b_Originlep_Nominal;   //!
    TBranch        *b_Typelep_Nominal;   //!
    TBranch        *b_IFFTypelep_Nominal;   //!
    TBranch        *b_passORlep_Nominal;   //!
    TBranch        *b_isSignallep_Nominal;   //!
    TBranch        *b_isMediumlep_Nominal;   //!
    TBranch        *b_isTightlep_Nominal;   //!
    TBranch        *b_d0sig_Nominal;   //!
    TBranch        *b_z0sinTheta_Nominal;   //!
    TBranch        *b_cleaningVeto_Nominal;   //!
    TBranch        *b_EtMiss_tstPhi_Nominal;   //!
    TBranch        *b_EtMiss_tst_Nominal;   //!
    TBranch        *b_EtMiss_SigObj_Nominal;   //!
    TBranch        *b_Etmiss_PVSoftTrkPhi_Nominal;   //!
    TBranch        *b_Etmiss_PVSoftTrk_Nominal;   //!
    TBranch        *b_Etmiss_JetPhi_Nominal;   //!
    TBranch        *b_Etmiss_Jet_Nominal;   //!
    TBranch        *b_Etmiss_ElePhi_Nominal;   //!
    TBranch        *b_Etmiss_Ele_Nominal;   //!
    TBranch        *b_Etmiss_MuonPhi_Nominal;   //!
    TBranch        *b_Etmiss_Muon_Nominal;   //!
    TBranch        *b_Etmiss_TauPhi_Nominal;   //!
    TBranch        *b_Etmiss_Tau_Nominal;   //!
    TBranch        *b_HLT_e120_lhloose;   //!
    TBranch        *b_HLT_e120_lhloose_match_Nominal;   //!
    TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
    TBranch        *b_HLT_e24_lhmedium_L1EM20VH_match_Nominal;   //!
    TBranch        *b_HLT_e60_lhmedium;   //!
    TBranch        *b_HLT_e60_lhmedium_match_Nominal;   //!
    TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
    TBranch        *b_HLT_mu20_iloose_L1MU15_match_Nominal;   //!
    TBranch        *b_HLT_e140_lhloose_nod0;   //!
    TBranch        *b_HLT_e140_lhloose_nod0_match_Nominal;   //!
    TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
    TBranch        *b_HLT_e26_lhtight_nod0_ivarloose_match_Nominal;   //!
    TBranch        *b_HLT_e60_lhmedium_nod0;   //!
    TBranch        *b_HLT_e60_lhmedium_nod0_match_Nominal;   //!
    TBranch        *b_HLT_mu26_ivarmedium;   //!
    TBranch        *b_HLT_mu26_ivarmedium_match_Nominal;   //!
    TBranch        *b_HLT_mu50;   //!
    TBranch        *b_HLT_mu50_match_Nominal;   //!
    TBranch        *b_HLT_e140_lhloose_L1EM22VHI;   //!
    TBranch        *b_HLT_e140_lhloose_L1EM22VHI_match_Nominal;   //!
    TBranch        *b_HLT_e26_lhtight_ivarloose_L1EM22VHI;   //!
    TBranch        *b_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal;   //!
    TBranch        *b_HLT_e60_lhmedium_L1EM22VHI;   //!
    TBranch        *b_HLT_e60_lhmedium_L1EM22VHI_match_Nominal;   //!
    TBranch        *b_HLT_mu24_ivarmedium_L1MU14FCH;   //!
    TBranch        *b_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal;   //!
    TBranch        *b_HLT_mu50_L1MU14FCH;   //!
    TBranch        *b_HLT_mu50_L1MU14FCH_match_Nominal;   //!

    void Init(TChain *tree) override {
        std::lock_guard<std::mutex> lock(_mutex);
        // The Init() function is called when the selector needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).

        // Set object pointer
        labeljets_reco_Nominal = 0;
        ptjets_Nominal = 0;
        etajets_Nominal = 0;
        phijets_Nominal = 0;
        massjets_Nominal = 0;
        isBjets_Nominal = 0;
        btag_weight_Nominal = 0;
        btag_pu_Nominal = 0;
        btag_pb_Nominal = 0;
        btag_pc_Nominal = 0;
        btag_ptau_Nominal = 0;
        JVTjets_Nominal = 0;
        passORjet_Nominal = 0;
        pttau_Nominal = 0;
        etatau_Nominal = 0;
        phitau_Nominal = 0;
        Etau_Nominal = 0;
        passORtau_Nominal = 0;
        signaltau_Nominal = 0;
        ntrkstau_Nominal = 0;
        chargtau_Nominal = 0;
        RNNJetScoretau_Nominal = 0;
        RNNJetScoreSigTranstau_Nominal = 0;
        qualitytau_Nominal = 0;
        evetotau_Nominal = 0;
        RNNEleScoretau_Nominal = 0;
        IsTruthMatchedtau_Nominal = 0;
        Origintau_Nominal = 0;
        Typetau_Nominal = 0;
        IsHadronictau_Nominal = 0;
        ptleptons_Nominal = 0;
        etaleptons_Nominal = 0;
        phileptons_Nominal = 0;
        massleptons_Nominal = 0;
        flavlep_Nominal = 0;
        Originlep_Nominal = 0;
        Typelep_Nominal = 0;
        IFFTypelep_Nominal = 0;
        passORlep_Nominal = 0;
        isSignallep_Nominal = 0;
        isMediumlep_Nominal = 0;
        isTightlep_Nominal = 0;
        d0sig_Nominal = 0;
        z0sinTheta_Nominal = 0;
        HLT_e120_lhloose_match_Nominal = 0;
        HLT_e24_lhmedium_L1EM20VH_match_Nominal = 0;
        HLT_e60_lhmedium_match_Nominal = 0;
        HLT_mu20_iloose_L1MU15_match_Nominal = 0;
        HLT_e140_lhloose_nod0_match_Nominal = 0;
        HLT_e26_lhtight_nod0_ivarloose_match_Nominal = 0;
        HLT_e60_lhmedium_nod0_match_Nominal = 0;
        HLT_mu26_ivarmedium_match_Nominal = 0;
        HLT_mu50_match_Nominal = 0;
        HLT_e140_lhloose_L1EM22VHI_match_Nominal = 0;
        HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal = 0;
        HLT_e60_lhmedium_L1EM22VHI_match_Nominal = 0;
        HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal = 0;
        HLT_mu50_L1MU14FCH_match_Nominal = 0;
        // Set branch addresses and branch pointers
        if (!tree) return;
        fChain = tree;

        fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
        fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
        fChain->SetBranchAddress("LumiBlockNumber", &LumiBlockNumber, &b_LumiBlock);
        fChain->SetBranchAddress("GenFiltMET", &GenFiltMET, &b_GenFiltMET);
        fChain->SetBranchAddress("GenFiltHTinclNu", &GenFiltHTinclNu, &b_GenFiltHTinclNu);
        fChain->SetBranchAddress("GenFiltPTZ", &GenFiltPTZ, &b_GenFiltPTZ);
        fChain->SetBranchAddress("GenFiltFatJ", &GenFiltFatJ, &b_GenFiltFatJ);
        fChain->SetBranchAddress("GenFiltHT", &GenFiltHT, &b_GenFiltHT);
        fChain->SetBranchAddress("xsec", &xsec, &b_xsec);
        fChain->SetBranchAddress("subprocess", &subprocess, &b_subprocess);
        fChain->SetBranchAddress("mcChannel", &mcChannel, &b_mcChannel);
        fChain->SetBranchAddress("Weight_mc", &Weight_mc, &b_Weight_mc);
        fChain->SetBranchAddress("fb_nBaseJet", &fb_nBaseJet, &b_fb_nBaseJet);
        fChain->SetBranchAddress("fb_Mtt_reco", &fb_Mtt_reco, &b_fb_Mtt_reco);
        fChain->SetBranchAddress("fb_dEtatt", &fb_dEtatt, &b_fb_dEtatt);
        fChain->SetBranchAddress("WeightEvents", &WeightEvents, &b_WeightEvents);
        fChain->SetBranchAddress("WeightEventsPU_Nominal", &WeightEventsPU_Nominal, &b_WeightEventsPU_Nominal);
        fChain->SetBranchAddress("WeightEventselSF_Nominal", &WeightEventselSF_Nominal, &b_WeightEventselSF_Nominal);
        fChain->SetBranchAddress("WeightEventsmuSF_Nominal", &WeightEventsmuSF_Nominal, &b_WeightEventsmuSF_Nominal);
        fChain->SetBranchAddress("WeightEventstauSF_Nominal", &WeightEventstauSF_Nominal, &b_WeightEventstauSF_Nominal);
        fChain->SetBranchAddress("WeightEventsSF_global_Nominal", &WeightEventsSF_global_Nominal, &b_WeightEventsSF_global_Nominal);
        fChain->SetBranchAddress("WeightEvents_trigger_ele_single_Nominal", &WeightEvents_trigger_ele_single_Nominal, &b_WeightEvents_trigger_ele_single_Nominal);
        fChain->SetBranchAddress("WeightEvents_trigger_mu_single_Nominal", &WeightEvents_trigger_mu_single_Nominal, &b_WeightEvents_trigger_mu_single_Nominal);
        fChain->SetBranchAddress("WeightEvents_trigger_global_Nominal", &WeightEvents_trigger_global_Nominal, &b_WeightEvents_trigger_global_Nominal);
        fChain->SetBranchAddress("WeightEventsbTag_Nominal", &WeightEventsbTag_Nominal, &b_WeightEventsbTag_Nominal);
        fChain->SetBranchAddress("WeightEventsJVT_Nominal", &WeightEventsJVT_Nominal, &b_WeightEventsJVT_Nominal);
        fChain->SetBranchAddress("WeightEventsfJVT_Nominal", &WeightEventsfJVT_Nominal, &b_WeightEventsfJVT_Nominal);
        fChain->SetBranchAddress("AverageInteractionsPerCrossing", &AverageInteractionsPerCrossing, &b_AverageInteractionsPerCrossing);
        fChain->SetBranchAddress("corrAverageInteractionsPerCrossing", &corrAverageInteractionsPerCrossing, &b_corrAverageInteractionsPerCrossing);
        fChain->SetBranchAddress("corrActualInteractionsPerCrossing", &corrActualInteractionsPerCrossing, &b_corrActualInteractionsPerCrossing);
        fChain->SetBranchAddress("NumPrimaryVertices", &NumPrimaryVertices, &b_NumPrimaryVertices);
        fChain->SetBranchAddress("passMETtrig", &passMETtrig, &b_passMETtrig);
        fChain->SetBranchAddress("TruthPDGID1", &TruthPDGID1, &b_TruthPDGID1);
        fChain->SetBranchAddress("TruthPDGID2", &TruthPDGID2, &b_TruthPDGID2);
        fChain->SetBranchAddress("TruthPDFID1", &TruthPDFID1, &b_TruthPDFID1);
        fChain->SetBranchAddress("TruthPDFID2", &TruthPDFID2, &b_TruthPDFID2);
        fChain->SetBranchAddress("TruthX1", &TruthX1, &b_TruthX1);
        fChain->SetBranchAddress("TruthX2", &TruthX2, &b_TruthX2);
        fChain->SetBranchAddress("TruthXF1", &TruthXF1, &b_TruthXF1);
        fChain->SetBranchAddress("TruthXF2", &TruthXF2, &b_TruthXF2);
        fChain->SetBranchAddress("TruthQ", &TruthQ, &b_TruthQ);
        fChain->SetBranchAddress("labeljets_reco_Nominal", &labeljets_reco_Nominal, &b_labeljets_reco_Nominal);
        fChain->SetBranchAddress("ptjets_Nominal", &ptjets_Nominal, &b_ptjets_Nominal);
        fChain->SetBranchAddress("etajets_Nominal", &etajets_Nominal, &b_etajets_Nominal);
        fChain->SetBranchAddress("phijets_Nominal", &phijets_Nominal, &b_phijets_Nominal);
        fChain->SetBranchAddress("massjets_Nominal", &massjets_Nominal, &b_massjets_Nominal);
        fChain->SetBranchAddress("isBjets_Nominal", &isBjets_Nominal, &b_isBjets_Nominal);
        fChain->SetBranchAddress("btag_weight_Nominal", &btag_weight_Nominal, &b_btag_weight_Nominal);
        fChain->SetBranchAddress("btag_pu_Nominal", &btag_pu_Nominal, &b_btag_pu_Nominal);
        fChain->SetBranchAddress("btag_pb_Nominal", &btag_pb_Nominal, &b_btag_pb_Nominal);
        fChain->SetBranchAddress("btag_pc_Nominal", &btag_pc_Nominal, &b_btag_pc_Nominal);
        fChain->SetBranchAddress("btag_ptau_Nominal", &btag_ptau_Nominal, &b_btag_ptau_Nominal);
        fChain->SetBranchAddress("JVTjets_Nominal", &JVTjets_Nominal, &b_JVTjets_Nominal);
        fChain->SetBranchAddress("passORjet_Nominal", &passORjet_Nominal, &b_passORjet_Nominal);
        fChain->SetBranchAddress("pttau_Nominal", &pttau_Nominal, &b_pttau_Nominal);
        fChain->SetBranchAddress("etatau_Nominal", &etatau_Nominal, &b_etatau_Nominal);
        fChain->SetBranchAddress("phitau_Nominal", &phitau_Nominal, &b_phitau_Nominal);
        fChain->SetBranchAddress("Etau_Nominal", &Etau_Nominal, &b_Etau_Nominal);
        fChain->SetBranchAddress("passORtau_Nominal", &passORtau_Nominal, &b_passORtau_Nominal);
        fChain->SetBranchAddress("signaltau_Nominal", &signaltau_Nominal, &b_signaltau_Nominal);
        fChain->SetBranchAddress("ntrkstau_Nominal", &ntrkstau_Nominal, &b_ntrkstau_Nominal);
        fChain->SetBranchAddress("chargtau_Nominal", &chargtau_Nominal, &b_chargtau_Nominal);
        fChain->SetBranchAddress("RNNJetScoretau_Nominal", &RNNJetScoretau_Nominal, &b_RNNJetScoretau_Nominal);
        fChain->SetBranchAddress("RNNJetScoreSigTranstau_Nominal", &RNNJetScoreSigTranstau_Nominal, &b_RNNJetScoreSigTranstau_Nominal);
        fChain->SetBranchAddress("qualitytau_Nominal", &qualitytau_Nominal, &b_qualitytau_Nominal);
        fChain->SetBranchAddress("evetotau_Nominal", &evetotau_Nominal, &b_evetotau_Nominal);
        fChain->SetBranchAddress("RNNEleScoretau_Nominal", &RNNEleScoretau_Nominal, &b_RNNEleScoretau_Nominal);
        fChain->SetBranchAddress("IsTruthMatchedtau_Nominal", &IsTruthMatchedtau_Nominal, &b_IsTruthMatchedtau_Nominal);
        fChain->SetBranchAddress("Origintau_Nominal", &Origintau_Nominal, &b_Origintau_Nominal);
        fChain->SetBranchAddress("Typetau_Nominal", &Typetau_Nominal, &b_Typetau_Nominal);
        fChain->SetBranchAddress("IsHadronictau_Nominal", &IsHadronictau_Nominal, &b_IsHadronictau_Nominal);
        fChain->SetBranchAddress("ptleptons_Nominal", &ptleptons_Nominal, &b_ptleptons_Nominal);
        fChain->SetBranchAddress("etaleptons_Nominal", &etaleptons_Nominal, &b_etaleptons_Nominal);
        fChain->SetBranchAddress("phileptons_Nominal", &phileptons_Nominal, &b_phileptons_Nominal);
        fChain->SetBranchAddress("massleptons_Nominal", &massleptons_Nominal, &b_massleptons_Nominal);
        fChain->SetBranchAddress("flavlep_Nominal", &flavlep_Nominal, &b_flavlep_Nominal);
        fChain->SetBranchAddress("Originlep_Nominal", &Originlep_Nominal, &b_Originlep_Nominal);
        fChain->SetBranchAddress("Typelep_Nominal", &Typelep_Nominal, &b_Typelep_Nominal);
        fChain->SetBranchAddress("IFFTypelep_Nominal", &IFFTypelep_Nominal, &b_IFFTypelep_Nominal);
        fChain->SetBranchAddress("passORlep_Nominal", &passORlep_Nominal, &b_passORlep_Nominal);
        fChain->SetBranchAddress("isSignallep_Nominal", &isSignallep_Nominal, &b_isSignallep_Nominal);
        fChain->SetBranchAddress("isMediumlep_Nominal", &isMediumlep_Nominal, &b_isMediumlep_Nominal);
        fChain->SetBranchAddress("isTightlep_Nominal", &isTightlep_Nominal, &b_isTightlep_Nominal);
        fChain->SetBranchAddress("d0sig_Nominal", &d0sig_Nominal, &b_d0sig_Nominal);
        fChain->SetBranchAddress("z0sinTheta_Nominal", &z0sinTheta_Nominal, &b_z0sinTheta_Nominal);
        fChain->SetBranchAddress("cleaningVeto_Nominal", &cleaningVeto_Nominal, &b_cleaningVeto_Nominal);
        fChain->SetBranchAddress("EtMiss_tstPhi_Nominal", &EtMiss_tstPhi_Nominal, &b_EtMiss_tstPhi_Nominal);
        fChain->SetBranchAddress("EtMiss_tst_Nominal", &EtMiss_tst_Nominal, &b_EtMiss_tst_Nominal);
        fChain->SetBranchAddress("EtMiss_SigObj_Nominal", &EtMiss_SigObj_Nominal, &b_EtMiss_SigObj_Nominal);
        fChain->SetBranchAddress("Etmiss_PVSoftTrkPhi_Nominal", &Etmiss_PVSoftTrkPhi_Nominal, &b_Etmiss_PVSoftTrkPhi_Nominal);
        fChain->SetBranchAddress("Etmiss_PVSoftTrk_Nominal", &Etmiss_PVSoftTrk_Nominal, &b_Etmiss_PVSoftTrk_Nominal);
        fChain->SetBranchAddress("Etmiss_JetPhi_Nominal", &Etmiss_JetPhi_Nominal, &b_Etmiss_JetPhi_Nominal);
        fChain->SetBranchAddress("Etmiss_Jet_Nominal", &Etmiss_Jet_Nominal, &b_Etmiss_Jet_Nominal);
        fChain->SetBranchAddress("Etmiss_ElePhi_Nominal", &Etmiss_ElePhi_Nominal, &b_Etmiss_ElePhi_Nominal);
        fChain->SetBranchAddress("Etmiss_Ele_Nominal", &Etmiss_Ele_Nominal, &b_Etmiss_Ele_Nominal);
        fChain->SetBranchAddress("Etmiss_MuonPhi_Nominal", &Etmiss_MuonPhi_Nominal, &b_Etmiss_MuonPhi_Nominal);
        fChain->SetBranchAddress("Etmiss_Muon_Nominal", &Etmiss_Muon_Nominal, &b_Etmiss_Muon_Nominal);
        fChain->SetBranchAddress("Etmiss_TauPhi_Nominal", &Etmiss_TauPhi_Nominal, &b_Etmiss_TauPhi_Nominal);
        fChain->SetBranchAddress("Etmiss_Tau_Nominal", &Etmiss_Tau_Nominal, &b_Etmiss_Tau_Nominal);
        fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
        fChain->SetBranchAddress("HLT_e120_lhloose_match_Nominal", &HLT_e120_lhloose_match_Nominal, &b_HLT_e120_lhloose_match_Nominal);
        fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
        fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH_match_Nominal", &HLT_e24_lhmedium_L1EM20VH_match_Nominal, &b_HLT_e24_lhmedium_L1EM20VH_match_Nominal);
        fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
        fChain->SetBranchAddress("HLT_e60_lhmedium_match_Nominal", &HLT_e60_lhmedium_match_Nominal, &b_HLT_e60_lhmedium_match_Nominal);
        fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
        fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15_match_Nominal", &HLT_mu20_iloose_L1MU15_match_Nominal, &b_HLT_mu20_iloose_L1MU15_match_Nominal);
        fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
        fChain->SetBranchAddress("HLT_e140_lhloose_nod0_match_Nominal", &HLT_e140_lhloose_nod0_match_Nominal, &b_HLT_e140_lhloose_nod0_match_Nominal);
        fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose_match_Nominal", &HLT_e26_lhtight_nod0_ivarloose_match_Nominal, &b_HLT_e26_lhtight_nod0_ivarloose_match_Nominal);
        fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
        fChain->SetBranchAddress("HLT_e60_lhmedium_nod0_match_Nominal", &HLT_e60_lhmedium_nod0_match_Nominal, &b_HLT_e60_lhmedium_nod0_match_Nominal);
        fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
        fChain->SetBranchAddress("HLT_mu26_ivarmedium_match_Nominal", &HLT_mu26_ivarmedium_match_Nominal, &b_HLT_mu26_ivarmedium_match_Nominal);
        fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
        fChain->SetBranchAddress("HLT_mu50_match_Nominal", &HLT_mu50_match_Nominal, &b_HLT_mu50_match_Nominal);
        fChain->SetBranchAddress("HLT_e140_lhloose_L1EM22VHI", &HLT_e140_lhloose_L1EM22VHI, &b_HLT_e140_lhloose_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e140_lhloose_L1EM22VHI_match_Nominal", &HLT_e140_lhloose_L1EM22VHI_match_Nominal, &b_HLT_e140_lhloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_L1EM22VHI", &HLT_e26_lhtight_ivarloose_L1EM22VHI, &b_HLT_e26_lhtight_ivarloose_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal", &HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal, &b_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("HLT_e60_lhmedium_L1EM22VHI", &HLT_e60_lhmedium_L1EM22VHI, &b_HLT_e60_lhmedium_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e60_lhmedium_L1EM22VHI_match_Nominal", &HLT_e60_lhmedium_L1EM22VHI_match_Nominal, &b_HLT_e60_lhmedium_L1EM22VHI_match_Nominal);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_L1MU14FCH", &HLT_mu24_ivarmedium_L1MU14FCH, &b_HLT_mu24_ivarmedium_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal", &HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal, &b_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal);
        fChain->SetBranchAddress("HLT_mu50_L1MU14FCH", &HLT_mu50_L1MU14FCH, &b_HLT_mu50_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu50_L1MU14FCH_match_Nominal", &HLT_mu50_L1MU14FCH_match_Nominal, &b_HLT_mu50_L1MU14FCH_match_Nominal);
    }
};
