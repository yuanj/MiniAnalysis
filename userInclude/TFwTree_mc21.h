#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

#include "AnaObjs.h"
#include "AnaTree.h"
#include "TLorentzVector.h"
#include "TVector3.h"

class TFwTree_mc21 : public AnaTree {
public:
    TFwTree_mc21(TChain *chain) : AnaTree(chain) { Init(this->fChain); }

    // Declaration of leaf types
    UInt_t HLT_e140_lhloose_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;
    UInt_t HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t HLT_e60_lhmedium_L1EM22VHI;
    UInt_t HLT_mu24_ivarmedium_L1MU14FCH;
    UInt_t HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;
    UInt_t HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;
    UInt_t HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;
    UInt_t HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t HLT_mu50_L1MU14FCH;
    Float_t NOMINAL_pileup_combined_weight;
    UInt_t NOMINAL_pileup_random_run_number;
    Double_t beamSpotWeight;
    UInt_t event_clean_EC_LooseBad;
    UInt_t event_clean_EC_TightBad;
    Int_t event_clean_detector_core;
    Int_t event_is_bad_batman;
    ULong64_t event_number;
    Float_t jet_NOMINAL_central_jets_global_effSF_JVT;
    Float_t jet_NOMINAL_central_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_forward_jets_global_effSF_JVT;
    Float_t jet_NOMINAL_forward_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_global_effSF_DL1dv01_FixedCutBEff_70;
    Float_t jet_NOMINAL_global_effSF_DL1dv01_FixedCutBEff_85;
    Float_t jet_NOMINAL_global_ineffSF_DL1dv01_FixedCutBEff_70;
    Float_t jet_NOMINAL_global_ineffSF_DL1dv01_FixedCutBEff_85;
    TLorentzVector *jet_nearest_tau_p4;
    std::vector<unsigned int> *jets;
    std::vector<float> *jets_Bottom_DL1_score;
    std::vector<float> *jets_Charm_DL1_score;
    std::vector<float> *jets_DL1dv01_FixedCutBEff_70_weight;
    std::vector<float> *jets_DL1dv01_FixedCutBEff_85_weight;
    std::vector<float> *jets_Light_DL1_score;
    std::vector<int> *jets_b_tag_quantile;
    std::vector<float> *jets_b_tag_score;
    std::vector<float> *jets_b_tagged_DL1dv01_FixedCutBEff_70;
    std::vector<float> *jets_b_tagged_DL1dv01_FixedCutBEff_85;
    std::vector<float> *jets_fjvt;
    std::vector<int> *jets_flavorlabel;
    std::vector<int> *jets_flavorlabel_cone;
    std::vector<int> *jets_flavorlabel_part;
    std::vector<int> *jets_is_Jvt_HS;
    std::vector<float> *jets_jvt;
    std::vector<int> *jets_origin;
    std::vector<TLorentzVector> *jets_p4;
    std::vector<float> *jets_q;
    std::vector<int> *jets_type;
    std::vector<float> *jets_width;
    UInt_t lep;
    Float_t lep_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;
    Float_t lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v15_isolLoose_VarRad;
    Float_t lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v15_isolTight_VarRad;
    Float_t lep_NOMINAL_EleEffSF_e26_lhtight_ivarloose_L1EM22VHI_OR_e60_lhmedium_L1EM22VHI_OR_e140_lhloose_L1EM22VHI_TightLLH_isolTight_VarRad_2022;
    Float_t lep_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v15;
    Float_t lep_NOMINAL_EleEffSF_offline_RecoTrk;
    Float_t lep_NOMINAL_EleEffSF_offline_TightLLH_d0z0_v15;
    Float_t lep_NOMINAL_MuEffSF_HLT_mu24_ivarmedium_L1MU14FCH_OR_HLT_mu50_L1MU14FCH_QualMedium;
    Float_t lep_NOMINAL_MuEffSF_IsoLoose_VarRad;
    Float_t lep_NOMINAL_MuEffSF_IsoTight_VarRad;
    Float_t lep_NOMINAL_MuEffSF_Reco_QualMedium;
    Float_t lep_NOMINAL_MuEffSF_TTVA;
    Float_t lep_cluster_eta;
    Float_t lep_cluster_eta_be2;
    UInt_t lep_elec_trigmatch_HLT_e140_lhloose_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t lep_elec_trigmatch_HLT_e60_lhmedium_L1EM22VHI;
    UInt_t lep_elec_trigmatch_trigger_matched;
    Int_t lep_id_bad;
    Int_t lep_id_charge;
    Int_t lep_id_loose;
    Int_t lep_id_medium;
    Int_t lep_id_tight;
    Int_t lep_id_veryloose;
    UInt_t lep_iso_HighPtCaloOnly;
    UInt_t lep_iso_Loose_VarRad;
    UInt_t lep_iso_PflowLoose_VarRad;
    UInt_t lep_iso_PflowTight_VarRad;
    UInt_t lep_iso_TightCaloOnly;
    UInt_t lep_iso_TightTrackOnly_FixedRad;
    UInt_t lep_iso_TightTrackOnly_VarRad;
    UInt_t lep_iso_Tight_VarRad;
    UInt_t lep_matched;
    Int_t lep_matched_classifierParticleOrigin;
    Int_t lep_matched_classifierParticleType;
    Int_t lep_matched_isHad;
    Int_t lep_matched_mother_pdgId;
    Int_t lep_matched_mother_status;
    Int_t lep_matched_origin;
    TLorentzVector *lep_matched_p4;
    Int_t lep_matched_pdgId;
    Float_t lep_matched_q;
    Int_t lep_matched_status;
    Int_t lep_matched_type;
    Int_t lep_muonAuthor;
    Int_t lep_muonType;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t lep_muon_trigmatch_HLT_mu50_L1MU14FCH;
    UInt_t lep_muon_trigmatch_trigger_matched;
    Int_t lep_origin;
    TLorentzVector *lep_p4;
    Float_t lep_q;
    Float_t lep_trk_d0;
    Float_t lep_trk_d0_sig;
    TLorentzVector *lep_trk_p4;
    Float_t lep_trk_pt_error;
    Float_t lep_trk_pvx_z0;
    Float_t lep_trk_pvx_z0_sig;
    Float_t lep_trk_pvx_z0_sintheta;
    Int_t lep_trk_vx;
    TVector3 *lep_trk_vx_v;
    Float_t lep_trk_z0;
    Float_t lep_trk_z0_sig;
    Float_t lep_trk_z0_sintheta;
    UInt_t lep_truth_classification;
    Int_t lep_type;
    Int_t lephad;
    Float_t lephad_CP_alphaminus_ip;
    Float_t lephad_CP_alphaminus_ip_rho;
    Float_t lephad_CP_alphaminus_rho_rho;
    Float_t lephad_CP_ip_tau0_mag;
    Float_t lephad_CP_ip_tau0_x_ip;
    Float_t lephad_CP_ip_tau0_y_ip;
    Float_t lephad_CP_ip_tau0_z_ip;
    Float_t lephad_CP_ip_tau1_mag;
    Float_t lephad_CP_ip_tau1_x_ip;
    Float_t lephad_CP_ip_tau1_y_ip;
    Float_t lephad_CP_ip_tau1_z_ip;
    Float_t lephad_CP_phi_star_cp_a1_rho;
    Float_t lephad_CP_phi_star_cp_ip_ip;
    Float_t lephad_CP_phi_star_cp_ip_rho;
    Float_t lephad_CP_phi_star_cp_ip_rho_opt;
    Float_t lephad_CP_phi_star_cp_rho_ip;
    Float_t lephad_CP_phi_star_cp_rho_rho;
    Float_t lephad_CP_tau0_upsilon;
    Float_t lephad_CP_tau1_upsilon;
    Float_t lephad_CP_upsilon_a1;
    Int_t lephad_coll_approx;
    Float_t lephad_coll_approx_m;
    Float_t lephad_coll_approx_x0;
    Float_t lephad_coll_approx_x1;
    Float_t lephad_cosalpha;
    Float_t lephad_deta;
    Float_t lephad_dphi;
    Float_t lephad_dpt;
    Float_t lephad_dr;
    Int_t lephad_met_bisect;
    Float_t lephad_met_centrality;
    Float_t lephad_met_lep0_cos_dphi;
    Float_t lephad_met_lep1_cos_dphi;
    Float_t lephad_met_min_dphi;
    Float_t lephad_met_sum_cos_dphi;
    Float_t lephad_mt_lep0_met;
    Float_t lephad_mt_lep1_met;
    TLorentzVector *lephad_p4;
    Float_t lephad_qxq;
    Float_t lephad_scal_sum_pt;
    UInt_t mc_channel_number;
    TLorentzVector *met_reco_p4;
    Float_t met_reco_sig;
    Float_t met_reco_sig_tracks;
    Float_t met_reco_sumet;
    Float_t min_dr_tau_jet;
    Float_t n_actual_int;
    Float_t n_actual_int_cor;
    Float_t n_avg_int;
    Float_t n_avg_int_cor;
    Int_t n_bjets_DL1dv01_FixedCutBEff_70;
    Int_t n_bjets_DL1dv01_FixedCutBEff_85;
    Int_t n_electrons;
    Int_t n_jets;
    Int_t n_jets_30;
    Int_t n_jets_40;
    Int_t n_jets_mc_hs;
    Int_t n_muons;
    Int_t n_muons_bad;
    Int_t n_muons_loose;
    Int_t n_muons_medium;
    Int_t n_muons_tight;
    Int_t n_muons_veryloose;
    Int_t n_photons;
    Int_t n_pvx;
    Int_t n_taus;
    Int_t n_taus_rnn_loose;
    Int_t n_taus_rnn_medium;
    Int_t n_taus_rnn_tight;
    Int_t n_taus_rnn_veryloose;
    UInt_t n_truth_gluon_jets;
    UInt_t n_truth_jets;
    UInt_t n_truth_jets_pt20_eta45;
    UInt_t n_truth_quark_jets;
    Int_t n_vx;
    Int_t primary_vertex;
    TVector3 *primary_vertex_v;
    UInt_t run_number;
    UInt_t tau;
    Float_t tau_NOMINAL_TauEffSF_JetRNNloose;
    Float_t tau_NOMINAL_TauEffSF_JetRNNmedium;
    Float_t tau_NOMINAL_TauEffSF_JetRNNtight;
    Float_t tau_NOMINAL_TauEffSF_LooseEleRNN_electron;
    Float_t tau_NOMINAL_TauEffSF_MediumEleRNN_electron;
    std::vector<float> *tau_charged_tracks_d0;
    std::vector<float> *tau_charged_tracks_d0_sig;
    std::vector<float> *tau_charged_tracks_eProbabilityHT;
    std::vector<int> *tau_charged_tracks_nInnermostPixelHits;
    std::vector<int> *tau_charged_tracks_nPixelHits;
    std::vector<int> *tau_charged_tracks_nSCTHits;
    std::vector<TLorentzVector> *tau_charged_tracks_p4;
    std::vector<float> *tau_charged_tracks_pt_err;
    std::vector<float> *tau_charged_tracks_rnn_chargedScore;
    std::vector<float> *tau_charged_tracks_rnn_conversionScore;
    std::vector<float> *tau_charged_tracks_rnn_isolationScore;
    std::vector<float> *tau_charged_tracks_z0;
    std::vector<float> *tau_charged_tracks_z0_sig;
    std::vector<float> *tau_charged_tracks_z0_sintheta;
    std::vector<float> *tau_charged_tracks_z0_sintheta_tjva;
    Int_t tau_ele_rnn_loose;
    Int_t tau_ele_rnn_medium;
    Float_t tau_ele_rnn_score;
    Float_t tau_ele_rnn_score_trans;
    Int_t tau_ele_rnn_tight;
    UInt_t tau_jet_deepset_loose;
    UInt_t tau_jet_deepset_medium;
    Float_t tau_jet_deepset_score;
    Float_t tau_jet_deepset_score_trans;
    UInt_t tau_jet_deepset_tight;
    UInt_t tau_jet_deepset_veryloose;
    UInt_t tau_jet_rnn_loose;
    UInt_t tau_jet_rnn_medium;
    Float_t tau_jet_rnn_score;
    Float_t tau_jet_rnn_score_trans;
    UInt_t tau_jet_rnn_tight;
    UInt_t tau_jet_rnn_veryloose;
    Float_t tau_jet_width;
    UInt_t tau_matched;
    Int_t tau_matched_classifierParticleOrigin;
    Int_t tau_matched_classifierParticleType;
    Int_t tau_matched_decayVertex;
    TVector3 *tau_matched_decayVertex_v;
    Int_t tau_matched_decay_mode;
    UInt_t tau_matched_isEle;
    UInt_t tau_matched_isHadTau;
    UInt_t tau_matched_isJet;
    UInt_t tau_matched_isMuon;
    UInt_t tau_matched_isTau;
    UInt_t tau_matched_isTruthMatch;
    Int_t tau_matched_mother_pdgId;
    Int_t tau_matched_mother_status;
    Int_t tau_matched_n_charged;
    Int_t tau_matched_n_charged_pion;
    Int_t tau_matched_n_neutral;
    Int_t tau_matched_n_neutral_pion;
    Int_t tau_matched_origin;
    TLorentzVector *tau_matched_p4;
    TLorentzVector *tau_matched_p4_vis_charged_track0;
    TLorentzVector *tau_matched_p4_vis_charged_track1;
    TLorentzVector *tau_matched_p4_vis_charged_track2;
    Int_t tau_matched_pdgId;
    Int_t tau_matched_productionVertex;
    TVector3 *tau_matched_productionVertex_v;
    Float_t tau_matched_pz;
    Float_t tau_matched_q;
    Int_t tau_matched_status;
    Int_t tau_matched_type;
    TLorentzVector *tau_matched_vis_charged_p4;
    TLorentzVector *tau_matched_vis_neutral_others_p4;
    TLorentzVector *tau_matched_vis_neutral_p4;
    TLorentzVector *tau_matched_vis_neutral_pions_p4;
    TLorentzVector *tau_matched_vis_p4;
    UInt_t tau_n_all_tracks;
    UInt_t tau_n_charged_tracks;
    UInt_t tau_n_conversion_tracks;
    UInt_t tau_n_fake_tracks;
    UInt_t tau_n_isolation_tracks;
    UInt_t tau_n_unclassified_tracks;
    std::vector<TLorentzVector> *tau_neutral_pfos_p4;
    UInt_t tau_nn_decay_mode;
    Float_t tau_nn_decay_mode_prob_1p0n;
    Float_t tau_nn_decay_mode_prob_1p1n;
    Float_t tau_nn_decay_mode_prob_1pXn;
    Float_t tau_nn_decay_mode_prob_3p0n;
    Int_t tau_origin;
    TLorentzVector *tau_p4;
    UInt_t tau_pantau_decay_mode;
    Int_t tau_pass_tst_muonolr;
    std::vector<TLorentzVector> *tau_pi0s_p4;
    Int_t tau_primary_vertex;
    TVector3 *tau_primary_vertex_v;
    Float_t tau_q;
    Int_t tau_secondary_vertex;
    TVector3 *tau_secondary_vertex_v;
    Double_t tau_tes_alpha_pt_shift;
    UInt_t tau_trigmatch_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t tau_trigmatch_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;
    UInt_t tau_trigmatch_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t tau_trigmatch_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t tau_trigmatch_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t tau_trigmatch_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t tau_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;
    UInt_t tau_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t tau_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;
    UInt_t tau_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;
    UInt_t tau_trigmatch_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t tau_trigmatch_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;
    UInt_t tau_trigmatch_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t tau_trigmatch_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t tau_trigmatch_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t tau_trigmatch_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t tau_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;
    UInt_t tau_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t tau_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;
    UInt_t tau_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;
    UInt_t tau_trigmatch_trigger_matched;
    Int_t tau_type;
    Float_t theory_weights_MUR1_MUF1_PDF303200_ASSEW;
    Float_t theory_weights_MUR1_MUF1_PDF303200_EXPASSEW;
    Float_t theory_weights_MUR1_MUF1_PDF303200_MULTIASSEW;
    Double_t weight_mc;
    Double_t Weight_mc;

    // List of branches
    TBranch *b_HLT_e140_lhloose_L1EM22VHI;                                                    //!
    TBranch *b_HLT_e26_lhtight_ivarloose_L1EM22VHI;                                           //!
    TBranch *b_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI; //!
    TBranch *b_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI; //!
    TBranch *b_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;  //!
    TBranch *b_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;  //!
    TBranch *b_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;  //!
    TBranch *b_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;  //!
    TBranch *b_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;  //!
    TBranch *b_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;  //!
    TBranch *b_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;  //!
    TBranch *b_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;  //!
    TBranch *b_HLT_e60_lhmedium_L1EM22VHI;                                                    //!
    TBranch *b_HLT_mu24_ivarmedium_L1MU14FCH;                                                 //!
    TBranch *b_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;       //!
    TBranch *b_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;       //!
    TBranch *b_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;        //!
    TBranch *b_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;        //!
    TBranch *b_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;        //!
    TBranch *b_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;        //!
    TBranch *b_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;        //!
    TBranch *b_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;        //!
    TBranch *b_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;        //!
    TBranch *b_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;        //!
    TBranch *b_HLT_mu50_L1MU14FCH;                                                            //!
    TBranch *b_NOMINAL_pileup_combined_weight;                                                //!
    TBranch *b_NOMINAL_pileup_random_run_number;                                              //!
    TBranch *b_beamSpotWeight;                                                                //!
    TBranch *b_event_clean_EC_LooseBad;                                                       //!
    TBranch *b_event_clean_EC_TightBad;                                                       //!
    TBranch *b_event_clean_detector_core;                                                     //!
    TBranch *b_event_is_bad_batman;                                                           //!
    TBranch *b_event_number;                                                                  //!
    TBranch *b_jet_NOMINAL_central_jets_global_effSF_JVT;                                     //!
    TBranch *b_jet_NOMINAL_central_jets_global_ineffSF_JVT;                                   //!
    TBranch *b_jet_NOMINAL_forward_jets_global_effSF_JVT;                                     //!
    TBranch *b_jet_NOMINAL_forward_jets_global_ineffSF_JVT;                                   //!
    TBranch *b_jet_NOMINAL_global_effSF_DL1dv01_FixedCutBEff_70;                              //!
    TBranch *b_jet_NOMINAL_global_effSF_DL1dv01_FixedCutBEff_85;                              //!
    TBranch *b_jet_NOMINAL_global_ineffSF_DL1dv01_FixedCutBEff_70;                            //!
    TBranch *b_jet_NOMINAL_global_ineffSF_DL1dv01_FixedCutBEff_85;                            //!
    TBranch *b_jet_nearest_tau_p4;                                                            //!
    TBranch *b_jets;                                                                          //!
    TBranch *b_jets_Bottom_DL1_score;                                                         //!
    TBranch *b_jets_Charm_DL1_score;                                                          //!
    TBranch *b_jets_DL1dv01_FixedCutBEff_70_weight;                                           //!
    TBranch *b_jets_DL1dv01_FixedCutBEff_85_weight;                                           //!
    TBranch *b_jets_Light_DL1_score;                                                          //!
    TBranch *b_jets_b_tag_quantile;                                                           //!
    TBranch *b_jets_b_tag_score;                                                              //!
    TBranch *b_jets_b_tagged_DL1dv01_FixedCutBEff_70;                                         //!
    TBranch *b_jets_b_tagged_DL1dv01_FixedCutBEff_85;                                         //!
    TBranch *b_jets_fjvt;                                                                     //!
    TBranch *b_jets_flavorlabel;                                                              //!
    TBranch *b_jets_flavorlabel_cone;                                                         //!
    TBranch *b_jets_flavorlabel_part;                                                         //!
    TBranch *b_jets_is_Jvt_HS;                                                                //!
    TBranch *b_jets_jvt;                                                                      //!
    TBranch *b_jets_origin;                                                                   //!
    TBranch *b_jets_p4;                                                                       //!
    TBranch *b_jets_q;                                                                        //!
    TBranch *b_jets_type;                                                                     //!
    TBranch *b_jets_width;                                                                    //!
    TBranch *b_lep;                                                                           //!
    TBranch *b_lep_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;                      //!
    TBranch *b_lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v15_isolLoose_VarRad;            //!
    TBranch *b_lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v15_isolTight_VarRad;            //!
    TBranch *
        b_lep_NOMINAL_EleEffSF_e26_lhtight_ivarloose_L1EM22VHI_OR_e60_lhmedium_L1EM22VHI_OR_e140_lhloose_L1EM22VHI_TightLLH_isolTight_VarRad_2022; //!
    TBranch *b_lep_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v15;                                                                                    //!
    TBranch *b_lep_NOMINAL_EleEffSF_offline_RecoTrk;                                                                                               //!
    TBranch *b_lep_NOMINAL_EleEffSF_offline_TightLLH_d0z0_v15;                                                                                     //!
    TBranch *b_lep_NOMINAL_MuEffSF_HLT_mu24_ivarmedium_L1MU14FCH_OR_HLT_mu50_L1MU14FCH_QualMedium;                                                 //!
    TBranch *b_lep_NOMINAL_MuEffSF_IsoLoose_VarRad;                                                                                                //!
    TBranch *b_lep_NOMINAL_MuEffSF_IsoTight_VarRad;                                                                                                //!
    TBranch *b_lep_NOMINAL_MuEffSF_Reco_QualMedium;                                                                                                //!
    TBranch *b_lep_NOMINAL_MuEffSF_TTVA;                                                                                                           //!
    TBranch *b_lep_cluster_eta;                                                                                                                    //!
    TBranch *b_lep_cluster_eta_be2;                                                                                                                //!
    TBranch *b_lep_elec_trigmatch_HLT_e140_lhloose_L1EM22VHI;                                                                                      //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_L1EM22VHI;                                                                             //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                   //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;                                   //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                    //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                    //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                    //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                    //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;                                    //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                    //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;                                    //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                    //!
    TBranch *b_lep_elec_trigmatch_HLT_e60_lhmedium_L1EM22VHI;                                                                                      //!
    TBranch *b_lep_elec_trigmatch_trigger_matched;                                                                                                 //!
    TBranch *b_lep_id_bad;                                                                                                                         //!
    TBranch *b_lep_id_charge;                                                                                                                      //!
    TBranch *b_lep_id_loose;                                                                                                                       //!
    TBranch *b_lep_id_medium;                                                                                                                      //!
    TBranch *b_lep_id_tight;                                                                                                                       //!
    TBranch *b_lep_id_veryloose;                                                                                                                   //!
    TBranch *b_lep_iso_HighPtCaloOnly;                                                                                                             //!
    TBranch *b_lep_iso_Loose_VarRad;                                                                                                               //!
    TBranch *b_lep_iso_PflowLoose_VarRad;                                                                                                          //!
    TBranch *b_lep_iso_PflowTight_VarRad;                                                                                                          //!
    TBranch *b_lep_iso_TightCaloOnly;                                                                                                              //!
    TBranch *b_lep_iso_TightTrackOnly_FixedRad;                                                                                                    //!
    TBranch *b_lep_iso_TightTrackOnly_VarRad;                                                                                                      //!
    TBranch *b_lep_iso_Tight_VarRad;                                                                                                               //!
    TBranch *b_lep_matched;                                                                                                                        //!
    TBranch *b_lep_matched_classifierParticleOrigin;                                                                                               //!
    TBranch *b_lep_matched_classifierParticleType;                                                                                                 //!
    TBranch *b_lep_matched_isHad;                                                                                                                  //!
    TBranch *b_lep_matched_mother_pdgId;                                                                                                           //!
    TBranch *b_lep_matched_mother_status;                                                                                                          //!
    TBranch *b_lep_matched_origin;                                                                                                                 //!
    TBranch *b_lep_matched_p4;                                                                                                                     //!
    TBranch *b_lep_matched_pdgId;                                                                                                                  //!
    TBranch *b_lep_matched_q;                                                                                                                      //!
    TBranch *b_lep_matched_status;                                                                                                                 //!
    TBranch *b_lep_matched_type;                                                                                                                   //!
    TBranch *b_lep_muonAuthor;                                                                                                                     //!
    TBranch *b_lep_muonType;                                                                                                                       //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_L1MU14FCH;                                                                                   //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                         //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;                                         //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                          //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                          //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                          //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                          //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;                                          //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                          //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;                                          //!
    TBranch *b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                          //!
    TBranch *b_lep_muon_trigmatch_HLT_mu50_L1MU14FCH;                                                                                              //!
    TBranch *b_lep_muon_trigmatch_trigger_matched;                                                                                                 //!
    TBranch *b_lep_origin;                                                                                                                         //!
    TBranch *b_lep_p4;                                                                                                                             //!
    TBranch *b_lep_q;                                                                                                                              //!
    TBranch *b_lep_trk_d0;                                                                                                                         //!
    TBranch *b_lep_trk_d0_sig;                                                                                                                     //!
    TBranch *b_lep_trk_p4;                                                                                                                         //!
    TBranch *b_lep_trk_pt_error;                                                                                                                   //!
    TBranch *b_lep_trk_pvx_z0;                                                                                                                     //!
    TBranch *b_lep_trk_pvx_z0_sig;                                                                                                                 //!
    TBranch *b_lep_trk_pvx_z0_sintheta;                                                                                                            //!
    TBranch *b_lep_trk_vx;                                                                                                                         //!
    TBranch *b_lep_trk_vx_v;                                                                                                                       //!
    TBranch *b_lep_trk_z0;                                                                                                                         //!
    TBranch *b_lep_trk_z0_sig;                                                                                                                     //!
    TBranch *b_lep_trk_z0_sintheta;                                                                                                                //!
    TBranch *b_lep_truth_classification;                                                                                                           //!
    TBranch *b_lep_type;                                                                                                                           //!
    TBranch *b_lephad;                                                                                                                             //!
    TBranch *b_lephad_CP_alphaminus_ip;                                                                                                            //!
    TBranch *b_lephad_CP_alphaminus_ip_rho;                                                                                                        //!
    TBranch *b_lephad_CP_alphaminus_rho_rho;                                                                                                       //!
    TBranch *b_lephad_CP_ip_tau0_mag;                                                                                                              //!
    TBranch *b_lephad_CP_ip_tau0_x_ip;                                                                                                             //!
    TBranch *b_lephad_CP_ip_tau0_y_ip;                                                                                                             //!
    TBranch *b_lephad_CP_ip_tau0_z_ip;                                                                                                             //!
    TBranch *b_lephad_CP_ip_tau1_mag;                                                                                                              //!
    TBranch *b_lephad_CP_ip_tau1_x_ip;                                                                                                             //!
    TBranch *b_lephad_CP_ip_tau1_y_ip;                                                                                                             //!
    TBranch *b_lephad_CP_ip_tau1_z_ip;                                                                                                             //!
    TBranch *b_lephad_CP_phi_star_cp_a1_rho;                                                                                                       //!
    TBranch *b_lephad_CP_phi_star_cp_ip_ip;                                                                                                        //!
    TBranch *b_lephad_CP_phi_star_cp_ip_rho;                                                                                                       //!
    TBranch *b_lephad_CP_phi_star_cp_ip_rho_opt;                                                                                                   //!
    TBranch *b_lephad_CP_phi_star_cp_rho_ip;                                                                                                       //!
    TBranch *b_lephad_CP_phi_star_cp_rho_rho;                                                                                                      //!
    TBranch *b_lephad_CP_tau0_upsilon;                                                                                                             //!
    TBranch *b_lephad_CP_tau1_upsilon;                                                                                                             //!
    TBranch *b_lephad_CP_upsilon_a1;                                                                                                               //!
    TBranch *b_lephad_coll_approx;                                                                                                                 //!
    TBranch *b_lephad_coll_approx_m;                                                                                                               //!
    TBranch *b_lephad_coll_approx_x0;                                                                                                              //!
    TBranch *b_lephad_coll_approx_x1;                                                                                                              //!
    TBranch *b_lephad_cosalpha;                                                                                                                    //!
    TBranch *b_lephad_deta;                                                                                                                        //!
    TBranch *b_lephad_dphi;                                                                                                                        //!
    TBranch *b_lephad_dpt;                                                                                                                         //!
    TBranch *b_lephad_dr;                                                                                                                          //!
    TBranch *b_lephad_met_bisect;                                                                                                                  //!
    TBranch *b_lephad_met_centrality;                                                                                                              //!
    TBranch *b_lephad_met_lep0_cos_dphi;                                                                                                           //!
    TBranch *b_lephad_met_lep1_cos_dphi;                                                                                                           //!
    TBranch *b_lephad_met_min_dphi;                                                                                                                //!
    TBranch *b_lephad_met_sum_cos_dphi;                                                                                                            //!
    TBranch *b_lephad_mt_lep0_met;                                                                                                                 //!
    TBranch *b_lephad_mt_lep1_met;                                                                                                                 //!
    TBranch *b_lephad_p4;                                                                                                                          //!
    TBranch *b_lephad_qxq;                                                                                                                         //!
    TBranch *b_lephad_scal_sum_pt;                                                                                                                 //!
    TBranch *b_mc_channel_number;                                                                                                                  //!
    TBranch *b_met_reco_p4;                                                                                                                        //!
    TBranch *b_met_reco_sig;                                                                                                                       //!
    TBranch *b_met_reco_sig_tracks;                                                                                                                //!
    TBranch *b_met_reco_sumet;                                                                                                                     //!
    TBranch *b_min_dr_tau_jet;                                                                                                                     //!
    TBranch *b_n_actual_int;                                                                                                                       //!
    TBranch *b_n_actual_int_cor;                                                                                                                   //!
    TBranch *b_n_avg_int;                                                                                                                          //!
    TBranch *b_n_avg_int_cor;                                                                                                                      //!
    TBranch *b_n_bjets_DL1dv01_FixedCutBEff_70;                                                                                                    //!
    TBranch *b_n_bjets_DL1dv01_FixedCutBEff_85;                                                                                                    //!
    TBranch *b_n_electrons;                                                                                                                        //!
    TBranch *b_n_jets;                                                                                                                             //!
    TBranch *b_n_jets_30;                                                                                                                          //!
    TBranch *b_n_jets_40;                                                                                                                          //!
    TBranch *b_n_jets_mc_hs;                                                                                                                       //!
    TBranch *b_n_muons;                                                                                                                            //!
    TBranch *b_n_muons_bad;                                                                                                                        //!
    TBranch *b_n_muons_loose;                                                                                                                      //!
    TBranch *b_n_muons_medium;                                                                                                                     //!
    TBranch *b_n_muons_tight;                                                                                                                      //!
    TBranch *b_n_muons_veryloose;                                                                                                                  //!
    TBranch *b_n_photons;                                                                                                                          //!
    TBranch *b_n_pvx;                                                                                                                              //!
    TBranch *b_n_taus;                                                                                                                             //!
    TBranch *b_n_taus_rnn_loose;                                                                                                                   //!
    TBranch *b_n_taus_rnn_medium;                                                                                                                  //!
    TBranch *b_n_taus_rnn_tight;                                                                                                                   //!
    TBranch *b_n_taus_rnn_veryloose;                                                                                                               //!
    TBranch *b_n_truth_gluon_jets;                                                                                                                 //!
    TBranch *b_n_truth_jets;                                                                                                                       //!
    TBranch *b_n_truth_jets_pt20_eta45;                                                                                                            //!
    TBranch *b_n_truth_quark_jets;                                                                                                                 //!
    TBranch *b_n_vx;                                                                                                                               //!
    TBranch *b_primary_vertex;                                                                                                                     //!
    TBranch *b_primary_vertex_v;                                                                                                                   //!
    TBranch *b_run_number;                                                                                                                         //!
    TBranch *b_tau;                                                                                                                                //!
    TBranch *b_tau_NOMINAL_TauEffSF_JetRNNloose;                                                                                                   //!
    TBranch *b_tau_NOMINAL_TauEffSF_JetRNNmedium;                                                                                                  //!
    TBranch *b_tau_NOMINAL_TauEffSF_JetRNNtight;                                                                                                   //!
    TBranch *b_tau_NOMINAL_TauEffSF_LooseEleRNN_electron;                                                                                          //!
    TBranch *b_tau_NOMINAL_TauEffSF_MediumEleRNN_electron;                                                                                         //!
    TBranch *b_tau_charged_tracks_d0;                                                                                                              //!
    TBranch *b_tau_charged_tracks_d0_sig;                                                                                                          //!
    TBranch *b_tau_charged_tracks_eProbabilityHT;                                                                                                  //!
    TBranch *b_tau_charged_tracks_nInnermostPixelHits;                                                                                             //!
    TBranch *b_tau_charged_tracks_nPixelHits;                                                                                                      //!
    TBranch *b_tau_charged_tracks_nSCTHits;                                                                                                        //!
    TBranch *b_tau_charged_tracks_p4;                                                                                                              //!
    TBranch *b_tau_charged_tracks_pt_err;                                                                                                          //!
    TBranch *b_tau_charged_tracks_rnn_chargedScore;                                                                                                //!
    TBranch *b_tau_charged_tracks_rnn_conversionScore;                                                                                             //!
    TBranch *b_tau_charged_tracks_rnn_isolationScore;                                                                                              //!
    TBranch *b_tau_charged_tracks_z0;                                                                                                              //!
    TBranch *b_tau_charged_tracks_z0_sig;                                                                                                          //!
    TBranch *b_tau_charged_tracks_z0_sintheta;                                                                                                     //!
    TBranch *b_tau_charged_tracks_z0_sintheta_tjva;                                                                                                //!
    TBranch *b_tau_ele_rnn_loose;                                                                                                                  //!
    TBranch *b_tau_ele_rnn_medium;                                                                                                                 //!
    TBranch *b_tau_ele_rnn_score;                                                                                                                  //!
    TBranch *b_tau_ele_rnn_score_trans;                                                                                                            //!
    TBranch *b_tau_ele_rnn_tight;                                                                                                                  //!
    TBranch *b_tau_jet_deepset_loose;                                                                                                              //!
    TBranch *b_tau_jet_deepset_medium;                                                                                                             //!
    TBranch *b_tau_jet_deepset_score;                                                                                                              //!
    TBranch *b_tau_jet_deepset_score_trans;                                                                                                        //!
    TBranch *b_tau_jet_deepset_tight;                                                                                                              //!
    TBranch *b_tau_jet_deepset_veryloose;                                                                                                          //!
    TBranch *b_tau_jet_rnn_loose;                                                                                                                  //!
    TBranch *b_tau_jet_rnn_medium;                                                                                                                 //!
    TBranch *b_tau_jet_rnn_score;                                                                                                                  //!
    TBranch *b_tau_jet_rnn_score_trans;                                                                                                            //!
    TBranch *b_tau_jet_rnn_tight;                                                                                                                  //!
    TBranch *b_tau_jet_rnn_veryloose;                                                                                                              //!
    TBranch *b_tau_jet_width;                                                                                                                      //!
    TBranch *b_tau_matched;                                                                                                                        //!
    TBranch *b_tau_matched_classifierParticleOrigin;                                                                                               //!
    TBranch *b_tau_matched_classifierParticleType;                                                                                                 //!
    TBranch *b_tau_matched_decayVertex;                                                                                                            //!
    TBranch *b_tau_matched_decayVertex_v;                                                                                                          //!
    TBranch *b_tau_matched_decay_mode;                                                                                                             //!
    TBranch *b_tau_matched_isEle;                                                                                                                  //!
    TBranch *b_tau_matched_isHadTau;                                                                                                               //!
    TBranch *b_tau_matched_isJet;                                                                                                                  //!
    TBranch *b_tau_matched_isMuon;                                                                                                                 //!
    TBranch *b_tau_matched_isTau;                                                                                                                  //!
    TBranch *b_tau_matched_isTruthMatch;                                                                                                           //!
    TBranch *b_tau_matched_mother_pdgId;                                                                                                           //!
    TBranch *b_tau_matched_mother_status;                                                                                                          //!
    TBranch *b_tau_matched_n_charged;                                                                                                              //!
    TBranch *b_tau_matched_n_charged_pion;                                                                                                         //!
    TBranch *b_tau_matched_n_neutral;                                                                                                              //!
    TBranch *b_tau_matched_n_neutral_pion;                                                                                                         //!
    TBranch *b_tau_matched_origin;                                                                                                                 //!
    TBranch *b_tau_matched_p4;                                                                                                                     //!
    TBranch *b_tau_matched_p4_vis_charged_track0;                                                                                                  //!
    TBranch *b_tau_matched_p4_vis_charged_track1;                                                                                                  //!
    TBranch *b_tau_matched_p4_vis_charged_track2;                                                                                                  //!
    TBranch *b_tau_matched_pdgId;                                                                                                                  //!
    TBranch *b_tau_matched_productionVertex;                                                                                                       //!
    TBranch *b_tau_matched_productionVertex_v;                                                                                                     //!
    TBranch *b_tau_matched_pz;                                                                                                                     //!
    TBranch *b_tau_matched_q;                                                                                                                      //!
    TBranch *b_tau_matched_status;                                                                                                                 //!
    TBranch *b_tau_matched_type;                                                                                                                   //!
    TBranch *b_tau_matched_vis_charged_p4;                                                                                                         //!
    TBranch *b_tau_matched_vis_neutral_others_p4;                                                                                                  //!
    TBranch *b_tau_matched_vis_neutral_p4;                                                                                                         //!
    TBranch *b_tau_matched_vis_neutral_pions_p4;                                                                                                   //!
    TBranch *b_tau_matched_vis_p4;                                                                                                                 //!
    TBranch *b_tau_n_all_tracks;                                                                                                                   //!
    TBranch *b_tau_n_charged_tracks;                                                                                                               //!
    TBranch *b_tau_n_conversion_tracks;                                                                                                            //!
    TBranch *b_tau_n_fake_tracks;                                                                                                                  //!
    TBranch *b_tau_n_isolation_tracks;                                                                                                             //!
    TBranch *b_tau_n_unclassified_tracks;                                                                                                          //!
    TBranch *b_tau_neutral_pfos_p4;                                                                                                                //!
    TBranch *b_tau_nn_decay_mode;                                                                                                                  //!
    TBranch *b_tau_nn_decay_mode_prob_1p0n;                                                                                                        //!
    TBranch *b_tau_nn_decay_mode_prob_1p1n;                                                                                                        //!
    TBranch *b_tau_nn_decay_mode_prob_1pXn;                                                                                                        //!
    TBranch *b_tau_nn_decay_mode_prob_3p0n;                                                                                                        //!
    TBranch *b_tau_origin;                                                                                                                         //!
    TBranch *b_tau_p4;                                                                                                                             //!
    TBranch *b_tau_pantau_decay_mode;                                                                                                              //!
    TBranch *b_tau_pass_tst_muonolr;                                                                                                               //!
    TBranch *b_tau_pi0s_p4;                                                                                                                        //!
    TBranch *b_tau_primary_vertex;                                                                                                                 //!
    TBranch *b_tau_primary_vertex_v;                                                                                                               //!
    TBranch *b_tau_q;                                                                                                                              //!
    TBranch *b_tau_secondary_vertex;                                                                                                               //!
    TBranch *b_tau_secondary_vertex_v;                                                                                                             //!
    TBranch *b_tau_tes_alpha_pt_shift;                                                                                                             //!
    TBranch *b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                        //!
    TBranch *b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;                                        //!
    TBranch *b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                         //!
    TBranch *b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                         //!
    TBranch *b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                         //!
    TBranch *b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                         //!
    TBranch *b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;                                         //!
    TBranch *b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                         //!
    TBranch *b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI;                                         //!
    TBranch *b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI;                                         //!
    TBranch *b_tau_trigmatch_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                              //!
    TBranch *b_tau_trigmatch_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;                                              //!
    TBranch *b_tau_trigmatch_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                               //!
    TBranch *b_tau_trigmatch_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                               //!
    TBranch *b_tau_trigmatch_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                               //!
    TBranch *b_tau_trigmatch_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                               //!
    TBranch *b_tau_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;                                               //!
    TBranch *b_tau_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                               //!
    TBranch *b_tau_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH;                                               //!
    TBranch *b_tau_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH;                                               //!
    TBranch *b_tau_trigmatch_trigger_matched;                                                                                                      //!
    TBranch *b_tau_type;                                                                                                                           //!
    TBranch *b_theory_weights_MUR1_MUF1_PDF303200_ASSEW;                                                                                           //!
    TBranch *b_theory_weights_MUR1_MUF1_PDF303200_EXPASSEW;                                                                                        //!
    TBranch *b_theory_weights_MUR1_MUF1_PDF303200_MULTIASSEW;                                                                                      //!
    TBranch *b_weight_mc;                                                                                                                          //!
    TBranch *b_Weight_mc;                                                                                                                          //!

    void Init(TChain *tree) override {
        // The Init() function is called when the selector needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).
        std::lock_guard<std::mutex> lock(_mutex);

        // Set object pointer
        jet_nearest_tau_p4 = 0;
        jets = 0;
        jets_Bottom_DL1_score = 0;
        jets_Charm_DL1_score = 0;
        jets_DL1dv01_FixedCutBEff_70_weight = 0;
        jets_DL1dv01_FixedCutBEff_85_weight = 0;
        jets_Light_DL1_score = 0;
        jets_b_tag_quantile = 0;
        jets_b_tag_score = 0;
        jets_b_tagged_DL1dv01_FixedCutBEff_70 = 0;
        jets_b_tagged_DL1dv01_FixedCutBEff_85 = 0;
        jets_fjvt = 0;
        jets_flavorlabel = 0;
        jets_flavorlabel_cone = 0;
        jets_flavorlabel_part = 0;
        jets_is_Jvt_HS = 0;
        jets_jvt = 0;
        jets_origin = 0;
        jets_p4 = 0;
        jets_q = 0;
        jets_type = 0;
        jets_width = 0;
        lep_matched_p4 = 0;
        lep_p4 = 0;
        lep_trk_p4 = 0;
        lep_trk_vx_v = 0;
        lephad_p4 = 0;
        met_reco_p4 = 0;
        primary_vertex_v = 0;
        tau_charged_tracks_d0 = 0;
        tau_charged_tracks_d0_sig = 0;
        tau_charged_tracks_eProbabilityHT = 0;
        tau_charged_tracks_nInnermostPixelHits = 0;
        tau_charged_tracks_nPixelHits = 0;
        tau_charged_tracks_nSCTHits = 0;
        tau_charged_tracks_p4 = 0;
        tau_charged_tracks_pt_err = 0;
        tau_charged_tracks_rnn_chargedScore = 0;
        tau_charged_tracks_rnn_conversionScore = 0;
        tau_charged_tracks_rnn_isolationScore = 0;
        tau_charged_tracks_z0 = 0;
        tau_charged_tracks_z0_sig = 0;
        tau_charged_tracks_z0_sintheta = 0;
        tau_charged_tracks_z0_sintheta_tjva = 0;
        tau_matched_decayVertex_v = 0;
        tau_matched_p4 = 0;
        tau_matched_p4_vis_charged_track0 = 0;
        tau_matched_p4_vis_charged_track1 = 0;
        tau_matched_p4_vis_charged_track2 = 0;
        tau_matched_productionVertex_v = 0;
        tau_matched_vis_charged_p4 = 0;
        tau_matched_vis_neutral_others_p4 = 0;
        tau_matched_vis_neutral_p4 = 0;
        tau_matched_vis_neutral_pions_p4 = 0;
        tau_matched_vis_p4 = 0;
        tau_neutral_pfos_p4 = 0;
        tau_p4 = 0;
        tau_pi0s_p4 = 0;
        tau_primary_vertex_v = 0;
        tau_secondary_vertex_v = 0;
        // Set branch addresses and branch pointers
        if (!tree) return;
        fChain = tree;
        fChain->SetMakeClass(1);

        fChain->SetBranchAddress("HLT_e140_lhloose_L1EM22VHI", &HLT_e140_lhloose_L1EM22VHI, &b_HLT_e140_lhloose_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_L1EM22VHI", &HLT_e26_lhtight_ivarloose_L1EM22VHI, &b_HLT_e26_lhtight_ivarloose_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI",
                                 &HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI,
                                 &b_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI",
                                 &HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI,
                                 &b_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI",
                                 &HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI,
                                 &b_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("HLT_e60_lhmedium_L1EM22VHI", &HLT_e60_lhmedium_L1EM22VHI, &b_HLT_e60_lhmedium_L1EM22VHI);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_L1MU14FCH", &HLT_mu24_ivarmedium_L1MU14FCH, &b_HLT_mu24_ivarmedium_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH",
                                 &HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH,
                                 &b_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH",
                                 &HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH,
                                 &b_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH",
                                 &HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH,
                                 &b_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("HLT_mu50_L1MU14FCH", &HLT_mu50_L1MU14FCH, &b_HLT_mu50_L1MU14FCH);
        fChain->SetBranchAddress("NOMINAL_pileup_combined_weight", &NOMINAL_pileup_combined_weight, &b_NOMINAL_pileup_combined_weight);
        fChain->SetBranchAddress("NOMINAL_pileup_random_run_number", &NOMINAL_pileup_random_run_number, &b_NOMINAL_pileup_random_run_number);
        fChain->SetBranchAddress("beamSpotWeight", &beamSpotWeight, &b_beamSpotWeight);
        fChain->SetBranchAddress("event_clean_EC_LooseBad", &event_clean_EC_LooseBad, &b_event_clean_EC_LooseBad);
        fChain->SetBranchAddress("event_clean_EC_TightBad", &event_clean_EC_TightBad, &b_event_clean_EC_TightBad);
        fChain->SetBranchAddress("event_clean_detector_core", &event_clean_detector_core, &b_event_clean_detector_core);
        fChain->SetBranchAddress("event_is_bad_batman", &event_is_bad_batman, &b_event_is_bad_batman);
        fChain->SetBranchAddress("event_number", &event_number, &b_event_number);
        fChain->SetBranchAddress("jet_NOMINAL_central_jets_global_effSF_JVT", &jet_NOMINAL_central_jets_global_effSF_JVT,
                                 &b_jet_NOMINAL_central_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_central_jets_global_ineffSF_JVT", &jet_NOMINAL_central_jets_global_ineffSF_JVT,
                                 &b_jet_NOMINAL_central_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_forward_jets_global_effSF_JVT", &jet_NOMINAL_forward_jets_global_effSF_JVT,
                                 &b_jet_NOMINAL_forward_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_forward_jets_global_ineffSF_JVT", &jet_NOMINAL_forward_jets_global_ineffSF_JVT,
                                 &b_jet_NOMINAL_forward_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_global_effSF_DL1dv01_FixedCutBEff_70", &jet_NOMINAL_global_effSF_DL1dv01_FixedCutBEff_70,
                                 &b_jet_NOMINAL_global_effSF_DL1dv01_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_NOMINAL_global_effSF_DL1dv01_FixedCutBEff_85", &jet_NOMINAL_global_effSF_DL1dv01_FixedCutBEff_85,
                                 &b_jet_NOMINAL_global_effSF_DL1dv01_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_NOMINAL_global_ineffSF_DL1dv01_FixedCutBEff_70", &jet_NOMINAL_global_ineffSF_DL1dv01_FixedCutBEff_70,
                                 &b_jet_NOMINAL_global_ineffSF_DL1dv01_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_NOMINAL_global_ineffSF_DL1dv01_FixedCutBEff_85", &jet_NOMINAL_global_ineffSF_DL1dv01_FixedCutBEff_85,
                                 &b_jet_NOMINAL_global_ineffSF_DL1dv01_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_nearest_tau_p4", &jet_nearest_tau_p4, &b_jet_nearest_tau_p4);
        fChain->SetBranchAddress("jets", &jets, &b_jets);
        fChain->SetBranchAddress("jets_Bottom_DL1_score", &jets_Bottom_DL1_score, &b_jets_Bottom_DL1_score);
        fChain->SetBranchAddress("jets_Charm_DL1_score", &jets_Charm_DL1_score, &b_jets_Charm_DL1_score);
        fChain->SetBranchAddress("jets_DL1dv01_FixedCutBEff_70_weight", &jets_DL1dv01_FixedCutBEff_70_weight, &b_jets_DL1dv01_FixedCutBEff_70_weight);
        fChain->SetBranchAddress("jets_DL1dv01_FixedCutBEff_85_weight", &jets_DL1dv01_FixedCutBEff_85_weight, &b_jets_DL1dv01_FixedCutBEff_85_weight);
        fChain->SetBranchAddress("jets_Light_DL1_score", &jets_Light_DL1_score, &b_jets_Light_DL1_score);
        fChain->SetBranchAddress("jets_b_tag_quantile", &jets_b_tag_quantile, &b_jets_b_tag_quantile);
        fChain->SetBranchAddress("jets_b_tag_score", &jets_b_tag_score, &b_jets_b_tag_score);
        fChain->SetBranchAddress("jets_b_tagged_DL1dv01_FixedCutBEff_70", &jets_b_tagged_DL1dv01_FixedCutBEff_70,
                                 &b_jets_b_tagged_DL1dv01_FixedCutBEff_70);
        fChain->SetBranchAddress("jets_b_tagged_DL1dv01_FixedCutBEff_85", &jets_b_tagged_DL1dv01_FixedCutBEff_85,
                                 &b_jets_b_tagged_DL1dv01_FixedCutBEff_85);
        fChain->SetBranchAddress("jets_fjvt", &jets_fjvt, &b_jets_fjvt);
        fChain->SetBranchAddress("jets_flavorlabel", &jets_flavorlabel, &b_jets_flavorlabel);
        fChain->SetBranchAddress("jets_flavorlabel_cone", &jets_flavorlabel_cone, &b_jets_flavorlabel_cone);
        fChain->SetBranchAddress("jets_flavorlabel_part", &jets_flavorlabel_part, &b_jets_flavorlabel_part);
        fChain->SetBranchAddress("jets_is_Jvt_HS", &jets_is_Jvt_HS, &b_jets_is_Jvt_HS);
        fChain->SetBranchAddress("jets_jvt", &jets_jvt, &b_jets_jvt);
        fChain->SetBranchAddress("jets_origin", &jets_origin, &b_jets_origin);
        fChain->SetBranchAddress("jets_p4", &jets_p4, &b_jets_p4);
        fChain->SetBranchAddress("jets_q", &jets_q, &b_jets_q);
        fChain->SetBranchAddress("jets_type", &jets_type, &b_jets_type);
        fChain->SetBranchAddress("jets_width", &jets_width, &b_jets_width);
        fChain->SetBranchAddress("lep", &lep, &b_lep);
        fChain->SetBranchAddress("lep_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose",
                                 &lep_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose,
                                 &b_lep_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose);
        fChain->SetBranchAddress("lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v15_isolLoose_VarRad",
                                 &lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v15_isolLoose_VarRad,
                                 &b_lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v15_isolLoose_VarRad);
        fChain->SetBranchAddress("lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v15_isolTight_VarRad",
                                 &lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v15_isolTight_VarRad,
                                 &b_lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v15_isolTight_VarRad);
        fChain->SetBranchAddress(
            "lep_NOMINAL_EleEffSF_e26_lhtight_ivarloose_L1EM22VHI_OR_e60_lhmedium_L1EM22VHI_OR_e140_lhloose_L1EM22VHI_TightLLH_isolTight_VarRad_2022",
            &lep_NOMINAL_EleEffSF_e26_lhtight_ivarloose_L1EM22VHI_OR_e60_lhmedium_L1EM22VHI_OR_e140_lhloose_L1EM22VHI_TightLLH_isolTight_VarRad_2022,
            &b_lep_NOMINAL_EleEffSF_e26_lhtight_ivarloose_L1EM22VHI_OR_e60_lhmedium_L1EM22VHI_OR_e140_lhloose_L1EM22VHI_TightLLH_isolTight_VarRad_2022);
        fChain->SetBranchAddress("lep_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v15", &lep_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v15,
                                 &b_lep_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v15);
        fChain->SetBranchAddress("lep_NOMINAL_EleEffSF_offline_RecoTrk", &lep_NOMINAL_EleEffSF_offline_RecoTrk,
                                 &b_lep_NOMINAL_EleEffSF_offline_RecoTrk);
        fChain->SetBranchAddress("lep_NOMINAL_EleEffSF_offline_TightLLH_d0z0_v15", &lep_NOMINAL_EleEffSF_offline_TightLLH_d0z0_v15,
                                 &b_lep_NOMINAL_EleEffSF_offline_TightLLH_d0z0_v15);
        fChain->SetBranchAddress("lep_NOMINAL_MuEffSF_HLT_mu24_ivarmedium_L1MU14FCH_OR_HLT_mu50_L1MU14FCH_QualMedium",
                                 &lep_NOMINAL_MuEffSF_HLT_mu24_ivarmedium_L1MU14FCH_OR_HLT_mu50_L1MU14FCH_QualMedium,
                                 &b_lep_NOMINAL_MuEffSF_HLT_mu24_ivarmedium_L1MU14FCH_OR_HLT_mu50_L1MU14FCH_QualMedium);
        fChain->SetBranchAddress("lep_NOMINAL_MuEffSF_IsoLoose_VarRad", &lep_NOMINAL_MuEffSF_IsoLoose_VarRad, &b_lep_NOMINAL_MuEffSF_IsoLoose_VarRad);
        fChain->SetBranchAddress("lep_NOMINAL_MuEffSF_IsoTight_VarRad", &lep_NOMINAL_MuEffSF_IsoTight_VarRad, &b_lep_NOMINAL_MuEffSF_IsoTight_VarRad);
        fChain->SetBranchAddress("lep_NOMINAL_MuEffSF_Reco_QualMedium", &lep_NOMINAL_MuEffSF_Reco_QualMedium, &b_lep_NOMINAL_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_NOMINAL_MuEffSF_TTVA", &lep_NOMINAL_MuEffSF_TTVA, &b_lep_NOMINAL_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_cluster_eta", &lep_cluster_eta, &b_lep_cluster_eta);
        fChain->SetBranchAddress("lep_cluster_eta_be2", &lep_cluster_eta_be2, &b_lep_cluster_eta_be2);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e140_lhloose_L1EM22VHI", &lep_elec_trigmatch_HLT_e140_lhloose_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e140_lhloose_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_L1EM22VHI", &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI",
                                 &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI",
                                 &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI",
                                 &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e60_lhmedium_L1EM22VHI", &lep_elec_trigmatch_HLT_e60_lhmedium_L1EM22VHI,
                                 &b_lep_elec_trigmatch_HLT_e60_lhmedium_L1EM22VHI);
        fChain->SetBranchAddress("lep_elec_trigmatch_trigger_matched", &lep_elec_trigmatch_trigger_matched, &b_lep_elec_trigmatch_trigger_matched);
        fChain->SetBranchAddress("lep_id_bad", &lep_id_bad, &b_lep_id_bad);
        fChain->SetBranchAddress("lep_id_charge", &lep_id_charge, &b_lep_id_charge);
        fChain->SetBranchAddress("lep_id_loose", &lep_id_loose, &b_lep_id_loose);
        fChain->SetBranchAddress("lep_id_medium", &lep_id_medium, &b_lep_id_medium);
        fChain->SetBranchAddress("lep_id_tight", &lep_id_tight, &b_lep_id_tight);
        fChain->SetBranchAddress("lep_id_veryloose", &lep_id_veryloose, &b_lep_id_veryloose);
        fChain->SetBranchAddress("lep_iso_HighPtCaloOnly", &lep_iso_HighPtCaloOnly, &b_lep_iso_HighPtCaloOnly);
        fChain->SetBranchAddress("lep_iso_Loose_VarRad", &lep_iso_Loose_VarRad, &b_lep_iso_Loose_VarRad);
        fChain->SetBranchAddress("lep_iso_PflowLoose_VarRad", &lep_iso_PflowLoose_VarRad, &b_lep_iso_PflowLoose_VarRad);
        fChain->SetBranchAddress("lep_iso_PflowTight_VarRad", &lep_iso_PflowTight_VarRad, &b_lep_iso_PflowTight_VarRad);
        fChain->SetBranchAddress("lep_iso_TightCaloOnly", &lep_iso_TightCaloOnly, &b_lep_iso_TightCaloOnly);
        fChain->SetBranchAddress("lep_iso_TightTrackOnly_FixedRad", &lep_iso_TightTrackOnly_FixedRad, &b_lep_iso_TightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_iso_TightTrackOnly_VarRad", &lep_iso_TightTrackOnly_VarRad, &b_lep_iso_TightTrackOnly_VarRad);
        fChain->SetBranchAddress("lep_iso_Tight_VarRad", &lep_iso_Tight_VarRad, &b_lep_iso_Tight_VarRad);
        fChain->SetBranchAddress("lep_matched", &lep_matched, &b_lep_matched);
        fChain->SetBranchAddress("lep_matched_classifierParticleOrigin", &lep_matched_classifierParticleOrigin,
                                 &b_lep_matched_classifierParticleOrigin);
        fChain->SetBranchAddress("lep_matched_classifierParticleType", &lep_matched_classifierParticleType, &b_lep_matched_classifierParticleType);
        fChain->SetBranchAddress("lep_matched_isHad", &lep_matched_isHad, &b_lep_matched_isHad);
        fChain->SetBranchAddress("lep_matched_mother_pdgId", &lep_matched_mother_pdgId, &b_lep_matched_mother_pdgId);
        fChain->SetBranchAddress("lep_matched_mother_status", &lep_matched_mother_status, &b_lep_matched_mother_status);
        fChain->SetBranchAddress("lep_matched_origin", &lep_matched_origin, &b_lep_matched_origin);
        fChain->SetBranchAddress("lep_matched_p4", &lep_matched_p4, &b_lep_matched_p4);
        fChain->SetBranchAddress("lep_matched_pdgId", &lep_matched_pdgId, &b_lep_matched_pdgId);
        fChain->SetBranchAddress("lep_matched_q", &lep_matched_q, &b_lep_matched_q);
        fChain->SetBranchAddress("lep_matched_status", &lep_matched_status, &b_lep_matched_status);
        fChain->SetBranchAddress("lep_matched_type", &lep_matched_type, &b_lep_matched_type);
        fChain->SetBranchAddress("lep_muonAuthor", &lep_muonAuthor, &b_lep_muonAuthor);
        fChain->SetBranchAddress("lep_muonType", &lep_muonType, &b_lep_muonType);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_L1MU14FCH", &lep_muon_trigmatch_HLT_mu24_ivarmedium_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &lep_muon_trigmatch_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH",
                                 &lep_muon_trigmatch_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &lep_muon_trigmatch_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &lep_muon_trigmatch_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &lep_muon_trigmatch_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &lep_muon_trigmatch_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH",
                                 &lep_muon_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &lep_muon_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH",
                                 &lep_muon_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &lep_muon_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu50_L1MU14FCH", &lep_muon_trigmatch_HLT_mu50_L1MU14FCH,
                                 &b_lep_muon_trigmatch_HLT_mu50_L1MU14FCH);
        fChain->SetBranchAddress("lep_muon_trigmatch_trigger_matched", &lep_muon_trigmatch_trigger_matched, &b_lep_muon_trigmatch_trigger_matched);
        fChain->SetBranchAddress("lep_origin", &lep_origin, &b_lep_origin);
        fChain->SetBranchAddress("lep_p4", &lep_p4, &b_lep_p4);
        fChain->SetBranchAddress("lep_q", &lep_q, &b_lep_q);
        fChain->SetBranchAddress("lep_trk_d0", &lep_trk_d0, &b_lep_trk_d0);
        fChain->SetBranchAddress("lep_trk_d0_sig", &lep_trk_d0_sig, &b_lep_trk_d0_sig);
        fChain->SetBranchAddress("lep_trk_p4", &lep_trk_p4, &b_lep_trk_p4);
        fChain->SetBranchAddress("lep_trk_pt_error", &lep_trk_pt_error, &b_lep_trk_pt_error);
        fChain->SetBranchAddress("lep_trk_pvx_z0", &lep_trk_pvx_z0, &b_lep_trk_pvx_z0);
        fChain->SetBranchAddress("lep_trk_pvx_z0_sig", &lep_trk_pvx_z0_sig, &b_lep_trk_pvx_z0_sig);
        fChain->SetBranchAddress("lep_trk_pvx_z0_sintheta", &lep_trk_pvx_z0_sintheta, &b_lep_trk_pvx_z0_sintheta);
        fChain->SetBranchAddress("lep_trk_vx", &lep_trk_vx, &b_lep_trk_vx);
        fChain->SetBranchAddress("lep_trk_vx_v", &lep_trk_vx_v, &b_lep_trk_vx_v);
        fChain->SetBranchAddress("lep_trk_z0", &lep_trk_z0, &b_lep_trk_z0);
        fChain->SetBranchAddress("lep_trk_z0_sig", &lep_trk_z0_sig, &b_lep_trk_z0_sig);
        fChain->SetBranchAddress("lep_trk_z0_sintheta", &lep_trk_z0_sintheta, &b_lep_trk_z0_sintheta);
        fChain->SetBranchAddress("lep_truth_classification", &lep_truth_classification, &b_lep_truth_classification);
        fChain->SetBranchAddress("lep_type", &lep_type, &b_lep_type);
        fChain->SetBranchAddress("lephad", &lephad, &b_lephad);
        fChain->SetBranchAddress("lephad_CP_alphaminus_ip", &lephad_CP_alphaminus_ip, &b_lephad_CP_alphaminus_ip);
        fChain->SetBranchAddress("lephad_CP_alphaminus_ip_rho", &lephad_CP_alphaminus_ip_rho, &b_lephad_CP_alphaminus_ip_rho);
        fChain->SetBranchAddress("lephad_CP_alphaminus_rho_rho", &lephad_CP_alphaminus_rho_rho, &b_lephad_CP_alphaminus_rho_rho);
        fChain->SetBranchAddress("lephad_CP_ip_tau0_mag", &lephad_CP_ip_tau0_mag, &b_lephad_CP_ip_tau0_mag);
        fChain->SetBranchAddress("lephad_CP_ip_tau0_x_ip", &lephad_CP_ip_tau0_x_ip, &b_lephad_CP_ip_tau0_x_ip);
        fChain->SetBranchAddress("lephad_CP_ip_tau0_y_ip", &lephad_CP_ip_tau0_y_ip, &b_lephad_CP_ip_tau0_y_ip);
        fChain->SetBranchAddress("lephad_CP_ip_tau0_z_ip", &lephad_CP_ip_tau0_z_ip, &b_lephad_CP_ip_tau0_z_ip);
        fChain->SetBranchAddress("lephad_CP_ip_tau1_mag", &lephad_CP_ip_tau1_mag, &b_lephad_CP_ip_tau1_mag);
        fChain->SetBranchAddress("lephad_CP_ip_tau1_x_ip", &lephad_CP_ip_tau1_x_ip, &b_lephad_CP_ip_tau1_x_ip);
        fChain->SetBranchAddress("lephad_CP_ip_tau1_y_ip", &lephad_CP_ip_tau1_y_ip, &b_lephad_CP_ip_tau1_y_ip);
        fChain->SetBranchAddress("lephad_CP_ip_tau1_z_ip", &lephad_CP_ip_tau1_z_ip, &b_lephad_CP_ip_tau1_z_ip);
        fChain->SetBranchAddress("lephad_CP_phi_star_cp_a1_rho", &lephad_CP_phi_star_cp_a1_rho, &b_lephad_CP_phi_star_cp_a1_rho);
        fChain->SetBranchAddress("lephad_CP_phi_star_cp_ip_ip", &lephad_CP_phi_star_cp_ip_ip, &b_lephad_CP_phi_star_cp_ip_ip);
        fChain->SetBranchAddress("lephad_CP_phi_star_cp_ip_rho", &lephad_CP_phi_star_cp_ip_rho, &b_lephad_CP_phi_star_cp_ip_rho);
        fChain->SetBranchAddress("lephad_CP_phi_star_cp_ip_rho_opt", &lephad_CP_phi_star_cp_ip_rho_opt, &b_lephad_CP_phi_star_cp_ip_rho_opt);
        fChain->SetBranchAddress("lephad_CP_phi_star_cp_rho_ip", &lephad_CP_phi_star_cp_rho_ip, &b_lephad_CP_phi_star_cp_rho_ip);
        fChain->SetBranchAddress("lephad_CP_phi_star_cp_rho_rho", &lephad_CP_phi_star_cp_rho_rho, &b_lephad_CP_phi_star_cp_rho_rho);
        fChain->SetBranchAddress("lephad_CP_tau0_upsilon", &lephad_CP_tau0_upsilon, &b_lephad_CP_tau0_upsilon);
        fChain->SetBranchAddress("lephad_CP_tau1_upsilon", &lephad_CP_tau1_upsilon, &b_lephad_CP_tau1_upsilon);
        fChain->SetBranchAddress("lephad_CP_upsilon_a1", &lephad_CP_upsilon_a1, &b_lephad_CP_upsilon_a1);
        fChain->SetBranchAddress("lephad_coll_approx", &lephad_coll_approx, &b_lephad_coll_approx);
        fChain->SetBranchAddress("lephad_coll_approx_m", &lephad_coll_approx_m, &b_lephad_coll_approx_m);
        fChain->SetBranchAddress("lephad_coll_approx_x0", &lephad_coll_approx_x0, &b_lephad_coll_approx_x0);
        fChain->SetBranchAddress("lephad_coll_approx_x1", &lephad_coll_approx_x1, &b_lephad_coll_approx_x1);
        fChain->SetBranchAddress("lephad_cosalpha", &lephad_cosalpha, &b_lephad_cosalpha);
        fChain->SetBranchAddress("lephad_deta", &lephad_deta, &b_lephad_deta);
        fChain->SetBranchAddress("lephad_dphi", &lephad_dphi, &b_lephad_dphi);
        fChain->SetBranchAddress("lephad_dpt", &lephad_dpt, &b_lephad_dpt);
        fChain->SetBranchAddress("lephad_dr", &lephad_dr, &b_lephad_dr);
        fChain->SetBranchAddress("lephad_met_bisect", &lephad_met_bisect, &b_lephad_met_bisect);
        fChain->SetBranchAddress("lephad_met_centrality", &lephad_met_centrality, &b_lephad_met_centrality);
        fChain->SetBranchAddress("lephad_met_lep0_cos_dphi", &lephad_met_lep0_cos_dphi, &b_lephad_met_lep0_cos_dphi);
        fChain->SetBranchAddress("lephad_met_lep1_cos_dphi", &lephad_met_lep1_cos_dphi, &b_lephad_met_lep1_cos_dphi);
        fChain->SetBranchAddress("lephad_met_min_dphi", &lephad_met_min_dphi, &b_lephad_met_min_dphi);
        fChain->SetBranchAddress("lephad_met_sum_cos_dphi", &lephad_met_sum_cos_dphi, &b_lephad_met_sum_cos_dphi);
        fChain->SetBranchAddress("lephad_mt_lep0_met", &lephad_mt_lep0_met, &b_lephad_mt_lep0_met);
        fChain->SetBranchAddress("lephad_mt_lep1_met", &lephad_mt_lep1_met, &b_lephad_mt_lep1_met);
        fChain->SetBranchAddress("lephad_p4", &lephad_p4, &b_lephad_p4);
        fChain->SetBranchAddress("lephad_qxq", &lephad_qxq, &b_lephad_qxq);
        fChain->SetBranchAddress("lephad_scal_sum_pt", &lephad_scal_sum_pt, &b_lephad_scal_sum_pt);
        fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
        fChain->SetBranchAddress("met_reco_p4", &met_reco_p4, &b_met_reco_p4);
        fChain->SetBranchAddress("met_reco_sig", &met_reco_sig, &b_met_reco_sig);
        fChain->SetBranchAddress("met_reco_sig_tracks", &met_reco_sig_tracks, &b_met_reco_sig_tracks);
        fChain->SetBranchAddress("met_reco_sumet", &met_reco_sumet, &b_met_reco_sumet);
        fChain->SetBranchAddress("min_dr_tau_jet", &min_dr_tau_jet, &b_min_dr_tau_jet);
        fChain->SetBranchAddress("n_actual_int", &n_actual_int, &b_n_actual_int);
        fChain->SetBranchAddress("n_actual_int_cor", &n_actual_int_cor, &b_n_actual_int_cor);
        fChain->SetBranchAddress("n_avg_int", &n_avg_int, &b_n_avg_int);
        fChain->SetBranchAddress("n_avg_int_cor", &n_avg_int_cor, &b_n_avg_int_cor);
        fChain->SetBranchAddress("n_bjets_DL1dv01_FixedCutBEff_70", &n_bjets_DL1dv01_FixedCutBEff_70, &b_n_bjets_DL1dv01_FixedCutBEff_70);
        fChain->SetBranchAddress("n_bjets_DL1dv01_FixedCutBEff_85", &n_bjets_DL1dv01_FixedCutBEff_85, &b_n_bjets_DL1dv01_FixedCutBEff_85);
        fChain->SetBranchAddress("n_electrons", &n_electrons, &b_n_electrons);
        fChain->SetBranchAddress("n_jets", &n_jets, &b_n_jets);
        fChain->SetBranchAddress("n_jets_30", &n_jets_30, &b_n_jets_30);
        fChain->SetBranchAddress("n_jets_40", &n_jets_40, &b_n_jets_40);
        fChain->SetBranchAddress("n_jets_mc_hs", &n_jets_mc_hs, &b_n_jets_mc_hs);
        fChain->SetBranchAddress("n_muons", &n_muons, &b_n_muons);
        fChain->SetBranchAddress("n_muons_bad", &n_muons_bad, &b_n_muons_bad);
        fChain->SetBranchAddress("n_muons_loose", &n_muons_loose, &b_n_muons_loose);
        fChain->SetBranchAddress("n_muons_medium", &n_muons_medium, &b_n_muons_medium);
        fChain->SetBranchAddress("n_muons_tight", &n_muons_tight, &b_n_muons_tight);
        fChain->SetBranchAddress("n_muons_veryloose", &n_muons_veryloose, &b_n_muons_veryloose);
        fChain->SetBranchAddress("n_photons", &n_photons, &b_n_photons);
        fChain->SetBranchAddress("n_pvx", &n_pvx, &b_n_pvx);
        fChain->SetBranchAddress("n_taus", &n_taus, &b_n_taus);
        fChain->SetBranchAddress("n_taus_rnn_loose", &n_taus_rnn_loose, &b_n_taus_rnn_loose);
        fChain->SetBranchAddress("n_taus_rnn_medium", &n_taus_rnn_medium, &b_n_taus_rnn_medium);
        fChain->SetBranchAddress("n_taus_rnn_tight", &n_taus_rnn_tight, &b_n_taus_rnn_tight);
        fChain->SetBranchAddress("n_taus_rnn_veryloose", &n_taus_rnn_veryloose, &b_n_taus_rnn_veryloose);
        fChain->SetBranchAddress("n_truth_gluon_jets", &n_truth_gluon_jets, &b_n_truth_gluon_jets);
        fChain->SetBranchAddress("n_truth_jets", &n_truth_jets, &b_n_truth_jets);
        fChain->SetBranchAddress("n_truth_jets_pt20_eta45", &n_truth_jets_pt20_eta45, &b_n_truth_jets_pt20_eta45);
        fChain->SetBranchAddress("n_truth_quark_jets", &n_truth_quark_jets, &b_n_truth_quark_jets);
        fChain->SetBranchAddress("n_vx", &n_vx, &b_n_vx);
        fChain->SetBranchAddress("primary_vertex", &primary_vertex, &b_primary_vertex);
        fChain->SetBranchAddress("primary_vertex_v", &primary_vertex_v, &b_primary_vertex_v);
        fChain->SetBranchAddress("run_number", &run_number, &b_run_number);
        fChain->SetBranchAddress("tau", &tau, &b_tau);
        fChain->SetBranchAddress("tau_NOMINAL_TauEffSF_JetRNNloose", &tau_NOMINAL_TauEffSF_JetRNNloose, &b_tau_NOMINAL_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_NOMINAL_TauEffSF_JetRNNmedium", &tau_NOMINAL_TauEffSF_JetRNNmedium, &b_tau_NOMINAL_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_NOMINAL_TauEffSF_JetRNNtight", &tau_NOMINAL_TauEffSF_JetRNNtight, &b_tau_NOMINAL_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_NOMINAL_TauEffSF_LooseEleRNN_electron", &tau_NOMINAL_TauEffSF_LooseEleRNN_electron,
                                 &b_tau_NOMINAL_TauEffSF_LooseEleRNN_electron);
        fChain->SetBranchAddress("tau_NOMINAL_TauEffSF_MediumEleRNN_electron", &tau_NOMINAL_TauEffSF_MediumEleRNN_electron,
                                 &b_tau_NOMINAL_TauEffSF_MediumEleRNN_electron);
        fChain->SetBranchAddress("tau_charged_tracks_d0", &tau_charged_tracks_d0, &b_tau_charged_tracks_d0);
        fChain->SetBranchAddress("tau_charged_tracks_d0_sig", &tau_charged_tracks_d0_sig, &b_tau_charged_tracks_d0_sig);
        fChain->SetBranchAddress("tau_charged_tracks_eProbabilityHT", &tau_charged_tracks_eProbabilityHT, &b_tau_charged_tracks_eProbabilityHT);
        fChain->SetBranchAddress("tau_charged_tracks_nInnermostPixelHits", &tau_charged_tracks_nInnermostPixelHits,
                                 &b_tau_charged_tracks_nInnermostPixelHits);
        fChain->SetBranchAddress("tau_charged_tracks_nPixelHits", &tau_charged_tracks_nPixelHits, &b_tau_charged_tracks_nPixelHits);
        fChain->SetBranchAddress("tau_charged_tracks_nSCTHits", &tau_charged_tracks_nSCTHits, &b_tau_charged_tracks_nSCTHits);
        fChain->SetBranchAddress("tau_charged_tracks_p4", &tau_charged_tracks_p4, &b_tau_charged_tracks_p4);
        fChain->SetBranchAddress("tau_charged_tracks_pt_err", &tau_charged_tracks_pt_err, &b_tau_charged_tracks_pt_err);
        fChain->SetBranchAddress("tau_charged_tracks_rnn_chargedScore", &tau_charged_tracks_rnn_chargedScore, &b_tau_charged_tracks_rnn_chargedScore);
        fChain->SetBranchAddress("tau_charged_tracks_rnn_conversionScore", &tau_charged_tracks_rnn_conversionScore,
                                 &b_tau_charged_tracks_rnn_conversionScore);
        fChain->SetBranchAddress("tau_charged_tracks_rnn_isolationScore", &tau_charged_tracks_rnn_isolationScore,
                                 &b_tau_charged_tracks_rnn_isolationScore);
        fChain->SetBranchAddress("tau_charged_tracks_z0", &tau_charged_tracks_z0, &b_tau_charged_tracks_z0);
        fChain->SetBranchAddress("tau_charged_tracks_z0_sig", &tau_charged_tracks_z0_sig, &b_tau_charged_tracks_z0_sig);
        fChain->SetBranchAddress("tau_charged_tracks_z0_sintheta", &tau_charged_tracks_z0_sintheta, &b_tau_charged_tracks_z0_sintheta);
        fChain->SetBranchAddress("tau_charged_tracks_z0_sintheta_tjva", &tau_charged_tracks_z0_sintheta_tjva, &b_tau_charged_tracks_z0_sintheta_tjva);
        fChain->SetBranchAddress("tau_ele_rnn_loose", &tau_ele_rnn_loose, &b_tau_ele_rnn_loose);
        fChain->SetBranchAddress("tau_ele_rnn_medium", &tau_ele_rnn_medium, &b_tau_ele_rnn_medium);
        fChain->SetBranchAddress("tau_ele_rnn_score", &tau_ele_rnn_score, &b_tau_ele_rnn_score);
        fChain->SetBranchAddress("tau_ele_rnn_score_trans", &tau_ele_rnn_score_trans, &b_tau_ele_rnn_score_trans);
        fChain->SetBranchAddress("tau_ele_rnn_tight", &tau_ele_rnn_tight, &b_tau_ele_rnn_tight);
        fChain->SetBranchAddress("tau_jet_deepset_loose", &tau_jet_deepset_loose, &b_tau_jet_deepset_loose);
        fChain->SetBranchAddress("tau_jet_deepset_medium", &tau_jet_deepset_medium, &b_tau_jet_deepset_medium);
        fChain->SetBranchAddress("tau_jet_deepset_score", &tau_jet_deepset_score, &b_tau_jet_deepset_score);
        fChain->SetBranchAddress("tau_jet_deepset_score_trans", &tau_jet_deepset_score_trans, &b_tau_jet_deepset_score_trans);
        fChain->SetBranchAddress("tau_jet_deepset_tight", &tau_jet_deepset_tight, &b_tau_jet_deepset_tight);
        fChain->SetBranchAddress("tau_jet_deepset_veryloose", &tau_jet_deepset_veryloose, &b_tau_jet_deepset_veryloose);
        fChain->SetBranchAddress("tau_jet_rnn_loose", &tau_jet_rnn_loose, &b_tau_jet_rnn_loose);
        fChain->SetBranchAddress("tau_jet_rnn_medium", &tau_jet_rnn_medium, &b_tau_jet_rnn_medium);
        fChain->SetBranchAddress("tau_jet_rnn_score", &tau_jet_rnn_score, &b_tau_jet_rnn_score);
        fChain->SetBranchAddress("tau_jet_rnn_score_trans", &tau_jet_rnn_score_trans, &b_tau_jet_rnn_score_trans);
        fChain->SetBranchAddress("tau_jet_rnn_tight", &tau_jet_rnn_tight, &b_tau_jet_rnn_tight);
        fChain->SetBranchAddress("tau_jet_rnn_veryloose", &tau_jet_rnn_veryloose, &b_tau_jet_rnn_veryloose);
        fChain->SetBranchAddress("tau_jet_width", &tau_jet_width, &b_tau_jet_width);
        fChain->SetBranchAddress("tau_matched", &tau_matched, &b_tau_matched);
        fChain->SetBranchAddress("tau_matched_classifierParticleOrigin", &tau_matched_classifierParticleOrigin,
                                 &b_tau_matched_classifierParticleOrigin);
        fChain->SetBranchAddress("tau_matched_classifierParticleType", &tau_matched_classifierParticleType, &b_tau_matched_classifierParticleType);
        fChain->SetBranchAddress("tau_matched_decayVertex", &tau_matched_decayVertex, &b_tau_matched_decayVertex);
        fChain->SetBranchAddress("tau_matched_decayVertex_v", &tau_matched_decayVertex_v, &b_tau_matched_decayVertex_v);
        fChain->SetBranchAddress("tau_matched_decay_mode", &tau_matched_decay_mode, &b_tau_matched_decay_mode);
        fChain->SetBranchAddress("tau_matched_isEle", &tau_matched_isEle, &b_tau_matched_isEle);
        fChain->SetBranchAddress("tau_matched_isHadTau", &tau_matched_isHadTau, &b_tau_matched_isHadTau);
        fChain->SetBranchAddress("tau_matched_isJet", &tau_matched_isJet, &b_tau_matched_isJet);
        fChain->SetBranchAddress("tau_matched_isMuon", &tau_matched_isMuon, &b_tau_matched_isMuon);
        fChain->SetBranchAddress("tau_matched_isTau", &tau_matched_isTau, &b_tau_matched_isTau);
        fChain->SetBranchAddress("tau_matched_isTruthMatch", &tau_matched_isTruthMatch, &b_tau_matched_isTruthMatch);
        fChain->SetBranchAddress("tau_matched_mother_pdgId", &tau_matched_mother_pdgId, &b_tau_matched_mother_pdgId);
        fChain->SetBranchAddress("tau_matched_mother_status", &tau_matched_mother_status, &b_tau_matched_mother_status);
        fChain->SetBranchAddress("tau_matched_n_charged", &tau_matched_n_charged, &b_tau_matched_n_charged);
        fChain->SetBranchAddress("tau_matched_n_charged_pion", &tau_matched_n_charged_pion, &b_tau_matched_n_charged_pion);
        fChain->SetBranchAddress("tau_matched_n_neutral", &tau_matched_n_neutral, &b_tau_matched_n_neutral);
        fChain->SetBranchAddress("tau_matched_n_neutral_pion", &tau_matched_n_neutral_pion, &b_tau_matched_n_neutral_pion);
        fChain->SetBranchAddress("tau_matched_origin", &tau_matched_origin, &b_tau_matched_origin);
        fChain->SetBranchAddress("tau_matched_p4", &tau_matched_p4, &b_tau_matched_p4);
        fChain->SetBranchAddress("tau_matched_p4_vis_charged_track0", &tau_matched_p4_vis_charged_track0, &b_tau_matched_p4_vis_charged_track0);
        fChain->SetBranchAddress("tau_matched_p4_vis_charged_track1", &tau_matched_p4_vis_charged_track1, &b_tau_matched_p4_vis_charged_track1);
        fChain->SetBranchAddress("tau_matched_p4_vis_charged_track2", &tau_matched_p4_vis_charged_track2, &b_tau_matched_p4_vis_charged_track2);
        fChain->SetBranchAddress("tau_matched_pdgId", &tau_matched_pdgId, &b_tau_matched_pdgId);
        fChain->SetBranchAddress("tau_matched_productionVertex", &tau_matched_productionVertex, &b_tau_matched_productionVertex);
        fChain->SetBranchAddress("tau_matched_productionVertex_v", &tau_matched_productionVertex_v, &b_tau_matched_productionVertex_v);
        fChain->SetBranchAddress("tau_matched_pz", &tau_matched_pz, &b_tau_matched_pz);
        fChain->SetBranchAddress("tau_matched_q", &tau_matched_q, &b_tau_matched_q);
        fChain->SetBranchAddress("tau_matched_status", &tau_matched_status, &b_tau_matched_status);
        fChain->SetBranchAddress("tau_matched_type", &tau_matched_type, &b_tau_matched_type);
        fChain->SetBranchAddress("tau_matched_vis_charged_p4", &tau_matched_vis_charged_p4, &b_tau_matched_vis_charged_p4);
        fChain->SetBranchAddress("tau_matched_vis_neutral_others_p4", &tau_matched_vis_neutral_others_p4, &b_tau_matched_vis_neutral_others_p4);
        fChain->SetBranchAddress("tau_matched_vis_neutral_p4", &tau_matched_vis_neutral_p4, &b_tau_matched_vis_neutral_p4);
        fChain->SetBranchAddress("tau_matched_vis_neutral_pions_p4", &tau_matched_vis_neutral_pions_p4, &b_tau_matched_vis_neutral_pions_p4);
        fChain->SetBranchAddress("tau_matched_vis_p4", &tau_matched_vis_p4, &b_tau_matched_vis_p4);
        fChain->SetBranchAddress("tau_n_all_tracks", &tau_n_all_tracks, &b_tau_n_all_tracks);
        fChain->SetBranchAddress("tau_n_charged_tracks", &tau_n_charged_tracks, &b_tau_n_charged_tracks);
        fChain->SetBranchAddress("tau_n_conversion_tracks", &tau_n_conversion_tracks, &b_tau_n_conversion_tracks);
        fChain->SetBranchAddress("tau_n_fake_tracks", &tau_n_fake_tracks, &b_tau_n_fake_tracks);
        fChain->SetBranchAddress("tau_n_isolation_tracks", &tau_n_isolation_tracks, &b_tau_n_isolation_tracks);
        fChain->SetBranchAddress("tau_n_unclassified_tracks", &tau_n_unclassified_tracks, &b_tau_n_unclassified_tracks);
        fChain->SetBranchAddress("tau_neutral_pfos_p4", &tau_neutral_pfos_p4, &b_tau_neutral_pfos_p4);
        fChain->SetBranchAddress("tau_nn_decay_mode", &tau_nn_decay_mode, &b_tau_nn_decay_mode);
        fChain->SetBranchAddress("tau_nn_decay_mode_prob_1p0n", &tau_nn_decay_mode_prob_1p0n, &b_tau_nn_decay_mode_prob_1p0n);
        fChain->SetBranchAddress("tau_nn_decay_mode_prob_1p1n", &tau_nn_decay_mode_prob_1p1n, &b_tau_nn_decay_mode_prob_1p1n);
        fChain->SetBranchAddress("tau_nn_decay_mode_prob_1pXn", &tau_nn_decay_mode_prob_1pXn, &b_tau_nn_decay_mode_prob_1pXn);
        fChain->SetBranchAddress("tau_nn_decay_mode_prob_3p0n", &tau_nn_decay_mode_prob_3p0n, &b_tau_nn_decay_mode_prob_3p0n);
        fChain->SetBranchAddress("tau_origin", &tau_origin, &b_tau_origin);
        fChain->SetBranchAddress("tau_p4", &tau_p4, &b_tau_p4);
        fChain->SetBranchAddress("tau_pantau_decay_mode", &tau_pantau_decay_mode, &b_tau_pantau_decay_mode);
        fChain->SetBranchAddress("tau_pass_tst_muonolr", &tau_pass_tst_muonolr, &b_tau_pass_tst_muonolr);
        fChain->SetBranchAddress("tau_pi0s_p4", &tau_pi0s_p4, &b_tau_pi0s_p4);
        fChain->SetBranchAddress("tau_primary_vertex", &tau_primary_vertex, &b_tau_primary_vertex);
        fChain->SetBranchAddress("tau_primary_vertex_v", &tau_primary_vertex_v, &b_tau_primary_vertex_v);
        fChain->SetBranchAddress("tau_q", &tau_q, &b_tau_q);
        fChain->SetBranchAddress("tau_secondary_vertex", &tau_secondary_vertex, &b_tau_secondary_vertex);
        fChain->SetBranchAddress("tau_secondary_vertex_v", &tau_secondary_vertex_v, &b_tau_secondary_vertex_v);
        fChain->SetBranchAddress("tau_tes_alpha_pt_shift", &tau_tes_alpha_pt_shift, &b_tau_tes_alpha_pt_shift);
        fChain->SetBranchAddress("tau_trigmatch_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &tau_trigmatch_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("tau_trigmatch_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI",
                                 &tau_trigmatch_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI,
                                 &b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("tau_trigmatch_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &tau_trigmatch_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("tau_trigmatch_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &tau_trigmatch_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("tau_trigmatch_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &tau_trigmatch_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("tau_trigmatch_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &tau_trigmatch_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("tau_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI",
                                 &tau_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI,
                                 &b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("tau_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &tau_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("tau_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI",
                                 &tau_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI,
                                 &b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("tau_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI",
                                 &tau_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI,
                                 &b_tau_trigmatch_HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI);
        fChain->SetBranchAddress("tau_trigmatch_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &tau_trigmatch_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_tau_trigmatch_HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("tau_trigmatch_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH",
                                 &tau_trigmatch_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH,
                                 &b_tau_trigmatch_HLT_mu24_ivarmedium_tau180_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("tau_trigmatch_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &tau_trigmatch_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_tau_trigmatch_HLT_mu24_ivarmedium_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("tau_trigmatch_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &tau_trigmatch_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_tau_trigmatch_HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("tau_trigmatch_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &tau_trigmatch_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_tau_trigmatch_HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("tau_trigmatch_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &tau_trigmatch_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_tau_trigmatch_HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("tau_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH",
                                 &tau_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH,
                                 &b_tau_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("tau_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &tau_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_tau_trigmatch_HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("tau_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH",
                                 &tau_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH,
                                 &b_tau_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoLLP_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("tau_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH",
                                 &tau_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH,
                                 &b_tau_trigmatch_HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH);
        fChain->SetBranchAddress("tau_trigmatch_trigger_matched", &tau_trigmatch_trigger_matched, &b_tau_trigmatch_trigger_matched);
        fChain->SetBranchAddress("tau_type", &tau_type, &b_tau_type);
        fChain->SetBranchAddress("theory_weights_MUR1_MUF1_PDF303200_ASSEW", &theory_weights_MUR1_MUF1_PDF303200_ASSEW,
                                 &b_theory_weights_MUR1_MUF1_PDF303200_ASSEW);
        fChain->SetBranchAddress("theory_weights_MUR1_MUF1_PDF303200_EXPASSEW", &theory_weights_MUR1_MUF1_PDF303200_EXPASSEW,
                                 &b_theory_weights_MUR1_MUF1_PDF303200_EXPASSEW);
        fChain->SetBranchAddress("theory_weights_MUR1_MUF1_PDF303200_MULTIASSEW", &theory_weights_MUR1_MUF1_PDF303200_MULTIASSEW,
                                 &b_theory_weights_MUR1_MUF1_PDF303200_MULTIASSEW);
        fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
        fChain->SetBranchAddress("Weight_mc", &Weight_mc, &b_Weight_mc);
    }
};
