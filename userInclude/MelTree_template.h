#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

#include <vector>

#include "AnaObjs.h"
#include "AnaTree.h"
#include "TLorentzVector.h"

class MelTree : public AnaTree {
public:
    MelTree(TChain *chain) : AnaTree(chain) { Init(this->fChain); }
    // todo: add truth id or properties, add scale factors
    AnaObjs getElectrons() {
        AnaObjs electronVec;
        for (unsigned int i = 0; i < this->electron_pt_Nominal->size(); i++) {
            int eleID(0);

            if (this->electron_isSignal_Nominal->at(i)) eleID |= EIsSignal;
            if (this->electron_isMedium_Nominal->at(i)) eleID |= EMediumLH;
            if (this->electron_isTight_Nominal->at(i)) eleID |= ETightLH;

            if (this->electron_isol_Loose_VarRad_Nominal->at(i)) eleID |= EIsoLoose_VarRad;
            if (this->electron_isol_Tight_VarRad_Nominal->at(i)) eleID |= EIsoTight_VarRad;
            if (this->electron_isol_TightTrk_VarRad_Nominal->at(i)) eleID |= EIsoTightTrackOnly_VarRad;
            if (this->electron_isol_TightTrk_FixedRad_Nominal->at(i)) eleID |= EIsoTightTrackOnly_FixedRad;
            if (this->electron_isol_HighPt_Nominal->at(i)) eleID |= EIsoFCHighPtCaloOnly;

            if (this->electron_HLT_e24_lhmedium_L1EM20VH_match_Nominal->at(i)) eleID |= E24_lhmedium_L1EM20VH;
            if (this->electron_HLT_e60_lhmedium_match_Nominal->at(i)) eleID |= E60_lhmedium;
            if (this->electron_HLT_e120_lhloose_match_Nominal->at(i)) eleID |= E120_lhloose;
            if (this->electron_HLT_e26_lhtight_nod0_ivarloose_match_Nominal->at(i)) eleID |= E26_lhtight_nod0_ivarloose;
            if (this->electron_HLT_e60_lhmedium_nod0_match_Nominal->at(i)) eleID |= E60_lhmedium_nod0;
            if (this->electron_HLT_e140_lhloose_nod0_match_Nominal->at(i)) eleID |= E140_lhloose_nod0;
            if (this->electron_HLT_e26_lhtight_ivarloose_L1EM22VHI_match_Nominal->at(i)) eleID |= E26_lhtight_ivarloose_L1EM22VHI;
            if (this->electron_HLT_e60_lhmedium_L1EM22VHI_match_Nominal->at(i)) eleID |= E60_lhmedium_L1EM22VHI;
            if (this->electron_HLT_e140_lhloose_L1EM22VHI_match_Nominal->at(i)) eleID |= E140_lhloose_L1EM22VHI;

            AnaObj ele(ELECTRON, (this->electron_pt_Nominal->at(i) / 1000), this->electron_eta_Nominal->at(i), this->electron_phi_Nominal->at(i),
                       (this->electron_e_Nominal->at(i) / 1000), this->electron_charge_Nominal->at(i), eleID);
            electronVec.emplace_back(ele);
        }
        //if(tree->mcChannel != 0){
        //    electronVec.setProperties("Origin",*(this->electron_Origin_Nominal));
        //    electronVec.setProperties("Type",*(this->electron_Type_Nominal));
        //    electronVec.setProperties("IFFType",*(this->electron_IFFType_Nominal));
        //}
        electronVec.setProperties("d0sig", *(this->electron_d0sig_Nominal));
        electronVec.setProperties("z0sinTheta", *(this->electron_z0sinTheta_Nominal));
        electronVec.setProperties("passOR", *(this->electron_passOR_Nominal));
        electronVec = electronVec.filterObjects(0, "passOR");
        electronVec.sortByPt();
        return electronVec;
    }

    AnaObjs getMuons() {
        AnaObjs muonVec;
        for (unsigned int i = 0; i < this->muon_pt_Nominal->size(); i++) {
            int muonID(0);
            if (this->muon_isSignal_Nominal->at(i)) muonID |= MuIsSignal;
            if (this->muon_isol_PfLoose_VarRad_Nominal->at(i)) muonID |= MuIsoPflowLoose_VarRad;
            if (this->muon_isol_PfTight_VarRad_Nominal->at(i)) muonID |= MuIsoPflowTight_VarRad;
            if (this->muon_isol_Loose_VarRad_Nominal->at(i)) muonID |= MuIsoLoose_VarRad;
            if (this->muon_isol_Tight_VarRad_Nominal->at(i)) muonID |= MuIsoTight_VarRad;

            if (this->muon_HLT_mu20_iloose_L1MU15_match_Nominal->at(i)) muonID |= Mu20_iloose_L1MU15;
            if (this->muon_HLT_mu24_ivarmedium_L1MU14FCH_match_Nominal->at(i)) muonID |= Mu24_ivarmedium_L1MU14FCH;
            if (this->muon_HLT_mu26_ivarmedium_match_Nominal->at(i)) muonID |= Mu26_ivarmedium;
            if (this->muon_HLT_mu50_match_Nominal->at(i)) muonID |= Mu50;
            if (this->muon_HLT_mu50_L1MU14FCH_match_Nominal->at(i)) muonID |= Mu50_L1MU14FCH;

            AnaObj muon(MUON, (this->muon_pt_Nominal->at(i) / 1000), this->muon_eta_Nominal->at(i), this->muon_phi_Nominal->at(i),
                        (this->muon_e_Nominal->at(i) / 1000), this->muon_charg_Nominal->at(i), muonID);
            muonVec.emplace_back(muon);
        }
        //if(tree->mcChannel != 0){
        //    muonVec.setProperties("Origin",*(this->muon_Origin_Nominal));
        //    muonVec.setProperties("Type",*(this->muon_Type_Nominal));
        //    muonVec.setProperties("IFFType",*(this->muon_IFFType_Nominal));
        //}
        muonVec.setProperties("d0sig", *(this->muon_d0sig_Nominal));
        muonVec.setProperties("z0sinTheta", *(this->muon_z0sinTheta_Nominal));
        muonVec.setProperties("passOR", *(this->muon_passOR_Nominal));
        muonVec = muonVec.filterObjects(0, "passOR");
        muonVec.sortByPt();
        return muonVec;
    }
    AnaObjs getTaus() {
        AnaObjs tauVec;
        for (unsigned int i = 0; i < this->tau_pt_Nominal->size(); i++) {
            int tauID(0);
            switch ((int)this->tau_quality_Nominal->at(i)) {
                case 0:
                    tauID |= TauVeryLoose;
                    break;
                case 1:
                    tauID |= TauVeryLoose | TauLoose;
                    break;
                case 2:
                    tauID |= TauVeryLoose | TauLoose | TauMedium;
                    break;
                case 3:
                    tauID |= TauVeryLoose | TauLoose | TauMedium | TauTight;
            }
            switch ((int)this->tau_eveto_Nominal->at(i)) {
                case 1:
                    tauID |= TauEvetoLoose;
                    break;
                case 2:
                    tauID |= TauEvetoLoose | TauEvetoMedium;
                    break;
                case 3:
                    tauID |= TauEvetoLoose | TauEvetoMedium | TauEvetoTight;
            }
            if (this->tau_ntrks_Nominal->at(i) == 1) {
                tauID |= TauOneProng;
            } else {
                tauID |= TauThreeProng;
            }
            AnaObj tau(TAU, (this->tau_pt_Nominal->at(i) / 1000), this->tau_eta_Nominal->at(i), this->tau_phi_Nominal->at(i),
                       (this->tau_E_Nominal->at(i) / 1000), this->tau_charg_Nominal->at(i), tauID);
            tauVec.emplace_back(tau);
        }
        //TODO: enable it after ntuple issue fixed.
        // tauVec.setProperties("Origin", *(this->tau_Origin_Nominal));
        // tauVec.setProperties("Type", *(this->tau_Type_Nominal));
        // tauVec.setProperties("IsTruthMatched", *(this->tau_IsTruthMatched_Nominal));

        tauVec.setProperties("passOR", *(this->tau_passOR_Nominal));
        tauVec.setProperties("RNNScore", *(this->tau_RNNJetScore_Nominal));
        tauVec = tauVec.filterObjects(0, "passOR");
        tauVec.sortByPt();
        return tauVec;
    }
    AnaObjs getJets() {
        AnaObjs jetVec;
        for (unsigned int i = 0; i < this->ptjets_Nominal->size(); i++) {
            int jetID(0);
            if (this->isBjets_Nominal->at(i)) jetID |= GoodBJet;
            // TODO: fix jvt,fjvt,nnjvt score/pass issue
            //if (fabs(this->etajets_Nominal->at(i)) < 2.4 && this->ptjets_Nominal->at(i) / 1000 > 20 && this->ptjets_Nominal->at(i) / 1000 < 60){
            //    if (this->JVTjets_Nominal->at(i)>0.5) jetID |= JetVertexTag;
            //}
            AnaObj jet(JET, 0, jetID);
            jet.SetPtEtaPhiM(this->ptjets_Nominal->at(i) / 1000, this->etajets_Nominal->at(i), this->phijets_Nominal->at(i),
                             (this->massjets_Nominal->at(i) / 1000));
            jetVec.emplace_back(jet);
        }
        jetVec.setProperties("JVTscore", *(this->JVTjets_Nominal));
        jetVec.setProperties("passOR", *(this->passORjet_Nominal));
        jetVec = jetVec.filterObjects(0, "passOR");
        jetVec.sortByPt();
        return jetVec;
    }
    int bNumber() {
        int bNum(0);
        for (unsigned int i = 0; i < this->isBjets_Nominal->size(); i++) {
            if (this->isBjets_Nominal->at(i) && this->passORjet_Nominal->at(i)) bNum++;
        }
        return bNum;
    }
    TLorentzVector getEtMiss() {
        TLorentzVector METVec;
        METVec.SetPtEtaPhiE(this->EtMiss_tst_Nominal / 1000, 0, this->EtMiss_tstPhi_Nominal, this->EtMiss_tst_Nominal / 1000);
        return METVec;
    }
    int treatAsYear(int runNumber) {
        if (RunNumber < 290000) {
            return 15;
        } else if (RunNumber < 320000) {
            return 16;
        } else if (RunNumber < 342000) {
            return 17;
        } else if (RunNumber < 400000) {
            return 18;
        } else if (RunNumber < 450000) {
            return 22;
        } else if (RunNumber < 460000) {
            return 23;
        } else {
            return -1;
        }
    }
    int getYear() { return this->treatAsYear(this->RunNumber); }

    // single lepton trigger
    AnaObjs getElectrons_Pass_1leptrig() {
        AnaObjs eleVec = this->getElectrons();
        AnaObjs outVec;
        int year = this->getYear();
        if (year < 16) {
            if (this->HLT_e24_lhmedium_L1EM20VH || this->HLT_e60_lhmedium || this->HLT_e120_lhloose)
                outVec = eleVec.filterObjects(25, 100, E24_lhmedium_L1EM20VH) + eleVec.filterObjects(61, 100, E60_lhmedium) +
                         eleVec.filterObjects(121, 100, E120_lhloose);
        } else if (year < 22) {
            if (this->HLT_e26_lhtight_nod0_ivarloose || this->HLT_e60_lhmedium_nod0 || this->HLT_e140_lhloose_nod0)
                outVec = eleVec.filterObjects(27, 100, E26_lhtight_nod0_ivarloose) + eleVec.filterObjects(61, 100, E60_lhmedium_nod0) +
                         eleVec.filterObjects(141, 100, E140_lhloose_nod0);
        } else {
            if (this->HLT_e26_lhtight_ivarloose_L1EM22VHI || this->HLT_e60_lhmedium_L1EM22VHI || this->HLT_e140_lhloose_L1EM22VHI)
                outVec = eleVec.filterObjects(27, 100, E26_lhtight_ivarloose_L1EM22VHI) + eleVec.filterObjects(61, 100, E60_lhmedium_L1EM22VHI) +
                         eleVec.filterObjects(141, 100, E140_lhloose_L1EM22VHI);
        }
        return outVec;
    }
    AnaObjs getMuons_Pass_1leptrig() {
        AnaObjs muonVec = this->getMuons();
        AnaObjs outVec;
        int year = this->getYear();
        if (year < 16) {
            if (this->HLT_mu20_iloose_L1MU15 || this->HLT_mu50)
                outVec = muonVec.filterObjects(20 * 1.05, 100, Mu20_iloose_L1MU15) + muonVec.filterObjects(50 * 1.05, 100, Mu50);
        } else if (year < 22) {
            if (this->HLT_mu26_ivarmedium || this->HLT_mu50)
                outVec = muonVec.filterObjects(26 * 1.05, 100, Mu26_ivarmedium) + muonVec.filterObjects(50 * 1.05, 100, Mu50);
        } else {
            if (this->HLT_mu24_ivarmedium_L1MU14FCH || this->HLT_mu50_L1MU14FCH)
                outVec = muonVec.filterObjects(24 * 1.05, 100, Mu24_ivarmedium_L1MU14FCH) + muonVec.filterObjects(50 * 1.05, 100, Mu50_L1MU14FCH);
        }
        return outVec;
    }
    Float_t SingleLeptonTrigger_SF(AnaObjs lepVec) {
        if (lepVec.size() == 0) return 1;
        lepVec.sortByPt();
        if (lepVec[0].type() == ELECTRON) {
            return this->WeightEvents_trigger_ele_single_Nominal;
        } else {
            return this->WeightEvents_trigger_mu_single_Nominal;
        }
    }

    // Declaration of leaf types
    ULong64_t EventNumber;
    //TODO

    // List of branches
    TBranch *b_EventNumber;                                                //!
    //TODO

    void Init(TChain *tree) override {
        std::lock_guard<std::mutex> lock(_mutex);
        // The Init() function is called when the selector needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).

        // Set object pointer
        labeljets_reco_Nominal = 0;
        //TODO
        // Set branch addresses and branch pointers
        if (!tree) return;
        fChain = tree;

        fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
        //TODO
    }
};
