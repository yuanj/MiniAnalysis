#include "AnaObjs.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineLooper(Wh_FullJERPart2, XamppTree);

void Wh_FullJERPart2::loop() {
    std::string outFullName = getOutName();
    auto treeName = currentTreeName();
    treeName.replace(treeName.find("__2"), 3, "__1");

    // for cut
    Var["totalWeight"] = 1;
    auto oTree = cloneCurrentAnaTree(treeName);
    oTree->SetBranchAddress("Weight_mc", &Var["totalWeight"]);

    Cutflow_ptr mCutflow = nullptr;
    if (treeName.find("Nominal") != std::string::npos) {
        mCutflow = addCutflow();
    } else {
        mCutflow = addCutflow(Cutflow::NO_HIST);
    }

    mCutflow->setWeight([&] { return Var["totalWeight"]; });

    mCutflow->setFillTree(oTree);

    mCutflow->registerCut("baseline", [&] { return true; });
    mCutflow->registerCut("the end", [&] { return true; });

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        Var["totalWeight"] = -1 * tree->Weight_mc;

        fillRegions();
        fillCutflows();
    }
    // Print out for region, cutflows if needed
    if (treeName.find("Nominal") != std::string::npos) {
        for (auto cut : getCutflows()) {
            cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
        }
    }
}
