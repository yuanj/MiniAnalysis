#include "AnaObjs.h"
#include "GRLDB.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

/* QCD contribution in Top-VR, Z-VR, and MultiBoson-VR */

//#include "XamppTree.h"
DefineLooper(C1N2OS_TopZMB_QCD, XamppTree);

void C1N2OS_TopZMB_QCD::loop() {
    std::string outFullName = getOutName();
    std::string currentName = currentTreeName();
    std::string outTree = Utils::splitStrBy(outFullName, '.')[0] + "_Staus";
    auto oTree = cloneCurrentAnaTree(outTree);

    bool isC1C1low = false;
    // if ( outFullName.find("C1C1") != std::string::npos ){
    //	isC1C1low = true;
    //}

    auto mCutflow = addCutflow();
    // define cuts
    Var["tau1Pt"] = 0;
    Var["tau1Mt"] = 0;
    Var["tau2Pt"] = 0;
    Var["tau2Mt"] = 0;
    Var["dPhitt"] = 0;
    Var["dRtt"] = 0;
    Var["mt12tau"] = 0;
    Var["meff_tau"] = 0;
    Var["Mtt_12"] = 0;
    Var["MT2"] = 0;
    Var["met_sig"] = 0;
    Var["met_sig_tj"] = 0;
    Var["evt_MET"] = 0;

    Var["bVeto"] = 0;
    Var["bNumber"] = 0;
    Var["totalWeight"] = 1;

    Var["n_signal_jets"] = 0;

    int nTightTau(0), nTaus(0), Ljet_n(0), nJet(0), Nemu(0);
    bool tauTriger, OS2Tau, zVeto, mllCut, mt2Cut, noemu;

    bool isSR1 = false;
    bool isSR2 = false;
    bool isSR3 = false;
    bool isSR4 = false;
    bool isWCR = false;
    bool isWVR = false;
    bool isTVR1 = false;
    bool isTVR2 = false;
    bool isZVR1 = false;
    bool isZVR2 = false;
    bool isBosonVR1 = false;
    bool isBosonVR2 = false;

    bool isQCDCR_SR1 = false;
    bool isQCDCR_SR2 = false;
    bool isQCDCR_SR3 = false;
    bool isQCDCR_SR4 = false;
    bool isQCDCR_WCR = false;
    bool isQCDCR_WVR = false;
    bool isQCDCR_TVR1 = false;
    bool isQCDCR_TVR2 = false;
    bool isQCDCR_ZVR1 = false;
    bool isQCDCR_ZVR2 = false;
    bool isQCDCR_BosonVR1 = false;
    bool isQCDCR_BosonVR2 = false;

    oTree->Branch("isSR1", &isSR1);
    oTree->Branch("isSR2", &isSR2);
    oTree->Branch("isSR3", &isSR3);
    oTree->Branch("isSR4", &isSR4);
    oTree->Branch("isWCR", &isWCR);
    oTree->Branch("isWVR", &isWVR);
    oTree->Branch("isTVR1", &isTVR1);
    oTree->Branch("isTVR2", &isTVR2);
    oTree->Branch("isZVR1", &isZVR1);
    oTree->Branch("isZVR2", &isZVR2);
    oTree->Branch("isBosonVR1", &isBosonVR1);
    oTree->Branch("isBosonVR2", &isBosonVR2);

    oTree->Branch("isQCDCR_SR1", &isQCDCR_SR1);
    oTree->Branch("isQCDCR_SR2", &isQCDCR_SR2);
    oTree->Branch("isQCDCR_SR3", &isQCDCR_SR3);
    oTree->Branch("isQCDCR_SR4", &isQCDCR_SR4);
    oTree->Branch("isQCDCR_WCR", &isQCDCR_WCR);
    oTree->Branch("isQCDCR_WVR", &isQCDCR_WVR);
    oTree->Branch("isQCDCR_TVR1", &isQCDCR_TVR1);
    oTree->Branch("isQCDCR_TVR2", &isQCDCR_TVR2);
    oTree->Branch("isQCDCR_ZVR1", &isQCDCR_ZVR1);
    oTree->Branch("isQCDCR_ZVR2", &isQCDCR_ZVR2);
    oTree->Branch("isQCDCR_BosonVR1", &isQCDCR_BosonVR1);
    oTree->Branch("isQCDCR_BosonVR2", &isQCDCR_BosonVR2);

    GRLDB mGRLDB;
    mGRLDB.loadGRLFile("/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/run2Ditau/include/ditauMETGRL.xml");

    mCutflow->setWeight([&] { return Var["totalWeight"]; });
    mCutflow->setFillTree(oTree);
    //将m_Tree导入Cutflow里的outTree
    mCutflow->registerCut("baseline", [&] { return fabs(Var["totalWeight"]) < 100; });
    mCutflow->registerCut(">= 2 medium tau", [&] { return nTaus >= 2; });
    mCutflow->registerCut(">= 1 tight tau", [&] { return nTightTau >= 1; });
    mCutflow->registerCut("SS", [&] { return !OS2Tau; });
    mCutflow->registerCut("inVR", [&] { return !tauTriger; });

    auto lastCut = mCutflow->registerCut("the END", [&] { return true; });

    // Loop Start
    Long64_t nentries = tree->GetEntries();
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        tree->GetEntry(jentry);
        if (0 == jentry % 100000) std::cout << jentry << " entry of " << nentries << "entries" << std::endl;

        Var["tau1Pt"] = 0;
        Var["tau1Mt"] = 0;
        Var["tau2Pt"] = 0;
        Var["tau2Mt"] = 0;
        Var["dPhitt"] = 999;
        Var["dRtt"] = 999;
        Var["mt12tau"] = -999;
        Var["meff_tau"] = -999;
        Var["Mtt_12"] = -999;
        Var["MT2"] = -999;
        Var["met_sig"] = -1;
        Var["met_sig_tj"] = -1;
        Var["evt_MET"] = -1;

        Var["dPhijj"] = 0;
        Var["dRjj"] = 0;
        Var["dEtajj"] = 0;
        Var["EtajEtaj"] = 0;
        Var["jet1Pt"] = 0;
        Var["jet2Pt"] = 0;
        Var["jet1Eta"] = 0;
        Var["jet2Eta"] = 0;
        Var["fabs_jet1Eta"] = 0;
        Var["fabs_jet2Eta"] = 0;
        Var["totalWeight"] = 1;
        Var["jet1Mt"] = 0;
        Var["jet2Mt"] = 0;
        nTightTau = 0;
        nTaus = 0;
        Ljet_n = 0;
        nJet = 0;
        tauTriger = false;
        Var["bVeto"] = false;
        OS2Tau = false, zVeto = false, noemu = true;
        Var["bNumber"] = 0;
        Var["evt_MET"] = tree->MetTST_met / 1000;

        Var["totalWeight"] = tree->Weight_mc;
        if (tree->mcChannelNumber != 0) {
            Var["totalWeight"] *= -1;
            tree->Weight_mc *= -1;
        }

        isSR1 = false;
        isSR2 = false;
        isSR3 = false;
        isSR4 = false;
        isWCR = false;
        isWVR = false;
        isTVR1 = false;
        isTVR2 = false;
        isZVR1 = false;
        isZVR2 = false;
        isBosonVR1 = false;
        isBosonVR2 = false;

        isQCDCR_SR1 = false;
        isQCDCR_SR2 = false;
        isQCDCR_SR3 = false;
        isQCDCR_SR4 = false;
        isQCDCR_WCR = false;
        isQCDCR_WVR = false;
        isQCDCR_TVR1 = false;
        isQCDCR_TVR2 = false;
        isQCDCR_ZVR1 = false;
        isQCDCR_ZVR2 = false;
        isQCDCR_BosonVR1 = false;
        isQCDCR_BosonVR2 = false;

        ///*******************************Event construct *********************/
        AnaObjs eleVec = tree->getElectrons();
        AnaObjs muonVec = tree->getMuons();
        AnaObjs tauVec = tree->getTaus();
        AnaObjs jetVec = tree->getJets();

        Var["bVeto"] = (tree->n_BJets == 0); // b-veto
        Var["bNumber"] = tree->n_BJets;      // b-veto
        noemu = ((tree->n_SignalMuon + tree->n_SignalElec) == 0);

        if (Var["evt_MET"] > 150) {
            bool useOldtrigger(true);
            if (tree->mergedRunNumber >= 325713 && tree->mergedRunNumber <= 340453) { // 17 data/MC
                if (!mGRLDB.checkInGRL(tree->mergedRunNumber, tree->mergedlumiBlock)) {
                    useOldtrigger = false;
                }
            }
            if (tree->mergedRunNumber >= 348885) {
                tauVec = tauVec.passTrig((tree->TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50),
                                         *(tree->taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50), {75, 40});
                if (tree->mcChannelNumber != 0) {
                    Var["totalWeight"] *= tree->TauWeightTrigHLT_tau60_medium1_tracktwoEF;
                    tree->Weight_mc *= tree->TauWeightTrigHLT_tau60_medium1_tracktwoEF;
                }
            } else if (!useOldtrigger) {
                tauVec = tauVec.passTrig((tree->TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50),
                                         *(tree->taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50), {75, 40});
                if (tree->mcChannelNumber != 0) {
                    Var["totalWeight"] *= tree->TauWeightTrigHLT_tau60_medium1_tracktwo * tree->TauWeightTrigHLT_tau25_medium1_tracktwo;
                    tree->Weight_mc *= tree->TauWeightTrigHLT_tau60_medium1_tracktwo * tree->TauWeightTrigHLT_tau25_medium1_tracktwo;
                }
            } else {
                tauVec = tauVec.passTrig((useOldtrigger && tree->TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50),
                                         *(tree->taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50), {50, 40});
                if (tree->mcChannelNumber != 0) {
                    Var["totalWeight"] *= tree->TauWeightTrigHLT_tau35_medium1_tracktwo * tree->TauWeightTrigHLT_tau25_medium1_tracktwo;
                    tree->Weight_mc *= tree->TauWeightTrigHLT_tau35_medium1_tracktwo * tree->TauWeightTrigHLT_tau25_medium1_tracktwo;
                }
            }
            tauVec = tauVec.filterObjects(0, 100, TauMedium);
            auto tightTauVec = tauVec.filterObjects(0, 100, TauTight);
            nTaus = tauVec.size();
            nTightTau = tightTauVec.size();
            if (nTaus >= 2 && nTightTau >= 1) {
                auto tau1 = tauVec[0];
                auto tau2 = tauVec[1];
                auto Ttau = tightTauVec[0];
                OS2Tau = Ttau.isOS(tau2) || Ttau.isOS(tau1);
                // offline PT select
                TLorentzVector METVec;
                METVec.SetPtEtaPhiE(Var["evt_MET"], 0, tree->MetTST_phi, Var["evt_MET"]);
                Var["tau1Pt"] = tau1.Pt();
                Var["tau1Mt"] = tau1.Mt();
                Var["tau2Pt"] = tau2.Pt();
                Var["tau2Mt"] = tau2.Mt();
                Var["dRtt"] = PhyUtils::deltaR(tau1, tau2);
                Var["mt12tau"] = Var["tau1Mt"] + Var["tau2Mt"];
                Var["Mtt_12"] = PhyUtils::calcMll(tau1, tau2);
                Var["dPhitt"] = PhyUtils::deltaPhi(tau1, tau2);
                Var["MT2"] = PhyUtils::MT2(tau1, tau2, METVec);
                if (Var["bNumber"] >= 1 && Var["Mtt_12"] > 120 && Var["dPhitt"] > 1 && Var["MT2"] > 30) {
                    isTVR2 = true;
                } else if (Var["bNumber"] == 0 && Var["Mtt_12"] < 60 && Var["dRtt"] < 1 && Var["MT2"] < 60) {
                    isZVR2 = true;
                } else if (Var["bNumber"] == 0 && Var["Mtt_12"] < 90 && Var["dRtt"] < 1.2 && Var["dPhitt"] < 1 && Var["MT2"] > 60 &&
                           Var["mt12tau"] > 180) {
                    isBosonVR2 = true;
                } else {
                    tauTriger = true;
                }
            }

        } else {
            if (tree->mergedRunNumber >= 348885) {
                tauVec = tauVec.passTrig((tree->TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40),
                                         *(tree->taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40), {95, 75});
                if (tree->mcChannelNumber != 0) {
                    Var["totalWeight"] *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF * tree->TauWeightTrigHLT_tau60_medium1_tracktwoEF;
                    tree->Weight_mc *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF * tree->TauWeightTrigHLT_tau60_medium1_tracktwoEF;
                }
            } else {
                tauVec = tauVec.passTrig((tree->TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12),
                                         *(tree->taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12), {95, 60});
                if (tree->mcChannelNumber != 0) {
                    Var["totalWeight"] *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo * tree->TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;
                    tree->Weight_mc *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo * tree->TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;
                }
            }
            tauVec = tauVec.filterObjects(0, 100, TauMedium);
            auto tightTauVec = tauVec.filterObjects(0, 100, TauTight);
            nTaus = tauVec.size();
            nTightTau = tightTauVec.size();
            if (nTaus >= 2 && nTightTau >= 1) {
                auto tau1 = tauVec[0];
                auto tau2 = tauVec[1];
                auto Ttau = tightTauVec[0];
                OS2Tau = Ttau.isOS(tau2) || Ttau.isOS(tau1);
                // offline PT select
                TLorentzVector METVec;
                METVec.SetPtEtaPhiE(Var["evt_MET"], 0, tree->MetTST_phi, Var["evt_MET"]);
                Var["tau1Pt"] = tau1.Pt();
                Var["tau1Mt"] = tau1.Mt();
                Var["tau2Pt"] = tau2.Pt();
                Var["tau2Mt"] = tau2.Mt();
                Var["dRtt"] = PhyUtils::deltaR(tau1, tau2);
                Var["mt12tau"] = Var["tau1Mt"] + Var["tau2Mt"];
                Var["Mtt_12"] = PhyUtils::calcMll(tau1, tau2);
                Var["dPhitt"] = PhyUtils::deltaPhi(tau1, tau2);
                Var["MT2"] = PhyUtils::MT2(tau1, tau2, METVec);
                if (Var["evt_MET"] > 20 && Var["bNumber"] >= 1 && Var["Mtt_12"] > 120 && Var["dPhitt"] > 1 && Var["MT2"] > 40) {
                    isTVR1 = true;
                } else if (Var["evt_MET"] > 40 && Var["bNumber"] == 0 && Var["Mtt_12"] < 70 && Var["dRtt"] < 1 && Var["MT2"] < 60) {
                    isZVR1 = true;
                } else if (Var["evt_MET"] > 70 && Var["bNumber"] == 0 && Var["Mtt_12"] < 80 && Var["dRtt"] < 1.2 && Var["dPhitt"] < 1 &&
                           Var["MT2"] > 60 && Var["mt12tau"] > 180) {
                    isBosonVR1 = true;
                } else {
                    tauTriger = true;
                }
            }
        }

        fillRegions();
        fillCutflows();
    }
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
