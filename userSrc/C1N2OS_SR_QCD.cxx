#include "AnaObjs.h"
#include "GRLDB.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

/* QCD contribution in SR */

//#include "XamppTree.h"
DefineLooper(C1N2OS_SR_QCD, XamppTree);

void C1N2OS_SR_QCD::loop() {
    std::string outFullName = getOutName();
    std::string currentName = currentTreeName();
    std::string outTree = Utils::splitStrBy(outFullName, '.')[0] + "_Staus";
    auto oTree = cloneCurrentAnaTree(outTree);

    auto mCutflow = addCutflow();
    // define cuts
    Var["tau1Pt"] = 0;
    Var["tau1Mt"] = 0;
    Var["tau2Pt"] = 0;
    Var["tau2Mt"] = 0;
    Var["dPhitt"] = 0;
    Var["dRtt"] = 0;
    Var["mt12tau"] = 0;
    Var["meff_tau"] = 0;
    Var["Mtt_12"] = 0;
    Var["MT2"] = 0;
    Var["met_sig"] = 0;
    Var["met_sig_tj"] = 0;
    Var["evt_MET"] = 0;

    Var["bVeto"] = 0;
    Var["bNumber"] = 0;
    Var["totalWeight"] = 1;
    Var["MT2max"] = 0;
    Var["n_signal_jets"] = 0;

    int nTightTau(0), nTaus(0), Ljet_n(0), nJet(0), Nemu(0);
    bool tauTriger, OS2Tau, zVeto, mllCut, mt2Cut, noemu;

    bool isSR1 = false;
    bool isSR2 = false;
    bool isSR3 = false;
    bool isSR4 = false;
    bool isWCR = false;
    bool isWVR = false;
    bool isTVR1 = false;
    bool isTVR2 = false;
    bool isZVR1 = false;
    bool isZVR2 = false;
    bool isBosonVR1 = false;
    bool isBosonVR2 = false;

    bool isQCDCR_SR1 = false;
    bool isQCDCR_SR2 = false;
    bool isQCDCR_SR3 = false;
    bool isQCDCR_SR4 = false;
    bool isQCDCR_WCR = false;
    bool isQCDCR_WVR = false;
    bool isQCDCR_TVR1 = false;
    bool isQCDCR_TVR2 = false;
    bool isQCDCR_ZVR1 = false;
    bool isQCDCR_ZVR2 = false;
    bool isQCDCR_BosonVR1 = false;
    bool isQCDCR_BosonVR2 = false;

    oTree->Branch("isSR1", &isSR1);
    oTree->Branch("isSR2", &isSR2);
    oTree->Branch("isSR3", &isSR3);
    oTree->Branch("isSR4", &isSR4);
    oTree->Branch("isWCR", &isWCR);
    oTree->Branch("isWVR", &isWVR);
    oTree->Branch("isTVR1", &isTVR1);
    oTree->Branch("isTVR2", &isTVR2);
    oTree->Branch("isZVR1", &isZVR1);
    oTree->Branch("isZVR2", &isZVR2);
    oTree->Branch("isBosonVR1", &isBosonVR1);
    oTree->Branch("isBosonVR2", &isBosonVR2);

    oTree->Branch("isQCDCR_SR1", &isQCDCR_SR1);
    oTree->Branch("isQCDCR_SR2", &isQCDCR_SR2);
    oTree->Branch("isQCDCR_SR3", &isQCDCR_SR3);
    oTree->Branch("isQCDCR_SR4", &isQCDCR_SR4);
    oTree->Branch("isQCDCR_WCR", &isQCDCR_WCR);
    oTree->Branch("isQCDCR_WVR", &isQCDCR_WVR);
    oTree->Branch("isQCDCR_TVR1", &isQCDCR_TVR1);
    oTree->Branch("isQCDCR_TVR2", &isQCDCR_TVR2);
    oTree->Branch("isQCDCR_ZVR1", &isQCDCR_ZVR1);
    oTree->Branch("isQCDCR_ZVR2", &isQCDCR_ZVR2);
    oTree->Branch("isQCDCR_BosonVR1", &isQCDCR_BosonVR1);
    oTree->Branch("isQCDCR_BosonVR2", &isQCDCR_BosonVR2);

    GRLDB mGRLDB;
    mGRLDB.loadGRLFile("/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/run2Ditau/include/ditauMETGRL.xml");

    mCutflow->setWeight([&] { return Var["totalWeight"]; });
    mCutflow->setFillTree(oTree);
    //将m_Tree导入Cutflow里的outTree
    mCutflow->registerCut("baseline", [&] { return true; });
    mCutflow->registerCut(">= 2 loose tau", [&] { return nTaus >= 2; });
    mCutflow->registerCut("<= 1 medium tau", [&] { return nTightTau <= 1; });
    mCutflow->registerCut(
        "bVeto", [&] { return Var["bVeto"]; }, "bTag", 2, 0, 2, [&] { return Var["bNumber"]; });
    mCutflow->registerCut("MET>40 GeV", [&] { return Var["evt_MET"] >= 40; });
    // mCutflow->registerCut("MT2>70 GeV", [&] {return Var["MT2"] >= 70; });
    // mCutflow->registerCut("OS", [&] {return (OS2Tau); });
    mCutflow->registerCut("z/hVeto(120Gev)", [&] { return Var["Mtt_12"] >= 120; });
    mCutflow->registerCut("CR1or2or3or4", [&] { return isSR1 || isSR2 || isSR3 || isSR4; });

    auto lastCut = mCutflow->registerCut("the END", [&] { return true; });

    // Loop Start
    Long64_t nentries = tree->GetEntries();
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        tree->GetEntry(jentry);
        if (0 == jentry % 100000) std::cout << jentry << " entry of " << nentries << "entries" << std::endl;

        Var["tau1Pt"] = 0;
        Var["tau1Mt"] = 0;
        Var["tau2Pt"] = 0;
        Var["tau2Mt"] = 0;
        Var["dPhitt"] = 999;
        Var["dRtt"] = 999;
        Var["mt12tau"] = -999;
        Var["meff_tau"] = -999;
        Var["Mtt_12"] = 0;
        Var["MT2"] = -999;
        Var["met_sig"] = -1;
        Var["met_sig_tj"] = -1;
        Var["evt_MET"] = 0;

        Var["dPhijj"] = 0;
        Var["dRjj"] = 0;
        Var["dEtajj"] = 0;
        Var["EtajEtaj"] = 0;
        Var["jet1Pt"] = 0;
        Var["jet2Pt"] = 0;
        Var["jet1Eta"] = 0;
        Var["jet2Eta"] = 0;
        Var["fabs_jet1Eta"] = 0;
        Var["fabs_jet2Eta"] = 0;
        Var["totalWeight"] = 1;
        Var["jet1Mt"] = 0;
        Var["jet2Mt"] = 0;
        nTightTau = 0;
        nTaus = 0;
        Ljet_n = 0;
        nJet = 0;
        tauTriger = false;
        Var["bVeto"] = false;
        OS2Tau = false, zVeto = false, noemu = true;
        Var["bNumber"] = 0;
        Var["evt_MET"] = tree->MetTST_met / 1000;
        Var["MT2max"] = 0;

        Var["totalWeight"] = tree->Weight_mc;

        isSR1 = false;
        isSR2 = false;
        isSR3 = false;
        isSR4 = false;
        isWCR = false;
        isWVR = false;
        isTVR1 = false;
        isTVR2 = false;
        isZVR1 = false;
        isZVR2 = false;
        isBosonVR1 = false;
        isBosonVR2 = false;

        isQCDCR_SR1 = false;
        isQCDCR_SR2 = false;
        isQCDCR_SR3 = false;
        isQCDCR_SR4 = false;
        isQCDCR_WCR = false;
        isQCDCR_WVR = false;
        isQCDCR_TVR1 = false;
        isQCDCR_TVR2 = false;
        isQCDCR_ZVR1 = false;
        isQCDCR_ZVR2 = false;
        isQCDCR_BosonVR1 = false;
        isQCDCR_BosonVR2 = false;

        ///*******************************Event construct *********************/
        AnaObjs eleVec = tree->getElectrons();
        AnaObjs muonVec = tree->getMuons();
        AnaObjs tauVec = tree->getTaus();
        AnaObjs jetVec = tree->getJets();

        Var["bVeto"] = (tree->n_BJets == 0); // b-veto
        Var["bNumber"] = tree->n_BJets;      // b-veto
        noemu = ((tree->n_SignalMuon + tree->n_SignalElec) == 0);
        Nemu = tree->n_SignalMuon + tree->n_SignalElec;

        if (Var["evt_MET"] > 150) {
            bool useOldtrigger(true);
            if (tree->mergedRunNumber >= 325713 && tree->mergedRunNumber <= 340453) { // 17 data/MC
                if (!mGRLDB.checkInGRL(tree->mergedRunNumber, tree->mergedlumiBlock)) {
                    useOldtrigger = false;
                }
            }
            if (tree->mergedRunNumber >= 348885) {
                tauVec = tauVec.passTrig((tree->TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50),
                                         *(tree->taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50), {75, 40});
                if (tree->mcChannelNumber != 0) {
                    Var["totalWeight"] *= tree->TauWeightTrigHLT_tau60_medium1_tracktwoEF;
                    tree->Weight_mc *= tree->TauWeightTrigHLT_tau60_medium1_tracktwoEF;
                }
            } else if (!useOldtrigger) {
                tauVec = tauVec.passTrig((tree->TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50),
                                         *(tree->taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50), {75, 40});
                if (tree->mcChannelNumber != 0) {
                    Var["totalWeight"] *= tree->TauWeightTrigHLT_tau60_medium1_tracktwo * tree->TauWeightTrigHLT_tau25_medium1_tracktwo;
                    tree->Weight_mc *= tree->TauWeightTrigHLT_tau60_medium1_tracktwo * tree->TauWeightTrigHLT_tau25_medium1_tracktwo;
                }
            } else {
                tauVec = tauVec.passTrig((useOldtrigger && tree->TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50),
                                         *(tree->taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50), {50, 40});
                if (tree->mcChannelNumber != 0) {
                    Var["totalWeight"] *= tree->TauWeightTrigHLT_tau35_medium1_tracktwo * tree->TauWeightTrigHLT_tau25_medium1_tracktwo;
                    tree->Weight_mc *= tree->TauWeightTrigHLT_tau35_medium1_tracktwo * tree->TauWeightTrigHLT_tau25_medium1_tracktwo;
                }
            }
            // tauTriger = (!(tauVec.size()==0));
            tauVec = tauVec.filterObjects(0, 100, TauVeryLoose);
            nTaus = tauVec.size();
            auto mediumTauVec = tauVec.filterObjects(0, 100, TauMedium);
            nTightTau = mediumTauVec.size();
            if (nTaus >= 2 && nTightTau < 1) {
                auto tau1 = tauVec[0];
                auto tau2 = tauVec[1];
                // offline PT select
                TLorentzVector METVec;
                METVec.SetPtEtaPhiE(Var["evt_MET"], 0, tree->MetTST_phi, Var["evt_MET"]);
                OS2Tau = tau1.isOS(tau2);
                Var["Mtt_12"] = PhyUtils::calcMll(tau1, tau2);
                Var["MT2"] = PhyUtils::MT2(tau1, tau2, METVec);
                Var["MT2max"] = PhyUtils::MT2Max(tauVec, METVec);
                Var["mt12tau"] = tau1.Mt() + tau2.Mt();
                if (tree->n_BaseTau == 2 && Var["mt12tau"] > 400 && Var["MT2"] > 85) {
                    isSR3 = true;
                }
                if (Var["mt12tau"] > 400 && Var["MT2max"] > 85) {
                    isSR4 = true;
                }
            }

        } else if (Var["evt_MET"] > 40) {
            if (tree->mergedRunNumber >= 348885) {
                tauVec = tauVec.passTrig((tree->TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40),
                                         *(tree->taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40), {95, 75});
                if (tree->mcChannelNumber != 0) {
                    Var["totalWeight"] *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF * tree->TauWeightTrigHLT_tau60_medium1_tracktwoEF;
                    tree->Weight_mc *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF * tree->TauWeightTrigHLT_tau60_medium1_tracktwoEF;
                }
            } else {
                tauVec = tauVec.passTrig((tree->TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12),
                                         *(tree->taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12), {95, 60});
                if (tree->mcChannelNumber != 0) {
                    Var["totalWeight"] *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo * tree->TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;
                    tree->Weight_mc *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo * tree->TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;
                }
            }

            tauVec = tauVec.filterObjects(0, 100, TauVeryLoose);
            nTaus = tauVec.size();
            auto mediumTauVec = tauVec.filterObjects(0, 100, TauMedium);
            nTightTau = mediumTauVec.size();
            nJet = jetVec.size();
            if (nTaus >= 2) {
                auto tau1 = tauVec[0];
                auto tau2 = tauVec[1];
                // offline PT select
                TLorentzVector METVec;
                METVec.SetPtEtaPhiE(Var["evt_MET"], 0, tree->MetTST_phi, Var["evt_MET"]);
                OS2Tau = tau1.isOS(tau2);
                Var["tau1Pt"] = tau1.Pt();
                Var["tau1Mt"] = tau1.Mt();
                Var["tau2Pt"] = tau2.Pt();
                Var["tau2Mt"] = tau2.Mt();
                Var["Mtt_12"] = PhyUtils::calcMll(tau1, tau2);
                Var["dPhitt"] = PhyUtils::deltaPhi(tau1, tau2);
                Var["MT2"] = PhyUtils::MT2(tau1, tau2, METVec);
                Var["MT2max"] = PhyUtils::MT2Max(tauVec, METVec);
                if (tree->n_BaseTau == 2 && Var["evt_MET"] > 60 && Var["MT2"] > 80 && Var["dPhitt"] > 1.6) {
                    isSR1 = true;
                }
                if (nJet < 3 && Var["MT2max"] > 70 && Var["evt_MET"] > 60) isSR2 = true;
            }
        }

        fillRegions();
        fillCutflows();
    }
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
