#include "AnaObjs.h"
#include "OneLepTree2nd.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "easylogging++.h"

DefineLooper(OneLep2nd_CRtt, OneLepTree2nd);

void OneLep2nd_CRtt::loop() {
    std::string outFullName = getOutName();

    auto oTree = cloneCurrentAnaTree();
    std::string histTagRaw = currentTreeName();
    std::string histTag;
    // check if str end with ".root"
    std::string s = "_NoSys";
    std::string::size_type idx = histTagRaw.rfind(s);
    if (idx != std::string::npos && histTagRaw.size() == idx + s.size()) { // if yes
        histTag = histTagRaw.erase(idx, histTagRaw.size());
    } else { // if not
        histTag = histTagRaw;
    }

    auto mCutflow = addCutflow();
    // for cut
    Var["totalWeight"] = 1;
    Var["singleLepTrig"] = 0;
    Var["dPhiLepMET"] = 999;
    Var["mjj"] = -999;

    // clean conflict tree

    mCutflow->setWeight([&] { return Var["totalWeight"]; });

    mCutflow->setFillTree(oTree);
    mCutflow->setName(histTag);

    mCutflow->registerCut("MET > 150 GeV", [&] { return true; });
    mCutflow->registerCut("1Lepton", [&] { return (tree->nLep_base == 1 && tree->nLep_signal == 1); });
    mCutflow->registerCut("singleLepTrig", [&] { return Var["singleLepTrig"]; });
    mCutflow->registerCut(
        "lepPt > 25", [&] { return tree->lep1Pt > 25; }, histTag + "_lepPt", 20, 0, 100, [&] { return tree->lep1Pt; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "2 <= NJet30 <=3", [&] { return tree->nJet30 >= 2 && tree->nJet30 <= 3; }, histTag + "_nJet30", 11, 0, 11, [&] { return tree->nJet30; });
    mCutflow->registerCut(
        "met > 200", [&] { return tree->met > 200; }, histTag + "_MET", 30, 0, 600, [&] { return tree->met; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "dPhiLepMET < 2.8", [&] { return Var["dPhiLepMET"] < 2.8; }, histTag + "_dPhiLepMET", 40, 0, 4, [&] { return Var["dPhiLepMET"]; },
        Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "70 < mjj < 105", [&] { return Var["mjj"] <= 105 && Var["mjj"] > 70; }, histTag + "_mjj", 20, 0, 200, [&] { return Var["mjj"]; },
        Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "nFatjets == 0", [&] { return tree->nFatjets == 0; }, histTag + "_nFatjets", 4, 0, 4, [&] { return tree->nFatjets; });
    mCutflow->registerCut(
        "50 < MT < 80", [&] { return tree->mt <= 80 && tree->mt > 50; }, histTag + "_mt", 40, 0, 400, [&] { return tree->mt; }, Hist::USE_OVERFLOW);
    auto lastCut = mCutflow->registerCut(
        "nBJet30 > 0", [&] { return tree->nBJet30_DL1 > 0; }, histTag + "_nBJet30", 5, 0, 5, [&] { return tree->nBJet30_DL1; });
    lastCut->addHist(
        histTag + "_lepPtCR", 20, 25, 125, [&] { return tree->lep1Pt; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        histTag + "_metCR", 20, 200, 600, [&] { return tree->met; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        histTag + "_dPhiLepMETCR", 20, 0, 3, [&] { return Var["dPhiLepMET"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        histTag + "_mjjCR", 14, 70, 105, [&] { return Var["mjj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        histTag + "_mtCR", 20, 50, 80, [&] { return tree->mt; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        histTag + "_dRJJCR", 20, 0, 8, [&] { return tree->dRJet; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        histTag + "_HTCR", 20, 0, 1000, [&] { return tree->Ht30; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        histTag + "_jet1PtCR", 20, 0, 500, [&] { return tree->jet1Pt; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        histTag + "_jet2PtCR", 20, 0, 500, [&] { return tree->jet2Pt; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        histTag + "_meffCR", 20, 0, 2000, [&] { return tree->meffInc30; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        histTag + "_met_SignifCR", 20, 0, 20, [&] { return tree->met_Signif; }, Hist::USE_OVERFLOW);

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        Var["dPhiLepMET"] = PhyUtils::deltaPhi(tree->met_Phi, tree->lep1Phi);

        TLorentzVector jet1, jet2;
        jet1.SetPtEtaPhiM(tree->jet1Pt, tree->jet1Eta, tree->jet1Phi, tree->jet1M);
        jet2.SetPtEtaPhiM(tree->jet2Pt, tree->jet2Eta, tree->jet2Phi, tree->jet2M);
        Var["mjj"] = (jet1 + jet2).M();
        int mRunnum(0);
        if (tree->DatasetNumber == 999) {
            Var["totalWeight"] = 1;
            mRunnum = tree->RunNumber;
        } else {
            double eventWei = tree->eventWeight;
            if (outFullName.find("jets") != std::string::npos || outFullName.find("boson") != std::string::npos ||
                outFullName.find("lvvv") != std::string::npos) {
                if (tree->eventWeight > 100) {
                    eventWei = 1;
                } else if (tree->eventWeight < -100) {
                    eventWei = -1;
                }
            }
            Var["totalWeight"] = tree->pileupWeight * eventWei * tree->leptonWeight * tree->jvtWeight * tree->trigWeight_singleLepTrig *
                                 tree->bTagWeight * tree->genWeight * 138965.20;
            mRunnum = tree->RandomRunNumber;
        }

        if (mRunnum < 290000) { // 2015
            Var["singleLepTrig"] = ((tree->match_HLT_e24_lhmedium_L1EM20VH && tree->lep1Pt > 25) ||
                                    (tree->match_HLT_e60_lhmedium && tree->lep1Pt > 61) || (tree->match_HLT_e120_lhloose && tree->lep1Pt > 121) ||
                                    (tree->match_HLT_mu20_iloose_L1MU15 && tree->lep1Pt > 21) || (tree->match_HLT_mu50 && tree->lep1Pt > 52.5));
        } else {
            Var["singleLepTrig"] =
                ((tree->match_HLT_e26_lhtight_nod0_ivarloose && tree->lep1Pt > 27) || (tree->match_HLT_e60_lhmedium_nod0 && tree->lep1Pt > 61) ||
                 (tree->match_HLT_e140_lhloose_nod0 && tree->lep1Pt > 141) || (tree->match_HLT_mu26_ivarmedium && tree->lep1Pt > 27.2) ||
                 (tree->match_HLT_mu50 && tree->lep1Pt > 52.5));
        }

        fillRegions();
        fillCutflows();
    }
    // Print out for region, cutflows if needed
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
