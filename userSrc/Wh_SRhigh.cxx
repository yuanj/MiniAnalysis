#include "AnaObjs.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineLooper(Wh_SRhigh, XamppTree);

void Wh_SRhigh::loop() {
    Var["lightBDM_score"] = 0;
    Var["Weight"] = 0;
    Var["MCtruth"] = true;
    Var["bVeto"] = 0;
    Var["OS2Tau"] = false;
    tree->mcChannelNumber = 0;
    bool addFF = true;

    std::string treeName = currentTreeName();
    std::string outTag = Utils::splitStrBy(currentTreeName(), 'N')[0];
    std::string treeTag = Utils::splitStrBy(currentTreeName(), '_').back();
    if (treeName.find("p0") != std::string::npos) {
        treeName.replace(treeName.find("p0"), 2, "");
        treeName.replace(treeName.find("p0"), 2, "");
    }
    if (treeName.find("p5") != std::string::npos) {
        treeName.replace(treeName.find("p5"), 2, ".5");
        treeName.replace(treeName.find("p5"), 2, ".5");
    }

    LOG(INFO) << treeName;
    auto oTree = cloneCurrentAnaTree(treeName);

    Bool_t isSRlow = false;
    Bool_t isSRhigh = true;
    Bool_t isTCRlow = false;
    Bool_t isTCRhigh = false;
    Bool_t isTVRlow = false;
    Bool_t isTVRhigh = false;
    Bool_t isBosonCRlow = false;
    Bool_t isBosonCRhigh = false;
    Bool_t isBosonVRlow = false;
    Bool_t isBosonVRhigh = false;
    double FF_up = 1, FF_dw = 1;

    oTree->Branch("isSRlow", &isSRlow);
    oTree->Branch("isSRhigh", &isSRhigh);
    oTree->Branch("isTCRlow", &isTCRlow);
    oTree->Branch("isTCRhigh", &isTCRhigh);
    oTree->Branch("isTVRlow", &isTVRlow);
    oTree->Branch("isTVRhigh", &isTVRhigh);
    oTree->Branch("isBosonCRlow", &isBosonCRlow);
    oTree->Branch("isBosonCRhigh", &isBosonCRhigh);
    oTree->Branch("isBosonVRlow", &isBosonVRlow);
    oTree->Branch("isBosonVRhigh", &isBosonVRhigh);
    if (treeName.find("QCD_Nominal") == std::string::npos) {
        oTree->Branch("Weight_mc_FF_up", &FF_up);
        oTree->Branch("Weight_mc_FF_dw", &FF_dw);
    }

    setWeight([&] { return Var["Weight"]; });
    Cutflow_ptr mCutflow = nullptr;
    if (treeName.find("Nominal") != std::string::npos) {
        mCutflow = addCutflow();
    } else {
        mCutflow = addCutflow(Cutflow::NO_HIST);
    }
    mCutflow->setName(currentTreeName());
    mCutflow->setFillTree(oTree);
    mCutflow->registerCut("baseline", [&] { return true; });
    mCutflow->registerCut("MC truth", [&] { return Var["MCtruth"]; });
    mCutflow->registerCut(
        "bVeto", [&] { return Var["bVeto"]; }, "bTag", 2, 0, 2, [&] { return !Var["bVeto"]; });
    mCutflow->registerCut(
        "OS", [&] { return Var["OS2Tau"]; }, "OS2Tau", 2, 0, 2, [&] { return !Var["OS2Tau"]; });
    mCutflow->registerCut(
        "mT2 > 80", [&] { return tree->mT2 > 80; }, outTag + "mT2", 20, 0, 200, [&] { return tree->mT2; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "mll > 80", [&] { return tree->Mtt > 80; }, outTag + "Mtt_low", 20, 0, 200, [&] { return tree->Mtt; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "mll < 160", [&] { return tree->Mtt < 160; }, outTag + "Mtt_high", 20, 0, 200, [&] { return tree->Mtt; }, Hist::USE_OVERFLOW);
    // mCutflow->registerCut("tau1Pt > 0",[&] {return tree->tau1Pt > 0;},outTag + "tau1Pt",20,0,200,[&]{return tree->tau1Pt ;}, Hist::USE_OVERFLOW);
    // mCutflow->registerCut("tau2Pt > 0",[&] {return tree->tau2Pt > 0;},outTag + "tau2Pt",20,0,200,[&]{return tree->tau2Pt ;}, Hist::USE_OVERFLOW);
    // mCutflow->registerCut("NTightTau >=0",[&] {return tree->NTightTau >= 0;},outTag + "NTightTau",4,0,4,[&]{return tree->NTightTau ;},
    // Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "dRtt < 2.2", [&] { return tree->dRtt < 2.2; }, outTag + "dRtt", 20, 0, 4, [&] { return tree->dRtt; }, Hist::USE_OVERFLOW);
    // mCutflow->registerCut("meff > 0",[&] {return tree->meff > 0;},outTag + "meff",20,200,600,[&]{return tree->meff ;}, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "MtLep > 80", [&] { return tree->lepMt > 80; }, outTag + "lepMt", 20, 0, 200, [&] { return tree->lepMt; }, Hist::USE_OVERFLOW);
    // mCutflow->registerCut("MET > 0",[&] {return tree->MET > 0;},outTag + "MET",20,0,200,[&]{return tree->MET ;}, Hist::USE_OVERFLOW);
    // mCutflow->registerCut("tau1Mt > 0",[&] {return tree->tau1Mt > 0;},outTag + "tau1Mt",20,0,300,[&]{return tree->tau1Mt ;}, Hist::USE_OVERFLOW);
    // mCutflow->registerCut("mCT > 0",[&] {return tree->mct > 0;},outTag + "mCT",20,0,200,[&]{return tree->mct ;}, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "MTsum > 450", [&] { return tree->MTsum > 450; }, outTag + "MTsum", 20, 100, 900, [&] { return tree->MTsum; }, Hist::USE_OVERFLOW);
    auto theEnd = mCutflow->registerCut("the end", [&] { return true; });
    theEnd->addHist(
        outTag + "lepPt", 20, 0, 200, [&] { return tree->lepPt; }, Hist::USE_OVERFLOW);
    theEnd->addHist(
        outTag + "MT12", 20, 100, 300, [&] { return tree->MT12; }, Hist::USE_OVERFLOW);
    theEnd->addHist(
        outTag + "tau2Mt", 20, 0, 200, [&] { return tree->tau2Mt; }, Hist::USE_OVERFLOW);
    theEnd->addHist(
        outTag + "NTightTau", 4, 0, 4, [&] { return tree->NTightTau; }, Hist::USE_OVERFLOW);
    theEnd->addHist(
        outTag + "TwotauPt", 20, 0, 200, [&] { return tree->TwotauPt; }, Hist::USE_OVERFLOW);

    theEnd->addHist(
        outTag + "mT2", 12, 80, 200, [&] { return tree->mT2; }, Hist::USE_OVERFLOW);
    theEnd->addHist(
        outTag + "Mtt_high", 8, 80, 160, [&] { return tree->Mtt; }, Hist::USE_OVERFLOW);
    theEnd->addHist(
        outTag + "dRtt", 11, 0, 2.2, [&] { return tree->dRtt; }, Hist::USE_OVERFLOW);
    theEnd->addHist(
        outTag + "meff", 20, 200, 600, [&] { return tree->meff; }, Hist::USE_OVERFLOW);
    theEnd->addHist(
        outTag + "lepMt", 12, 80, 200, [&] { return tree->lepMt; }, Hist::USE_OVERFLOW);
    theEnd->addHist(
        outTag + "MET", 20, 0, 200, [&] { return tree->MET; }, Hist::USE_OVERFLOW);
    theEnd->addHist(
        outTag + "MTsum", 10, 450, 900, [&] { return tree->MTsum; }, Hist::USE_OVERFLOW);

    // Start the loop!
    Long64_t nentries = getEntries();
    for (Long64_t jentry = 0; jentry < tree->GetEntries(); jentry++) {
        getEntry(jentry);

        Var["MCtruth"] = true;
        Var["bVeto"] = tree->n_BJets == 0;
        auto taus = tree->getTaus();
        Var["OS2Tau"] = taus[0].isOS(taus[1]);
        if (addFF && tree->mcChannelNumber != 0 && (outTag.find("Data_") == std::string::npos || outTag.find("Wh_") == std::string::npos)) {
            AnaObjs tauVec = tree->getTaus();
            int nTruthTau = (tauVec.filterObjects(0, 100, TauIsTruthTau)).size();
            Var["MCtruth"] = (nTruthTau >= 1);
        }
        Var["Weight"] = tree->Weight_mc;
        FF_up = tree->Weight_mc;
        FF_dw = tree->Weight_mc;

        fillCutflows();
    }
    // Print out for region, cutflows if needed
    if (treeTag == "Nominal") {
        int i = 0;
        for (auto c : getCutflows()) {
            c->PrintOut("cutflow" + std::to_string(i));
            i++;
        }
    }
}
