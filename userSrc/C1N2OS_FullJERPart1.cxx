#include "AnaObjs.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineLooper(C1N2OS_FullJERPart1, XamppTree);

void C1N2OS_FullJERPart1::loop() {
    std::string outFullName = getOutName();
    std::string currentName = currentTreeName();
    std::string outTree = Utils::splitStrBy(outFullName, '.')[0] + "_" + currentName;
    auto oTree = cloneCurrentAnaTree();

    auto mCutflow = addCutflow();
    // define cuts
    mCutflow->setWeight([&] { return tree->Weight_mc; });
    mCutflow->setFillTree(oTree);
    mCutflow->registerCut("baseline", [&] { return true; });
    mCutflow->registerCut("OS", [&] { return true; });

    // Loop Start
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        fillRegions();
        fillCutflows();
    }
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
