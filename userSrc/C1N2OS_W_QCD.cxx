#include "AnaObjs.h"
#include "GRLDB.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

/* W-CR and W-VR QCD contribution */

//#include "XamppTree.h"
DefineLooper(C1N2OS_W_QCD, XamppTree);

void C1N2OS_W_QCD::loop() {
    std::string outFullName = getOutName();
    std::string currentName = currentTreeName();
    std::string outTree = Utils::splitStrBy(outFullName, '.')[0] + "_Staus";
    auto oTree = cloneCurrentAnaTree(outTree);
    // std::string outTree = Utils::splitStrBy(outFullName,'.')[0] + "_Nominal";
    // std::string outTree = treeName.replace(treeName.find("Staus"), 5, Utils::splitStrBy(outFullName,'.')[0]);

    // auto oTree = cloneCurrentAnaTree(outTree);

    auto mCutflow = addCutflow();
    //	Cutflow* mCutflow = nullptr;
    //	if(treeName == "Staus_Nominal"){
    //		mCutflow = addCutflow();
    //	}else{
    //		mCutflow = addCutflow(Cutflow::NO_HIST);
    //	}
    // define cuts
    Var["tau1Pt"] = 0;
    Var["tau1Mt"] = 0;
    Var["mu1Pt"] = 0;
    Var["mu1Mt"] = 0;
    Var["dPhitt"] = 0;
    Var["dRtm"] = 0;
    Var["mt12tm"] = 0;
    Var["meff_tau"] = 0;
    Var["Mtm_12"] = 0;
    Var["MT2"] = 0;
    Var["met_sig"] = 0;
    Var["met_sig_tj"] = 0;
    Var["evt_MET"] = 0;

    Var["dPhijj"] = 0;
    Var["dRjj"] = 0;
    Var["dEtajj"] = 0;
    Var["EtajEtaj"] = 0;
    Var["jet1Pt"] = 0;
    Var["jet2Pt"] = 0;
    Var["jet1Eta"] = 0;
    Var["jet2Eta"] = 0;
    Var["jet1Mt"] = 0;
    Var["jet2Mt"] = 0;
    Var["fabs_jet1Eta"] = 0;
    Var["fabs_jet2Eta"] = 0;
    Var["bVeto"] = 0;
    Var["bNumber"] = 0;
    Var["totalWeight"] = 1;

    Var["n_signal_jets"] = 0;

    int nTightTau(0), nMuons(0), nTaus(0), Ljet_n(0), mergedRunNumber(0), mlumiBlock(0), nJet(0), Nemu(0), topTagger(0);
    bool muTriger, OS2TM, zVeto, mllCut, mt2Cut, noemu;
    bool isSR1 = false;
    bool isSR2 = false;
    bool isSR3 = false;
    bool isSR4 = false;
    bool isWCR = false;
    bool isWVR = false;
    bool isTVR1 = false;
    bool isTVR2 = false;
    bool isZVR1 = false;
    bool isZVR2 = false;
    bool isBosonVR1 = false;
    bool isBosonVR2 = false;

    bool isQCDCR_SR1 = false;
    bool isQCDCR_SR2 = false;
    bool isQCDCR_SR3 = false;
    bool isQCDCR_SR4 = false;
    bool isQCDCR_WCR = false;
    bool isQCDCR_WVR = false;
    bool isQCDCR_TVR1 = false;
    bool isQCDCR_TVR2 = false;
    bool isQCDCR_ZVR1 = false;
    bool isQCDCR_ZVR2 = false;
    bool isQCDCR_BosonVR1 = false;
    bool isQCDCR_BosonVR2 = false;

    oTree->Branch("isSR1", &isSR1);
    oTree->Branch("isSR2", &isSR2);
    oTree->Branch("isSR3", &isSR3);
    oTree->Branch("isSR4", &isSR4);
    oTree->Branch("isWCR", &isWCR);
    oTree->Branch("isWVR", &isWVR);
    oTree->Branch("isTVR1", &isTVR1);
    oTree->Branch("isTVR2", &isTVR2);
    oTree->Branch("isZVR1", &isZVR1);
    oTree->Branch("isZVR2", &isZVR2);
    oTree->Branch("isBosonVR1", &isBosonVR1);
    oTree->Branch("isBosonVR2", &isBosonVR2);

    oTree->Branch("isQCDCR_SR1", &isQCDCR_SR1);
    oTree->Branch("isQCDCR_SR2", &isQCDCR_SR2);
    oTree->Branch("isQCDCR_SR3", &isQCDCR_SR3);
    oTree->Branch("isQCDCR_SR4", &isQCDCR_SR4);
    oTree->Branch("isQCDCR_WCR", &isQCDCR_WCR);
    oTree->Branch("isQCDCR_WVR", &isQCDCR_WVR);
    oTree->Branch("isQCDCR_TVR1", &isQCDCR_TVR1);
    oTree->Branch("isQCDCR_TVR2", &isQCDCR_TVR2);
    oTree->Branch("isQCDCR_ZVR1", &isQCDCR_ZVR1);
    oTree->Branch("isQCDCR_ZVR2", &isQCDCR_ZVR2);
    oTree->Branch("isQCDCR_BosonVR1", &isQCDCR_BosonVR1);
    oTree->Branch("isQCDCR_BosonVR2", &isQCDCR_BosonVR2);

    mCutflow->setWeight([&] { return Var["totalWeight"]; });
    mCutflow->setFillTree(oTree);
    //将m_Tree导入Cutflow里的outTree
    mCutflow->registerCut("baseline_before_MT2", [&] { return fabs(Var["totalWeight"]) < 100; });
    // mCutflow->registerCut("isQCDCR_WVR",[&] {return isQCDCR_WVR ;});

    auto lastCut = mCutflow->registerCut("the END", [&] { return true; });

    // Loop Start
    Long64_t nentries = tree->GetEntries();
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        tree->GetEntry(jentry);
        if (0 == jentry % 100000) std::cout << jentry << " entry of " << nentries << "entries" << std::endl;

        Var["tau1Pt"] = 0;
        Var["tau1Mt"] = 0;
        Var["mu1Pt"] = 0;
        Var["mu1Mt"] = 0;
        Var["dPhitt"] = 999;
        Var["dRtm"] = 999;
        Var["mt12tm"] = -999;
        Var["meff_tau"] = -999;
        Var["Mtm_12"] = -999;
        Var["MT2"] = -999;
        Var["met_sig"] = -1;
        Var["met_sig_tj"] = -1;
        Var["evt_MET"] = -1;

        Var["dPhijj"] = 0;
        Var["dRjj"] = 0;
        Var["dEtajj"] = 0;
        Var["EtajEtaj"] = 0;
        Var["jet1Pt"] = 0;
        Var["jet2Pt"] = 0;
        Var["jet1Eta"] = 0;
        Var["jet2Eta"] = 0;
        Var["fabs_jet1Eta"] = 0;
        Var["fabs_jet2Eta"] = 0;
        Var["jet1Mt"] = 0;
        Var["jet2Mt"] = 0;
        nTightTau = 0;
        nMuons = 0;
        nTaus = 0;
        Ljet_n = 0;
        mergedRunNumber = 0;
        nJet = 0;
        muTriger = false;
        Var["bVeto"] = false;
        OS2TM = false, zVeto = false, noemu = true;
        Var["bNumber"] = 0;
        Var["evt_MET"] = tree->MetTST_met / 1000;
        topTagger = 1;

        Var["totalWeight"] = tree->Weight_mc;

        isSR1 = false;
        isSR2 = false;
        isSR3 = false;
        isSR4 = false;
        isWCR = false;
        isWVR = false;
        isTVR1 = false;
        isTVR2 = false;
        isZVR1 = false;
        isZVR2 = false;
        isBosonVR1 = false;
        isBosonVR2 = false;

        isQCDCR_SR1 = false;
        isQCDCR_SR2 = false;
        isQCDCR_SR3 = false;
        isQCDCR_SR4 = false;
        isQCDCR_WCR = false;
        isQCDCR_WVR = false;
        isQCDCR_TVR1 = false;
        isQCDCR_TVR2 = false;
        isQCDCR_ZVR1 = false;
        isQCDCR_ZVR2 = false;
        isQCDCR_BosonVR1 = false;
        isQCDCR_BosonVR2 = false;

        ///*******************************Event construct *********************/
        AnaObjs eleVec = tree->getElectrons();
        AnaObjs muonVec = tree->getMuons();
        AnaObjs tauVec = tree->getTaus();

        // Taus
        tauVec = tauVec.filterObjects(0, 100, TauMedium);
        nTaus = tauVec.size();
        muonVec = muonVec.filterObjects(0, 100, MuGood | MuIso);
        nMuons = muonVec.size();

        if (nTaus >= 1 and nMuons >= 1) {
            auto tau = tauVec[0];
            auto muon = muonVec[0];
            // offline PT select
            TLorentzVector METVec;
            METVec.SetPtEtaPhiE(Var["evt_MET"], 0, tree->MetTST_phi, Var["evt_MET"]);
            Var["MT2"] = PhyUtils::MT2(tau, muon, METVec);
            if (Var["MT2"] < 70) {
                isWCR = true;
            } else {
                isWVR = true;
            }
        }
        // if (Mtt_12 > 120000) zVeto = true;

        // Jets

        fillRegions();
        fillCutflows();
    }
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
