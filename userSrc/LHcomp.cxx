#include <fstream>
#include <iostream>
#include <set>

#include "AnaObjs.h"
#include "GRLDB.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

using namespace std;
DefineLooper(LHcomp, XamppTree);

void LHcomp::loop() {
    std::string outFullName = getOutName();
    LOG(INFO) << "outFullName: " << outFullName;

    // std::string proc = outFullName.substr(0, outFullName.find("."));
    // LOG(INFO) << "current process name: " << proc;
    // std::map<std::string,std::set<std::string>> bkg_dict={
    //    {"Vqq",{"Sherpa_Vqq",}},
    //    {"Wjets",{"Sherpa2211_W","Sherpa221_W"}},
    //    {"Zjets",{"Sherpa2211_Z","Sherpa221_Z"}},
    //    {"MultiBoson",{"Sherpa_MultiBoson"}},
    //    {"TopQaurk",{"SingleTopS","SingleTopT","SingleTopWt","multi_t","ttbar","ttV","ttX"}},
    //};
    // for(auto bkg_set : bkg_dict){
    //    if(bkg_set.second.find(proc) != bkg_set.second.end()){
    //        proc = bkg_set.first;
    //        break;
    //    }
    //}
    // proc = proc+"_Nom";

    this->anaChain->SetBranchStatus("GenWeight_LHE_*", 0);
    // auto oTree = cloneCurrentAnaTree(proc);
    auto oTree = cloneCurrentAnaTree();
    std::string treeName = currentTreeName();
    LOG(INFO) << "currentTree name: " << treeName;
    this->anaChain->SetBranchStatus("*", 1);

    auto mCutflow = addCutflow();

    // add to variable member Var
    Var["MET"] = 0;
    Var["totalWeight"] = 1;
    Var["bVeto"] = 0;
    Var["bNumber"] = 0;

    Var["Pttau1"] = 0;
    Var["Ptlep1"] = 0;
    Var["Ptjet1"] = 0;
    Var["Ptjet2"] = 0;
    Var["Mttau1"] = 0;
    Var["Mtlep1"] = 0;
    Var["Mtjet1"] = 0;
    Var["Etatau1"] = 0;
    Var["Etalep1"] = 0;
    Var["Etajet1"] = 0;
    Var["Rapdtau1"] = 0;
    Var["Rapdlep1"] = 0;
    Var["Rapdjet1"] = 0;
    Var["Phitau1"] = 0;
    Var["Philep1"] = 0;
    Var["Phijet1"] = 0;
    Var["Phimet"] = 0;

    Var["dRlt"] = 0;
    Var["dRtj"] = 0;
    Var["dRlj"] = 0;
    Var["dRte"] = 0;
    Var["dRle"] = 0;
    Var["dRje"] = 0;

    Var["dEtalt"] = 0;
    Var["dEtatj"] = 0;
    Var["dEtalj"] = 0;
    Var["dRapidlt"] = 0;
    Var["dRapidtj"] = 0;
    Var["dRapidlj"] = 0;

    Var["dPhilt"] = 0;
    Var["dPhitj"] = 0;
    Var["dPhilj"] = 0;
    Var["dPhite"] = 0;
    Var["dPhile"] = 0;
    Var["dPhije"] = 0;

    Var["MTsum"] = 0;
    Var["Meff"] = 0;
    Var["Meff_j"] = 0;
    Var["log_Meff"] = 0;
    Var["log_Meff_j"] = 0;

    Var["MT_tau"] = 0;
    Var["MT_lep"] = 0;
    Var["MT_jet"] = 0;

    Var["M_inv_tl"] = 0;
    Var["M_inv_te"] = 0;
    Var["M_inv_tj"] = 0;
    Var["M_inv_le"] = 0;
    Var["M_inv_lj"] = 0;
    Var["M_inv_je"] = 0;

    Var["MT2_tl"] = 0;
    Var["MT2_tj"] = 0;
    Var["MT2_lj"] = 0;

    Var["MCT_tl"] = 0;
    Var["MCT_tj"] = 0;
    Var["MCT_lj"] = 0;
    Var["MCT_te"] = 0;
    Var["MCT_le"] = 0;
    Var["MCT_je"] = 0;

    Var["Truth_tau_pt"] = 0;
    Var["Fake_tau_pt"] = 0;

    Var["TriggerMatch"] = 0;

    int nBaseTau(0), nBaseEle(0), nBaseMuon(0), nBaseLep(0), nBaseJet(0), nTaus(0), nEles(0), nMuons(0), nLeps(0), nJets(0), mergedRunNumber(0),
        mlumiBlock(0);
    double dnBaseJet(0);
    bool OSLepHad;

    //-----ADD VARIABLES
    oTree->Branch("Weight_mc", &Var["totalWeight"]);
    oTree->Branch("mergedRunNumber", &mergedRunNumber);
    oTree->Branch("mergedlumiBlock", &mlumiBlock);
    oTree->Branch("MET", &Var["MET"]);
    oTree->Branch("Pttau1", &Var["Pttau1"]);
    oTree->Branch("Ptlep1", &Var["Ptlep1"]);
    oTree->Branch("Ptjet1", &Var["Ptjet1"]);
    oTree->Branch("Mttau1", &Var["Mttau1"]);
    oTree->Branch("Mtlep1", &Var["Mtlep1"]);
    oTree->Branch("Mtjet1", &Var["Mtjet1"]);
    oTree->Branch("Etatau1", &Var["Etatau1"]);
    oTree->Branch("Etalep1", &Var["Etalep1"]);
    oTree->Branch("Etajet1", &Var["Etajet1"]);
    oTree->Branch("Rapdtau1", &Var["Rapdtau1"]);
    oTree->Branch("Rapdlep1", &Var["Rapdlep1"]);
    oTree->Branch("Rapdjet1", &Var["Rapdjet1"]);
    oTree->Branch("Phitau1", &Var["Phitau1"]);
    oTree->Branch("Philep1", &Var["Philep1"]);
    oTree->Branch("Phijet1", &Var["Phijet1"]);
    oTree->Branch("Phimet", &Var["Phimet"]);
    oTree->Branch("dRlt", &Var["dRlt"]);
    oTree->Branch("dRtj", &Var["dRtj"]);
    oTree->Branch("dRlj", &Var["dRlj"]);
    oTree->Branch("dRte", &Var["dRte"]);
    oTree->Branch("dRle", &Var["dRle"]);
    oTree->Branch("dRje", &Var["dRje"]);
    oTree->Branch("dEtalt", &Var["dEtalt"]);
    oTree->Branch("dEtatj", &Var["dEtatj"]);
    oTree->Branch("dEtalj", &Var["dEtalj"]);
    oTree->Branch("dRapidlt", &Var["dRapidlt"]);
    oTree->Branch("dRapidtj", &Var["dRapidtj"]);
    oTree->Branch("dRapidlj", &Var["dRapidlj"]);
    oTree->Branch("dPhilt", &Var["dPhilt"]);
    oTree->Branch("dPhitj", &Var["dPhitj"]);
    oTree->Branch("dPhilj", &Var["dPhilj"]);
    oTree->Branch("dPhite", &Var["dPhite"]);
    oTree->Branch("dPhile", &Var["dPhile"]);
    oTree->Branch("dPhije", &Var["dPhije"]);
    oTree->Branch("MTsum", &Var["MTsum"]);
    oTree->Branch("Meff_0", &Var["Meff"]);
    oTree->Branch("Meff_j", &Var["Meff_j"]);
    oTree->Branch("log_Meff", &Var["log_Meff"]);
    oTree->Branch("log_Meff_j", &Var["log_Meff_j"]);
    oTree->Branch("MT_tau", &Var["MT_tau"]);
    oTree->Branch("MT_lep", &Var["MT_lep"]);
    oTree->Branch("MT_jet", &Var["MT_jet"]);
    oTree->Branch("M_inv_tl", &Var["M_inv_tl"]);
    oTree->Branch("M_inv_te", &Var["M_inv_te"]);
    oTree->Branch("M_inv_tj", &Var["M_inv_tj"]);
    oTree->Branch("M_inv_le", &Var["M_inv_le"]);
    oTree->Branch("M_inv_lj", &Var["M_inv_lj"]);
    oTree->Branch("M_inv_je", &Var["M_inv_je"]);
    oTree->Branch("MT2_tl", &Var["MT2_tl"]);
    oTree->Branch("MT2_tj", &Var["MT2_tj"]);
    oTree->Branch("MT2_lj", &Var["MT2_lj"]);
    oTree->Branch("MCT_tl", &Var["MCT_tl"]);
    oTree->Branch("MCT_tj", &Var["MCT_tj"]);
    oTree->Branch("MCT_lj", &Var["MCT_lj"]);
    oTree->Branch("MCT_te", &Var["MCT_te"]);
    oTree->Branch("MCT_le", &Var["MCT_le"]);
    oTree->Branch("MCT_je", &Var["MCT_je"]);
    oTree->Branch("nBaseJet", &dnBaseJet);
    oTree->Branch("bNumber", &Var["bNumber"]);

    mCutflow->setWeight([&] { return Var["totalWeight"]; });
    mCutflow->setFillTree(oTree);
    mCutflow->registerCut("baseline", [&] { return fabs(Var["totalWeight"]) < 1000; });
    mCutflow->registerCut(
        ">= 1 base tau", [&] { return nBaseTau >= 1; }, "NBaseTau", 3, 0, 3, [&] { return nBaseTau; });
    mCutflow->registerCut(
        ">= 1 medium tau", [&] { return nTaus >= 1; }, "nTaus", 3, 0, 3, [&] { return nTaus; });
    mCutflow->registerCut(
        ">= 1 base lep", [&] { return nBaseLep >= 1; }, "NBaseLep", 3, 0, 3, [&] { return nBaseLep; });
    mCutflow->registerCut(
        ">= 1 medium lep", [&] { return nLeps >= 1; }, "nLeps", 3, 0, 3, [&] { return nLeps; });
    mCutflow->registerCut(
        "OSLepHad", [&] { return OSLepHad; }, "OSLepHad", 2, 0, 2, [&] { return OSLepHad; });
    mCutflow->registerCut(
        ">= 1 base jet", [&] { return nBaseJet >= 1; }, "NBaseJet", 20, 0, 20, [&] { return nBaseJet; });
    mCutflow->registerCut(
        "B veto", [&] { return Var["bVeto"]; }, "bTag", 3, 0, 3, [&] { return Var["bNumber"]; });
    mCutflow->registerCut(
        "MET trigger", [&] { return Var["TriggerMatch"]; }, "MET_Trigger", 2, 0, 2, [&] { return Var["TriggerMatch"]; });
    mCutflow->registerCut(
        "MET > 200 GeV", [&] { return Var["MET"] >= 200; }, "MET_c", 240, 0, 1200, [&] { return Var["MET"]; });
    // mCutflow->registerCut("dPhite < 1.4", [&] {return Var["dPhite"] <= 1.4;},"dPhite_c",40,0,4,[&]{return Var["dPhite"];});

    // mCutflow->registerCut("Ptjet1 > 60 GeV", [&] {return Var["Ptjet1"] >= 60;},"Ptjet1_60",200,0,1000,[&]{return Var["Ptjet1"];});
    // mCutflow->registerCut("Etajet1 < 3", [&] {return Var["Etajet1"] <= 3;},"Etajet1_3",40,0,4,[&]{return Var["Etajet1"];});
    // mCutflow->registerCut("dRlt < 3.6", [&] {return Var["dRlt"] <= 3.6;},"dRlt_3.6",100,0,10,[&]{return Var["dRlt"];});

    auto lastCut = mCutflow->registerCut("the END", [&] { return true; });
    lastCut->addHist(
        "MET", 240, 0, 1200, [&] { return Var["MET"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Pttau1", 800, 0, 800, [&] { return Var["Pttau1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Ptlep1", 800, 0, 800, [&] { return Var["Ptlep1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Ptjet1", 1400, 0, 1400, [&] { return Var["Ptjet1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Ptjet2", 400, 0, 400, [&] { return Var["Ptjet2"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Mttau1", 400, 0, 400, [&] { return Var["Mttau1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Mtlep1", 400, 0, 400, [&] { return Var["Mtlep1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Mtjet1", 400, 0, 400, [&] { return Var["Mtjet1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Etatau1", 90, 0, 4.5, [&] { return Var["Etatau1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Etalep1", 90, 0, 4.5, [&] { return Var["Etalep1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Etajet1", 90, 0, 4.5, [&] { return Var["Etajet1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Rapdtau1", 100, 0, 5, [&] { return Var["Rapdtau1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Rapdlep1", 100, 0, 5, [&] { return Var["Rapdlep1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Rapdjet1", 100, 0, 5, [&] { return Var["Rapdjet1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Phitau1", 80, 0, 4, [&] { return Var["Phitau1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Philep1", 80, 0, 4, [&] { return Var["Philep1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Phijet1", 80, 0, 4, [&] { return Var["Phijet1"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Phimet", 80, 0, 4, [&] { return Var["Phimet"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dRlt", 200, 0, 10, [&] { return Var["dRlt"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dRtj", 200, 0, 10, [&] { return Var["dRtj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dRlj", 200, 0, 10, [&] { return Var["dRlj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dRte", 200, 0, 10, [&] { return Var["dRte"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dRle", 200, 0, 10, [&] { return Var["dRle"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dRje", 200, 0, 10, [&] { return Var["dRje"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dEtalt", 200, 0, 10, [&] { return Var["dEtalt"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dEtatj", 200, 0, 10, [&] { return Var["dEtatj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dEtalj", 200, 0, 10, [&] { return Var["dEtalj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dRapidlt", 200, 0, 10, [&] { return Var["dRapidlt"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dRapidtj", 200, 0, 10, [&] { return Var["dRapidtj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dRapidlj", 200, 0, 10, [&] { return Var["dRapidlj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dPhilt", 80, 0, 4, [&] { return Var["dPhilt"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dPhitj", 80, 0, 4, [&] { return Var["dPhitj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dPhilj", 80, 0, 4, [&] { return Var["dPhilj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dPhite", 80, 0, 4, [&] { return Var["dPhite"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dPhile", 80, 0, 4, [&] { return Var["dPhile"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "dPhije", 80, 0, 4, [&] { return Var["dPhije"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MTsum", 400, 0, 400, [&] { return Var["MTsum"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Meff", 200, 0, 2000, [&] { return Var["Meff"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Meff_j", 200, 0, 2000, [&] { return Var["Meff_j"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "log_Meff", 600, 4, 10, [&] { return Var["log_Meff"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "log_Meff_j", 600, 4, 10, [&] { return Var["log_Meff_j"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MT_tau", 400, 0, 400, [&] { return Var["MT_tau"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MT_lep", 400, 0, 400, [&] { return Var["MT_lep"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MT_jet", 400, 0, 400, [&] { return Var["MT_jet"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "M_inv_tl", 400, 0, 400, [&] { return Var["M_inv_tl"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "M_inv_te", 400, 0, 400, [&] { return Var["M_inv_te"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "M_inv_tj", 400, 0, 400, [&] { return Var["M_inv_tj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "M_inv_le", 400, 0, 400, [&] { return Var["M_inv_le"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "M_inv_lj", 400, 0, 400, [&] { return Var["M_inv_lj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "M_inv_je", 400, 0, 400, [&] { return Var["M_inv_je"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MT2_tl", 400, 0, 400, [&] { return Var["MT2_tl"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MT2_tj", 400, 0, 400, [&] { return Var["MT2_tj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MT2_lj", 400, 0, 400, [&] { return Var["MT2_lj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MCT_tl", 400, 0, 400, [&] { return Var["MCT_tl"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MCT_tj", 400, 0, 400, [&] { return Var["MCT_tj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MCT_lj", 400, 0, 400, [&] { return Var["MCT_lj"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MCT_te", 400, 0, 400, [&] { return Var["MCT_te"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MCT_le", 400, 0, 400, [&] { return Var["MCT_le"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "MCT_je", 400, 0, 400, [&] { return Var["MCT_je"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Truth_tau_pt", 400, 0, 400, [&] { return Var["Truth_tau_pt"]; }, Hist::USE_OVERFLOW);
    lastCut->addHist(
        "Fake_tau_pt", 400, 0, 400, [&] { return Var["Fake_tau_pt"]; }, Hist::USE_OVERFLOW);

    // GoodonelistDatabase
    GRLDB mGRLDB;
    mGRLDB.loadGRLFile("/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/run2Ditau/include/ditauMETGRL.xml");

    // Loop Start
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);
        // initialize
        Var["MET"] = 0;
        Var["totalWeight"] = 1;
        Var["bVeto"] = false;
        Var["bNumber"] = 0;
        Var["Pttau1"] = 0;
        Var["Ptlep1"] = 0;
        Var["Ptjet1"] = 0;
        Var["Ptjet2"] = 0;
        Var["Mttau1"] = 0;
        Var["Mtlep1"] = 0;
        Var["Mtjet1"] = 0;
        Var["Etatau1"] = 0;
        Var["Etalep1"] = 0;
        Var["Etajet1"] = 0;
        Var["Rapdtau1"] = 0;
        Var["Rapdlep1"] = 0;
        Var["Rapdjet1"] = 0;
        Var["Phitau1"] = 0;
        Var["Philep1"] = 0;
        Var["Phijet1"] = 0;
        Var["Phimet"] = 0;
        Var["dRlt"] = 0;
        Var["dRtj"] = 0;
        Var["dRlj"] = 0;
        Var["dRte"] = 0;
        Var["dRle"] = 0;
        Var["dRje"] = 0;
        Var["dPhilt"] = 0;
        Var["dPhitj"] = 0;
        Var["dPhilj"] = 0;
        Var["dPhite"] = 0;
        Var["dPhile"] = 0;
        Var["dPhije"] = 0;
        Var["dEtalt"] = 0;
        Var["dEtatj"] = 0;
        Var["dEtalj"] = 0;
        Var["dRapidlt"] = 0;
        Var["dRapidtj"] = 0;
        Var["dRapidlj"] = 0;
        Var["MTsum"] = 0;
        Var["Meff"] = 0;
        Var["Meff_j"] = 0;
        Var["log_Meff"] = 0;
        Var["log_Meff_j"] = 0;
        Var["MT_tau"] = 0;
        Var["MT_lep"] = 0;
        Var["MT_jet"] = 0;
        Var["M_inv_tl"] = 0;
        Var["M_inv_te"] = 0;
        Var["M_inv_tj"] = 0;
        Var["M_inv_le"] = 0;
        Var["M_inv_lj"] = 0;
        Var["M_inv_je"] = 0;
        Var["MT2_tl"] = 0;
        Var["MT2_tj"] = 0;
        Var["MT2_lj"] = 0;
        Var["MCT_tl"] = 0;
        Var["MCT_tj"] = 0;
        Var["MCT_lj"] = 0;
        Var["MCT_te"] = 0;
        Var["MCT_le"] = 0;
        Var["MCT_je"] = 0;

        Var["Truth_tau_pt"] = 0;
        Var["Fake_tau_pt"] = 0;
        Var["TriggerMatch"] = false;

        nBaseTau = 0;
        nBaseEle = 0;
        nBaseMuon = 0;
        nBaseLep = 0;
        nBaseJet = 0;
        nTaus = 0;
        nEles = 0;
        nMuons = 0;
        nLeps = 0;
        nJets = 0;
        mergedRunNumber = 0;
        mlumiBlock = 0;
        dnBaseJet = 0;
        OSLepHad = false;

        //
        Var["MET"] = tree->MetTST_met / 1000;
        Var["mcChannelNumber"] = tree->mcChannelNumber;
        Var["evtNum"] = tree->eventNumber;
        // weight
        if (tree->mcChannelNumber == 0) { //表示Data，totalWeight为1
            Var["totalWeight"] = 1;
            mergedRunNumber = tree->runNumber;
            mlumiBlock = tree->lumiBlock;
        } else { //表示MC
            mergedRunNumber = tree->RandomRunNumber;
            mlumiBlock = tree->RandomLumiBlockNumber;
        }
        if (tree->mcChannelNumber != 0) { //表示MC
            double extraWei = tree->GenWeight * tree->muWeight * tree->TauWeight * tree->EleWeight * tree->MuoWeight * tree->JetWeight;
            Var["totalWeight"] =
                MetaDB::Instance().getWeight(PhyUtils::getCompaign(mergedRunNumber), tree->mcChannelNumber, tree->SUSYFinalState, extraWei);
            if (tree->SUSYFinalState != 0) cout << "YJR: channel, SUSYfs: " << tree->mcChannelNumber << "," << tree->SUSYFinalState << endl;
            // cout<<"weights:"<<tree->GenWeight<<","<<tree->muWeight<<","<<tree->TauWeight<<","<<tree->EleWeight<<","<<tree->MuoWeight<<","<<tree->JetWeight<<";"<<Var["totalWeight"]<<endl;
        }

        ///*******************************Event construct *********************/
        AnaObjs eleVec = tree->getElectrons();
        AnaObjs muonVec = tree->getMuons();
        AnaObjs tauVec = tree->getTaus();
        AnaObjs jetVec = tree->getJets();
        Var["bVeto"] = (tree->n_BJets == 0); // b-veto
        Var["bNumber"] = tree->n_BJets;

        nBaseTau = tauVec.size();
        nBaseEle = eleVec.size();
        nBaseMuon = muonVec.size();
        nBaseLep = nBaseEle + nBaseMuon;
        nBaseJet = jetVec.size();
        dnBaseJet = nBaseJet;

        if (mergedRunNumber <= 284484) { // 15 -J
            Var["TriggerMatch"] = tree->TrigHLT_xe70_mht;
        } else if (mergedRunNumber <= 311481) { // 16 -L
            if (mergedRunNumber <= 302872) {    // A-D3
                Var["TriggerMatch"] = tree->TrigHLT_xe90_mht_L1XE50;
            } else { // D4-
                Var["TriggerMatch"] = tree->TrigHLT_xe110_mht_L1XE50;
            }
        } else if (mergedRunNumber <= 340453) { // 17 -K
            if (mergedRunNumber <= 331975) {    // B-D5
                Var["TriggerMatch"] = tree->TrigHLT_xe110_pufit_L1XE55;
            } else { // D6-K
                Var["TriggerMatch"] = tree->TrigHLT_xe110_pufit_L1XE50;
            }
        } else { // 18
            if (mergedRunNumber >= 350067) {
                Var["TriggerMatch"] = tree->TrigHLT_xe110_pufit_xe65_L1XE50;
            } else {
                Var["TriggerMatch"] = tree->TrigHLT_xe110_pufit_xe70_L1XE50;
            }
        }

        eleVec.sortByPt();
        muonVec.sortByPt();
        tauVec.sortByPt();
        jetVec.sortByPt();

        auto TruthtauVec = tauVec.filterObjects(0, 100, TauIsTruthTau);
        if (TruthtauVec.size() > 0) Var["Truth_tau_pt"] = TruthtauVec[0].Pt();
        auto FaketauVec = tauVec.filterObjects(0, 100, TauIsNotTruthTau);
        if (FaketauVec.size() > 0) Var["Fake_tau_pt"] = FaketauVec[0].Pt();

        tauVec = tauVec.filterObjects(0, 100, TauMedium);
        eleVec = eleVec.filterObjects(0, 100, EMediumLH | EIso);
        muonVec = muonVec.filterObjects(0, 100, MuMedium | MuIso);

        nTaus = tauVec.size();
        nEles = eleVec.size();
        nMuons = muonVec.size();
        nLeps = nEles + nMuons;
        auto lepVec = eleVec + muonVec;
        nJets = jetVec.size();

        if (nTaus >= 1 && nLeps >= 1 && nJets >= 1) {
            auto tau1 = tauVec[0];
            auto lep1 = lepVec[0];
            auto jet1 = jetVec[0];
            if (nEles > 0 && nMuons > 0) {
                lep1 = (eleVec[0].Pt() > muonVec[0].Pt()) ? eleVec[0] : muonVec[0];
            }
            TLorentzVector METVec;
            METVec.SetPtEtaPhiE(Var["MET"], 0, tree->MetTST_phi, Var["MET"]);
            OSLepHad = tau1.isOS(lep1);
            Var["Pttau1"] = tau1.Pt();
            Var["Ptlep1"] = lep1.Pt();
            Var["Ptjet1"] = jet1.Pt();
            if (nJets >= 2) Var["Ptjet2"] = jetVec[1].Pt();
            Var["Mttau1"] = tau1.Mt();
            Var["Mtlep1"] = lep1.Mt();
            Var["Mtjet1"] = jet1.Mt();
            Var["Etatau1"] = fabs(tau1.Eta());
            Var["Etalep1"] = fabs(lep1.Eta());
            Var["Etajet1"] = fabs(jet1.Eta());
            Var["Rapdtau1"] = fabs(tau1.Rapidity());
            Var["Rapdlep1"] = fabs(lep1.Rapidity());
            Var["Rapdjet1"] = fabs(jet1.Rapidity());
            Var["Phitau1"] = fabs(tau1.Phi());
            Var["Philep1"] = fabs(lep1.Phi());
            Var["Phijet1"] = fabs(jet1.Phi());
            Var["Phimet"] = fabs(tree->MetTST_phi);

            Var["dRlt"] = PhyUtils::deltaR(tau1, lep1);
            Var["dRtj"] = PhyUtils::deltaR(tau1, jet1);
            Var["dRlj"] = PhyUtils::deltaR(lep1, jet1);
            Var["dRte"] = PhyUtils::deltaR(tau1, METVec);
            Var["dRle"] = PhyUtils::deltaR(lep1, METVec);
            Var["dRje"] = PhyUtils::deltaR(jet1, METVec);

            Var["dEtalt"] = PhyUtils::deltaEta(tau1, lep1);
            Var["dEtatj"] = PhyUtils::deltaEta(tau1, jet1);
            Var["dEtalj"] = PhyUtils::deltaEta(lep1, jet1);
            Var["dRapidlt"] = PhyUtils::deltaRapidity(tau1, lep1);
            Var["dRapidtj"] = PhyUtils::deltaRapidity(tau1, jet1);
            Var["dRapidlj"] = PhyUtils::deltaRapidity(lep1, jet1);

            Var["dPhilt"] = PhyUtils::deltaPhi(tau1, lep1);
            Var["dPhitj"] = PhyUtils::deltaPhi(tau1, jet1);
            Var["dPhilj"] = PhyUtils::deltaPhi(lep1, jet1);
            Var["dPhite"] = PhyUtils::deltaPhi(tau1, METVec);
            Var["dPhile"] = PhyUtils::deltaPhi(lep1, METVec);
            Var["dPhije"] = PhyUtils::deltaPhi(jet1, METVec);

            Var["MTsum"] = tau1.Mt() + lep1.Mt() + jet1.Mt();
            Var["Meff"] = tau1.Pt() + lep1.Pt() + Var["MET"];
            Var["Meff_j"] = tau1.Pt() + lep1.Pt() + jet1.Pt() + Var["MET"];
            Var["log_Meff"] = std::log(Var["Meff"]);
            Var["log_Meff_j"] = std::log(Var["Meff_j"]);

            Var["MT_tau"] = PhyUtils::calcMT(tau1, METVec);
            Var["MT_lep"] = PhyUtils::calcMT(lep1, METVec);
            Var["MT_jet"] = PhyUtils::calcMT(jet1, METVec);

            Var["M_inv_tl"] = PhyUtils::calcMll(tau1, lep1);
            Var["M_inv_te"] = PhyUtils::calcMll(tau1, METVec);
            Var["M_inv_tj"] = PhyUtils::calcMll(tau1, jet1);
            Var["M_inv_le"] = PhyUtils::calcMll(lep1, METVec);
            Var["M_inv_lj"] = PhyUtils::calcMll(lep1, jet1);
            Var["M_inv_je"] = PhyUtils::calcMll(jet1, METVec);

            Var["MT2_tl"] = PhyUtils::MT2(tau1, lep1, METVec, 0);
            Var["MT2_tj"] = PhyUtils::MT2(tau1, jet1, METVec, 0);
            Var["MT2_lj"] = PhyUtils::MT2(lep1, jet1, METVec, 0);

            Var["MCT_tl"] = PhyUtils::calcMCT(tau1, lep1);
            Var["MCT_tj"] = PhyUtils::calcMCT(tau1, jet1);
            Var["MCT_lj"] = PhyUtils::calcMCT(lep1, jet1);
            Var["MCT_te"] = PhyUtils::calcMCT(tau1, METVec);
            Var["MCT_le"] = PhyUtils::calcMCT(lep1, METVec);
            Var["MCT_je"] = PhyUtils::calcMCT(jet1, METVec);
        }
        fillRegions();
        fillCutflows();
    }
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
