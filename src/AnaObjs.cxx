#include "AnaObjs.h"

#include <bitset>

#include "PhyUtils.h"

AnaObj operator+(const AnaObj& lhs, const AnaObj& rhs) {
    const TLorentzVector& tlhs = lhs;
    return AnaObj(COMBINED, tlhs + rhs, lhs.charge() + rhs.charge(), 0, lhs.Mt() + rhs.Mt());
}

AnaObjs operator+(const AnaObjs& lhs, const AnaObjs& rhs) {
    AnaObjs combined;
    combined.assign(lhs.begin(), lhs.end());
    for (auto& cand : rhs) {
        bool noOverlap = true;
        for (auto& candExist : combined) {
            if ((cand.type() == candExist.type()) && (cand.Pt() == candExist.Pt()) && (cand.Eta() == candExist.Eta()) &&
                (cand.Phi() == candExist.Phi()) && (cand.E() == candExist.E()) && (cand.charge() == candExist.charge())) {
                noOverlap = false;
                break;
            }
        }
        if (noOverlap) {
            combined.emplace_back(cand);
        }
    }
    combined.sortByPt();
    return combined;
}

AnaObjs AnaObjs::filterObjects(float ptCut, float etaCut, int id, unsigned int maxNum) {
    AnaObjs reducedList;
    unsigned int cnt = 0;
    for (auto&& cand : *this) {
        if ((cand.Pt() >= ptCut) && (fabs(cand.Eta()) < etaCut) && (cand.passID(id))) reducedList.emplace_back(cand);
        cnt++;
        if (cnt == maxNum) break;
    }
    reducedList.sortByPt();
    return reducedList;
}

AnaObjs AnaObjs::filterObjects(float ptCut, std::string propertyCut) {
    AnaObjs reducedList;
    for (auto&& cand : *this) {
        if (cand.getProperty(propertyCut) && (cand.Pt() >= ptCut)) reducedList.emplace_back(cand);
    }
    reducedList.sortByPt();
    return reducedList;
}

std::pair<AnaObjs, AnaObjs> AnaObjs::splitObjects(float ptCut, float etaCut, int id, int maxNum) {
    AnaObjs reducedList1, reducedList2;
    for (auto&& cand : *this) {
        if ((cand.Pt() >= ptCut) && (fabs(cand.Eta()) < etaCut) && (cand.passID(id)) && reducedList1.size() < maxNum) {
            reducedList1.emplace_back(cand);
        } else {
            reducedList2.emplace_back(cand);
        }
    }
    reducedList1.sortByPt();
    reducedList2.sortByPt();
    return std::make_pair(reducedList1, reducedList2);
}

int AnaObjs::countObjects(float ptCut, float etaCut, int id) {
    int count = 0;
    for (const auto& cand : *this) {
        if (cand.Pt() >= ptCut && fabs(cand.Eta()) < etaCut && cand.passID(id)) count++;
    }
    return count;
}

float AnaObjs::sumObjectsPt(unsigned int maxNum, float ptCut) {
    float sum = 0;
    for (int ii = 0; ii < std::min((int)maxNum, (int)this->size()); ii++)
        if (this->at(ii).Pt() > ptCut) sum += this->at(ii).Pt();
    return sum;
}

float AnaObjs::sumObjectsM(unsigned int maxNum, float mCut) {
    float sum = 0;
    for (int ii = 0; ii < std::min((int)maxNum, (int)this->size()); ii++)
        if (this->at(ii).M() > mCut) sum += this->at(ii).M();
    return sum;
}

AnaObj AnaObjs::sumObjects() {
    AnaObj tot;
    for (int ii = 0; ii < (int)this->size(); ii++) tot += this->at(ii);
    return tot;
}

float AnaObjs::minDphi(float ptCut) {
    float dPhiMin = 1e10;
    if (this->size() < 2) {
        return dPhiMin;
    }
    for (unsigned int i = 1; i < this->size(); i++) {
        if (this->at(i).Pt() < ptCut) continue;
        for (unsigned int j = 0; j < i; j++) {
            if (this->at(j).Pt() < ptCut) continue;
            float dPhiTmp = PhyUtils::deltaPhi(this->at(i), this->at(j));
            if (dPhiMin > dPhiTmp) {
                dPhiMin = dPhiTmp;
            }
        }
    }
    return dPhiMin;
}

float AnaObjs::minDR(float ptCut) {
    float dRMin = 1e10;
    if (this->size() < 2) {
        return dRMin;
    }
    for (unsigned int i = 1; i < this->size(); i++) {
        if (this->at(i).Pt() < ptCut) continue;
        for (unsigned int j = 0; j < i; j++) {
            if (this->at(j).Pt() < ptCut) continue;
            float dRTmp = PhyUtils::deltaR(this->at(i), this->at(j));
            if (dRMin > dRTmp) {
                dRMin = dRTmp;
            }
        }
    }
    return dRMin;
}

AnaObjs AnaObjs::filterCrack(float minEta, float maxEta) {
    AnaObjs reducedList;
    for (const auto& cand : *this) {
        if (fabs(cand.Eta()) < minEta || fabs(cand.Eta()) > maxEta) reducedList.emplace_back(cand);
    }
    return reducedList;
}

AnaObjs AnaObjs::lowMassRemoval(std::function<bool(const AnaObj&, const AnaObj&)> RemoveSwitch, float MinMass, float MaxMass, int type) {
    AnaObjs reducedList;
    unsigned int NObj = this->size();
    std::vector<bool> Good(NObj, true);

    AnaObjs::const_iterator begin_cand = this->begin(), end_cand = this->end();
    std::vector<bool>::iterator good_begin = Good.begin(), good_end = Good.end();

    std::vector<bool>::iterator good = good_begin;
    for (AnaObjs::const_iterator obj = begin_cand; obj != end_cand; ++obj) {
        // Check if the object is already rejected
        // if (!Good.at(obj)) continue;
        const AnaObj& Object = (*obj);
        std::vector<bool>::iterator good1 = good_begin;
        for (AnaObjs::const_iterator obj1 = begin_cand; obj1 != obj; ++obj1) {
            // if (!Good.at(obj1)) continue;
            // Remove the pair
            const AnaObj& Object1 = (*obj1);
            float InvMass = (Object + Object1).M();
            if (MinMass < InvMass && InvMass < MaxMass && RemoveSwitch(Object, Object1)) {
                (*good) = false;
                (*good1) = false;
            }
            ++good1;
        }
        ++good;
    }
    good = good_begin;

    for (AnaObjs::const_iterator obj = begin_cand; obj != end_cand; ++obj) {
        bool Continue = !(*good);
        ++good;
        if (Continue) continue;
        if (type == -1 || obj->type() == type) reducedList.emplace_back(*obj);
    }
    return reducedList;
}

TVectorD AnaObjs::calcEigenValues() {
    TMatrixDSym momTensor(3);
    TVectorD eigenValues(3);
    momTensor.Zero();
    float norm = 0;
    for (const auto& jet : *this) {
        momTensor(0, 0) += jet.Px() * jet.Px();
        momTensor(0, 1) += jet.Px() * jet.Py();
        momTensor(0, 2) += jet.Px() * jet.Pz();
        momTensor(1, 0) += jet.Py() * jet.Px();
        momTensor(1, 1) += jet.Py() * jet.Py();
        momTensor(1, 2) += jet.Py() * jet.Pz();
        momTensor(2, 0) += jet.Pz() * jet.Px();
        momTensor(2, 1) += jet.Pz() * jet.Py();
        momTensor(2, 2) += jet.Pz() * jet.Pz();
        norm += jet.Vect().Mag2();
    }
    momTensor *= 1. / norm;
    momTensor.EigenVectors(eigenValues);
    return eigenValues;
}

float AnaObjs::aplanarity() {
    if (this->size() < 2) return 0;
    TVectorD eigenValues = calcEigenValues();
    return 1.5 * eigenValues(2);
}

float AnaObjs::sphericity() {
    if (this->size() < 2) return 0;
    TVectorD eigenValues = calcEigenValues();
    return 1.5 * (eigenValues(1) + eigenValues(2));
}

std::vector<float> AnaObjs::getProperties(std::string name) {
    std::vector<float> outList;
    for (auto& o : *this) {
        outList.emplace_back(o.getProperty(name));
    }
    return outList;
}

AnaObjs AnaObjs::passTrig(bool trigVar, float offLinePt) {
    AnaObjs outList;
    if (trigVar) {
        for (auto&& o : *this) {
            if (o.Pt() >= offLinePt) {
                outList.emplace_back(o);
            }
        }
    }
    return outList;
}

void printObject(const AnaObj& obj) {
    std::cout << " Pt: " << obj.Pt() << " Phi: " << obj.Phi() << " Eta:" << obj.Eta() << " charge: " << obj.charge()
              << " id: " << std::bitset<16>(obj.id()) << " type:" << obj.type() << std::endl;
}

void printObjects(const AnaObjs& objs) {
    int ii = 0;
    for (const auto& obj : objs) {
        std::cout << " " << ii;
        printObject(obj);
        ii++;
    }
}
