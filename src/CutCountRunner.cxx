#include "CutCountRunner.h"

#include <algorithm>

#include "PhyUtils.h"

void CutCountRunner::addStep(std::string varName, std::vector<float> steps, char compare) {
    if (steps.size() == 0) {
        LOG(ERROR) << "Error: No step in the cut: " << varName << ". Will not do anything";
        return;
    }
    std::sort(steps.begin(), steps.end());
    this->varNames.emplace_back(varName);
    this->stepPools.emplace_back(steps);

    if (compare == '>') {
        this->compareFuncPools.emplace_back(true);
    } else {
        this->compareFuncPools.emplace_back(false);
    }
}

void CutCountRunner::readSamples(std::vector<std::string> chains) { // read all tree names in a chain, then recognize signal by config file.
    std::vector<std::string> bkgsampleNames = AnaConfigReader::Instance().readKey<std::vector<std::string>>("BkgSamples");
    std::string sigSampleName = AnaConfigReader::Instance().readKey<std::string>("SigSample");
    if (std::find(chains.begin(), chains.end(), sigSampleName) == chains.end()) {
        LOG(FATAL) << "No signal tree named " << sigSampleName << " is found in the target processed trees. Could "
                   << "not do CutCount. Program terminalted";
    }

    for (auto name : bkgsampleNames) {
        if (std::find(chains.begin(), chains.end(), name) == chains.end()) {
            LOG(ERROR) << "No tree named " << name << " is found in the target processed trees. This tree is skiped";
            continue;
        }
        LOG(INFO) << "Read Bkg Sample:" << name;
        Looper_Ptr looper = getLooper(name, this->varNames);
        looper->run(); // read all entry's variables' value and weight, store in eventTables.
        auto sampleTable = looper->returnTables();
        this->bkgSamples[name] = sampleTable;
    }
    LOG(INFO) << "Read Signal Sample:" << sigSampleName;
    Looper_Ptr looper = getLooper(sigSampleName, this->varNames);
    looper->run();
    auto sigSampleTable = looper->returnTables();
    this->signalSample = std::make_pair(sigSampleName, sigSampleTable);
    // We added sample weight in the looper->run() function, so we should add an extra 'weight' column
    this->varNames.emplace_back("weight"); //////////          a col of varNames
}

void CutCountRunner::preparePool() {
    LOG(INFO) << "Prepare CutCount pools...";
    // We don't need the totalSize of first ele because it nerver been used.
    // More infos about _stepSizeHelper could be found at the comments in CutCountVecFiller::fillSingleEntry
    int totalSize(1);
    std::string stepStr = "The CouCount will run ";
    for (int i = this->stepPools.size() - 1; i > 0; i--) { // read [1,N-1] element vectors for their length.
        totalSize *= this->stepPools[i].size();
        stepStr = stepStr + std::to_string(this->stepPools[i].size()) + " * ";
        this->_stepSizeHelper.emplace_back(totalSize);
    }
    std::reverse(this->_stepSizeHelper.begin(), this->_stepSizeHelper.end());
    totalSize *= stepPools[0].size();
    stepStr = stepStr + std::to_string(this->stepPools[0].size()) + " = " + std::to_string(totalSize) + " steps";
    LOG(INFO) << stepStr;
    for (auto iter : this->bkgSamples) {
        std::vector<float> cutcountPool(totalSize, 0);
        std::vector<float> cutcountErrPool(totalSize, 0);
        std::vector<int> cutcountRawnumPool(totalSize, 0);
        this->_cutcountPools[iter.first] = cutcountPool; // iter.first is the string index of these bkgSample, aka tree name "process_Nom"
        this->_cutcountErrPools[iter.first] = cutcountErrPool;
        this->_cutcountRawnumPools[iter.first] = cutcountRawnumPool;
    }
    std::vector<float> cutcountPool(totalSize, 0);
    std::vector<float> cutcountErrPool(totalSize, 0);
    std::vector<int> cutcountRawnumPool(totalSize, 0);
    this->_cutcountPools[this->signalSample.first] = cutcountPool; // add Pool for sig nominal tree
    this->_cutcountErrPools[this->signalSample.first] = cutcountErrPool;
    this->_cutcountRawnumPools[this->signalSample.first] = cutcountRawnumPool;
}

void CutCountRunner::startCut() {
    preparePool();
    unsigned int threadNum = AnaConfigReader::Instance().getThreadNum();
    unsigned int sampleNum = this->_cutcountPools.size(); // aka nominal tree numbers
    threadNum = (threadNum > sampleNum) ? sampleNum : threadNum;
    LOG(INFO) << "Start the CutCount with " << threadNum << " Threads";
    if (threadNum == 1) {
        std::unique_ptr<CutCountVecFiller> sigFiller(
            new CutCountVecFiller(this->signalSample.first, &this->signalSample.second, this->stepPools, this->_stepSizeHelper,
                                  this->compareFuncPools, &this->_cutcountPools[this->signalSample.first],
                                  &this->_cutcountErrPools[this->signalSample.first], &this->_cutcountRawnumPools[this->signalSample.first]));
        sigFiller->run();

        for (auto&& sample : this->bkgSamples) {
            std::unique_ptr<CutCountVecFiller> filler(new CutCountVecFiller(
                sample.first, &sample.second, this->stepPools, this->_stepSizeHelper, this->compareFuncPools, &this->_cutcountPools[sample.first],
                &this->_cutcountErrPools[sample.first], &this->_cutcountRawnumPools[sample.first]));
            filler->run();
        }
    } else {
        std::unique_ptr<ThreadPool> pool(new ThreadPool(threadNum));
        pool->addTask(std::unique_ptr<CutCountVecFiller>(
            new CutCountVecFiller(this->signalSample.first, &this->signalSample.second, this->stepPools, this->_stepSizeHelper,
                                  this->compareFuncPools, &this->_cutcountPools[this->signalSample.first],
                                  &this->_cutcountErrPools[this->signalSample.first], &this->_cutcountRawnumPools[this->signalSample.first])));

        for (auto&& sample : this->bkgSamples) {
            pool->addTask(std::unique_ptr<CutCountVecFiller>(new CutCountVecFiller(
                sample.first, &sample.second, this->stepPools, this->_stepSizeHelper, this->compareFuncPools, &this->_cutcountPools[sample.first],
                &this->_cutcountErrPools[sample.first], &this->_cutcountRawnumPools[sample.first])));
        }
        pool->shutDownPool();
    }
}

// Todo : Could skip some steps if a loose cut already not meet the constraint
void CutCountRunner::printOut() {
    LOG(INFO) << "CutCount is done, now filter the results and get the cut combinations";
    // output file
    std::string outname = this->_name + ".csv";
    std::ofstream cutcout(outname);
    // 1st line: Zn, Signal, totalBkg, var1, var2, ..., varn
    cutcout << "Zn"
            << ", Signal"
            << ", totalBkg,";
    for (int j = 0; j < varNames.size() - 2; j++) {
        cutcout << varNames[j] << ", ";
    }
    cutcout << varNames[varNames.size() - 2] << std::endl; // because varNames[varNames.size() - 1] is "weight"
    // Get the signal sample name
    std::string sigName = this->signalSample.first;
    // Take the first vec to get size and etc
    auto firstVec = this->_cutcountPools.begin()->second; // a vector, length is totolsize
    for (int i = 0; i < firstVec.size(); i++) {
        float totalBkg(0), totalBkgError(0), totalBKGrawnum(0);
        float sigYield(0), sigError(0);
        bool badStep(false);
        for (const auto& [sName, sYields] :
             this->_cutcountPools) { // sName is tree name, sYields is yield vector for the tree. In this loop, filter every tree seperately.
            float yield = sYields[i];
            float error = this->_cutcountErrPools[sName][i];
            float rawnum = this->_cutcountRawnumPools[sName][i];
            auto yieldCon = this->yieldsLevel.find(sName); // the filter set in userfile for this tree.
            auto rawnumCon = this->rawnumLevel.find(sName);
            if (yieldCon != this->yieldsLevel.end() && yield <= yieldCon->second) {
                badStep = true;
                break; // break the further process for this cut step
            }
            if (rawnumCon != this->rawnumLevel.end() && rawnum < rawnumCon->second) {
                badStep = true;
                break; // break the further process for this cut step
            }
            auto errorCon = this->relErrorLevel.find(sName);
            if (errorCon != this->relErrorLevel.end() && (error / yield) >= errorCon->second) {
                badStep = true;
                break; // break the further process for this cut step
            }
            if (sName == sigName) {
                sigYield += yield;
                sigError = sqrt(sigError * sigError + error * error);
            } else {
                totalBkg += yield;
                totalBKGrawnum += rawnum;
                totalBkgError = sqrt(totalBkgError * totalBkgError + error * error);
            }
        }
        if (!badStep) {
            // Total Bkg constraint
            auto totalBkgCon = this->yieldsLevel.find("totalBkg");
            if (totalBkgCon != this->yieldsLevel.end() && totalBkg <= totalBkgCon->second) {
                continue;
            }
            auto totalBkgErrCon = this->relErrorLevel.find("totalBkg");
            if (totalBkgErrCon != this->relErrorLevel.end() && (totalBkgError / totalBkg) >= totalBkgErrCon->second) {
                continue;
            }
            auto signalErrCon = this->relErrorLevel.find("signal");
            if (signalErrCon != this->relErrorLevel.end() && (sigError / sigYield) >= signalErrCon->second) {
                continue;
            }
            // Zn constraint
            float Zn = PhyUtils::calZn(sigYield, totalBkg,
                                       sqrt(totalBkgError * totalBkgError / totalBkg / totalBkg + this->bkgExtraSyst * this->bkgExtraSyst));
            if (Zn >= this->ZnLevel) {
                // Get the N dimension posi for this 1 dimension vector using the helper func
                std::vector<int> posis;
                posis.reserve(this->_stepSizeHelper.size() + 1);
                int posi1D = i;
                for (auto size : this->_stepSizeHelper) {
                    int posiX = posi1D / size;
                    posi1D = posi1D % size;
                    posis.emplace_back(posiX);
                }
                // print Zn and yields info
                //                cutcout << "Zn: " << Zn << ", Signal: " << sigYield << " +- " << sigError << ", totalBkg: " << totalBkg << " +- " <<
                //                totalBkgError
                cutcout << Zn << "," << sigYield << " +- " << sigError << "," << totalBkg << " +- " << totalBkgError << "(" << totalBKGrawnum << ")"
                        << ", ";
                // print out Cut Combinations
                for (int j = 0; j < varNames.size() - 2; j++) {
                    cutcout << stepPools[j][posis[j]] << ", ";
                }
                //                cutcout << varNames[varNames.size() - 2] << ": " << stepPools.back().at(posi1D) << std::endl;
                cutcout << stepPools.back().at(posi1D) << std::endl;
            }
        }
    }
}
