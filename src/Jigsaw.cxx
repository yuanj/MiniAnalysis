#include "Jigsaw.h"
// using LorentzVector = ROOT::Math::LorentzVector;
using PxPyPzEVector = ROOT::Math::PxPyPzEVector;
using PtEtaPhiMVector  = ROOT::Math::PtEtaPhiMVector ;
using PxPyPzEVector = ROOT::Math::PxPyPzEVector;
using Boost = ROOT::Math::Boost;


// std::pair<PxPyPzEVector, PxPyPzEVector> 
void JigsawTool::reco_neutralinos(double mu){
    // apply the invisible mass jigsaw rule, choose MI as the smallest possible quantity.
    double MV = (this->_p4_Va + this->_p4_Vb).M();
    double MI = sqrt( MV * MV + 4 * mu * mu );
    // apply the invisible rapidity jigsaw rule, choosing pIz using V.
    auto frame_M_min = this->_p4_Va + this->_p4_Vb;
    if (this->_jets_available) frame_M_min += this->_p4_jets;
    PtEtaPhiMVector p4_I_xyz(this->_p4_I.Pt(), frame_M_min.Eta(), this->_p4_I.Phi(), MI);

    PtEtaPhiMVector p4_F = p4_I_xyz + this->_p4_Va + this->_p4_Vb;
    Boost boostF(p4_F.BoostToCM());
    auto p4_V_F = boostF(this->_p4_Va + this->_p4_Vb);
    double E_V_F = p4_V_F.E(); 
    
    double c = (1 + sqrt(E_V_F*E_V_F - MV*MV + MI*MI) / E_V_F) / 2;
    auto p4_Va_F = boostF(this->_p4_Va);
    auto p4_Vb_F = boostF(this->_p4_Vb);
    
    auto p3_Ia_F = (c-1) * p4_Va_F.Vect() - c * p4_Vb_F.Vect();
    auto p3_Ib_F = (c-1) * p4_Vb_F.Vect() - c * p4_Va_F.Vect();
    double e_Ia_F = (c-1) * p4_Va_F.E() + c * p4_Vb_F.E();
    double e_Ib_F = (c-1) * p4_Vb_F.E() + c * p4_Va_F.E();
    PxPyPzEVector p4_Ia_F(p3_Ia_F.x(),p3_Ia_F.y(),p3_Ia_F.z(),e_Ia_F);
    PxPyPzEVector p4_Ib_F(p3_Ib_F.x(),p3_Ib_F.y(),p3_Ib_F.z(),e_Ib_F);
    auto boostF_Inv = boostF.Inverse();
    PxPyPzEVector p4_Ia = boostF_Inv(p4_Ia_F);
    PxPyPzEVector p4_Ib = boostF_Inv(p4_Ib_F);
    
    // assign
    this->_p4_Ia = p4_Ia;
    this->_p4_Ib = p4_Ib;
    // return void; //std::make_pair(p4_Ia, p4_Ib);
}


// double Jigsaw::get_MC2(){
//     auto frame_Va_stst = this->boostLabToStSt(this->_frame_Va);
//     auto frame_Vb_stst = this->boostLabToStSt(this->_frame_Vb);
//     double mc2 = 2 * ( frame_Va_stst.E()*frame_Vb_stst.E() + frame_Va_stst.Vect()*frame_Vb_stst.Vect() );
//     return mc2;
// }

// double Jigsaw::get_MI2_variant(double mu){
//     auto frame_V_stst = this->boostLabToStSt(this->_frame_V);
//     double MI2 = this->get_MV()*this->get_MV() + 4 * mu * mu * frame_V_stst.E() * frame_V_stst.E() / this->get_MC2();
//     return MI2;
// }