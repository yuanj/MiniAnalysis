#include "Utils.h"

#include <cmath>

#include "easylogging++.h"

// no rounding
void Utils::roundCSV(std::ofstream& stream, double num, double err) {
    stream << std::setprecision(std::numeric_limits<double>::digits10 + 1) << num << " +- "
           << std::setprecision(std::numeric_limits<double>::digits10 + 1) << err;
}

// pdg rounding rule
// int first_n_digits(double num, int n=3){
//    if(num<1){
//        return num / pow(10, ( (int)std::log10(num) -n ));
//    } else{
//        return num / pow(10, ( (int)std::log10(num) -n + 1 ));
//    }
//}
// double DRound(double num, int decdigit){// dec is the number of digits after point
//    return round(num * pow(10,decdigit) )/pow(10,decdigit);
//}
// void Utils::roundCSV(std::ofstream& stream, double num, double err) {
//    int err_head = first_n_digits(err);
//    int effd,digit,decdigit;
//    if(err_head <= 354){//100-354
//        effd = 2;
//        decdigit = effd ; // (int)log10(err) : 0.12 ~ 0; 0.012 ~ -1; 1.2 ~ 0;
//        if(err < 1) { decdigit = effd - (int)log10(err); } // digits after point
//        else{ decdigit = effd - (int)log10(err) -1; }
//        num = DRound(num,decdigit);
//        err = DRound(err,decdigit);
//    }else if(err_head <= 949){//355-949
//        effd = 1;
//        decdigit = effd ;
//        if(err < 1) { decdigit = effd - (int)log10(err); }
//        else{ decdigit = effd - (int)log10(err) -1; }
//        num = DRound(num,decdigit);
//        err = DRound(err,decdigit);
//    }else{//950-999
//        effd = 2;
//        decdigit = effd ;
//        if(err < 1) { decdigit = effd - (int)log10(err); }
//        else{ decdigit = effd - (int)log10(err) -1; }
//        num = DRound(num,decdigit);
//        err = DRound(err,decdigit-1);// round up to 1000
//    }
//    if(decdigit>=0){
//        stream << std::fixed << std::setprecision(decdigit) << num << " +- " << std::fixed << std::setprecision(decdigit) << err;
//    }else{
//        stream << std::scientific << std::setprecision(effd-1) << num << " +- " << std::scientific << std::setprecision(effd-1) << err;
//    }
//}
// if err >  0.095, will round the num and err to 2 digist after the point.
//    if (err >= 0.095 && err < 9999.945) {
//        // if num > 9999.945, will not round sum
//        if (num >= 9999.945) {
//            int digit = getIntDigit(num) + 2;
//            stream << std::resetiosflags(std::ios::fixed) << std::setprecision(digit) << num << " +- " << std::fixed << std::setprecision(2) << err;
//        } else {
//            stream << std::fixed << std::setprecision(2) << num << " +- " << err;
//        }
//    } else {
//        // else, will be kind of difficult and will round by hand
//        stream << std::resetiosflags(std::ios::fixed) << std::setprecision(6) << num << " +- " << err;
//    }

void Utils::roundLatex(std::ofstream& stream, double num, double err) {
    // if err >  0.095, will round the num and err to 2 digist after the point.
    if (err >= 0.095 && err < 9999.945) {
        // if num > 9999.945, will not round sum
        if (num >= 9999.945) {
            int digit = getIntDigit(num);
            stream << "$  " << std::resetiosflags(std::ios::fixed) << std::setprecision(digit) << num << " \\pm " << std::fixed
                   << std::setprecision(2) << err << "  $";
        } else {
            stream << std::fixed << std::setprecision(2) << "$  " << num << " \\pm " << err << "  $";
        }
    } else {
        // else, will be kind of difficult and will round by hand
        stream << std::resetiosflags(std::ios::fixed) << std::setprecision(6) << "$  " << num << " \\pm " << err << "  $";
    }
}

std::tuple<double, double> Utils::addYields(std::tuple<double, double> yields1, std::tuple<double, double> yields2) {
    double totalYields = std::get<0>(yields1) + std::get<0>(yields2);
    double totalError = sqrt(std::get<1>(yields1) * std::get<1>(yields1) + std::get<1>(yields2) * std::get<1>(yields2));
    return {totalYields, totalError};
}

std::vector<std::vector<std::tuple<double, double>>> Utils::getYieldsTable(std::string filename, std::initializer_list<std::string> excludeNameReg,
                                                                           std::initializer_list<std::string> includeNameReg) {
    std::ifstream fileIn(filename);
    CHECK(fileIn) << filename << " don't exist! Please Check!";
    std::string strLine;
    std::vector<std::vector<std::tuple<double, double>>> outTable;
    std::regex yieldsMatcher("\\s*(\\d+\\.*\\d*[eE\\+-]*\\d*)\\s*\\+-\\s*(\\d+\\.*\\d*[eE\\+-]*\\d*)\\s*");
    // exclude/only include lines which matches reg pattern
    bool excludeLine(false), includeLine(false);
    std::vector<std::regex> excludeReg, includeReg;
    if (excludeNameReg.size() != 0) {
        excludeLine = true;
        for (const auto& regStr : excludeNameReg) {
            excludeReg.emplace_back(std::regex(regStr));
        }
    }
    if (includeNameReg.size() != 0) {
        includeLine = true;
        for (const auto& regStr : includeNameReg) {
            includeReg.emplace_back(std::regex(regStr));
        }
    }

    while (std::getline(fileIn, strLine)) {
        // Ignore empty line
        if (strLine.empty()) {
            continue;
        }

        // If we have the option for include/exclude lines, first do include then do the exclude
        bool ignoreLine(false);
        if (includeLine) {
            ignoreLine = true;
            for (const auto& reg : includeReg) {
                if (regexMatch(reg, strLine).size() != 0) {
                    ignoreLine = false;
                    break;
                }
            }
        }
        if (excludeLine) {
            for (const auto& reg : excludeReg) {
                if (regexMatch(reg, strLine).size() != 0) {
                    ignoreLine = true;
                    break;
                }
            }
        }
        if (ignoreLine) {
            continue;
        }

        auto colums = splitStrBy(strLine, ',');
        std::vector<std::tuple<double, double>> yields;
        for (const auto& ele : colums) {
            auto matchResult = regexMatch(yieldsMatcher, ele);
            if (matchResult.size() != 0) {
                std::tuple<double, double> yieldEle(std::stod(matchResult[1]), std::stod(matchResult[2]));
                yields.emplace_back(yieldEle);
            }
        }
        if (yields.size() == 0) {
            continue;
        } else {
            // check if the sizes are same for each lines when emplacing back lines
            if (outTable.size() == 0 || (yields.size() == outTable.back().size())) {
                outTable.emplace_back(yields);
            } else {
                LOG(ERROR) << "The decoded colum for line '" << strLine << "' is not same as others, ignore it!";
            }
        }
    }
    return outTable;
}

std::string Utils::getTotalYields(std::string filename, std::initializer_list<std::string> excludeNameReg,
                                  std::initializer_list<std::string> includeNameReg) {
    auto numTables = getYieldsTable(filename, excludeNameReg, includeNameReg);
    if (numTables.size() == 0) {
        LOG(ERROR) << "No a single line match the yields format ' $yields +- $error'! return empty str";
        return "";
    }
    std::string outStr = "Total yields";
    for (int i = 0; i < numTables[0].size(); i++) {
        std::tuple<double, double> totalYield(0, 0);
        for (const auto& line : numTables) {
            totalYield = addYields(totalYield, line[i]);
        }
        outStr = outStr + ", " + std::to_string(std::get<0>(totalYield)) + " +- " + std::to_string(std::get<1>(totalYield));
    }
    return outStr;
}

std::string Utils::eraseToEnd(std::string target, std::string src) {
    int pos = target.rfind(src);
    if (pos > -1) {
        int sEnd = target.size();
        return target.erase(pos, sEnd);
    } else {
        LOG(WARNING) << "Warning: target string \"" << target << "\" dosen't contain \"" << src << "\" ! Return target string";
    }
    return target;
}

std::vector<int> Utils::vecRange(int end) {
    if (end <= 0) {
        return std::vector<int>();
    }
    std::vector<int> outVec(end);
    int i = 0;
    std::generate(outVec.begin(), outVec.end(), [&]() { return i++; });
    return outVec;
}

std::vector<int> Utils::vecRange(int begin, int end, int step) {
    int vecSize;
    if (step == 0) {
        return std::vector<int>();
    }
    if (step > 0) {
        // typically the int divide is round down like 5/3 = 1, 10/3 = 3.
        // But for this case the vecSize int divide should use round up. Like 5/3 = 2, 10/3 = 4
        vecSize = (end - begin + step - 1) / step;
    } else {
        vecSize = (end - begin + step + 1) / step;
    }
    if (vecSize <= 0) {
        return std::vector<int>();
    }
    std::vector<int> outVec(vecSize);
    // 'return x += 1' is not like 'return x++', the ++ operater will do after the return while the += is before
    begin = begin - step;
    std::generate(outVec.begin(), outVec.end(), [&]() { return begin += step; });
    return outVec;
}

std::vector<std::string> Utils::splitStrBy(std::string names, char symbol) {
    std::vector<std::string> result;
    std::stringstream ss(names);
    while (ss.good()) {
        std::string substr;
        getline(ss, substr, symbol);
        result.emplace_back(substr);
    }
    return result;
}

std::vector<std::string> Utils::regexMatch(const std::regex& pattern, std::string ip) {
    std::smatch result;
    std::vector<std::string> ret;
    bool valid = std::regex_match(ip, result, pattern);
    LOG(DEBUG) << "regexMatch: " << ip << "  " << (valid ? "valid" : "invalid");
    if (valid) {
        for (unsigned int i = 0; i < result.size(); i++) {
            ret.emplace_back(result[i]);
        }
    }
    return ret;
}

std::vector<std::string> Utils::getTreeList(std::string filename, std::string filter) {
    std::vector<std::string> outlist;
    std::regex pattern(filter);
    TFile f(filename.c_str());
    if (f.IsZombie()) {
        LOG(FATAL) << "Error opening file";
    }

    TIter nextkey(f.GetListOfKeys());
    TKey* key;
    while ((key = (TKey*)nextkey())) {
        TObject* obj = key->ReadObj();
        if (obj->IsA()->InheritsFrom(TTree::Class())) {
            std::string name = obj->GetName();
            if (filter == "") {
                outlist.emplace_back(name);
            } else {
                if (regexMatch(pattern, name).size() != 0) {
                    outlist.emplace_back(name);
                }
            }
        }
        SafeDelete(obj);
    }
    SafeDelete(key);
    f.Close();
    return outlist;
}

double Utils::reserveDigit(double num, unsigned int digist) {
    int timesNum = pow(10, digist);
    int mid = (int)(num * timesNum + 0.5);
    double end = (double)mid / timesNum;
    return end;
}
std::vector<std::filesystem::path> Utils::getFiles(std::string folder, std::string pattern) {
    std::filesystem::path p(folder);
    CHECK(exists(p)) << folder << " don't exist! Please Check!";
    std::filesystem::directory_entry entry(p);
    CHECK(entry.is_directory()) << folder << " is not a directory! Please Check!";

    std::regex pat;
    if (pattern != "") {
        pat = std::regex(pattern);
    }
    if (folder[folder.length() - 1] != '/') {
        folder.append(1, '/');
    }

    std::vector<std::filesystem::path> fVec;
    for (auto& fe : std::filesystem::directory_iterator(p)) {
        auto fp = fe.path();
        auto fname = fp.filename();
        std::string fStr = fname.string();
        if (pattern != "") {
            std::vector<std::string> result = regexMatch(pat, fStr);
            if (result.size() != 0) {
                fVec.emplace_back(p / fname);
            }
        } else {
            fVec.emplace_back(p / fname);
        }
        sort(fVec.begin(), fVec.end());
    }
    return fVec;
}

std::vector<std::filesystem::path> Utils::getFiles(std::string folder, std::string pattern, std::vector<std::vector<std::string>>& extraCapture) {
    std::filesystem::path p(folder);
    CHECK(exists(p)) << folder << " don't exist! Please Check!";
    std::filesystem::directory_entry entry(p);
    CHECK(entry.is_directory()) << folder << " is not a directory! Please Check!";

    std::regex pat(pattern);
    if (folder[folder.length() - 1] != '/') {
        folder.append(1, '/');
    }

    std::vector<std::filesystem::path> fVec;
    for (auto& fe : std::filesystem::directory_iterator(p)) {
        auto fp = fe.path();
        auto fname = fp.filename();
        std::string fStr = fname.string();
        std::vector<std::string> result = regexMatch(pat, fStr);
        if (result.size() != 0) {
            fVec.emplace_back(p / fname);
            extraCapture.emplace_back(result);
        }
    }
    return fVec;
}
