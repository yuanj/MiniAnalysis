#include "Thrust.h"
using namespace ROOT::Math;

Thrust::Thrust(AnaObjs momenta4, bool noZ, bool boost2CM, bool invRapidity, double mu) {
    std::vector<TVector3> momenta3;
    //assign momemtum for frame I and frame V
    for (const auto& vec : momenta4) {
        if (vec.type() == MET){
            _momentum_I += vec;
        } else {
            _momentum_V += vec;
        }
    }
    // remove Pz if noZ
    if (noZ) {
        _momentum_I.SetCoordinates(_momentum_I.Pt(), 0, _momentum_I.Phi(), _momentum_I.M());
        _momentum_V.SetCoordinates(_momentum_V.Pt(), 0, _momentum_V.Phi(), _momentum_V.M());
    }
    // set eta and mass of frame I
    if (invRapidity) {
        _momentum_I.SetCoordinates(_momentum_I.Pt(), _momentum_V.Eta(), _momentum_I.Phi(), sqrt(_momentum_V.M()*_momentum_V.M() + 4 * mu * mu)); //larger M_I leads to larger momemtum in CM frame
    }
    _momentum_CM = _momentum_I + _momentum_V;
    Boost boostToCM(_momentum_CM.BoostToCM());

    // loop over momenta to be calculated for thrust
    for (const auto& obj_lab : momenta4) {
        double x(0),y(0),z(0);
        //boost to CM frame if requried
        if (boost2CM){
            ROOT::Math::PtEtaPhiMVector vec_lab(obj_lab);
            auto vec = boostToCM(vec_lab);
            x=vec.X();y=vec.Y();z=vec.Z();
        }else{
            x=obj_lab.X();y=obj_lab.Y();z=obj_lab.Z();
        }
        // skip Pz if noZ
        momenta3.emplace_back(TVector3(x, y, noZ ? 0 : z));
    }
    _momenta = momenta3;
    this->calculate_thrust();
    // this->split_system();
}

bool Thrust::calculate_thrust() {
    double thrust_norm(0);
    for (const auto& v : _momenta) {
        thrust_norm += v.Mag();
    }

    if (_momenta.size() < 2) {
        _thrust = -1;
        _thrust_axis.SetXYZ(0., 0., 0.);
        return true;
    }
    if (_momenta.size() == 2) {
        _thrust = 1;
        _thrust_minor = 0.0;
        _thrust_minor = 0.0;

        _thrust_axis = _momenta[0].Unit();
        if (_thrust_axis.Z() < 0) _thrust_axis = -_thrust_axis;

        if (_thrust_axis.Z() < 0.75) {
            _thrust_axis_major = (_thrust_axis.Cross(TVector3(0, 0, 1))).Unit();
        } else {
            _thrust_axis_major = (_thrust_axis.Cross(TVector3(0, 1, 0))).Unit();
        }
        _thrust_axis_minor = _thrust_axis.Cross(_thrust_axis_major);
        _validity = true;
        return true;
    }

    // temp variables for calculation
    TVector3 axis(0, 0, 0);
    double val = 0.;

    // get thrust
    this->calculate_general_thrust(_momenta, val, axis);
    _thrust = val / thrust_norm;

    if (axis.Z() < 0) axis = -axis;
    axis = axis.Unit();
    _thrust_axis = axis;

    // get thrust major
    std::vector<TVector3> momenta_major;
    for (const auto& v : _momenta) {
        const TVector3 vpare = (v.Dot(axis.Unit())) * axis.Unit();
        momenta_major.push_back(v - vpare);
    }
    val = 0.0;
    this->calculate_general_thrust(momenta_major, val, axis);
    _thrust_major = val / thrust_norm;
    if (axis.X() < 0) axis = -axis;
    axis = axis.Unit();
    _thrust_axis_major = axis;

    // get thrust minor
    if (_thrust_axis.Dot(_thrust_axis_major) < 1e-10) { // check if thrust axes are perpendicular
        axis = _thrust_axis.Cross(_thrust_axis_major);
        _thrust_axis_minor = axis;
        val = 0.0;
        for (const auto& v : _momenta) {
            val += std::fabs(axis.Dot(v));
        }
        _thrust_minor = val / thrust_norm;
    } else {
        _thrust_minor = -1.0;
        _thrust_axis_minor.SetXYZ(0, 0, 0);
    }

    // calculation validity
    if (_thrust == 0 || (_thrust <= 1.0 && _thrust >= 0.5)) {
        _validity = true;
    }
    return true;
}

void Thrust::calculate_general_thrust(const std::vector<TVector3>& momenta, double& val, TVector3& taxis) {
    /**
     * @brief Take 8 (4) different starting vectors  constructed from the 4 (3) leading particles
     * to make sure that a local maximum is not found
     */
    std::vector<TVector3> p = momenta;
    if (p.size() < 3) {
        Error("calculate_general_thrust", "Requires at least 3 objects.");
        return;
    }

    int n(4);
    if (p.size() == 3) n = 3;

    std::sort(p.begin(), p.end(), [](const TVector3& a, const TVector3& b) -> bool { return a.Mag2() > b.Mag2(); });

    std::vector<TVector3> tvec;
    std::vector<double> tval;
    // create the starting vectors and iterate to get the maximum
    for (int i = 0; i < std::pow(2, n - 1); ++i) {
        // create initial vectors from the leading four momenta
        TVector3 vector_initial_leading_momenta(0., 0., 0.);
        int sign = i;
        for (int k = 0; k < n; k++) {
            (sign % 2) == 1 ? vector_initial_leading_momenta += p.at(k) : vector_initial_leading_momenta -= p.at(k);
            sign /= 2;
        }

        vector_initial_leading_momenta = vector_initial_leading_momenta.Unit();

        // iterate
        double diff = 1e3;
        while (diff > 1e-5) {
            TVector3 vector_initial_leading_momenta_t(0., 0., 0.);
            for (int k = 0; k < p.size(); k++) {
                (vector_initial_leading_momenta.Dot(p.at(k)) > 0) ? vector_initial_leading_momenta_t += p.at(k)
                                                                  : vector_initial_leading_momenta_t -= p.at(k);
            }
            diff = (vector_initial_leading_momenta - vector_initial_leading_momenta_t.Unit()).Mag();
            vector_initial_leading_momenta = vector_initial_leading_momenta_t.Unit();
        }

        // calculate thrust for the vector which was found
        val = 0.;
        for (int k = 0; k < p.size(); k++) {
            val += std::fabs(vector_initial_leading_momenta.Dot(p.at(k)));
        }

        // store value and axis
        tval.push_back(val);
        tvec.push_back(vector_initial_leading_momenta);
    }

    // pick the largest thrust and the thrust axis
    val = 0.;
    for (int i = 0; i < tvec.size(); i++) {
        if (tval.at(i) > val) {
            val = tval.at(i);
            taxis = tvec.at(i);
        }
    }
}

// std::pair<Vector4, Vector4> Thrust::assgin_frame(AnaObjs allobjs, AnaObj FOI) {
//     Boost boostToCM(_momentum_CM.BoostToCM());
//     auto boostToLab(boostToCM.Inverse());
//     auto FOI_CM = boostToCM(FOI);

//     Vector4 _pos_sum, _neg_sum;
//     double doi = FOI_CM.Vect().Dot(_thrust_axis);
//     for (const auto& obj_lab : allobjs) {
//         Vector4 vec_lab(obj_lab);
//         auto vec = boostToCM(vec_lab);
//         if (vec.Vect().Dot(_thrust_axis) * doi > 0){
//             _pos_sum += vec_lab;
//         } else{
//             _neg_sum += vec_lab;
//         }
//     }
//     return std::make_pair(_pos_sum, _neg_sum);
// }

// std::pair<int, int> Thrust::count_objects(AnaObjs allobjs, AnaObj FOI) {
//     int pos(0), neg(0);
//     double doi = FOI.Vect().Dot(_thrust_axis);
//     for (auto v : allobjs) {
//         if (v.Vect().Dot(_thrust_axis) * doi > 0) {
//             pos++;
//         } else {
//             neg++;
//         }
//     }
//     return std::make_pair(pos, neg);
// }
