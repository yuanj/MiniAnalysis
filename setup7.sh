# Centos7
setupATLAS
lsetup cmake
lsetup "root 6.22.00-x86_64-centos7-gcc8-opt"; unset PYTHONHOME
export Dir_MiniAnalysis=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
export Dir_BOOST="/cvmfs/sft.cern.ch/lcg/releases/Boost/1.72.0-7bbce/x86_64-centos7-gcc8-opt"
