#pragma once
#include <TH1F.h>
#include <TH2F.h>

#include <functional>
#include <iostream>

// 1D Histgram with its fill position
class Hist {
public:
    static const bool USE_OVERFLOW = true;
    static const bool NO_OVERFLOW = false;

    Hist(TH1* hist, std::function<double()> fillPosition, bool fillOverflow = false) {
        hist->Sumw2();
        this->hist = hist;
        this->fillPosition = fillPosition;
        this->fillOverflow = fillOverflow;
    }
    // In this way, you can only use TH1F
    Hist(std::string histname, int nBin, double binStart, double binEnd, std::function<double()> fillPosition, bool fillOverflow = NO_OVERFLOW) {
        TH1* hist = new TH1F(histname.c_str(), histname.c_str(), nBin, binStart, binEnd);
        hist->Sumw2();
        this->hist = hist;
        this->fillPosition = fillPosition;
        this->fillOverflow = fillOverflow;
    }

    // TH1F Vector
    Hist(std::string histname, int nBin, double binStart, double binEnd, std::function<std::vector<double>()> fillPosition,
         bool fillOverflow = NO_OVERFLOW) {
        TH1* hist = new TH1F(histname.c_str(), histname.c_str(), nBin, binStart, binEnd);
        hist->Sumw2();
        this->hist = hist;
        this->fillPositionV = fillPosition;
        this->fillOverflow = fillOverflow;
        this->inputVector = true;
    }

    // TH2F vector
    Hist(std::string histname, int nBinX, double binStartX, double binEndX, int nBinY, double binStartY, double binEndY,
         std::function<std::pair<std::vector<double>, std::vector<double> >()> fillPosition, bool fillOverflow = NO_OVERFLOW) {
        TH2* hist2D = new TH2F(histname.c_str(), histname.c_str(), nBinX, binStartX, binEndX, nBinY, binStartY, binEndY);
        hist2D->Sumw2();
        this->hist2 = hist2D;
        this->fillPositionV2 = fillPosition;
        this->fillOverflow = fillOverflow;
        this->inputVector = true;
        this->hist1D = false;
    }

    // TH2F
    Hist(std::string histname, int nBinX, double binStartX, double binEndX, int nBinY, double binStartY, double binEndY,
         std::function<std::pair<double, double>()> fillPosition, bool fillOverflow = NO_OVERFLOW) {
        TH2* hist2D = new TH2F(histname.c_str(), histname.c_str(), nBinX, binStartX, binEndX, nBinY, binStartY, binEndY);
        hist2D->Sumw2();
        this->hist2 = hist2D;
        this->fillPosition2 = fillPosition;
        this->fillOverflow = fillOverflow;
        this->hist1D = false;
    }
    void Fill(double weight) {
        if (hist1D) {
            double lastbinCenter = hist->GetBinCenter(hist->GetNbinsX());
            if (!inputVector) {
                auto posi = fillPosition();
                if (fillOverflow) {
                    posi = (posi > lastbinCenter) ? lastbinCenter : posi;
                }
                hist->Fill(posi, weight);
            } else {
                auto posi = fillPositionV();
                for (int i = 0; i < posi.size(); i++) {
                    if (fillOverflow) {
                        posi.at(i) = (posi.at(i) > lastbinCenter) ? lastbinCenter : posi.at(i);
                    }
                    hist->Fill(posi.at(i), weight);
                }
            }
        } else {
            double lastbinCenterX = hist2->GetXaxis()->GetBinCenter(hist2->GetNbinsX());
            double lastbinCenterY = hist2->GetYaxis()->GetBinCenter(hist2->GetNbinsY());
            if (!inputVector) {
                auto [posiX, posiY] = fillPosition2();
                if (fillOverflow) {
                    posiX = (posiX > lastbinCenterX) ? lastbinCenterX : posiX;
                    posiY = (posiY > lastbinCenterY) ? lastbinCenterY : posiY;
                }
                if (posiX < 10 || posiY < 10) {
                }
                hist2->Fill(posiX, posiY, weight);
            } else {
                auto [posiX, posiY] = fillPositionV2();
                for (int i = 0; i < posiX.size(); i++) {
                    if (fillOverflow) {
                        posiX.at(i) = (posiX.at(i) > lastbinCenterX) ? lastbinCenterX : posiX.at(i);
                        posiY.at(i) = (posiY.at(i) > lastbinCenterY) ? lastbinCenterY : posiY.at(i);
                    }
                    hist2->Fill(posiX.at(i), posiY.at(i), weight);
                }
            } // end of hist2D+inputVector
        }     // end of hist2D
    }

    void SetName(std::string newName) {
        if (this->hist1D) {
            hist->SetName(newName.c_str());
        } else {
            hist2->SetName(newName.c_str());
        }
    }
    int getBinNum() { return hist->GetNbinsX(); }
    double getBinUp() { return hist->GetBinLowEdge(hist->GetNbinsX() + 1); }
    double getBinDown() { return hist->GetBinLowEdge(1); }
    void SetDirectory(TDirectory* dir) {
        if (this->hist1D) {
            hist->SetDirectory(dir);
        } else {
            hist2->SetDirectory(dir);
        }
    }
    void Write() {
        if (this->hist1D) {
            hist->Write();
        } else {
            hist2->Write();
        }
    }
    ~Hist() {
        /*Note: In root framework, when deleting a file, all objects owned by this file will be deleted(See Root chapter
        10: ObjectOwnership). So it's kind of dangerous to delete the histograms or trees by hand. Though may cause
        mermory leak, won't do anything when destructing*/
        // if (this->hist1D && hist != nullptr) {
        //     hist->Write();
        //     delete hist;
        //     hist = nullptr;
        // }
        // if (!this->hist1D && hist2 != nullptr) {
        //     hist2->Write();
        //     delete hist;
        //     hist = nullptr;
        // }
    }

private:
    TH1* hist;
    TH2* hist2;
    bool fillOverflow;
    bool hist1D = true;
    bool inputVector = false;
    std::function<double()> fillPosition;
    std::function<std::vector<double>()> fillPositionV;
    std::function<std::pair<double, double>()> fillPosition2;
    std::function<std::pair<std::vector<double>, std::vector<double> >()> fillPositionV2;
};

// A class that allow you to create many Hist with same bin definition and fill Position
class HistTemplate {
public:
    HistTemplate(std::function<double()> fillValue, int binNum, double binStart, double binEnd, bool useOverflow = false) {
        this->fillValue = fillValue;
        this->binNum = binNum;
        this->binStart = binStart;
        this->binEnd = binEnd;
        this->useOverflow = useOverflow;
    }
    Hist* createHist(std::string name) { return new Hist(name, binNum, binStart, binEnd, fillValue, useOverflow); }
    ~HistTemplate(){};

private:
    std::function<double()> fillValue;
    bool useOverflow;
    int binNum;
    double binStart, binEnd;
};
