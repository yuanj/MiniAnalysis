#pragma once

#include <TLorentzVector.h>

#include <algorithm>
#include <functional>
#include <initializer_list>
#include <iostream>
#include <map>
#include <vector>

#include "TVectorD.h"
#include "easylogging++.h"

/**************************************************************
**********Steal and modify from SimpleAnalysis package*********
****https://gitlab.cern.ch/atlas-phys-susy-wg/SimpleAnalysis***
**************************************************************/

enum AnalysisObjectType { PHOTON, ELECTRON, MUON, TAU, JET, MET, COMBINED };

enum AnalysisElectronID {
    EVeryLooseLH = 1 << 0,
    ELooseLH = 1 << 1,
    EMediumLH = 1 << 2,            //!
    ETightLH = 1 << 3,             //!
    EIsoFCHighPtCaloOnly = 1 << 4, //!
    EIsoFCLoose = 1 << 5,
    EIsoFCTight = 1 << 6,
    EIsoGradient = 1 << 7,
    EIsTruthE = 1 << 8,
    EIsNotTruthE = 1 << 9,
    EIso = 1 << 10,
    EIsSignal = 1 << 11, //!
    EGood = EVeryLooseLH | ELooseLH | EMediumLH | ETightLH | EIsSignal,
    EIsoGood = EGood | EIso,
    EIsoSig = EIso | EIsSignal,
    EIsoLoose_VarRad = 1 << 12,                //!
    EIsoTight_VarRad = 1 << 13,                //!
    EIsoTightTrackOnly_VarRad = 1 << 14,       //!
    EIsoTightTrackOnly_FixedRad = 1 << 15,     //!
    E24_lhmedium_L1EM20VH = 1 << 16,           //!
    E60_lhmedium = 1 << 17,                    //!
    E120_lhloose = 1 << 18,                    //!
    E26_lhtight_nod0_ivarloose = 1 << 19,      //!
    E60_lhmedium_nod0 = 1 << 20,               //!
    E140_lhloose_nod0 = 1 << 21,               //!
    E26_lhtight_ivarloose_L1EM22VHI = 1 << 22, //!
    E60_lhmedium_L1EM22VHI = 1 << 23,          //!
    E140_lhloose_L1EM22VHI = 1 << 24,          //!
};

enum AnalysisMuonID {
    MuVeryLoose = 1 << 0,
    MuLoose = 1 << 1,
    MuMedium = 1 << 2,
    MuTight = 1 << 3,
    MuHighPt = 1 << 4,
    MuIsoHighPtTrackOnly = 1 << 6,
    MuIsoLoose_FixedRad = 1 << 7,
    MuIsoLoose_VarRad = 1 << 8, //!
    MuIsoPflowLoose_FixedRad = 1 << 9,
    MuIsoPflowLoose_VarRad = 1 << 10, //!
    MuIsoPflowTight_FixedRad = 1 << 11,
    MuIsoPflowTight_VarRad = 1 << 12, //!
    MuIsoTightTrackOnly_FixedRad = 1 << 13,
    MuIsoTightTrackOnly_VarRad = 1 << 14,
    MuIsoTight_FixedRad = 1 << 15,
    MuIsoTight_VarRad = 1 << 16, //!
    MuIso = 1 << 17,
    MuIsSignal = 1 << 18, //!
    MuGood = MuLoose | MuMedium | MuTight | MuVeryLoose | MuIsSignal,
    MuIsoGood = MuGood | MuIso,
    MuIsoSig = MuIso | MuIsSignal,
    MuIsTruthMu = 1 << 19,
    MuIsNotTruthMu = 1 << 20,
    Mu20_iloose_L1MU15 = 1 << 21,
    Mu24_ivarmedium_L1MU14FCH = 1 << 22,
    Mu26_ivarmedium = 1 << 23,
    Mu50 = 1 << 24,
    Mu50_L1MU14FCH = 1 << 25,
};

enum AnalysisTauID {
    TauVeryLoose = 1 << 1,   //!
    TauLoose = 1 << 2,       //!
    TauMedium = 1 << 3,      //!
    TauTight = 1 << 4,       //!
    TauOneProng = 1 << 5,    //!
    TauThreeProng = 1 << 6,  //!
    TauEvetoLoose = 1 << 7,  //!
    TauEvetoMedium = 1 << 8, //!
    TauEvetoTight = 1 << 9,  //!
    TauIsTruthTau = 1 << 10,
    TauIsTruthEle = 1 << 11,
    TauIsTruthMuon = 1 << 12,
    TauIsTruthJet = 1 << 13,
    TauIsTruthMatched = 1 << 14,
    TauIsTruthHad = 1 << 15,
    TauIsSignal = 1 << 20,
    TauIsNotTruthTau = TauIsTruthEle | TauIsTruthMuon | TauIsTruthJet,
    TauGood = TauLoose | TauMedium | TauTight | TauIsSignal,
    TauIsoGood = TauGood
};

enum AnalysisPhotonID {
    PhotonLoose = 1 << 0,
    PhotonTight = 1 << 1,
    PhotonIsoFixedCutLoose = 1 << 8,
    PhotonIsoFixedCutTight = 1 << 9,
    PhotonGood = PhotonLoose | PhotonTight,
    PhotonIsoGood = PhotonGood | PhotonIsoFixedCutLoose | PhotonIsoFixedCutTight
};

enum AnalysisJetID {
    GoodBJet = 1 << 7,     //!
    JetVertexTag = 1 << 8, //!
    LightJet = 1 << 1,
    GoodCJet = 1 << 3,
    ForwardJet = 1 << 4,
    LessThan3Tracks = 1 << 8, // most signal jets should fail this
    JetIsSignal = 1 << 20,
    GoodJet = LightJet | GoodBJet | GoodCJet | ForwardJet | JetIsSignal,
    TrueLightJet = 1 << 27, // These should not be used for actual analysis selection, only for understanding
    TrueCJet = 1 << 28,
    TrueBJet = 1 << 29,
    TrueTau = 1 << 30,

};

// Todo: once c++ has a good and easy tensor library we could move the AnaObjs to tensor
class AnaObj : public TLorentzVector {
public:
    AnaObj(){};
    AnaObj(AnalysisObjectType type, float Pt, float Eta, float Phi, float E, int charge, int id = 0, float Mt = 0)
        : _charge(charge), _id(id), _type(type), _mt(Mt) {
        this->SetPtEtaPhiE(Pt, Eta, Phi, E);
    }
    AnaObj(AnalysisObjectType type, TLorentzVector tlv, int charge, int id = 0, float Mt = 0)
        : TLorentzVector(tlv), _charge(charge), _id(id), _type(type), _mt(Mt){};
    AnaObj(AnalysisObjectType type, int charge = 0, int id = 0) : _charge(charge), _id(id), _type(type) {}
    AnaObj(double x, double y, double z, double t): _charge(0), _id(0), _type(COMBINED), _mt(0) {
        this->SetXYZT(x, y, z, t);
    }
    virtual bool passID(int id) const { return (id & _id) == id; };
    virtual int charge() const { return _charge; };
    virtual AnalysisObjectType type() const { return _type; };
    virtual int id() const { return _id; };
    // The MT we use most of times, it is calculated by Pt and MET
    virtual float Mt() const { return _mt; }
    // In case, we create a func to return lorentz Mt
    virtual float lorentzMt() const {
        TLorentzVector tlv;
        tlv.SetPtEtaPhiM(Pt(), Eta(), Phi(), M());
        return tlv.Mt();
    }
    virtual void setMt(float Mt) { _mt = Mt; }
    virtual void setProperty(std::string name, float value) { this->otherProperties[name] = value; }

    virtual float getProperty(std::string name) {
        auto iter = otherProperties.find(name);
        if (iter != otherProperties.end()) {
            return iter->second;
        }
        // easyLogging issue. The AnaObj class is kind of out of framework and could used in root macros. But if we use
        // easyLogging in macros without init, it will throw errors. If we init it at here, usually it will cause
        // multiple init in framework. To let it compatiable with framework mode and macro mode. Here we use cerr
        // instead of easyLogging
        std::cerr << "AnaObj: Don't find the property '" << name << "' in AnaObj! Please check the code to see if you"
                  << "registered this property or not, return 1e-10" << std::endl;
        return 1e-10;
    }

    virtual TLorentzVector lorentzVector() const {
        TLorentzVector tlv;
        tlv.SetPtEtaPhiM(Pt(), Eta(), Phi(), M());
        return tlv;
    };

    bool isOS(AnaObj o2) { return (this->_charge * o2.charge() < 0); }
    bool isSS(AnaObj o2) { return (this->_charge * o2.charge() > 0); }

    bool isSFOS(AnaObj o2) { return this->type() == o2.type() && this->charge() * o2.charge() < 0.; }
    inline double x() const {return X();}
    inline double y() const {return Y();}
    inline double z() const {return Z();}
    inline double t() const {return T();}
private:
    int _charge; // not used for jets or photons
    int _id;
    float _mt;
    AnalysisObjectType _type;
    // Use a otherProperties map to store other double vars avoid creating many small classes
    std::map<std::string, float> otherProperties;
};

class AnaObjs : public std::vector<AnaObj> {
    using std::vector<AnaObj>::vector; // reuse vector initializers
                                       // force range check when accessing objects directly
public:
    AnaObjs(){};
    template <typename T> AnaObjs(AnalysisObjectType type, std::vector<T> PtVec, std::vector<T> EtaVec, std::vector<T> PhiVec, std::vector<T> EVec,
                                  std::vector<int> chargeVec, std::vector<int> idVec, std::vector<double> MtVec) {
        AnaObjs objs;
        if ((PtVec.size() != EtaVec.size()) || (PtVec.size() != PhiVec.size()) || (PtVec.size() != EVec.size()) ||
            (PtVec.size() != chargeVec.size()) || (PtVec.size() != idVec.size()) || (PtVec.size() != MtVec.size())) {
            LOG(ERROR) << "Warnning! There is a event which the vectors size are different!!! Please Check!! Will "
                          "return an empty Object collection";
        } else {
            for (unsigned int i = 0; i < PtVec.size(); i++) {
                AnaObj o(type, PtVec[i], EtaVec[i], PhiVec[i], EVec[i], chargeVec[i], idVec[i], MtVec[i]);
                objs.emplace_back(o);
            }
        }
        new (this) AnaObjs(objs);
    };

    template <typename T> AnaObjs(AnalysisObjectType type, std::vector<T> PtVec, std::vector<T> EtaVec, std::vector<T> PhiVec, std::vector<T> EVec,
                                  std::vector<int> chargeVec, std::vector<int> idVec) {
        AnaObjs objs;
        if ((PtVec.size() != EtaVec.size()) || (PtVec.size() != PhiVec.size()) || (PtVec.size() != EVec.size()) ||
            (PtVec.size() != chargeVec.size()) || (PtVec.size() != idVec.size())) {
            LOG(ERROR) << "Warnning! There is a event which the vectors size are different!!! Please Check!! Will "
                          "return an empty Object collection";
        } else {
            for (unsigned int i = 0; i < PtVec.size(); i++) {
                AnaObj o(type, PtVec[i], EtaVec[i], PhiVec[i], EVec[i], chargeVec[i], idVec);
                objs.emplace_back(o);
            }
        }
        new (this) AnaObjs(objs);
    };

    template <typename T> AnaObjs(AnalysisObjectType type, std::vector<T> PtVec, std::vector<T> EtaVec, std::vector<T> PhiVec, std::vector<T> EVec,
                                  std::vector<int> chargeVec) {
        AnaObjs objs;
        if ((PtVec.size() != EtaVec.size()) || (PtVec.size() != PhiVec.size()) || (PtVec.size() != EVec.size()) ||
            (PtVec.size() != chargeVec.size())) {
            LOG(ERROR) << "Warnning! There is a event which the vectors size are different!!! Please Check!! Will "
                          "return an empty Object collection";
        } else {
            for (unsigned int i = 0; i < PtVec.size(); i++) {
                AnaObj o(type, PtVec[i], EtaVec[i], PhiVec[i], EVec[i], chargeVec[i]);
                objs.emplace_back(o);
            }
        }
        new (this) AnaObjs(objs);
    };

    template <typename T>
    AnaObjs(AnalysisObjectType type, std::vector<TLorentzVector> tlvVec, std::vector<int> chargeVec, std::vector<int> idVec, std::vector<T> MtVec) {
        AnaObjs objs;
        if ((tlvVec.size() != chargeVec.size()) || (tlvVec.size() != idVec.size()) || (tlvVec.size() != MtVec.size())) {
            LOG(ERROR) << "Warnning! There is a event which the vectors size are different!!! Please Check!! Will "
                          "return an empty Object collection";
        } else {
            for (unsigned int i = 0; i < tlvVec.size(); i++) {
                AnaObj o(type, tlvVec[i], chargeVec[i], idVec[i], MtVec[i]);
                objs.emplace_back(o);
            }
        }
        new (this) AnaObjs(objs);
    };

    AnaObjs(AnalysisObjectType type, std::vector<TLorentzVector> tlvVec, std::vector<int> chargeVec, std::vector<int> idVec) {
        AnaObjs objs;
        if ((tlvVec.size() != chargeVec.size()) || (tlvVec.size() != idVec.size())) {
            LOG(ERROR) << "Warnning! There is a event which the vectors size are different!!! Please Check!! Will "
                          "return an empty Object collection";
        } else {
            for (unsigned int i = 0; i < tlvVec.size(); i++) {
                AnaObj o(type, tlvVec[i], chargeVec[i], idVec[i]);
                objs.emplace_back(o);
            }
        }
        new (this) AnaObjs(objs);
    };

    AnaObjs(AnalysisObjectType type, std::vector<TLorentzVector> tlvVec, std::vector<int> chargeVec) {
        AnaObjs objs;
        if (tlvVec.size() != chargeVec.size()) {
            LOG(ERROR) << "Warnning! There is a event which the vectors size are different!!! Please Check!! Will "
                          "return an empty Object collection";
        } else {
            for (unsigned int i = 0; i < tlvVec.size(); i++) {
                AnaObj o(type, tlvVec[i], chargeVec[i]);
                objs.emplace_back(o);
            }
        }
        new (this) AnaObjs(objs);
    };

    AnaObj& operator[](std::vector<AnaObj>::size_type ii) { return this->at(ii); };
    const AnaObj& operator[](std::vector<AnaObj>::size_type ii) const { return this->at(ii); };
    void sortByPt() {
        std::sort(this->begin(), this->end(), [](AnaObj a, AnaObj b) { return a.Pt() > b.Pt(); });
    }
    AnaObjs filterObjects(float ptCut, float etaCut = 100., int id = 0, unsigned int maxNum = 10000);
    AnaObjs filterObjects(float ptCut, std::string propertyCut);
    std::pair<AnaObjs, AnaObjs> splitObjects(float ptCut, float etaCut = 100., int id = 0, int maxNum = 10000);
    int countObjects(float ptCut, float etaCut = 1e10, int id = 0);
    float sumObjectsPt(unsigned int maxNum = 10000, float ptCut = 0);
    float sumObjectsM(unsigned int maxNum = 10000, float mCut = 0);
    AnaObj sumObjects();
    float minDphi(float ptCut = 0);
    float minDR(float ptCut = 0);
    AnaObjs filterCrack(float minEta = 1.37, float maxEta = 1.52);
    AnaObjs lowMassRemoval(std::function<bool(const AnaObj&, const AnaObj&)>, float MinMass = 0, float MaxMass = FLT_MAX, int type = -1);
    TVectorD calcEigenValues();
    float aplanarity();
    float sphericity();
    template <typename T> void setProperties(std::string name, std::vector<T> PropertyVec) {
        if (PropertyVec.size() != this->size()) {
            LOG_N_TIMES(50, ERROR) << "The Property " << name
                                   << " size is not equal to the AnaObjs vector! Please check the code to setup the property"
                                   << " immediately after you construct AnaObjs! Will not add this property. PropertyVec size: " << PropertyVec.size()
                                   << ", AnaObjs size: " << this->size();
            return;
        }
        for (unsigned int i = 0; i < this->size(); i++) {
            this->at(i).setProperty(name, PropertyVec[i]);
        }
    }
    std::vector<float> getProperties(std::string name);
    AnaObjs passTrig(bool trigVar, float offLinePt = 0);
    template <typename T>
    AnaObjs passTrig(bool trigVar, std::vector<T> trigMatch, std::initializer_list<float> offLinePts, bool vecRangeCheck = true) {
        AnaObjs outList;
        if (trigMatch.size() != this->size()) {
            if ((vecRangeCheck) || (trigMatch.size() < this->size())) {
                LOG_N_TIMES(50, ERROR) << "VecRangeCheck: The trigMatch vector size is not equal to AnaObjs vector!"
                                       << " Please check the code to do the trig pass immediately after you construct AnaObjs! "
                                       << "Will not add this trigger";
                outList.assign(this->begin(), this->end());
                return outList;
            } else {
                LOG_N_TIMES(3, WARNING) << "Warning! The trigMatch vector size is larger than AnaObjs vector "
                                        << "but you force the func to do the match! Please make sure you know what you are doing "
                                        << "and will only use the first X values of the trigMatch vector to do the match";
            }
        }
        /* Strategy: First check if the event pass the trigger. Then let each AnaObj match the trigger match
         * For offline, there will be three cases:
         * Case 1: Single-object like TrigHLT_mu26_ivarmedium, we need to filter all objs with its offlinePt
         * Case 2: Multi-object trigger but need still one offline for each object. Like HLT_e17_lhloose_mu14, same
         * treatment as Case 1. Case 3: Multi-object trigger and need multiple offline for objs. Like HLT_mu18_mu8noL1.
         * We should first let AnaObjs[0] > leadingOffline, AnaObjs[1] > subleadingOffline... If AnaObjs size larger
         * than Offline, let others pass the lowest offline
         */
        if (trigVar) {
            // first sort the inputs
            std::vector<float> offLinePt = offLinePts;
            if (offLinePt.size() == 0) {
                LOG_N_TIMES(20, WARNING) << "No offline Pt inputs for the trigger. Set the offLine pt to default value: 0";
                offLinePt.emplace_back(0);
            }
            std::sort(offLinePt.begin(), offLinePt.end(), [](float a, float b) { return a > b; });
            for (unsigned int i = 0; i < this->size(); i++) {
                if (trigMatch[i]) {
                    outList.emplace_back(this->at(i));
                }
            }
            if (outList.size() >= offLinePt.size()) {
                bool passOffline = true;
                outList.sortByPt();
                for (int i = 0; i < outList.size(); i++) {
                    if (i < offLinePt.size()) {
                        if (outList[i].Pt() < offLinePt[i]) {
                            passOffline = false;
                            break;
                        }
                    } else {
                        if (outList[i].Pt() < offLinePt.back()) {
                            outList.erase(outList.begin() + i, outList.end() - 1);
                            break;
                        }
                    }
                }
                if (passOffline) {
                    return outList;
                }
            }
        }
        return AnaObjs(); // return an empty vector if failed any trigger step
    }
};

// AnaObj1 + AnaObj2 = a cmbined AnaObj which the tvl, charge, Mt are combined
AnaObj operator+(const AnaObj& lhs, const AnaObj& rhs);
/* AnaObjs1 + AnaObjs2 = a combined AnaObjs set. The objects in AnaObjs2 which is exactly same tlv as one object
 *  in AnaObjs1 will not be added. In a phycics analysis it means they are same object which should be killed by OR!
 */
AnaObjs operator+(const AnaObjs& lhs, const AnaObjs& rhs);

void printObject(const AnaObj& obj);
void printObjects(const AnaObjs& objs);

class SUSYP {
public:
    SUSYP(std::vector<int> input) : pdgid(input[0]), status(input[1]), type(input[2]), origin(input[3]), outcome(input[4]) {}
    SUSYP(int pdgid, int status, int type, int origin, int outcome) : pdgid(pdgid), status(status), type(type), origin(origin), outcome(outcome) {}
    bool IsSUSY() { return type == 22; }
    bool IsN1() { return type == 22 && pdgid == 1000022; }

private:
    int pdgid;
    int status;
    int type;
    int origin;
    int outcome;
};
