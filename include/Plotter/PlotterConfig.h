class file_config {
public:
    enum { Data = 1, Bkg = 2, Sig = 3 };
    file_config() {}
    file_config(std::string name, int draw_type, int color, int sample_type, std::string legend_text) {
        this->name = name;
        this->draw_type = draw_type;
        this->color = color;
        this->sample_type = sample_type;
        this->legend_text = legend_text;
    }
    std::string get_name() { return this->name; }
    int get_draw_type() { return this->draw_type; }
    int get_color() { return this->color; }
    int get_sample_type() { return this->sample_type; }
    std::string get_legend_text() { return this->legend_text; }

private:
    std::string name;
    int draw_type;
    int color;
    int sample_type;
    std::string legend_text;
};

class axis_config {
public:
    axis_config(std::string x_text, std::string x_title, int rebinNumber = 1, double rangeS = -99, double rangeE = -99,
                std::vector<double> rebinList = {})
        : x_text(x_text), x_title(x_title), rangeS(rangeS), rangeE(rangeE), rebinNumber(rebinNumber), rebinList(rebinList) {}

    std::string get_x_text() { return this->x_text; }
    std::string get_x_title() { return this->x_title; }
    int get_rebin_number() { return this->rebinNumber; }
    double get_range_S() { return this->rangeS; }
    double get_range_E() { return this->rangeE; }
    std::vector<double> get_rebin_list() { return this->rebinList; }
    bool simpleRebin() { return this->rebinList.size() == 0; }

private:
    std::string x_text;
    std::string x_title;
    double rangeS;
    double rangeE;
    int rebinNumber;
    std::vector<double> rebinList;
};

class binlabel_config : public axis_config {
public:
    binlabel_config(std::string x_text, std::vector<std::string> ilabels) : axis_config(x_text, ""), axis_labels(ilabels) {
        nlabels = ilabels.size();
    }
    std::vector<std::string> get_labels() { return this->axis_labels; }
    int get_length() { return this->nlabels; }

private:
    std::vector<std::string> axis_labels;
    int nlabels;
};

class n_1_config : public axis_config {
public:
    n_1_config(std::string x_text, double rangeS = -99, double rangeE = -99) : axis_config(x_text, "", 0, rangeS, rangeE) {}
};
