#pragma once
#include <TArrow.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TGraphAsymmErrors.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TLine.h>
#include <TPad.h>
#include <TStyle.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>

#include <filesystem>
#include <glob.h>
#include <cmath>
#include <iostream>
#include <string>

#include "PhyUtils.h"

std::vector<std::string> ExpandGlob(std::string glob);

class Plotter {
public:
    const static int ONE_PAD = 0;
    const static int USE_ZN_PAD = 1;
    const static int USE_COM_PAD = 2;
    const static int USE_INT_PAD = 3;
    const static int USE_MRT_PAD = 4;
    const static int BINNED_ZN = 2;
    const static int FLIP_DIC = 1;
    const static int NOM_DIC = 0;
    const static int STANDARD_COM = 0;
    const static int NORMALIZE_COM = 1;
    const static int INTEGRAL_COM = 2;
    const static bool USE_LOG_Y = true;
    const static bool NO_LOG_Y = false;

    const static int USE_OVERFLOW = 1;
    const static int NO_OVERFLOW = 0;

    enum { Line = 0, Block = 1, BlankBlock = 2, Dash = 3, DataLine = 4, Dot = 5 };

    Plotter(std::string title, int padChoice = ONE_PAD, bool logYChoice = USE_LOG_Y, std::string use_log_x = "");
    Plotter(int padChoice = ONE_PAD, bool logYChoice = USE_LOG_Y);

    void Initialize(int padChoice = ONE_PAD, bool logYChoice = USE_LOG_Y, std::string use_log_x = "", double topMargin = 0.01);

    // static method
    static TH1* integralHist(TH1* hist, std::string newname);
    static TH1* pickHistRange(TH1* hist, double startValue, double endvalue);
    static TGraphAsymmErrors* TH1TOTGraphAsymNoerrorx(TH1* h1, TH1* scaleHist = nullptr);
    static std::vector<std::vector<double>> printHist(TH1* hist, bool isData = false);
    void attachIntegralPad();
    void attachZnPad();
    void attachflipZnPad();
    void attachComparePad();
    void attachMultiComparePad();
    void Draw(std::string name);
    void drawHists();
    void addArrow(int color, double start, double end = -1.3123e10, double midY = -1.3123e10);
    void drawArrow();
    // Set Rebin
    void Rebin(std::vector<double> xbins);
    void Rebin(int rebinNum, const Double_t* xbins = 0);
    void autoRebin(double BkgRelErrorMax=0.3,double BkgYieldsMin=0, 
        double SigRelErrorMax=1,double SigYieldsMin=0);

    static void drawText(double x, double y, std::string text, double tsize = 0.06, Color_t color = 1) {
        TLatex* l = new TLatex();
        l->SetTextAlign(12);
        l->SetTextSize(tsize);
        l->SetNDC();
        l->SetTextColor(color);
        l->DrawLatex(x, y, text.c_str());
    }

    template <typename T> static T* getHist(std::string file, std::string name, int color = -1);
    static TH1* getHist(std::string file, std::string name, int type, int color);
    static TH1* getHist(std::string file, std::string treename, std::string branchname, std::string bWeight, int nBin, double binStart, double binEnd,
                        int type, int color);
    static TH1* getHist(std::string file, std::string region, std::string name, int type, int color);
    static TH1* getHist(std::string file, std::string region1, std::string region2, std::string name, int type, int color);
    void deallocateMemory();

    // Add TLatex Text
    TLatex* addUpperText(double x, double y, std::string text, double tsize = 0.06, Color_t color = 1) {
        TLatex* l = new TLatex(x, y, text.c_str());
        l->SetTextAlign(12);
        l->SetTextSize(tsize);
        l->SetNDC();
        l->SetTextColor(color);
        this->upperLatexs.emplace_back(l);
        return l;
    }

    TLatex* addLowerText(double x, double y, std::string text, double tsize = 0.06, Color_t color = 1) {
        TLatex* l = new TLatex(x, y, text.c_str());
        l->SetTextAlign(12);
        l->SetTextSize(tsize);
        l->SetNDC();
        l->SetTextColor(color);
        this->lowerLatexs.emplace_back(l);
        return l;
    }

    TLatex* addATLASText(double x, double y, double tsize = 0.06) {
        TLatex* l = new TLatex(x, y, "ATLAS");
        l->SetTextFont(72);
        l->SetTextAlign(12);
        l->SetTextSize(tsize);
        l->SetNDC();
        this->upperLatexs.emplace_back(l);
        return l;
    }

    // Set upper legend
    TLegend* addUpperLegend(double xStart, double yStart, double xEnd, double yEnd) {
        TLegend* legend = new TLegend(xStart, yStart, xEnd, yEnd);
        legend->SetBorderSize(0);
        legend->SetTextFont(42);
        if (this->padChoice == ONE_PAD) {
            legend->SetTextSize(0.04);
        } else {
            legend->SetTextSize(0.05);
        }
        legend->SetFillColor(0);
        legend->SetLineColor(0);
        upperLegend.emplace_back(legend);
        return legend;
    }

    // Set lower legend
    TLegend* addLowerLegend(double xStart, double yStart, double xEnd, double yEnd) {
        TLegend* legend = new TLegend(xStart, yStart, xEnd, yEnd);
        legend->SetBorderSize(0);
        legend->SetTextFont(42);
        legend->SetTextSize(0.05);
        legend->SetFillColor(0);
        legend->SetLineColor(0);
        lowerLegend.emplace_back(legend);
        return legend;
    }

    // Add histograms
    void addBkgHist(TH1* hist) {
        bkgStack.emplace_back(hist);
        if (bkghist == nullptr) {
            bkghist = (TH1*)hist->Clone("TotalBkgHist");
            bkghist->SetLineColor(1);
            bkghist->SetFillColor(1);
            bkghist->SetMarkerStyle(1);
            bkghist->SetFillStyle(3345);
        } else {
            bkghist->Add(hist);
        }
    }

    void addSigHist(TH1* hist) { sigStack.emplace_back(hist); }

    void setDataHist(TH1* hist) { this->datahist = hist; }

    void addOtherHist(TH1* hist) { otherHists.emplace_back(hist); }

    void setOtherHistPaintStyle(std::string style) { this->otherHistPaintStyle = style; }

    // getter
    TH1* getDataHist() { return datahist; }

    TH1* getBkgHist() { return bkghist; }

    std::vector<TH1*> getSigHist() { return sigStack; }

    std::vector<TH1*> getBkgStack() { return bkgStack; }

    std::vector<TH1*> getOtherHist() { return otherHists; }

    // Zn pad property
    void setZnProperty(int ZnProperty, double syst = 0.3) {
        this->ZnProperty = ZnProperty;
        this->znsyst = syst;
    }

    void setHistSyst(TH1* hist, float relativeSyst) {
        int nbins = hist->GetNbinsX(); // We may want to add Syst to all underflow and overflow bin.
        for (int i = 0; i <= nbins + 1; i++) {
            float err_low = hist->GetBinErrorLow(i); // it should be asymmetric. But depends.
            // float err_up  = hist->GetBinErrorUp(i);
            float yields = hist->GetBinContent(i);
            hist->SetBinError(i, sqrt(err_low * err_low + relativeSyst * relativeSyst * yields * yields));
        }
    }

    void setCompareHist(TH1* compare1, TH1* compare2, int com_choice = 0) {
        this->compare1 = compare1;
        this->compare2 = compare2;
        this->normalizeCom = (com_choice == NORMALIZE_COM);
        this->doIntegral = (com_choice == INTEGRAL_COM);
    }
    void setDenomHist(TH1* denomhist) { this->denomhist = denomhist; }

    void saveRatio(TH1* compare1, TH1* compare2, std::string name) {
        auto hratio = (TH1*)compare1->Clone("Ratio");
        hratio->Divide(compare2);
        ofstream oFile(name + "_sf.csv");
        for (int i(1); i <= hratio->GetNbinsX(); i++) {
            double ilow = hratio->GetXaxis()->GetBinLowEdge(i);
            double ihigh = hratio->GetXaxis()->GetBinLowEdge(i + 1);
            double isf = hratio->GetBinContent(i);
            oFile << i << "," << ilow << "," << ihigh << "," << isf << std::endl;
        }
        oFile.close();
    }

    // Set Range User
    void setUpperRange(double start, double end) {
        upYRangeS = start;
        upYRangeE = end;
    }

    void setLowerRange(double start, double end) {
        lowYRangeS = start;
        lowYRangeE = end;
    }

    void setXRange(double start, double end) {
        XRangeS = start;
        XRangeE = end;
    }

    void AutoSetUpperRange(double times = 2) {
        double max_bin_value = 0;
        if (this->bkghist != nullptr) {
            max_bin_value = this->bkghist->GetMaximum();
            for (auto i : this->bkgStack) {
                if (i->GetMaximum() > max_bin_value) {
                    max_bin_value = i->GetMaximum();
                }
            }
        }
        if (this->sigStack.size() != 0) {
            for (auto i : this->sigStack) {
                if (i->GetMaximum() > max_bin_value) {
                    max_bin_value = i->GetMaximum();
                }
            }
        }
        if (this->datahist != nullptr) {
            if (this->datahist->GetMaximum() > max_bin_value) {
                max_bin_value = this->datahist->GetMaximum();
            }
        }
        double len = log10(max_bin_value);
        double new_up_range = 0;

        if (this->isLogY) {
            double low_len = log10(this->upYRangeS);
            len -= low_len;
            len *= times;
            len += low_len;
            new_up_range = pow(10, len);
            if (new_up_range < 1) new_up_range = 1;
            if (len >= 9) new_up_range = 2000000000;
            // std::cout << this->upYRangeS<<","<<len << " , "<<new_up_range<< " , " << int(new_up_range) << std::endl;
            this->setUpperRange(this->upYRangeS, int(new_up_range));
        } else {
            // std::cout << "Auto Y-axis:" << max_bin_value << "," << this->upYRangeS << std::endl;
            max_bin_value = max_bin_value - this->upYRangeS;
            // int head_num = int( max_bin_value/(pow(10,len)) ) + 1 ;
            // new_up_range = head_num*times*pow(10,len) ;
            new_up_range = times * max_bin_value;
            this->setUpperRange(this->upYRangeS, int(100 * (new_up_range + this->upYRangeS)) / 100.);
            // this->setUpperRange(this->upYRangeS, int(new_up_range + this->upYRangeS));
        }
    }
    void AutoSetTightUpperRange(double times = 1.5) {
        double max_bin_value = 0;
        if (this->bkghist != nullptr) {
            for (auto i : this->bkgStack) {
                double histpeak = i->GetMaximum() + i->GetBinError(i->GetMaximumBin());
                if (histpeak > max_bin_value) {
                    max_bin_value = histpeak;
                }
            }
        }
        if (this->sigStack.size() != 0) {
            for (auto i : this->sigStack) {
                double histpeak = i->GetMaximum() + i->GetBinError(i->GetMaximumBin());
                if (histpeak > max_bin_value) {
                    max_bin_value = histpeak;
                }
            }
        }
        if (this->datahist != nullptr) {
            double histpeak = this->datahist->GetMaximum() + this->datahist->GetBinError(this->datahist->GetMaximumBin());
            if (histpeak > max_bin_value) {
                max_bin_value = histpeak;
            }
        }
        double len = log10(max_bin_value);
        double new_up_range = 0;

        if (this->isLogY) {
            double low_len = log10(this->upYRangeS);
            if (len >= 1) {
                len *= times;
                len -= low_len;
            } else {
                len /= times;
                len -= low_len;
            }
            std::cout << len << std::endl;
            len++;
            new_up_range = pow(10, len);
            if (len >= 9) new_up_range = 2000000000;
            std::cout << this->upYRangeS << " , " << int(new_up_range) << std::endl;
            this->setUpperRange(this->upYRangeS, int(new_up_range));
        } else {
            std::cout << "Auto Y-axis:" << max_bin_value << " " << this->upYRangeS << std::endl;
            max_bin_value = max_bin_value - this->upYRangeS;
            new_up_range = times * max_bin_value;
            new_up_range = int(new_up_range + this->upYRangeS) > 1 ? int(new_up_range + this->upYRangeS) : (new_up_range + this->upYRangeS);
            this->setUpperRange(this->upYRangeS, new_up_range);
        }
    }
    // Set Title
    void setLowerYTitle(std::string title) { this->lowerTitle = title; }

    void setUpperYTitle(std::string title) { this->upperTitle = title; }

    void setXTitle(std::string title) { this->xTitle = title; }

    // autoset
    void autosetUpperYTitle(std::string dimension = "GeV") {
        this->_useAutoYTitle = true;
        this->_AutoYTitle = dimension;
    }

    void ApplyAutoSetUpperYTitle(std::string dimension = "GeV") {
        double binwidth;
        std::string yname;
        if (bkghist != nullptr) {
            binwidth = this->bkghist->GetXaxis()->GetBinWidth(1);
        } else if (datahist != nullptr) {
            binwidth = this->datahist->GetXaxis()->GetBinWidth(1);
        } else if (sigStack.size() != 0) {
            auto hist = this->sigStack[0];
            binwidth = hist->GetXaxis()->GetBinWidth(1);
        }
        std::string binwidth_str = std::to_string(binwidth);
        binwidth_str.erase(binwidth_str.find_last_not_of('0') + 1, std::string::npos);
        binwidth_str.erase(binwidth_str.find_last_not_of('.') + 1, std::string::npos);
        yname = "Events / " + binwidth_str + " " + dimension;
        this->setUpperYTitle(yname);
    }
    // end auto set

    inline void doSimpleCompare() { this->simpleRatio = true; }

    inline void drawUpperText() {
        for (auto&& latex : upperLatexs) {
            latex->Draw();
        }
    }

    inline void drawLowerText() {
        for (auto&& latex : lowerLatexs) {
            latex->Draw();
        }
    }

    inline void drawUpperLegend() {
        for (auto&& legend : upperLegend) {
            legend->Draw("same");
        }
    }
    inline void drawLowerLegend() {
        for (auto&& legend : lowerLegend) {
            legend->Draw("same");
        }
    }

    inline void setXLabelNames(std::vector<std::string> XLabelNames) { this->XLabelNames = XLabelNames; }

    inline void setUpperYLabelNames(std::vector<std::string> upperYLabelNames) { this->upperYLabelNames = upperYLabelNames; }

    inline void setLowerYLabelNames(std::vector<std::string> lowerYLabelNames) { this->lowerYLabelNames = lowerYLabelNames; }

    inline void reDrawXLabel(TH1* hist) {
        TAxis* axis = hist->GetXaxis();
        int NBins = axis->GetNbins();
        if (NBins != this->XLabelNames.size()) {
            std::cerr << "The Bin Number of the X axis(" << NBins << ") is not same as the label size (" << this->XLabelNames.size()
                      << "). Please check and now will do nothing!" << std::endl;
            return;
        }
        axis->LabelsOption("h");
        for (unsigned int i = 1; i <= NBins; i++) {
            axis->SetBinLabel(i, XLabelNames[i - 1].c_str());
        }
    }

    inline void drawUpperTitle() {
        if (this->_useAutoYTitle == true) {
            ApplyAutoSetUpperYTitle(this->_AutoYTitle);
        }
        upperhist->GetYaxis()->SetTitle(upperTitle.c_str());
        if (upYRangeS != upYRangeE) {
            upperhist->GetYaxis()->SetRangeUser(upYRangeS, upYRangeE);
        }
        upperhist->GetYaxis()->SetTitleOffset(1.1);
        upperhist->GetYaxis()->SetMaxDigits(4);
        if (padChoice == USE_ZN_PAD || padChoice == USE_COM_PAD || padChoice == USE_INT_PAD || padChoice == USE_MRT_PAD) {
            upperhist->GetYaxis()->SetLabelOffset(0.005);
            upperhist->GetYaxis()->SetTitleOffset(0.9);
            upperhist->GetYaxis()->SetLabelSize(0.07);
            upperhist->GetYaxis()->SetTitleSize(0.07);
        } else if (padChoice == ONE_PAD) {
            upperhist->GetYaxis()->SetLabelOffset(0.02);
            upperhist->GetYaxis()->SetTitleOffset(1.25);
            // upperhist->GetYaxis()->SetLabelSize(0.07);
            // upperhist->GetYaxis()->SetTitleSize(0.07);
        }
    }

    inline void drawLowerTitle() {
        lowerhist->GetYaxis()->SetTitle(lowerTitle.c_str());
        if (lowYRangeS != lowYRangeE) {
            lowerhist->GetYaxis()->SetRangeUser(lowYRangeS, lowYRangeE);
        }
        lowerhist->GetYaxis()->SetNdivisions(505);
        lowerhist->GetYaxis()->SetLabelSize(0.18);
        lowerhist->GetYaxis()->SetTitleOffset(0.3);
        lowerhist->GetYaxis()->SetLabelOffset(0.02);
        lowerhist->GetYaxis()->SetTitleSize(0.16);
        lowerhist->GetYaxis()->SetTitleOffset(0.4);
        // lowerhist->GetYaxis()->SetTitleOffset(0.3);
        // lowerhist->GetYaxis()->SetTitleSize(0.16);
        lowerhist->GetYaxis()->SetNdivisions(503);
    }

    inline void drawXTitle() {
        if (padChoice == USE_ZN_PAD || padChoice == USE_COM_PAD || padChoice == USE_INT_PAD || padChoice == USE_MRT_PAD) {
            if (XRangeS != XRangeE) {
                upperhist->GetXaxis()->SetRangeUser(XRangeS, XRangeE);
                lowerhist->GetXaxis()->SetRangeUser(XRangeS, XRangeE);
            }
            upperhist->GetXaxis()->SetLabelSize(0);

            lowerhist->GetXaxis()->SetTitle(xTitle.c_str());
            lowerhist->GetXaxis()->SetLabelSize(0.2);
            lowerhist->GetXaxis()->SetTitleOffset(1.15);
            lowerhist->GetXaxis()->SetTitleSize(0.2);
            lowerhist->GetXaxis()->SetLabelOffset(0.02);

            if (XLabelNames.size() != 0) {
                reDrawXLabel(lowerhist);
                // lowerhist->GetXaxis()->SetLabelSize(0.1);
                lowerhist->GetXaxis()->SetLabelSize(0.3);
            }
            if (USE_LOG_X != "") {
                upperhist->GetXaxis()->SetMoreLogLabels();
                lowerhist->GetXaxis()->SetMoreLogLabels();
                lowerhist->GetXaxis()->SetTitle(xTitle.c_str());
                lowerhist->GetXaxis()->SetLabelSize(0.2);
                lowerhist->GetXaxis()->SetTitleOffset(1.15);
                lowerhist->GetXaxis()->SetTitleSize(0.2);
                lowerhist->GetXaxis()->SetLabelOffset(0.02);
            }

        } else {
            if (XRangeS != XRangeE) {
                upperhist->GetXaxis()->SetRangeUser(XRangeS, XRangeE);
            }
            if (XLabelNames.size() != 0) {
                reDrawXLabel(upperhist);
                upperhist->GetXaxis()->SetLabelSize(0.025);
            }
            upperhist->GetXaxis()->SetTitle(xTitle.c_str());
            //			upperhist->GetXaxis()->SetLabelSize(0.2);
            //			upperhist->GetXaxis()->SetTitleOffset(1.1);
            //			upperhist->GetXaxis()->SetTitleSize(0.2);
            //			upperhist->GetXaxis()->SetLabelOffset(0.02);
        }
    }

    TH1* GetLowerHist() { return this->lowerhist; }

    static double calcPoissonCLLower(double q, double obs) {
        if (obs > 0) {
            double a = (1. - q) / 2.;
            return TMath::ChisquareQuantile(a, 2. * obs) / 2.;
        }
        return 0;
    }

    static double calcPoissonCLUpper(double q, double obs) {
        if (obs > 0) {
            double a = 1 - (1. - q) / 2.;
            return TMath::ChisquareQuantile(a, 2. * (obs + 1.)) / 2.;
        }
        return 0;
    }

    double calZn(double sig, double nb, double relativeBkgUncert) {
        double Err = relativeBkgUncert * nb;
        double E2 = Err * Err;
        double n = sig + nb;
        double dll = n * TMath::Log(((sig + nb) * (nb + E2)) / (nb * nb + (sig + nb) * E2)) -
                     ((nb * nb) / (E2)) * TMath::Log((nb * nb + (sig + nb) * E2) / (nb * (nb + E2)));
        double nsigma = TMath::Sqrt(2. * dll);
        return nsigma;
    }

    ~Plotter();

private:
    // Inner classes
    class PlotArrow {
    public:
        PlotArrow(int color, double start, double end = -1.3123e10, double midY = -1.3123e10)
            : arrowColor(color), arrowS(start), arrowE(end), arrowMidY(midY) {}
        int arrowColor;
        double arrowS, arrowE, arrowMidY;
    };

    bool isLogY = false;
    bool _draw_lower_pad_arrow = true;
    bool _useAutoYTitle = false;
    std::string _AutoYTitle = "GeV";

    int padChoice;
    std::string USE_LOG_X = "";
    int RebinNumber;

    // zn pad property
    int ZnProperty = NOM_DIC;
    double znsyst = 0.3;

    // compare pad property
    TH1* compare1 = nullptr;
    TH1* compare2 = nullptr;
    TH1* denomhist = nullptr;
    bool normalizeCom = false;
    bool doIntegral = false;
    std::string otherHistPaintStyle = "l";

    std::string upperTitle = "";
    std::string lowerTitle = "";
    std::string plotTitle = "";
    std::string xTitle = "";

    // arrow seeting
    bool showArrow = false;
    bool simpleRatio = false;
    std::vector<PlotArrow> arrows;

    double upYRangeS = -1e10;
    double upYRangeE = -1e10;
    double lowYRangeS = -1e10;
    double lowYRangeE = -1e10;
    double XRangeS = -1e10;
    double XRangeE = -1e10;

    TPad* upperPad = nullptr;
    TPad* lowerPad = nullptr;
    TCanvas* canvas = nullptr;

    TH1* datahist = nullptr;
    TH1* bkghist = nullptr;

    std::vector<TH1*> bkgStack;
    std::vector<TH1*> sigStack;
    std::vector<TH1*> otherHists;

    // legends
    std::vector<TLegend*> upperLegend;
    std::vector<TLegend*> lowerLegend;

    // Tlatexs
    std::vector<TLatex*> upperLatexs;
    std::vector<TLatex*> lowerLatexs;

    // Axis label names
    std::vector<std::string> XLabelNames;
    std::vector<std::string> upperYLabelNames;
    std::vector<std::string> lowerYLabelNames;

    TH1* upperhist = nullptr;
    TH1* lowerhist = nullptr;

    static std::vector<TFile*> allocatedMemory;
};

std::vector<TFile*> Plotter::allocatedMemory;

Plotter::Plotter(std::string title, int padChoice, bool logYChoice, std::string use_log_x) {
    // Add the up Plot title
    addUpperText(0.25, 0.95, title, 0.05);
    Initialize(padChoice, logYChoice, use_log_x, 0.08);
}

Plotter::Plotter(int padChoice, bool logYChoice) { Initialize(padChoice, logYChoice); }

void Plotter::Initialize(int padChoice, bool logYChoice, std::string use_log_x, double topMargin) {
    this->RebinNumber = 1;
    this->padChoice = padChoice;
    canvas = new TCanvas("canvas", "canvas", 0, 0, 800, 600);
    if (padChoice == USE_ZN_PAD || padChoice == USE_COM_PAD || padChoice == USE_INT_PAD || padChoice == USE_MRT_PAD) {
        upperPad = new TPad("upperPad", "upperPad", .001, .24, 1.0, 1.0);
        lowerPad = new TPad("lowerPad", "lowerPad", .001, .001, 1.0, .27);
        upperPad->SetTopMargin(topMargin);
        upperPad->SetBottomMargin(0.063);
        upperPad->SetLeftMargin(0.12);
        upperPad->SetRightMargin(0.03);
        upperPad->Draw();
        if (USE_LOG_Y == logYChoice) {
            upperPad->SetLogy();
            this->isLogY = true;
        }
        if (use_log_x != "") {
            upperPad->SetLogx();
            lowerPad->SetLogx();
            this->USE_LOG_X = use_log_x;
        }
        if (use_log_x != "") {
            upperPad->SetLogx();
            lowerPad->SetLogx();
            this->USE_LOG_X = use_log_x;
        }
        lowerPad->SetTopMargin(0.0485);
        lowerPad->SetFrameFillStyle(4000);
        lowerPad->SetBottomMargin(0.5);
        // lowerPad->SetBottomMargin(0.47);
        lowerPad->SetLeftMargin(0.12);
        lowerPad->SetRightMargin(0.03);
        // lowerPad->SetGrid();
        lowerPad->Draw();
    } else {
        if (USE_LOG_Y == logYChoice) {
            canvas->SetLogy();
            this->isLogY = true;
        }
        if (USE_LOG_X != "") {
            canvas->SetLogx();
        }
        if (USE_LOG_X != "") {
            canvas->SetLogx();
        }
        canvas->SetTopMargin(topMargin);
        canvas->SetLeftMargin(0.12);
        canvas->SetRightMargin(0.05);
    }
}

TH1* Plotter::integralHist(TH1* hist, std::string newname) {
    TH1* newhist = (TH1*)hist->Clone(newname.c_str());
    for (int i = 0; i <= hist->GetNbinsX(); i++) {
        double histbin(0), histErr(0);
        histbin = hist->IntegralAndError(i, (hist->GetNbinsX() + 1), histErr);
        if (histbin < 0) histbin = 0;
        newhist->SetBinContent(i, histbin);
        newhist->SetBinError(i, histErr);
    }
    return newhist;
}

TH1* Plotter::pickHistRange(TH1* hist, double startValue, double endvalue) {
    std::string oldname = hist->GetName();
    std::string newname = oldname + "_" + std::to_string(startValue) + "_" + std::to_string(endvalue);
    TH1* newhist = (TH1*)hist->Clone(newname.c_str());
    for (int i = 0; i <= hist->GetNbinsX(); i++) {
        double histbin(0), histErr(0);
        if (hist->GetBinLowEdge(i + 1) > startValue && hist->GetBinLowEdge(i) < endvalue) {
            histbin = hist->GetBinContent(i);
            histErr = hist->GetBinError(i);
        }
        newhist->SetBinContent(i, histbin);
        newhist->SetBinError(i, histErr);
    }
    return newhist;
}

template <typename T> T* Plotter::getHist(std::string file, std::string name, int color) {
    TFile* tfile = new TFile(file.c_str());
    T* hist = (T*)tfile->Get(name.c_str());
    hist->Sumw2();
    if (color >= 0) {
        hist->SetLineColor(color);
    }
    allocatedMemory.push_back(tfile);
    return hist;
}

TH1* Plotter::getHist(std::string file, std::string name, int type, int color) {
    if(file.find_first_of("[]*?") != string::npos){
        TH1* hist = nullptr;
        auto TList = ExpandGlob(file);
        for (const auto &t : TList){
            TH1* h = (TH1*)getHist(t, name, type, color);
            if(!hist){
                hist = (TH1*)h->Clone(name.c_str());
                hist->SetDirectory(0);
            }else{
                hist->Add(h);
            }
        }
        return hist;
    } else {
        TFile* tfile = new TFile(file.c_str());
        TH1* hist = (TH1*)tfile->Get(name.c_str());
        hist->Sumw2();

        if (type == Block) {
            hist->SetFillColor(color);
            hist->SetLineColor(color);
        } else if (type == BlankBlock) {
            hist->SetFillColor(color);
            hist->SetLineColor(1);
        } else {
            hist->SetMarkerColor(color);
            hist->SetLineColor(color);
            if (type == DataLine) {
                hist->SetMarkerStyle(20);
            } else if (type == Dash) {
                hist->SetMarkerStyle(1);
                hist->SetLineStyle(7);
                // hist->SetLineWidth(3);
            } else if (type == Dot) {
                hist->SetMarkerStyle(1);
                hist->SetLineStyle(3);
            }
        }
      
        allocatedMemory.push_back(tfile);
        return hist;
    }
}

TH1* Plotter::getHist(std::string file, std::string region, std::string name, int type, int color) {
    TFile* tfile = new TFile(file.c_str());
    TH1* hist = (TH1*)((TDirectoryFile*)tfile->Get(region.c_str()))->Get(name.c_str());
    hist->Sumw2();

    if (type == Block) {
        hist->SetFillColor(color);
        hist->SetLineColor(color);
    } else if (type == BlankBlock) {
        hist->SetFillColor(color);
        hist->SetLineColor(1);
    } else {
        hist->SetMarkerColor(color);
        hist->SetLineColor(color);
        if (type == DataLine) {
            hist->SetMarkerStyle(20);
        } else if (type == Dash) {
            hist->SetMarkerStyle(1);
            hist->SetLineStyle(7);
            // hist->SetLineWidth(3);
        } else if (type == Dot) {
            hist->SetMarkerStyle(1);
            hist->SetLineStyle(3);
        }
    }
    allocatedMemory.push_back(tfile);
    return hist;
}

TH1* Plotter::getHist(std::string file, std::string region1, std::string region2, std::string name, int type, int color) {
    TFile* tfile = new TFile(file.c_str());
    TH1* hist = (TH1*)((TDirectoryFile*)tfile->Get(region1.c_str()))->Get(name.c_str());
    TH1* hist2 = (TH1*)((TDirectoryFile*)tfile->Get(region2.c_str()))->Get(name.c_str());
    hist->Sumw2();
    hist2->Sumw2();
    hist->Add(hist2);

    if (type == Block) {
        hist->SetFillColor(color);
        hist->SetLineColor(color);
    } else if (type == BlankBlock) {
        hist->SetFillColor(color);
        hist->SetLineColor(1);
    } else {
        hist->SetMarkerColor(color);
        hist->SetLineColor(color);
        if (type == DataLine) {
            hist->SetMarkerStyle(20);
        } else if (type == Dash) {
            hist->SetMarkerStyle(1);
            hist->SetLineStyle(7);
            // hist->SetLineWidth(3);
        } else if (type == Dot) {
            hist->SetMarkerStyle(1);
            hist->SetLineStyle(3);
        }
    }
    allocatedMemory.push_back(tfile);
    return hist;
}

TH1* Plotter::getHist(std::string file, std::string treename, std::string branchname, std::string bWeight, int nBin, double binStart, double binEnd,
                      int type, int color) {
    TFile* tfile = new TFile(file.c_str());
    TTree* ttree = (TTree*)tfile->Get(treename.c_str());
    std::string bDataType = ttree->GetLeaf(branchname.c_str())->GetTypeName();

    TH1* hist = new TH1F(branchname.c_str(), branchname.c_str(), nBin, binStart, binEnd);
    hist->Sumw2();
    double lastbinCenter = hist->GetBinCenter(hist->GetNbinsX());

    if (type == Block) {
        hist->SetFillColor(color);
        hist->SetLineColor(color);
    } else if (type == BlankBlock) {
        hist->SetFillColor(color);
        hist->SetLineColor(1);
    } else {
        hist->SetMarkerColor(color);
        hist->SetLineColor(color);
        if (type == DataLine) {
            hist->SetMarkerStyle(20);
        } else if (type == Dash) {
            hist->SetMarkerStyle(1);
            hist->SetLineStyle(7);
            // hist->SetLineWidth(3);
        } else if (type == Dot) {
            hist->SetMarkerStyle(1);
            hist->SetLineStyle(3);
        }
    }

    TTreeReader* TReader = new TTreeReader(treename.c_str(), tfile);
    TTreeReaderValue<double> TReaderWei(*TReader, bWeight.c_str());
    if (bDataType == "Double_t") {
        TTreeReaderValue<double> TReaderVal(*TReader, branchname.c_str());
        while (TReader->Next()) {
            double histVal = *TReaderVal;
            double histWei = *TReaderWei;
            histVal = (histVal > lastbinCenter) ? lastbinCenter : histVal;
            hist->Fill(histVal, histWei);
        }
    } else if (bDataType == "Float_t") {
        TTreeReaderValue<float> TReaderVal(*TReader, branchname.c_str());
        while (TReader->Next()) {
            float histVal = *TReaderVal;
            double histWei = *TReaderWei;
            histVal = (histVal > lastbinCenter) ? lastbinCenter : histVal;
            hist->Fill(histVal, histWei);
        }
    } else if (bDataType == "Int_t") {
        TTreeReaderValue<int> TReaderVal(*TReader, branchname.c_str());
        while (TReader->Next()) {
            float histVal = *TReaderVal;
            double histWei = *TReaderWei;
            histVal = (histVal > lastbinCenter) ? lastbinCenter : histVal;
            hist->Fill(histVal, histWei);
        }
    }
    SafeDelete(TReader);
    SafeDelete(ttree);
    allocatedMemory.push_back(tfile);
    return hist;
}

void Plotter::addArrow(int color, double start, double end, double midY) {
    showArrow = true;
    this->arrows.emplace_back(PlotArrow(color, start, end, midY));
}

void Plotter::Rebin(std::vector<double> xbins) {
    int rebinNum = xbins.size() - 1;
    Double_t arr[xbins.size()];
    std::copy(xbins.begin(), xbins.end(), arr);
    // for(int i(0);i<xbins.size();i++){
    //    std::cout<<arr[i]<<std::endl;
    //}
    this->Rebin(rebinNum, arr);
}

void Plotter::Rebin(int rebinNum, const Double_t* xbins) {
    this->RebinNumber = rebinNum;
    if (bkghist != nullptr) {
        if (xbins == 0) {
            bkghist->Rebin(rebinNum);
            for (auto&& hist : bkgStack) {
                hist->Rebin(rebinNum);
            }
        } else {
            int i = 0;
            std::string newname = (std::string)bkghist->GetName() + "new";
            bkghist = bkghist->Rebin(rebinNum, newname.c_str(), xbins);
            for (auto&& hist : bkgStack) {
                std::string newname = (std::string)hist->GetName() + "new";
                bkgStack[i] = hist->Rebin(rebinNum, newname.c_str(), xbins);
                i++;
            }
        }
    }
    // We have signal hists
    if (sigStack.size() != 0) {
        int i = 0;
        for (auto&& hist : sigStack) {
            if (xbins == 0) {
                hist->Rebin(rebinNum);
            } else {
                std::string newname = (std::string)hist->GetName() + "new";
                sigStack[i] = hist->Rebin(rebinNum, newname.c_str(), xbins);
                i++;
            }
        }
    }
    // We have data hists
    if (datahist != nullptr) {
        if (xbins == 0) {
            datahist->Rebin(rebinNum);
        } else {
            std::string newname = (std::string)datahist->GetName() + "new";
            datahist = datahist->Rebin(rebinNum, newname.c_str(), xbins);
        }
    }
    if (otherHists.size() != 0) {
        int i = 0;
        for (auto&& hist : otherHists) {
            if (xbins == 0) {
                hist->Rebin(rebinNum);
            } else {
                std::string newname = (std::string)hist->GetName() + "new";
                otherHists[i] = hist->Rebin(rebinNum, newname.c_str(), xbins);
                i++;
            }
        }
    }
}
void Plotter::autoRebin(double BkgRelErrorMax,double BkgYieldsMin, 
        double SigRelErrorMax, double SigYieldsMin){
    std::vector<Double_t> newBins;
    if (bkghist != nullptr) {
        Double_t total = 0;
        Double_t totalErr = 0;
        Double_t totalSig = 0;
        Double_t totalErrSig = 0;
        Double_t binEdge = bkghist->GetXaxis()->GetXmin();
        newBins.push_back(binEdge);
        
        for (int i = 1; i <= bkghist->GetNbinsX(); i++) {
            Double_t binContent = bkghist->GetBinContent(i);
            Double_t binError = bkghist->GetBinError(i);
            
            total += binContent;
            totalErr = sqrt(totalErr * totalErr + binError * binError);
            if (sigStack.size()>0) {
                Double_t binContentSig = sigStack[0]->GetBinContent(i);
                Double_t binErrorSig = sigStack[0]->GetBinError(i);
                totalSig += binContentSig;
                totalErrSig = sqrt(totalErrSig * totalErrSig + binErrorSig * binErrorSig);
            }
            if (total > 0 && (totalErr / total) < BkgRelErrorMax && total > BkgYieldsMin && 
                (sigStack.size()==0 || ((totalErrSig/totalSig) < SigRelErrorMax && totalSig > SigYieldsMin)) ){
                binEdge = bkghist->GetXaxis()->GetBinUpEdge(i);
                newBins.push_back(binEdge);
                total = 0;
                totalErr = 0;
                totalSig = 0;
                totalErrSig = 0;
            }
        }
        
        if (newBins.back() != bkghist->GetXaxis()->GetXmax()) {
            newBins.push_back(bkghist->GetXaxis()->GetXmax());
        }
    }
    if (newBins.size() > 1) {
        Double_t* xbins = &newBins[0];
        int newBinsSize = newBins.size() - 1;
        
        // Apply rebinning to histograms
        std::string newname = std::string(bkghist->GetName()) + "_rebin";
        bkghist = bkghist->Rebin(newBinsSize, newname.c_str(), xbins);
        
        for (size_t i = 0; i < bkgStack.size(); i++) {
            std::string newname = std::string(bkgStack[i]->GetName()) + "_rebin";
            bkgStack[i] = bkgStack[i]->Rebin(newBinsSize, newname.c_str(), xbins);
        }
        
        for (size_t i = 0; i < sigStack.size(); i++) {
            std::string newname = std::string(sigStack[i]->GetName()) + "_rebin";
            sigStack[i] = sigStack[i]->Rebin(newBinsSize, newname.c_str(), xbins);
        }
        
        if (datahist != nullptr) {
            std::string newname = std::string(datahist->GetName()) + "_rebin";
            datahist = datahist->Rebin(newBinsSize, newname.c_str(), xbins);
        }
    }
}
void Plotter::attachIntegralPad() {
    if (lowerPad == nullptr) {
        std::cerr << "No lowerPad!" << std::endl;
        return;
    }
    lowerPad->cd();
    lowerPad->SetGridy();
    if (sigStack.size() == 0) {
        std::cerr << "No signal Hist!" << std::endl;
        return;
    }
    int num = 666;
    for (unsigned int i = 0; i < sigStack.size(); i++) {
        num = num * (i + 1);
        auto hist = sigStack[i];
        // Clone a signal hist named with a number;
        TH1* znHist = (TH1*)hist->Clone((std::to_string(num)).c_str());
        int NBins = znHist->GetXaxis()->GetNbins();
        for (int ibin = 1; ibin <= NBins; ibin++) {
            double errsig = 0;
            double sigYield = hist->IntegralAndError(1, ibin, errsig);
            znHist->SetBinContent(ibin, sigYield);
            znHist->SetBinError(ibin, errsig);
        }
        znHist->SetTitle("");
        znHist->SetLineStyle(1);
        znHist->SetFillStyle(0);
        znHist->SetMarkerSize(.8);
        if (i == 0) {
            znHist->Draw("hist");
            lowerhist = znHist;
        } else {
            znHist->Draw("histsame");
        }
    }
}
void Plotter::attachZnPad() {
    if (lowerPad == nullptr) {
        std::cerr << "No lowerPad!" << std::endl;
        return;
    }
    lowerPad->cd();
    lowerPad->SetGridy();
    // lowerPad->SetGridx();
    if (nullptr == bkghist || sigStack.size() == 0) {
        std::cerr << "No bkg Hist or signal Hist!" << std::endl;
        return;
    }
    int num = 666;
    double bestZn(0),sumZn2(0);// writeZn
    for (unsigned int i = 0; i < sigStack.size(); i++) {
        num = num * (i + 1);
        auto hist = sigStack[i];
        // Clone a signal hist named with a number;
        TH1* znHist = (TH1*)hist->Clone((std::to_string(num)).c_str());
        int NBins = znHist->GetXaxis()->GetNbins();
        for (int ibin = 1; ibin <= NBins; ibin++) {
            double errb = 0;
            double errsig = 0;
            double sigYield, bkgYield;
            if (this->ZnProperty == BINNED_ZN) {
                sigYield = hist->GetBinContent(ibin);
                bkgYield = bkghist->GetBinContent(ibin);
                errsig = hist->GetBinError(ibin);
                errb = bkghist->GetBinError(ibin);
            } else {
                sigYield = hist->IntegralAndError(ibin, NBins + 1, errsig);
                bkgYield = bkghist->IntegralAndError(ibin, NBins + 1, errb);
            }
            if (bkgYield <= 0) {
                znHist->SetBinContent(ibin, 0);
            } else {
                double Zn = calZn(sigYield, bkgYield, sqrt(pow(znsyst, 2) + pow((errb / bkgYield), 2)));
                znHist->SetBinContent(ibin, Zn);
                sumZn2 += Zn * Zn ;// writeZn
                if (sigYield <= 0) {
                    znHist->SetBinContent(ibin, -3);
                }
            }
        }
        bestZn = std::max(bestZn, znHist->GetMaximum());         // writeZn  
        if (this->ZnProperty == BINNED_ZN) bestZn = sqrt(sumZn2);// writeZn 
        znHist->SetTitle("");
        znHist->SetLineStyle(1);
        znHist->SetFillStyle(0);
        znHist->SetMarkerSize(.8);
        if (i == 0) {
            znHist->Draw("hist");
            lowerhist = znHist;
        } else {
            znHist->Draw("histsame");
        }
    }
    TLatex* tex = new TLatex();// writeZn
    tex->SetNDC(true);
    tex->SetTextAlign(31);
    tex->SetTextFont(42);
    tex->SetTextSize(0.09);
    tex->DrawLatex(0.88, 0.88, Form("Z_{n} = %.2f", bestZn));
}

void Plotter::attachMultiComparePad() {
    // this->denomhist
    if (lowerPad == nullptr) {
        std::cerr << "No lowerPad!" << std::endl;
        return;
    }
    lowerPad->cd();
    lowerPad->SetGridy();
    // lowerPad->SetGridx();
    if (nullptr == denomhist || sigStack.size() == 0) {
        std::cerr << "No denominator Hist or signal Hist!" << std::endl;
        return;
    }
    denomhist->Sumw2();
    int num = 666;
    // double max_bin_value =0;
    for (unsigned int i = 0; i < sigStack.size(); i++) {
        num = num * (i + 1);
        auto hist = sigStack[i];
        TH1* rHist = (TH1*)hist->Clone((std::to_string(num)).c_str());
        rHist->Sumw2();
        int NBins = rHist->GetXaxis()->GetNbins();
        rHist->Divide(denomhist);

        rHist->SetTitle("");
        rHist->SetLineStyle(1);
        rHist->SetFillStyle(0);
        rHist->SetMarkerSize(.8);

        // if (rHist->GetMaximum() > max_bin_value){ max_bin_value = rHist->GetMaximum();}
        if (i == 0) {
            rHist->Draw("hist");
            lowerhist = rHist;
        } else {
            rHist->Draw("histsame");
        }
    }
    // this->setLowerRange(this->lowYRangeS, max_bin_value*1.3);
}

void Plotter::attachflipZnPad() {
    if (lowerPad == nullptr) {
        std::cerr << "No lowerPad!" << std::endl;
        return;
    }
    lowerPad->cd();
    lowerPad->SetGridy();
    if (nullptr == bkghist || sigStack.size() == 0) {
        std::cerr << "No bkg Hist or signal Hist!" << std::endl;
        return;
    }
    int num = 666;
    for (unsigned int i = 0; i < sigStack.size(); i++) {
        num = num * (i + 1);
        auto hist = sigStack[i];
        TH1* znHist = (TH1*)hist->Clone((std::to_string(num)).c_str());
        int NBins = znHist->GetXaxis()->GetNbins();
        for (int ibin = 1; ibin <= NBins; ibin++) {
            double errb = 0;
            double errsig = 0;
            double sigYield = hist->IntegralAndError(1, ibin, errsig);
            double bkgYield = bkghist->IntegralAndError(1, ibin, errb);
            if (bkgYield <= 0) {
                znHist->SetBinContent(ibin, 0);
            } else {
                double Zn = calZn(sigYield, bkgYield, sqrt(pow(znsyst, 2) + pow((errb / bkgYield), 2)));
                znHist->SetBinContent(ibin, Zn);
                if (sigYield <= 0) {
                    znHist->SetBinContent(ibin, -3);
                }
            }
        }
        znHist->SetTitle("");
        znHist->SetLineStyle(1);
        znHist->SetFillStyle(0);
        znHist->SetMarkerSize(.8);
        if (i == 0) {
            znHist->Draw("hist");
            lowerhist = znHist;
        } else {
            znHist->Draw("histsame");
        }
    }
}

void Plotter::attachComparePad() {
    if (lowerPad == nullptr) {
        std::cerr << "No lowerPad!" << std::endl;
        return;
    }
    lowerPad->cd();
    lowerPad->SetGridy();
    // lowerPad->SetGridx();
    if (nullptr == compare1 || nullptr == compare2) {
        std::cerr << "No compare1 hist or compare2 Hist!" << std::endl;
        return;
    }
    TH1F* ratio = (TH1F*)compare1->Clone("ratio");
    TH1F* compare222 = (TH1F*)compare2->Clone("compare222");

    ratio->Sumw2();
    compare222->Sumw2();
    if (doIntegral) {
        Double_t err1, err2;
        for (int i = 0; i <= compare222->GetNbinsX(); i++) {
            Double_t content1 = ratio->IntegralAndError(i, ratio->GetNbinsX(), err1);
            Double_t content2 = compare222->IntegralAndError(i, compare222->GetNbinsX(), err2);
            ratio->SetBinContent(i, content1);
            ratio->SetBinError(i, err1);
            compare222->SetBinContent(i, content2);
            compare222->SetBinError(i, err2);
        }
    }
    if (normalizeCom) {
        ratio->Scale(1 / ratio->Integral());
        compare222->Scale(1 / compare222->Integral());
    }
    ratio->SetStats(0);
    ratio->Divide(compare222);
    if (!simpleRatio) {
        // set error
        for (int i = 1; i <= ratio->GetNbinsX(); i++) {
            // relative bin_err = bin_err/bin_content
            // relative bin_err(scaled) = (a_err/a_content) * (a_content/b_content) = a_err/b_content
            if (compare2->GetBinContent(i) != 0) {
                ratio->SetBinError(i, compare1->GetBinError(i) / compare2->GetBinContent(i));
            }
        }
        compare222->Divide(compare222);
        for (int i = 1; i <= compare222->GetNbinsX(); i++) {
            if (compare2->GetBinContent(i) != 0) {
                compare222->SetBinError(i, compare2->GetBinError(i) / compare2->GetBinContent(i));
            }
        }
        compare222->SetFillStyle(3002);
        compare222->SetFillColor(kBlack);
        compare222->SetMarkerStyle(1);
        ratio->SetLineColor(kBlack);
        lowerhist = ratio;
        // do something for data/MC
        if (compare1 == datahist) {
            ratio->Draw("HISTP");
            // ratio->SetMarkerSize(0.0);
            // ratio->SetLineWidth(1);
            // ratio->SetLineStyle(1);
            TGraphAsymmErrors* data_err = TH1TOTGraphAsymNoerrorx(compare1, compare2);
            data_err->SetMarkerStyle(20);
            data_err->SetMarkerColor(kBlack);
            data_err->SetLineWidth(3);
            data_err->SetMarkerSize(0.0);
            // data_err->SetMarkerSize(1.5);
            data_err->Draw("PE same");
        } else {
            ratio->Draw("EP");
        }
        compare222->Draw("e2][same");
    } else {
        lowerhist = ratio;
        ratio->Draw("EP");
    }
    for (int loopbin = 1; loopbin <= ratio->GetNbinsX(); loopbin++) {
        double y_ratio = ratio->GetBinContent(loopbin);
        double x = ratio->GetBinCenter(loopbin);
        if (_draw_lower_pad_arrow && y_ratio > lowYRangeE && (XRangeE == XRangeS || (x < XRangeE && x > XRangeS))) {
            TArrow* _b = new TArrow(x, (lowYRangeS + 3 * lowYRangeE) / 4, x, lowYRangeE, 0.01, "->");
            _b->SetLineWidth(2);
            _b->SetLineColor(kRed);
            _b->SetLineStyle(1);
            _b->Draw();
        }
        if (_draw_lower_pad_arrow && y_ratio < lowYRangeS && (XRangeE == XRangeS || (x < XRangeE && x > XRangeS))) {
            TArrow* _b = new TArrow(x, lowYRangeS, x, (3 * lowYRangeS + lowYRangeE) / 4, 0.01, "<-");
            _b->SetLineWidth(2);
            _b->SetLineColor(kRed);
            _b->SetLineStyle(1);
            _b->Draw();
        }
    }
}

void Plotter::Draw(std::string name) {
    if (!(padChoice == USE_ZN_PAD || padChoice == USE_COM_PAD || padChoice == USE_INT_PAD || padChoice == USE_MRT_PAD)) {
        drawHists();
        drawUpperLegend();
        drawUpperText();
        drawUpperTitle();
        if (showArrow) {
            drawArrow();
        }
        gPad->RedrawAxis();
    } else {
        upperPad->cd();
        drawHists();
        drawUpperLegend();
        drawUpperText();
        drawUpperTitle();
        if (showArrow) {
            drawArrow();
        }
        gPad->RedrawAxis();
        if (padChoice == USE_ZN_PAD) {
            if (ZnProperty == FLIP_DIC) {
                attachflipZnPad();
            } else {
                attachZnPad();
            }
        } else if (padChoice == USE_COM_PAD) {
            attachComparePad();
        } else if (padChoice == USE_INT_PAD) {
            attachIntegralPad();
        } else if (padChoice == USE_MRT_PAD) {
            attachMultiComparePad();
        }
        drawLowerLegend();
        drawLowerText();
        drawLowerTitle();
    }
    drawXTitle();
    canvas->SetTitle(name.c_str());
    gStyle->SetTitleFontSize(0.08);
    const std::string saveName1 = name + ".png";
    const std::string saveName2 = name + ".pdf";
    // const std::string saveName3 = name + ".eps";
    canvas->SaveAs(saveName1.c_str());
    canvas->SaveAs(saveName2.c_str());
    // canvas->SaveAs(saveName3.c_str());
}

// draw hists
void Plotter::drawHists() {
    // For the first hist, we set it as the upper hist and use Draw("xxx") directly. For the other hists, we use
    // Draw("xxxsame") We have bkg hists
    if (bkghist != nullptr) {
        if (upperhist == nullptr) {
            upperhist = bkghist;
            bkghist->Draw("e2");
        } else {
            bkghist->Draw("e2same");
        }

        THStack* bkg = new THStack("", "Distribution");
        for (auto&& bkghist : bkgStack) {
            bkg->Add(bkghist);
        }

        bkg->Draw("histsame");
        bkghist->Draw("e2same");
        TH1F* bkghist2 = (TH1F*)bkghist->Clone("TotalBkgHist2");
        bkghist2->SetFillStyle(0);
        bkghist2->SetLineColor(1);
        bkghist2->SetLineWidth(2);
        bkghist2->SetFillColor(0);
        bkghist2->Draw("histsame");
    }
    // We have signal hists
    if (sigStack.size() != 0) {
        for (auto&& hist : sigStack) {
            if (upperhist == nullptr) {
                upperhist = hist;
                hist->Draw("Ehist");
            } else {
                hist->Draw("Ehistsame");
            }
        }
    }
    // We have data hists
    if (datahist != nullptr) {
        if (upperhist == nullptr) {
            upperhist = datahist;
            datahist->Draw("HISTP");
        } else {
            datahist->Draw("HISTPsame");
        }
        TGraphAsymmErrors* data_err = TH1TOTGraphAsymNoerrorx(datahist);
        data_err->SetMarkerStyle(20);
        data_err->SetMarkerColor(kBlack);
        data_err->SetLineWidth(3);
        if (padChoice == ONE_PAD) {
            data_err->SetMarkerSize(1.5);
        } else {
            data_err->SetMarkerSize(0.8);
        }
        data_err->Draw("P same");
    }
    if (otherHists.size() != 0) {
        for (auto&& hist : otherHists) {
            if (upperhist == nullptr) {
                upperhist = hist;
                hist->Draw(otherHistPaintStyle.c_str());
            } else {
                hist->Draw((otherHistPaintStyle + "same").c_str());
            }
        }
    }
    // There is no any hist in the program
    if (upperhist == nullptr) {
        std::cerr << "Fetal Error!!!!!!! There is no any histograms!!!" << std::endl;
    }
}

// draw Arrow
void Plotter::drawArrow() {
    if (upperhist == nullptr) {
        std::cerr << "You must add histograms before adding a arrow!" << std::endl;
        return;
    }
    // loop and draw all arrow
    for (auto&& a : this->arrows) {
        if (a.arrowMidY == -1.3123e10) {
            double maxY = upperhist->GetMaximum();
            double minY = upperhist->GetMinimum();
            // midY = 10^(0.4*lg(max) + 0.4*lg(min)) = 10^(lg(max^0.4)) * 10^(lg(min^0.4)) = max^0.4 * min^0.4 = (max *
            // min) ^ 0.4
            a.arrowMidY = pow(maxY * minY, 0.4);
        }
        if (a.arrowE == -1.3123e10) {
            double XEnd = upperhist->GetXaxis()->GetXmax();
            a.arrowE = a.arrowS + (XEnd - a.arrowS) * 1 / 2;
        }
        TLine* line = new TLine(a.arrowS, 0, a.arrowS, a.arrowMidY);
        line->SetLineWidth(4);
        line->SetLineStyle(1);
        line->SetLineColor(a.arrowColor);

        TArrow* arrow = new TArrow(a.arrowS, a.arrowMidY, a.arrowE, a.arrowMidY, 0.02, "->");
        arrow->SetLineWidth(4);
        arrow->SetLineColor(a.arrowColor);
        arrow->SetLineStyle(1);

        line->Draw();
        arrow->Draw();
    }
}

TGraphAsymmErrors* Plotter::TH1TOTGraphAsymNoerrorx(TH1* h1, TH1* scaleHist) {
    TGraphAsymmErrors* g1 = new TGraphAsymmErrors();

    if (scaleHist != nullptr && h1->GetNbinsX() != scaleHist->GetNbinsX()) {
        std::cerr << "Error!!!: scale hist is wrong!" << std::endl;
    }

    Double_t x, y, eylow, eyhigh;
    for (Int_t i = 1; i <= h1->GetNbinsX(); i++) {
        y = h1->GetBinContent(i);
        double scale;
        if (scaleHist != nullptr) {
            scale = scaleHist->GetBinContent(i);
        } else {
            scale = 1;
        }
        if (scale == 0) scale = 1e-20;
        x = h1->GetBinCenter(i);
        if (y == 0.)
            eyhigh = 0.;
        else
            eyhigh = calcPoissonCLUpper(0.68, y) - y;
        eylow = y - calcPoissonCLLower(0.68, y);
        if (y > 0) {
            g1->SetPoint((i - 1), x, y / scale);
            g1->SetPointError((i - 1), 0., 0., eylow / scale, eyhigh / scale);
        } else {
            g1->SetPoint((i - 1), x, 1e-30);
        }
    }
    return g1;
}

std::vector<std::vector<double>> Plotter::printHist(TH1* hist, bool isData) {
    std::vector<std::vector<double>> info;
    std::vector<double> infoyield;
    std::vector<double> infoErrUp;
    std::vector<double> infoErrDw;
    if (!isData) {
        for (int i = 1; i <= hist->GetNbinsX(); i++) {
            double binC = hist->GetBinContent(i);
            double errUp = hist->GetBinError(i);
            double errDw = 0 - hist->GetBinError(i);
            std::cout << hist->GetBinLowEdge(i) << " - " << hist->GetBinLowEdge(i + 1) << "   ,   " << binC << "   ,   " << errUp << "   ,   "
                      << errDw << std::endl;
            infoyield.emplace_back(binC);
            infoErrUp.emplace_back(errUp);
            infoErrDw.emplace_back(errDw);
        }
    } else {
        for (int i = 1; i <= hist->GetNbinsX(); i++) {
            double binC = hist->GetBinContent(i);
            double eyhigh = (binC == 0) ? 0 : calcPoissonCLUpper(0.68, binC) - binC;
            double eylow = 0 - ((binC == 0) ? 0 : binC - calcPoissonCLLower(0.68, binC));
            std::cout << hist->GetBinLowEdge(i) << " - " << hist->GetBinLowEdge(i + 1) << "   ,   " << binC << "   ,   " << eyhigh << "   ,   "
                      << eylow << std::endl;
            infoyield.emplace_back(binC);
            infoErrUp.emplace_back(eyhigh);
            infoErrDw.emplace_back(eylow);
        }
    }
    info.emplace_back(infoyield);
    info.emplace_back(infoErrUp);
    info.emplace_back(infoErrDw);
    return info;
}

void Plotter::deallocateMemory() {
    for (auto tf : allocatedMemory) {
        delete tf;
    }
    allocatedMemory.clear();
}

Plotter::~Plotter() {}

std::vector<std::string> ExpandGlob(std::string glob){
    // search files in one directory
    std::vector<std::string> out;
    std::string dirname;
    std::string basename;
  
    const char *wildcardSpecials = "[]*?";
    const auto wildcardPos = glob.find_first_of(wildcardSpecials);
    auto slashLPos = glob.rfind('/', wildcardPos);
  
    if (slashLPos != std::string::npos) {
        dirname = glob.substr(0, slashLPos);
    } else {
        dirname = std::filesystem::current_path();
        slashLPos = -1;
    }
    basename = glob.substr(slashLPos + 1);
 
    std::string regexPattern;
    for (char ch : basename) {
        switch (ch) {
            case '*':
                regexPattern += ".*";
                break;
            case '?':
                regexPattern += ".";
                break;
            case '[':
            case ']':
                regexPattern += ch;
                break;
            default:
                regexPattern += ch;
                break;
        }
    }
    std::regex regex(regexPattern);
    for (const auto & entry : std::filesystem::directory_iterator(dirname)){
        std::string entryName = entry.path().filename().string();
        if (std::regex_match(entryName, regex)) {
            out.push_back(entry.path().string());
        }
    }
    return out;
}