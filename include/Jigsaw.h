#pragma once
#include <Math/Vector4D.h>
#include <Math/Boost.h>
#include <math.h>

#include <vector>

#include "AnaObjs.h"
// https://arxiv.org/pdf/1705.10733
class JigsawTool {
public:
    JigsawTool(AnaObj Va, AnaObj Vb, AnaObj I, AnaObjs Jets={}): 
        _p4_Va(Va), _p4_Vb(Vb), _p4_I(I)
    {
        if(Jets.size()>0) {
            _jets_available = true;
            _p4_jets = Jets.sumObjects();
        }
    };
    // std::pair<ROOT::Math::PxPyPzEVector, ROOT::Math::PxPyPzEVector> 
    void reco_neutralinos(double mu=70);
    ROOT::Math::PxPyPzEVector get_IA(){return this->_p4_Ia;};
    ROOT::Math::PxPyPzEVector get_IB(){return this->_p4_Ib;};
    ROOT::Math::PxPyPzEVector get_A(){return this->_p4_Ia + this->_p4_Va;};
    ROOT::Math::PxPyPzEVector get_B(){return this->_p4_Ib + this->_p4_Vb;};

private:
    AnaObj _p4_Va;
    AnaObj _p4_Vb;
    AnaObj _p4_I;
    AnaObj _p4_jets;
    bool _jets_available;
    ROOT::Math::PxPyPzEVector _p4_Ia;
    ROOT::Math::PxPyPzEVector _p4_Ib;

};
