#pragma once
#include <Math/Vector4D.h>
#include <Math/Boost.h>
#include <math.h>

#include <vector>

#include "AnaObjs.h"
class Thrust {
public:
    Thrust(AnaObjs momenta4, bool noZ = false, bool boost2CM = false, bool invRapidity = false, double mu=70);
    double get_thrust() { return this->_thrust; };
    double get_thrust_major() { return this->_thrust_major; };
    double get_thrust_minor() { return this->_thrust_minor; };

    // RJR
    std::pair<ROOT::Math::PtEtaPhiMVector, ROOT::Math::PtEtaPhiMVector> assgin_frame(AnaObjs allobjs, AnaObj FOI);
    // std::pair<int, int> count_objects(AnaObjs allobjs, AnaObj FOI);

private:
    bool calculate_thrust();
    void calculate_general_thrust(const std::vector<TVector3>& momenta, double& val, TVector3& taxis);

    std::vector<TVector3> _momenta;
    ROOT::Math::PtEtaPhiMVector _momentum_CM;
    ROOT::Math::PtEtaPhiMVector _momentum_V;
    ROOT::Math::PtEtaPhiMVector _momentum_I;
    double _thrust;
    double _thrust_major;
    double _thrust_minor;
    TVector3 _thrust_axis;
    TVector3 _thrust_axis_major;
    TVector3 _thrust_axis_minor;
    bool _validity;
};
