#pragma once
#include <TChain.h>

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "AnaConfigReader.h"
#include "AnaRunner.h"
#include "CutCountLooper.h"
#include "ThreadPool.h"
#include "Utils.h"
#include "easylogging++.h"

/**************  Warning! Be wary of the large memory consumption! *****************
 * Strategy: 1. Read the signal tree Name and background tree Name
 *           2. Load the cut variables names and their steps, compare cunctions
 *           3. Loop each trees and get the 2D vector about the event and var, weights --> Done at CutCountLooper
 *           4. Loop again these 2D vectors and fill the yields tables and error tables of each sample. Here we
 *              can safely use multi-thread
 *           5. filter the reaults using the defined constraint
 *           6. Print out the reaults
 *************************************************************************************/

class CutCountVecFiller : public Runnable {
public:
    template <class T> using vector2D = std::vector<std::vector<T>>;

    CutCountVecFiller(std::string sName, vector2D<float>* eSlices, vector2D<float> sPools, std::vector<int> stepSizeHelper, std::vector<bool> cFuncs,
                      std::vector<float>* yPool, std::vector<float>* ePool, std::vector<int>* rPool)
        : sampleName(sName), eventsSlices(eSlices), yieldsPools(yPool), errorPools(ePool), rawnumPools(rPool) {
        if (this->eventsSlices->size() > 0) {
            std::lock_guard<std::mutex> lock(_mutex);

            this->stepPools = sPools;
            this->_stepSizeHelper = stepSizeHelper;
            this->compareFuncs = cFuncs;
            // Here we should use size - 1 because the last one is weight
            // defined in setVariables()
            int varNums = eventsSlices[0].size() - 1;
            // The fillVecs stores the fill position start and end of each var. So it's varNums
            // vector::reserve(n), Requests that the vector capacity be at least enough to contain n elements.
            this->fillStore.reserve(varNums);
            this->vecFillStarts.reserve(varNums);
            this->vecFillEnds.reserve(varNums);
        }
        int eSliceColumn = (eSlices->size() == 0) ? 0 : eSlices->at(0).size() - 1;
        LOG(DEBUG) << "The normalized event column: " << eSliceColumn << " stepPool column: " << sPools.size()
                   << " stepHelper column: " << stepSizeHelper.size() + 1 << " compareFuncs column: " << cFuncs.size()
                   << " output event yield table size: " << yPool->size() << " output event error table size: " << ePool->size();
    };

    void run() override {
        LOG(INFO) << "Start the CutCount for sample " << this->sampleName << " with " << this->eventsSlices->size() << " events and "
                  << stepPools[0].size() * this->_stepSizeHelper[0] << " cut steps";
        for (int i = 0; i < this->eventsSlices->size(); i++) {
            // badEvent: It cannot pass any combination of the selection
            bool badEvent(false);

            this->vecFillStarts.clear();
            this->vecFillEnds.clear();
            this->fillStore.clear();
            int varNums = this->eventsSlices->at(i).size() - 1;
            for (int j = 0; j < varNums; j++) {
                int posi = Utils::positionSearch(this->eventsSlices->at(i)[j], stepPools.at(j));
                // true == larger, false == smaller
                if (this->compareFuncs.at(j)) {
                    if (posi < 0) {
                        badEvent = true;
                        break;
                    }
                    // If the found posi is vec.size() -1, means we should fill the whole vec: [0, posi]
                    this->vecFillStarts.emplace_back(0);
                    this->fillStore.emplace_back(0);
                    this->vecFillEnds.emplace_back(posi);
                } else {
                    if (posi == stepPools.at(j).size() - 1) {
                        badEvent = true;
                        break;
                    }
                    // If the found posi is -1, means we should fill the whole vec: [0, vec.size() -1]
                    this->vecFillStarts.emplace_back(posi + 1);
                    this->fillStore.emplace_back(posi + 1);
                    this->vecFillEnds.emplace_back(stepPools.at(j).size() - 1);
                }
            }
            if (!badEvent) {
                fill(this->eventsSlices->at(i).back());
            }
        }
        LOG(INFO) << "CutCount for sample " << this->sampleName << " is done";
    }

    void fill(const float& weight) {
        while (true) {
            fillSingleEntry(weight);
            this->fillStore.back()++;
            // Add the number up, if it's larger than the value of endVec, set to initial and let the before value + 1
            // then check the before values. Don't treat the first value
            for (int i = this->fillStore.size() - 1; i > 0; i--) {
                if (this->fillStore[i] > this->vecFillEnds[i]) {
                    this->fillStore[i] = this->vecFillStarts[i];
                    this->fillStore[i - 1]++;
                } else {
                    break;
                }
            }
            // If the first value larger than the value of endVec, it means the loop done and we break the filling
            if (this->fillStore[0] > this->vecFillEnds[0]) {
                break;
            }
        }
    }

    /* We name the position_i as p_i, the size of the vector i is s_i. Then the finalPosi of
     * {p_{i1}, p_{i2}, p_{i3}, ...} should be p_{i1} * s_2 * s_3 * s_4 ... + p_{i2} * s_3 * s4... + ... + p_{in}
     * Eg: If we have 3D vector which is s1 * s2 * s3 = {0, 1, 2, 3, 4, 5} * {10, 20, 30, 40} * {100, 200, 400}.
     * After flatten to a 1 dimension vector. The size will be 6 * 4 * 3 = 72. The position of the 3D vector
     * p_3 * p_1 * p_0(means (3, 20, 100)) in 1D should be:
     * p_3 * s_2.size() * s_3.size() + p_1 * s_3.size() + p_0  = 3 * (4 * 3)  + 1 * (3) + 0 = 39.
     * We store the s_2 * s_3 * s_4... results in a helper vector named _stepSizeHelper for a faster locating
     */
    inline void fillSingleEntry(float weight) {
        int finalPosi = (this->fillStore.back());
        for (int i = this->fillStore.size() - 2; i >= 0; i--) {
            finalPosi += this->fillStore[i] * _stepSizeHelper.at(i);
        }
        yieldsPools->at(finalPosi) += weight;
        errorPools->at(finalPosi) = sqrt(errorPools->at(finalPosi) * errorPools->at(finalPosi) + weight * weight);
        rawnumPools->at(finalPosi) += 1;
    }

private:
    vector2D<float>* eventsSlices;
    std::vector<float>* yieldsPools;
    std::vector<float>* errorPools;
    std::vector<int>* rawnumPools;
    vector2D<float> stepPools;
    std::vector<bool> compareFuncs;
    std::vector<int> _stepSizeHelper;

    std::string sampleName;

    // We want lock free when filling events, so initiallize these helper vectors in the construction func

    std::vector<int> fillStore;
    std::vector<int> vecFillStarts;
    std::vector<int> vecFillEnds;
    // For mutex
    std::mutex _mutex;
};

class CutCountRunner : public AnaRunner {
public:
    using Looper_Ptr = std::unique_ptr<CutCountLooper>;
    template <class T> using vector2D = std::vector<std::vector<T>>;

    std::function<Looper_Ptr(std::string chainname, std::vector<std::string> storedVars)> getLooper;

    CutCountRunner(std::function<Looper_Ptr(std::string chainname, std::vector<std::string> storedVars)> looper) {
        this->getLooper = looper;
        this->_name = "cutResults";
        this->ZnLevel = -999;
        this->bkgExtraSyst = 0.3;
    }
    ~CutCountRunner(){};

    void runChains(std::vector<std::string> chains) override { // main process
        setCutSteps();
        readSamples(chains);
        setResultFilters();
        startCut();
        printOut();
    }

    void addStep(std::string varName, std::vector<float> steps, char compare = '>');

    virtual void setCutSteps() = 0;

    void readSamples(std::vector<std::string> chains);

    virtual void setResultFilters() = 0;

    // 5 things to do in setResultFilters
    void addYieldsFilter(std::string sampleName, float yieldsCon) { this->yieldsLevel[sampleName] = yieldsCon; }
    void addRawnumFilter(std::string sampleName, float rawnumCon) { this->rawnumLevel[sampleName] = rawnumCon; }
    void addRelErrorFilter(std::string sampleName, float relError) { this->relErrorLevel[sampleName] = relError; }
    void setZnFilter(float ZnFilter) { this->ZnLevel = ZnFilter; }

    void setBkgRelSyst(float relSyst) { this->bkgExtraSyst = relSyst; }

    void preparePool(); // 1st step in startCut()
    void startCut();
    void printOut();

private:
    /*************************************
    *********private vars****************
    **************************************/

    std::string _name;

    // Tables for each sample
    std::pair<std::string, vector2D<float>> signalSample;
    std::unordered_map<std::string, vector2D<float>> bkgSamples;

    // CutCount steps
    std::vector<std::string> varNames; // varNames: [names of all variables], "weight"
    vector2D<float> stepPools;         // one of the vector in stepPools is all steps for a var.
    std::vector<bool> compareFuncPools;

    // Yields and relError constraints for each samples used for filter
    std::unordered_map<std::string, float> yieldsLevel;
    std::unordered_map<std::string, int> rawnumLevel;
    std::unordered_map<std::string, float> relErrorLevel;
    float ZnLevel;
    float bkgExtraSyst;

    // Yields and errors for each samples each steps
    std::unordered_map<std::string, std::vector<float>> _cutcountPools;
    std::unordered_map<std::string, std::vector<float>> _cutcountErrPools;
    std::unordered_map<std::string, std::vector<int>> _cutcountRawnumPools;

    /* The _stepSizeHelper stores the total vecter size after this element. It is used to get the location of a certain location
     * We choose the _stepSizeHelper as vector instead of deque. Because we will only initialize once but use multiple times,
     * while the locating time of deque is slightly slower than vector*/
    std::vector<int> _stepSizeHelper;
};

#define DefineCutCount(ANALYSISNAME, ANAYLSISTREE)                                                                                               \
    class ANALYSISNAME : public CutCountRunner {                                                                                                 \
    public:                                                                                                                                      \
        ANALYSISNAME(std::function<Looper_Ptr(std::string chainname, std::vector<std::string> storedVars)> looper) : CutCountRunner(looper){};   \
        void setCutSteps() override;                                                                                                             \
        void setResultFilters() override;                                                                                                        \
    };                                                                                                                                           \
    class Looper_##ANALYSISNAME : public CutCountLooper {                                                                                        \
    public:                                                                                                                                      \
        std::shared_ptr<ANAYLSISTREE> tree;                                                                                                      \
        Long64_t tree_Entries;                                                                                                                   \
        Looper_##ANALYSISNAME() {                                                                                                                \
            if (getAnaCollection().find(#ANALYSISNAME) == getAnaCollection().end()) {                                                            \
                getAnaCollection()[#ANALYSISNAME] = [] {                                                                                         \
                    return new ANALYSISNAME([](std::string chainname, std::vector<std::string> storedVars) {                                     \
                        return std::unique_ptr<Looper_##ANALYSISNAME>(new Looper_##ANALYSISNAME(chainname, storedVars));                         \
                    });                                                                                                                          \
                };                                                                                                                               \
            }                                                                                                                                    \
            this->tree = nullptr;                                                                                                                \
        }                                                                                                                                        \
        Looper_##ANALYSISNAME(std::string chainname, std::vector<std::string> storedVars) : CutCountLooper(chainname, storedVars) {              \
            this->tree = std::shared_ptr<ANAYLSISTREE>(new ANAYLSISTREE(this->anaChain));                                                        \
            this->tree_Entries = tree->GetEntries();                                                                                             \
        }                                                                                                                                        \
        Long64_t getEntries() { return this->tree_Entries; }                                                                                     \
        void getEntry(Long64_t i, int LOG_EVERY_N = 100000) {                                                                                    \
            this->tree->GetEntry(i);                                                                                                             \
            if (0 == i % LOG_EVERY_N) {                                                                                                          \
                if (i == 0) {                                                                                                                    \
                    Timer::Instance().clock("AnaRunner_S");                                                                                      \
                } else {                                                                                                                         \
                    Timer::Instance().clock("AnaRunner");                                                                                        \
                    auto t_duration = Timer::Instance().duration("AnaRunner_S", "AnaRunner");                                                    \
                    auto ETA = t_duration * (this->tree_Entries - i) / i;                                                                        \
                    LOG(INFO) << i << " entry of " << this->tree_Entries << " entries. E.T.A. for this tree: " << Timer::Instance().format(ETA); \
                }                                                                                                                                \
            }                                                                                                                                    \
        }                                                                                                                                        \
        void loop() {                                                                                                                            \
            for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {                                                                         \
                getEntry(jentry);                                                                                                                \
                std::vector<float> varValues;                                                                                                    \
                for (auto func : this->_varFuncs) {                                                                                              \
                    varValues.emplace_back(func());                                                                                              \
                }                                                                                                                                \
                varValues.emplace_back(this->weight());                                                                                          \
                this->eventTables.emplace_back(varValues);                                                                                       \
            }                                                                                                                                    \
        }                                                                                                                                        \
        void setVariables();                                                                                                                     \
    };                                                                                                                                           \
    static CutCountLooper* instance_##ANALYSISNAME = new Looper_##ANALYSISNAME();
// Comment: When the multi thread issue solved, the clock should add the thread ID to seperate them The commented
// line in defineTree func
