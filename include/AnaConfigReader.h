#pragma once
#include <filesystem>
#include <string>

#include "Utils.h"
#include "easylogging++.h"
#include "json.hpp"

class AnaConfigReader {
    using json = nlohmann::json;

public:
    static AnaConfigReader& Instance() {
        static AnaConfigReader mReader;
        return mReader;
    }

    void loadConfig(std::string fileName) {
        std::filesystem::path file(fileName);
        CHECK(exists(file) && std::filesystem::is_regular_file(file))
            << "************Error! No valid analysis config file named " << fileName << "! Program terminated******************";
        this->configFile = std::filesystem::canonical(file);
        std::ifstream cFile(this->configFile);
        cFile >> this->config;
    }

    std::filesystem::path getConfigFile() { return this->configFile; }

    void setInputs(std::string infiles) {
        std::vector<std::string> inFilenames = Utils::splitStrBy(infiles, ',');
        config["input-files"] = inFilenames;
    }

    void setWeight(double exceptionWei) { config["weight"] = exceptionWei; }

    void setOutput(std::string outName) { config["output"] = outName; }

    void setAnalysis(std::string name) { config["analysis"] = name; }

    void setDoNtuple() {
        LOG(INFO) << "MakeNtuple Option is overridden by the program option -n. Set it to: \"On\"";
        config["MakeNtuple"] = true;
    }

    void setThreadNum(unsigned int num) { config["thread"] = (num > 0 ? num : 1); }

    std::vector<std::string> getInputs() {
        if (config["input-files"].empty()) {
            LOG(FATAL) << "********************Error! No input file! Program terminated****************";
        }
        return config["input-files"];
    }

    std::string getOutputName() {
        if (config["output"].empty()) {
            LOG(FATAL) << "********************Error! No output file name! Program terminated****************";
        }
        return config["output"];
    }

    std::string getAnalysis() {
        if (config["analysis"].empty()) {
            LOG(FATAL) << "************Error! No analysis is selected! Please use -l to see the supported analysises! "
                          "Program terminated****************";
        }
        return config["analysis"];
    }

    unsigned int getThreadNum() {
        if (config["thread"].empty()) {
            return 1;
        }
        return config["thread"];
    }

    double getOverriddenWeight() {
        if (config["weight"].empty()) {
            return 0; // I think no one will set the exception weight to 0, right?
        }
        return config["weight"];
    }

    bool ntupleConfig() {
        bool makeNtuple = false;
        if (config["MakeNtuple"].empty()) {
            LOG(INFO) << "No MakeNtuple option is set. Don't make ntuple by default";
        } else {
            makeNtuple = config["MakeNtuple"];
            LOG(INFO) << "Make Ntuple Option: " << (makeNtuple ? "On" : "Off");
        }
        return makeNtuple;
    }

    std::vector<std::string> getTrees() {
        std::vector<std::string> treeRegs;
        if (config["Trees"].empty() || !(config["Trees"].is_string() || config["Trees"].is_array())) {
            LOG(FATAL) << "You should provide a list of tree named to run in config file using"
                       << "'\"Trees\": \" TreeReg\"' (regex is supported) or '\"Trees\": [\" TreeReg1\", \" "
                          "TreeReg2\"]' for the analysis.";
        } else {
            if (config["Trees"].is_string()) {
                std::string tReg = config["Trees"];
                treeRegs.emplace_back(tReg);
            } else if (config["Trees"].is_array()) {
                std::vector<std::string> regTemp = config["Trees"]; // nlohmann json could only transfer complicate type using this way. Cannot pass
                                                                    // the value to a decleared complicate object
                treeRegs = regTemp;
            }
        }
        return treeRegs;
    }

    std::vector<std::string> getExcludedTrees() {
        std::vector<std::string> treeRegs;
        if (config["excludedTrees"].is_string()) {
            std::string tReg = config["excludedTrees"];
            treeRegs.emplace_back(tReg);
        } else if (config["excludedTrees"].is_array()) {
            std::vector<std::string> regTemp = config["excludedTrees"]; // nlohmann json could only transfer complicate type using this way. Cannot
                                                                        // pass the value to a decleared complicate object
            treeRegs = regTemp;
        }
        return treeRegs;
    }

    std::vector<std::filesystem::path> metadata() {
        std::vector<std::filesystem::path> metadatas;
        if (!config["MetaData"].empty()) {
            if (config["MetaData"].is_string()) {
                std::string metadataFile = config["MetaData"];
                std::filesystem::path filePath(metadataFile);
                // Check the file path is a relative path or not. If is, transform to absolute path
                if (metadataFile.at(0) != '/') {
                    auto absolutePath = (configFile.parent_path()) / filePath;
                    try {
                        filePath = std::filesystem::canonical(absolutePath);
                    } catch (std::filesystem::filesystem_error e) {
                        LOG(FATAL) << "MetaDB File " << absolutePath << " not Found! Loading terminated";
                    }
                }
                metadatas.emplace_back(filePath);
            } else if (config["MetaData"].is_array()) {
                std::vector<std::string> metadataFiles = config["MetaData"];
                for (const auto& metadataFile : metadataFiles) {
                    std::filesystem::path filePath(metadataFile);
                    // Check the file path is a relative path or not. If is, transform to absolute path
                    if (metadataFile.at(0) != '/') {
                        auto absolutePath = (configFile.parent_path()) / filePath;
                        try {
                            filePath = std::filesystem::canonical(absolutePath);
                        } catch (std::filesystem::filesystem_error e) {
                            LOG(FATAL) << "MetaDB File " << absolutePath << " not Found! Loading terminated";
                        }
                    }
                    metadatas.emplace_back(filePath);
                }
            }
        }
        return metadatas;
    }

    std::string GRL() {
        if (!config["GRL"].empty()) {
            std::string GRLFile = config["GRL"];
            LOG(INFO) << "Use GRL database file:" << GRLFile;
            return GRLFile;
        }
        return "";
    }

    template <typename T> T readKey(std::string name) {
        T value;
        if (!config[name].empty()) {
            T tmp = config[name];
            value = tmp;
        } else {
            LOG(ERROR) << "No key named " << name << " in the config file";
        }
        return value;
    }
    ~AnaConfigReader(){};

private:
    AnaConfigReader(const AnaConfigReader&);            // forbiden in singleton
    AnaConfigReader& operator=(const AnaConfigReader&); // forbiden in singleton
    AnaConfigReader(){};
    std::filesystem::path configFile;
    json config;
};
