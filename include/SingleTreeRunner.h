#include <functional>
#include <memory>

#include "AnaConfigReader.h"
#include "AnaOutputHandler.h"
#include "AnaRunner.h"
#include "SingleTreeLooper.h"
#include "ThreadPool.h"
#include "easylogging++.h"

class SingleTreeRunner : public AnaRunner {
public:
    using SingleTreeLooper_ptr = std::unique_ptr<SingleTreeLooper>;
    using AnaOutputHandler_ptr = std::shared_ptr<AnaOutputHandler>;

    SingleTreeRunner(std::function<SingleTreeLooper_ptr(std::string chainname, AnaOutputHandler_ptr output)> looper) { this->getLooper = looper; }

    void runChains(std::vector<std::string> chains) override {
        // check if we need the ntuple output
        bool nConfig = AnaConfigReader::Instance().ntupleConfig();

        int chainSize = chains.size();
        unsigned int threadNum = AnaConfigReader::Instance().getThreadNum();
        threadNum = (threadNum > chainSize) ? chainSize : threadNum;
        LOG(INFO) << "Running " << chainSize << " trees with " << threadNum << " thread";
        if (threadNum == 1) {
            // get the output file name
            std::string outName = AnaConfigReader::Instance().getOutputName();
            AnaOutputHandler_ptr output(new AnaOutputHandler(outName, nConfig));
            for (const auto& chainname : chains) {
                SingleTreeLooper_ptr looper = getLooper(chainname, output);
                looper->run();
            }
            output->Write();
        } else {
            ROOT::EnableThreadSafety();

            std::string outName = AnaConfigReader::Instance().getOutputName();
            AnaOutputHandler_ptr output(new AnaOutputHandler(outName, nConfig));
            std::unique_ptr<ThreadPool> pool(new ThreadPool(threadNum));
            for (const auto& chainname : chains) {
                pool->addTask(std::move(getLooper(chainname, output)));
            }
            pool->shutDownPool();
            LOG(INFO) << "**********All thread done. Delete pools and Write all obj*********";
            output->Write();
        }
    }

    std::function<SingleTreeLooper_ptr(std::string chainname, AnaOutputHandler_ptr output)> getLooper;

    virtual ~SingleTreeRunner(){};
};

/********************************************************************************************************
 * Used for define a new analysis! You could define your analysis using DefineAnalysis(YouAnalysisName).
 * Then it will be registered in the analysis list! Thanks the simpleAnalysis for this great idea!
 * Differences between this version and the simpleAnalysis: In my version, the analysis
 * 1. Could run only one analysis in a run.
 * 2. Has slightly more memory waste.
 * They are due to in simpleAnalysis, for each analysis there will be only one runner which is the one registered
 * here to run analysis. But in my version we have multiple analysis runners for multiple thread support.
 *********************************************************************************************************/

#define DefineLooper(ANALYSISNAME, ANAYLSISTREE)                                                                                                 \
    class ANALYSISNAME : public SingleTreeLooper {                                                                                               \
    public:                                                                                                                                      \
        std::shared_ptr<ANAYLSISTREE> tree;                                                                                                      \
        Long64_t tree_Entries;                                                                                                                   \
        ANALYSISNAME() {                                                                                                                         \
            if (getAnaCollection().find(#ANALYSISNAME) == getAnaCollection().end()) {                                                            \
                getAnaCollection()[#ANALYSISNAME] = [] {                                                                                         \
                    return new SingleTreeRunner([](std::string chainName, std::shared_ptr<AnaOutputHandler> output) {                            \
                        return std::unique_ptr<ANALYSISNAME>(new ANALYSISNAME(chainName, output));                                               \
                    });                                                                                                                          \
                };                                                                                                                               \
            }                                                                                                                                    \
            this->tree = nullptr;                                                                                                                \
        }                                                                                                                                        \
        ANALYSISNAME(std::string chainName, std::shared_ptr<AnaOutputHandler> output) : SingleTreeLooper(chainName, output) {                    \
            this->tree = std::shared_ptr<ANAYLSISTREE>(new ANAYLSISTREE(this->anaChain));                                                        \
            this->tree_Entries = tree->GetEntries();                                                                                             \
        }                                                                                                                                        \
        Long64_t getEntries() { return this->tree_Entries; }                                                                                     \
        void getEntry(Long64_t i, int LOG_EVERY_N = 100000) {                                                                                    \
            this->tree->GetEntry(i);                                                                                                             \
            if (0 == i % LOG_EVERY_N) {                                                                                                          \
                if (i == 0) {                                                                                                                    \
                    Timer::Instance().clock("AnaRunner_S");                                                                                      \
                } else {                                                                                                                         \
                    Timer::Instance().clock("AnaRunner");                                                                                        \
                    auto t_duration = Timer::Instance().duration("AnaRunner_S", "AnaRunner");                                                    \
                    auto ETA = t_duration * (this->tree_Entries - i) / i;                                                                        \
                    LOG(INFO) << i << " entry of " << this->tree_Entries << " entries. E.T.A. for this tree: " << Timer::Instance().format(ETA); \
                }                                                                                                                                \
            }                                                                                                                                    \
        }                                                                                                                                        \
        void loop();                                                                                                                             \
    };                                                                                                                                           \
    static SingleTreeLooper* instance_##ANALYSISNAME = new ANALYSISNAME();
// Comment: When the multi thread issue solved, the clock should add the thread ID to seperate them The commented
// line in defineTree func
