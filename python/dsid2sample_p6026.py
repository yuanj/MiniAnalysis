import re
from collections import OrderedDict

def intrange(string_range): 
    start, end = map(int, string_range.strip().split('-'))
    return range(start, end + 1)

def clean_lines(input_string):
    lines = input_string.split('\n')
    processed_lines = []
    for line in lines:
        if line.startswith('#'):
            continue
        line = line.split('#')[0].strip()
        processed_lines.append(line)
    result_string = '\n'.join(processed_lines)
    return result_string

def parse_input_string(input_string):
    results = []

    # remove #
    input_string = clean_lines(input_string) 

    # remove alphabeta 
    pp = re.split(r'\s+', input_string)
    input_string = ','.join(p for p in pp if not any(c.isalpha() for c in p))
    parts = input_string.split(',')

    for part in parts:
        if part == '': continue
        if '-' in part:
            results.extend(intrange(part))
        else:
            results.append(int(part))
    return results

def read_and_filter(file_path, filter_nums):
    result_lines = []
    with open(file_path, 'r') as file:
        for line_number, line in enumerate(file, start=1):
            current_number = int(line.split('.')[1].strip())
            if current_number in filter_nums:
                result_lines.append(line.strip())
    return result_lines

def remove_duplicate(list_of_dsid):
    lines_dict = {}
    
    for dsid in list_of_dsid:
        six_digit = dsid.split('.')[1]
        stag = int(dsid.split('_')[-3][1:])
        ptag = int(dsid.split('_p')[-1])

        if six_digit in lines_dict:
            if ptag > lines_dict[six_digit][2] or (ptag == lines_dict[six_digit][2] and stag > lines_dict[six_digit][1]):
                lines_dict[six_digit] = (dsid, stag, ptag)
        else:
            lines_dict[six_digit] = (dsid, stag, ptag)
    #unique_lines = [line[0] for line in lines_dict.values()]
    unique_lines = [line[0] for line in sorted(lines_dict.values(), key=lambda x: x[0])]
    return unique_lines
    
def printt(bkgs, input_path, output=None):
    results = []
    for key, value in bkgs.items():
        lines = read_and_filter(input_path, parse_input_string(value))
        results += lines
    results = remove_duplicate(results)
    if output:
        with open(output, 'w') as file:
            for line in results:
                file.write(line + '\n')
    else:
        for line in results: 
            pass

def split_dict(input_dict, keys_list1, keys_list2):
    dict1 = {}
    dict2 = {}
    dict3 = {}

    for key, value in input_dict.items():
        if key in keys_list1:
            dict1[key] = value
        elif key in keys_list2:
            dict2[key] = value
        else:
            dict3[key] = value

    return dict1, dict2, dict3

bkgs= OrderedDict()

bkgs["dijet"]="Pythia8_dijet	364700-364712"

bkgs["Zjets"] = """
Zjets_ll
Sherpa_2211_Z	700320-700325 #,700335-700337 
Sherpa_2211_Z_EWK	700358-700360, #700361
Sherpa_2211_Z_mll_10_40	700467-700472
Sherpa_2214_Z	700792-700794
Sherpa_2214_Z_mll_10_40	700901-700903
"""

bkgs["Wjets"]="""
Wjets_lv
Sherpa_2211_W	700338-700349
Sherpa_2211_W_EWK	700362-700364
"""

bkgs["VV"]="""
Diboson
Sherpa2214_VV_EWK 701000,701005,701010,701015,701020,701025,701030,701035
Sherpa2214_VV_QCD 
leptonic 701040,701045,701050,701055,701060,701065
semi-leptonic 701085,701090,701095,701100,701105,701110,701115,701120,701125
Sherpa222_VV_QCD	
gg-loop 345705-345706,345718,345723, 364302-364305,
lowMllPt 364288-364290,
"""

bkgs["VVV"]="""
Triboson
Sherpa_VVV	364242-364249 #  WWW->lvlvjj samples are dropped 364336-364339
"""

bkgs["MainTop"]="""
TopPair
PowhegPythia	410470,410471
SingleTop
PowhegPythia-schan	410644-410645
PowhegPythia-tchan	410658-410659
PowhegPythia8-DynScale-Wt	601352,601355
"""

# bkgs["ttgamma"]="ttgamma  410082, 410087"

bkgs["ttV"]="""
aMcAtNloPythia-ttll_mll_1_5	410276
aMcAtNloPythia-ttll_mll_1_6	410277
aMcAtNloPythia-ttll_mll_1_7	410278
aMcAtNloPythia-ttV	410155-410157,410218-410220
#aMcAtNloPythia-ttWW	410081
"""

bkgs["topOther"]="""
#tttt 412043 # only af3, 06 Feb 2024
ttt 516978
tWZ 410408
tZ 410560
PowhegPythia-ttH	346343-346345
"""

bkgs["ggH"]="""
Baseline
#H4l	345060,346695,346701
#Het	345125
# Hmt	345124
#HWW	345324
#Hyy	343981
#HZy	345316,345961
Hmm	345097
Htt	345120-345123,# 346531-346532,346564
"""

bkgs["VBFH"]="""
Baseline
#H4l	346228,346317,346696,346702
#Het	346194
#Hmt	346195
#HWW	345948
#Hyy	346214
#HZy	345833-345834
Hbb	345949
Htt	346190-346193, #346569-346570,346573
"""

bkgs["VH"]="""
VH
Description	DSID range
Baseline
#H4l	345066,346645-346646,346697-346700,346703-346706
#Het	345213-345214,345218
#Hincl	346310-346312
#Hinv	345596,346693-346694
#Hmt	345215-345216,345219
#HWW	341450,341452,341454,341456,341458,341460,345325-345327,345336-345337,345433,345445-345446,346524,346560-346561
#Hyy	345061,345317-345319
#HZy	345320-345322,345963-345965
Hbb	345053-345057
Hcc	345109-345113
Hmm	345098,345103-345105
Htt	345211-345212,345217,346329
"""
if __name__ == "__main__":
    printt(bkgs, 'ALLmc20a.txt', 'mc20a.txt')
    printt(bkgs, 'ALLmc20d.txt', 'mc20d.txt')
    printt(bkgs, 'ALLmc20e.txt', 'mc20e.txt')
    #dict1, dict2, dict3 = split_dict(bkgs,["dijet","Zjets","Wjets"],["VV","VVV","MainTop"])
    #
    #printt(dict1, 'ALLmc20a.txt', 'mc20a1.txt')
    #printt(dict1, 'ALLmc20d.txt', 'mc20d1.txt')
    #printt(dict1, 'ALLmc20e.txt', 'mc20e1.txt')
    #
    #printt(dict2, 'ALLmc20a.txt', 'mc20a2.txt')
    #printt(dict2, 'ALLmc20d.txt', 'mc20d2.txt')
    #printt(dict2, 'ALLmc20e.txt', 'mc20e2.txt')
    #
    #printt(dict3, 'ALLmc20a.txt', 'mc20a3.txt')
    #printt(dict3, 'ALLmc20d.txt', 'mc20d3.txt')
    #printt(dict3, 'ALLmc20e.txt', 'mc20e3.txt')

bkgs23= OrderedDict()

bkgs23["Dijet"]="""
Pythia8	601700-601707
"""
bkgs23["top"]="""
TTbar 601229-601230,601237
SingleTop tW 601352,601355
"""

bkgs23["VV"]="""
Sherpa_2214_VV 701000,701005,701010,701015,701020,701025,701030,701035,701040,701045,701050,701055,701060,701065,701085,701090,701095,701100,701105,701110,701115,701120,701125
Sherpa_2214_VVV	700584,700866-700868 # WWW samples are dropped 700864-700865
"""

bkgs23["Vjets"]="""
Sherpa_2214	700777-700797, 700895-700903#,700877-700903
"""

if __name__ == "__main__":
    printt(bkgs23, 'ALLmc23a.txt', 'mc23a.txt')
    printt(bkgs23, 'ALLmc23c.txt', 'mc23c.txt')
    printt(bkgs23, 'ALLmc23d.txt', 'mc23d.txt')
