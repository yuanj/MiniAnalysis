import argparse
import numpy as np
import pandas as pd
import json
import uproot
import warnings

JsonFile="./MetaData.json"

with open(JsonFile) as f:
    _LinkDB = json.load(f)
    NNORM_DB = _LinkDB["Nnorm"]
    VARIATION_DB = _LinkDB["link"]

GenDBfile="./generatorDB.json"
with open(GenDBfile) as f:
    GEN_DB = json.load(f)

ROOT_FILE_PATH = "/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/WhAna/WhTheoUncert/calcLHESysts/merge/"
    
VARIATION_NAME = ""
REGION_NAME = ''
SAMPLE_GEN = ''

def sortUpDw(up, dw):
    U = float(up)
    D = float(dw)
    tmp = U
    if U < D:
        U = D
        D = tmp
    return str(U), str(D)

# get DataFrame
def initPD(rootpath, rootname, treename , leafname = []):
    with uproot.open(rootpath+rootname) as root_file:
        _var_pandas = root_file[treename].pandas.df(leafname)
    return _var_pandas


# get Variation weight
def get_var_weight(PDMap):
    
    run_number = PDMap['mergedRunNumber']
    
    mc_compaign = ""
    if run_number >= 348885 :
        mc_compaign = "18"
    elif run_number >= 325713 :
        mc_compaign = "17"
    else :
        mc_compaign = "16"
    
    mcChannel = PDMap['mcChannelNumber']
    weight = PDMap['Weight_mc']
    if VARIATION_NAME == "Nominal_Inclusive":
        branch_name = "GenWeight"
    else:
        branch_name = "GenWeight_LHE_" + VARIATION_NAME
    var_GenWeight = PDMap[branch_name]
    nom_GenWeight = PDMap['GenWeight']
    if nom_GenWeight == 0:
        return weight

    if SAMPLE_GEN != "DSID" :
        if len(VARIATION_DB[SAMPLE_GEN]) == 1:
            process_ID = '0'
            #warnings.warn( SAMPLE_GEN + " only has Nominal_Inclusive")
        else:
            if VARIATION_NAME in VARIATION_DB[SAMPLE_GEN]:
                process_ID = VARIATION_DB[SAMPLE_GEN][VARIATION_NAME]
            else:
                raise KeyError( SAMPLE_GEN + " does not have any matched variation : " + VARIATION_NAME )
    else:
        if len(VARIATION_DB[str(mcChannel)]) == 1:
            process_ID = '0'
            #warnings.warn( str(mcChannel) + " only has Nominal_Inclusive")
        else:
            if VARIATION_NAME in VARIATION_DB[str(mcChannel)]:
                process_ID = VARIATION_DB[str(mcChannel)][VARIATION_NAME]
            else:
                raise KeyError(str(mcChannel) + " does not have any matched variation : " + VARIATION_NAME )

    nom_sumWeight = NNORM_DB["0"][str(mcChannel)][mc_compaign]
    try:
        var_sumWeight = NNORM_DB[process_ID][str(mcChannel)][mc_compaign]
    except KeyError:
        return weight
    if abs( weight * (nom_sumWeight/var_sumWeight) * (var_GenWeight/nom_GenWeight) ) < 50 :
        return weight * (nom_sumWeight/var_sumWeight) * (var_GenWeight/nom_GenWeight)
    return weight

def get_nom_weight(PDMap):
    weight = PDMap['Weight_mc']
    return weight

# genDict return str to find sumWeight # GenDB return variation Name
def control_dic(sample_name, var_name):
    genDict = {
            'W' : 'Sherpa221', 
            'Z' : 'Sherpa221',
            'MultiBoson' : 'Sherpa',
            'ttbar': 'PowHegPy8_ttbar',
            'ttV' : 'MG5',
            'ttX_multiTop' : 'DSID',
            'ttX_tZ' : 'DSID',
            'ttX_ttX' : 'DSID',
            'SingleTop_S_Chan' : 'DSID',
            'SingleTop_T_Chan' : 'DSID',
            'SingleTop_Wt_Chan' : 'DSID',
            'VBFHiggs' : 'VBFHiggs',
            'VHiggs' : 'VHiggs',
            'ggHiggs' : 'ggHiggs',
            'ttHiggs' : 'ttHiggs'
            }
    _genDict = {
            'W' : 'Sherpa', 
            'Z' : 'Sherpa',
            'MultiBoson' : 'Sherpa',
            'SingleTop_S_Chan' : 'PowHegPy8_SingleTop',
            'SingleTop_T_Chan' : 'PowHegPy8_SingleTop_t_chan',
            'SingleTop_Wt_Chan' : 'PowHegPy8_SingleTop',
            'ttbar': 'PowHegPy8_ttbar',
            'ttV' : 'MG5_aMCatNLO_Py8',
            'ttX_multiTop' : 'OnlyNominal',
            'ttX_tZ' : 'MG5Py8',
            'ttX_ttX' : 'OnlyNominal',
            'VBFHiggs' : 'VBFHiggs',
            'VHiggs' : 'VHiggs',
            'ggHiggs' : 'ggHiggs',
            'ttHiggs' : 'ttHiggs'
            }
    gen_name = _genDict[sample_name]
    return genDict[sample_name] , GEN_DB[gen_name ][var_name]


def main( root_file_name_list, sample_name_dic, tree_name, region_name, var_name_list, preselect_region_name = ""):
    
    region_nom_weight_dic = {'nomWeight' : 0.0}
    region_var_weight_dic = {}
    for i in var_name_list:
        region_var_weight_dic['varWeight'+i] = 0.0
    
    preselection_nom_weight_dic = {'nomWeight' : 0.0}
    preselection_var_weight_dic = {}
    for i in var_name_list:
        preselection_var_weight_dic['varWeight'+i] = 0.0
    
    total_raw_event_num = 0

    global VARIATION_NAME,REGION_NAME,SAMPLE_GEN
    
    REGION_NAME = region_name

    for sample_name in root_file_name_list:
        root_file_name = sample_name_dic[sample_name]
        
        variation_name_list = []
        variation_branch_list = []
        sample_gen_list = []
        for i in var_name_list:
            try:
                a,b = control_dic(sample_name, i)
                variation_name_list.append(b)
                sample_gen_list.append(a)
                if b == "Nominal_Inclusive" :
                    variation_branch_list.append("GenWeight")
                else:
                    variation_branch_list.append("GenWeight_LHE_" + b)
            except KeyError:
                warnings.warn( sample_name + " don't have " + i)

        branchList = [REGION_NAME, "mcChannelNumber" ,"mergedRunNumber" ,"Weight_mc","GenWeight"] + variation_branch_list
        if preselect_region_name != "":
            branchList = [REGION_NAME, preselect_region_name, "mcChannelNumber" ,"mergedRunNumber" ,"Weight_mc","GenWeight"] + variation_branch_list

        re_branch_list = []
        for i in branchList:
            if i not in re_branch_list:
                re_branch_list.append(i)
        PDMap_total = initPD(ROOT_FILE_PATH, root_file_name, tree_name, re_branch_list)
        # filter by region
        PDMap = PDMap_total.loc[PDMap_total[REGION_NAME]].copy()
        PDMap = PDMap[(PDMap['Weight_mc'] >= -30)]
        PDMap = PDMap[(PDMap['Weight_mc'] <= 30)]

        if preselect_region_name != "":
            preselection_PD = PDMap_total.loc[PDMap_total[preselect_region_name]].copy()
            preselection_PD = preselection_PD[(preselection_PD['Weight_mc'] >= -30)]
            preselection_PD = preselection_PD[(preselection_PD['Weight_mc'] <= 30)]
        
        if PDMap.empty :
            continue
        
        raw_event_num = len(PDMap)
    
        PDMap['nomWeight'] = PDMap.apply(get_nom_weight, axis=1)
        region_nom_weight_dic[ 'nomWeight' ] += PDMap[ 'nomWeight' ].sum()
        if preselect_region_name != "" and not preselection_PD.empty :
            preselection_PD['nomWeight'] = preselection_PD.apply(get_nom_weight, axis=1)
            preselection_nom_weight_dic[ 'nomWeight' ] += preselection_PD[ 'nomWeight' ].sum()

        if raw_event_num == 0:
            pass
            #print(sample_name + " has 0 Raw events in Region " + region_name)
        else:
            for i in range(len(variation_name_list)):
                #print(i,len(variation_name_list))
                VARIATION_NAME = variation_name_list[i]
                SAMPLE_GEN = sample_gen_list[i]
                PDMap[ 'varWeight'+var_name_list[i] ] = PDMap.apply(get_var_weight, axis=1)
                region_var_weight_dic[ 'varWeight'+var_name_list[i] ] += PDMap[ 'varWeight'+var_name_list[i] ].sum()
                if preselect_region_name != "" and not preselection_PD.empty :
                    preselection_PD[ 'varWeight'+var_name_list[i] ] = preselection_PD.apply(get_var_weight, axis=1)
                    preselection_var_weight_dic[ 'varWeight'+var_name_list[i] ] += preselection_PD[ 'varWeight'+var_name_list[i] ].sum()
            total_raw_event_num = total_raw_event_num + raw_event_num
    
    systematic_val = 0
    max_systematic_val = 0
    max_i = ""
    #print("raw",total_raw_event_num)
    for i in var_name_list:
        systematic_val = 0
        # usually we could not get a 0 yields event for a PDF set
        if region_nom_weight_dic[ 'nomWeight' ] != 0 and region_var_weight_dic[ 'varWeight'+i ] != 0:
            if preselect_region_name != "":
                systematic_val = abs( ( region_var_weight_dic['varWeight'+i]*preselection_nom_weight_dic['nomWeight']) / ( preselection_var_weight_dic['varWeight'+i]*region_nom_weight_dic['nomWeight'] ) -1 )
            else:
                systematic_val = ( region_var_weight_dic[ 'varWeight'+i ] - region_nom_weight_dic[ 'nomWeight' ] ) / region_nom_weight_dic[ 'nomWeight' ] 

            if abs(systematic_val) > abs(max_systematic_val):
                max_systematic_val = systematic_val
                max_i = i
    print("max syst is " + max_i)
    return '%.3f' % max_systematic_val

if __name__ == "__main__":
    
    SampleTreeNameDic = {
            #'Z' : 'Z_Nominal',
            'MultiBoson' : 'Boson_Nominal',
            #'Top' : 'Top_Nominal',
            'Higgs' : 'Higgs_Nominal'
            }
    SampleRootNameDic = {
            'Z' : 'Z.root',
            'MultiBoson' : 'MultiBoson.root',
            'ttbar': 'Top.PowHegPy8_ttbar.root',
            'ttV' : 'Top.ttV.root',
            'ttX_multiTop' : 'Top.MG5Py8_multi_t.root',
            'ttX_tZ' : 'Top.MG5Py8_tZ.root',
            'ttX_ttX' : 'Top.MG5Py8_ttX.root',
            'SingleTop_T_Chan' : 'Top.PowHegPy8_single_top_tchan.root',
            'SingleTop_Wt_Chan' : 'Top.PowHegPy8_single_top_Wt.root',
            'VBFHiggs' : 'Higgs.VBFHiggs.root',
            'VHiggs' : 'Higgs.VHiggs.root',
            'ggHiggs' : 'Higgs.ggHiggs.root',
            'ttHiggs' : 'Higgs.ttHiggs.root'
            }

    SampleGroupDic = {
            'Top' : ['ttbar', 'ttV', 'ttX_multiTop', 'ttX_tZ', 'ttX_ttX', 'SingleTop_Wt_Chan'],
            #'Top' : ['ttbar', 'ttV', 'ttX_multiTop', 'ttX_tZ', 'ttX_ttX', 'SingleTop_T_Chan', 'SingleTop_Wt_Chan'],
            'MultiBoson' : ['MultiBoson'],
            'Z' : ['Z'],
            'Higgs' : ['VHiggs', 'VBFHiggs', 'ggHiggs', 'ttHiggs']
            }
    
    RegionNameS = ['isSRlow', 'isSRhigh', 'isTCR', 'isTVR', 'isBosonVR']
    
    #SelectedSample = "Z"
    for SelectedSample in SampleTreeNameDic.keys():
        printSample = SelectedSample;
        print( "Do the Sample " + SelectedSample + ":")
        if SelectedSample == "Z":
            printSample = "Zjets"
        for r in RegionNameS :
            RenorFactoDw = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["05_05"] )
            RenorFactoUp = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["20_20"] )
            RenorDw = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["20_10"] )
            RenorUp = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["05_10"] )
            FactoDw = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["10_05"] )
            FactoUp = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["10_20"] )
            #PDF = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["PDFset_00", "PDFset_01", "PDFset_02", "PDFset_03", "PDFset_04", "PDFset_05", "PDFset_06", "PDFset_07", "PDFset_08", "PDFset_09", "PDFset_10", "PDFset_11", "PDFset_12", "PDFset_13", "PDFset_14", "PDFset_15", "PDFset_16", "PDFset_17", "PDFset_18", "PDFset_19", "PDFset_20", "PDFset_21", "PDFset_22", "PDFset_23", "PDFset_24", "PDFset_25", "PDFset_26", "PDFset_27", "PDFset_28", "PDFset_29", "PDFset_30", "PDFset_31", "PDFset_32", "PDFset_33", "PDFset_34", "PDFset_35", "PDFset_36", "PDFset_37", "PDFset_38", "PDFset_39", "PDFset_40", "PDFset_41", "PDFset_42", "PDFset_43", "PDFset_44", "PDFset_45", "PDFset_46", "PDFset_47", "PDFset_48", "PDFset_49", "PDFset_50", "PDFset_51", "PDFset_52", "PDFset_53", "PDFset_54", "PDFset_55", "PDFset_56", "PDFset_57", "PDFset_58", "PDFset_59", "PDFset_60", "PDFset_61", "PDFset_62", "PDFset_63", "PDFset_64", "PDFset_65", "PDFset_66", "PDFset_67", "PDFset_68", "PDFset_69", "PDFset_70", "PDFset_71", "PDFset_72", "PDFset_73", "PDFset_74", "PDFset_75", "PDFset_76", "PDFset_77", "PDFset_78", "PDFset_79", "PDFset_80", "PDFset_81", "PDFset_82", "PDFset_83", "PDFset_84", "PDFset_85", "PDFset_86", "PDFset_87", "PDFset_88", "PDFset_89", "PDFset_90", "PDFset_91", "PDFset_92", "PDFset_93", "PDFset_94", "PDFset_95", "PDFset_96", "PDFset_97", "PDFset_98", "PDFset_99" ])
            PDF = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["PDFset_00", "PDFset_01", "PDFset_02", "PDFset_03", "PDFset_04", "PDFset_05", "PDFset_06", "PDFset_07", "PDFset_08", "PDFset_09", "PDFset_10", "PDFset_11", "PDFset_12", "PDFset_13", "PDFset_14", "PDFset_15", "PDFset_16", "PDFset_17", "PDFset_18", "PDFset_19", "PDFset_20", "PDFset_21", "PDFset_22", "PDFset_23", "PDFset_24", "PDFset_25", "PDFset_26", "PDFset_27", "PDFset_28", "PDFset_29", "PDFset_30"])
            #, "PDFset_31", "PDFset_32", "PDFset_33", "PDFset_34", "PDFset_35", "PDFset_36", "PDFset_37", "PDFset_38", "PDFset_39", "PDFset_40", "PDFset_41", "PDFset_42", "PDFset_43", "PDFset_44", "PDFset_45", "PDFset_46", "PDFset_47", "PDFset_48", "PDFset_49", "PDFset_50", "PDFset_51", "PDFset_52", "PDFset_53", "PDFset_54", "PDFset_55", "PDFset_56", "PDFset_57", "PDFset_58", "PDFset_59", "PDFset_60", "PDFset_61", "PDFset_62", "PDFset_63", "PDFset_64", "PDFset_65", "PDFset_66", "PDFset_67", "PDFset_68", "PDFset_69", "PDFset_70", "PDFset_71", "PDFset_72", "PDFset_73", "PDFset_74", "PDFset_75", "PDFset_76", "PDFset_77", "PDFset_78", "PDFset_79", "PDFset_80", "PDFset_81", "PDFset_82", "PDFset_83", "PDFset_84", "PDFset_85", "PDFset_86", "PDFset_87", "PDFset_88", "PDFset_89", "PDFset_90", "PDFset_91", "PDFset_92", "PDFset_93", "PDFset_94", "PDFset_95", "PDFset_96", "PDFset_97", "PDFset_98", "PDFset_99" ])

            RenorFactoUp,RenorFactoDw = sortUpDw(RenorFactoUp, RenorFactoDw)
            RenorUp,RenorDw = sortUpDw(RenorUp, RenorDw)
            FactoUp,FactoDw = sortUpDw(FactoUp, FactoDw)
            pdfVar = abs(float(PDF))
            PDF = str(pdfVar)

            print(printSample+'Theo["'+printSample+'RenorFacto'+'_'+r+'"] = Systematic("'+printSample+'RenorFacto'+'", configMgr.weights,1.+('+RenorFactoDw+'),1.+('+RenorFactoUp+'),"user","userOverallSys")')
            print(printSample+'Theo["'+printSample+'Renor'+'_'+r+'"] = Systematic("'+printSample+'Renor'+'", configMgr.weights,1.+('+RenorDw+'),1.+('+RenorUp+'),"user","userOverallSys")')
            print(printSample+'Theo["'+printSample+'Facto'+'_'+r+'"] = Systematic("'+printSample+'Facto'+'", configMgr.weights,1.+('+FactoDw+'),1.+('+FactoUp+'),"user","userOverallSys")')
            if SelectedSample is not "Top":
                print(printSample+'Theo["'+printSample+'PDF'+'_'+r+'"] = Systematic("'+printSample+'PDF'+'", configMgr.weights,1.-('+PDF+'),1.+('+PDF+'),"user","userOverallSys")')
            if SelectedSample == "Z":
                ckkwWeight_Dw = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["ckkwWeight_dw"] )
                ckkwWeight_Up = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["ckkwWeight_up"] )
                qsfWeight_Dw = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["qsfWeight_dw"] )
                qsfWeight_Up = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["qsfWeight_up"] )
                
                ckkwWeight_Up,ckkwWeight_Dw = sortUpDw(ckkwWeight_Up, ckkwWeight_Dw)
                qsfWeight_Up,qsfWeight_Dw = sortUpDw(qsfWeight_Up, qsfWeight_Dw)
                print(printSample+'Theo["'+printSample+'Ckkw'+'_'+r+'"] = Systematic("'+printSample+'Ckkw'+'", configMgr.weights,1.+('+ckkwWeight_Dw+'),1.+('+ckkwWeight_Up+'),"user","userOverallSys")')
                print(printSample+'Theo["'+printSample+'Qsf'+'_'+r+'"] = Systematic("'+printSample+'Qsf'+'", configMgr.weights,1.+('+qsfWeight_Dw+'),1.+('+qsfWeight_Up+'),"user","userOverallSys")')
            elif SelectedSample == "Higgs":
                Var3c_Dw = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["Var3cDown"] )
                Var3c_Up = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["Var3cUp"] )
                FSRfac_Dw = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["FSRfac_05"] )
                FSRfac_Up = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["FSRfac_20"] )
                Var3c_Up,Var3c_Dw = sortUpDw(Var3c_Up,Var3c_Dw)
                FSRfac_Up,FSRfac_Dw = sortUpDw(FSRfac_Up,FSRfac_Dw)
                print(printSample+'Theo["'+printSample+'ISR'+'_'+r+'"] = Systematic("'+printSample+'ISR'+'", configMgr.weights,1.+('+Var3c_Dw+'),1.+('+Var3c_Up+'),"user","userOverallSys")')
                print(printSample+'Theo["'+printSample+'FSR'+'_'+r+'"] = Systematic("'+printSample+'FSR'+'", configMgr.weights,1.+('+FSRfac_Dw+'),1.+('+FSRfac_Up+'),"user","userOverallSys")')
            elif SelectedSample == "Top":
                Var3c_Dw = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["Var3cDown"] , preselect_region_name = "isTCR")
                Var3c_Up = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["Var3cUp"]  , preselect_region_name = "isTCR")
                FSRfac_Dw = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["FSRfac_05"] , preselect_region_name = "isTCR")
                FSRfac_Up = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["FSRfac_20"] , preselect_region_name = "isTCR")
                PDF = main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = ["PDFset_00", "PDFset_01", "PDFset_02", "PDFset_03", "PDFset_04", "PDFset_05", "PDFset_06", "PDFset_07", "PDFset_08", "PDFset_09", "PDFset_10", "PDFset_11", "PDFset_12", "PDFset_13", "PDFset_14", "PDFset_15", "PDFset_16", "PDFset_17", "PDFset_18", "PDFset_19", "PDFset_20", "PDFset_21", "PDFset_22", "PDFset_23", "PDFset_24", "PDFset_25", "PDFset_26", "PDFset_27", "PDFset_28", "PDFset_29", "PDFset_30", "PDFset_31", "PDFset_32", "PDFset_33", "PDFset_34", "PDFset_35", "PDFset_36", "PDFset_37", "PDFset_38", "PDFset_39", "PDFset_40", "PDFset_41", "PDFset_42", "PDFset_43", "PDFset_44", "PDFset_45", "PDFset_46", "PDFset_47", "PDFset_48", "PDFset_49", "PDFset_50", "PDFset_51", "PDFset_52", "PDFset_53", "PDFset_54", "PDFset_55", "PDFset_56", "PDFset_57", "PDFset_58", "PDFset_59", "PDFset_60", "PDFset_61", "PDFset_62", "PDFset_63", "PDFset_64", "PDFset_65", "PDFset_66", "PDFset_67", "PDFset_68", "PDFset_69", "PDFset_70", "PDFset_71", "PDFset_72", "PDFset_73", "PDFset_74", "PDFset_75", "PDFset_76", "PDFset_77", "PDFset_78", "PDFset_79", "PDFset_80", "PDFset_81", "PDFset_82", "PDFset_83", "PDFset_84", "PDFset_85", "PDFset_86", "PDFset_87", "PDFset_88", "PDFset_89", "PDFset_90", "PDFset_91", "PDFset_92", "PDFset_93", "PDFset_94", "PDFset_95", "PDFset_96", "PDFset_97", "PDFset_98", "PDFset_99" ], preselect_region_name = "isTCR")
                Var3c_Up,Var3c_Dw = sortUpDw(Var3c_Up,Var3c_Dw)
                FSRfac_Up,FSRfac_Dw = sortUpDw(FSRfac_Up,FSRfac_Dw)
                pdfVar = abs(float(PDF))
                PDF = str(pdfVar)
                print(printSample+'Theo["'+printSample+'ISR'+'_'+r+'"] = Systematic("'+printSample+'ISR'+'", configMgr.weights,1.+('+Var3c_Dw+'),1.+('+Var3c_Up+'),"user","userOverallSys")')
                print(printSample+'Theo["'+printSample+'FSR'+'_'+r+'"] = Systematic("'+printSample+'FSR'+'", configMgr.weights,1.+('+FSRfac_Dw+'),1.+('+FSRfac_Up+'),"user","userOverallSys")')
                print(printSample+'Theo["'+printSample+'PDF'+'_'+r+'"] = Systematic("'+printSample+'PDF'+'", configMgr.weights,1.-('+PDF+'),1.+('+PDF+'),"user","userOverallSys")')



    print(' * Done * ')

