import os
import shlex, subprocess
import time
import logging
from collections import defaultdict
import multiprocessing

def folder_name(input_name):
    if not os.path.exists(input_name):
        return input_name
    else:
        i = 1
        while True:
            new_folder_name = input_name+str(i)
            if not os.path.exists(new_folder_name):
                return new_folder_name
                break
            i += 1
def group_files_by_size(files, max_size):
    """
    Groups files so that the sum of their sizes does not exceed max_size.
    If a single file is larger than max_size, it is placed in its own group.
    """
    groups = []
    current_group = []
    current_size = 0
    for f in files:
        try:
            size_GB = os.path.getsize(f) / (1024 * 1024 * 1024)
        except OSError as e:
            print(f"Warning: Could not get size for {f}: {e}")
            continue  # skip files with errors
        # If adding this file would exceed the max_size, then start a new group.
        if current_group and (current_size + size_GB > max_size):
            groups.append(current_group)
            current_group = [f]
            current_size = size_GB
        else:
            current_group.append(f)
            current_size += size_GB
    if current_group:
        groups.append(current_group)
    return groups

def write_ma_cmd(ana, inputs, name, save_path='run', addnum=True):
    if "mc23" in name or "data2" in name:
        config = "config_run3.json"
    elif "mc20" in name or "data1" in name:
        config = "config.json"
    else:
        config = "config.json"
    ginds = [int(f.split('.')[-1]) for f in os.listdir(save_path) if f.startswith('sub.sh.')]
    gind = max(ginds)+1 if ginds else 0
    with open(save_path+"/sub.sh."+str(gind), "w") as f:
        if addnum:
            name = ".".join(name.split('.')[:-2])
            f.write("./mini_analysis -c ../config/"+config+" -a "+ana+" -i "+inputs+" -o "+name+'.'+str(gind)+".root" )
        else:
            f.write("./mini_analysis -c ../config/"+config+" -a "+ana+" -i "+inputs+" -o "+name)

def prepare_folder(analysis, name):
    save_path = folder_name(name)
    # prepare run folder
    subprocess.run(['mkdir','-p', save_path])
    subprocess.run(['cp','build/mini_analysis',save_path])
    if os.path.isfile('./'+analysis+'.cxx'): 
        subprocess.run(['cp','./'+analysis+'.cxx',save_path])

    if len( [name for name in os.listdir(args.input) if os.path.isfile(os.path.join(args.input, name)) and name.endswith('.root')] ) > 1000:
        # Large number of well-structured files in a single directory
        # Group files by process name (excluding numerical suffix) and apply filters
        file_dict = defaultdict(list)
        for file in os.listdir(args.input):
            if not file.endswith('.root'): continue
            process = '.'.join(file.split('.')[:-2])
            file_dict[process].append( os.path.abspath(os.path.join(args.input, file)) )

        limit = int(args.limit)
        for process, files in file_dict.items():
            filtered_files = [f for f in files if any(filt in f for filt in args.filter)]
            for i in range(0, len(filtered_files), limit):
                tmp_files = filtered_files[i:i+limit]
                write_ma_cmd(analysis, ','.join(tmp_files), process+'.output.root', save_path, addnum = True)
    else:
        # Process files in the input directory and its subdirectories
        for root, dirs, files in os.walk(args.input, followlinks=True):
            if root == args.input:
                # Handle well-named ROOT files directly in the main input directory
                # Apply filters based on file names
                root_files = [os.path.abspath(os.path.join(root, f)) for f in files if any(filt in f for filt in args.filter) and (not f.startswith('log')) and f.endswith('root')]
                for file in root_files:
                    name = file.split("/")[-1]
                    write_ma_cmd(analysis, file, name, save_path, args.input != root)
            else:
                # Handle subdirectories separately
                # Apply filters based on folder names
                if len(args.filter) > 0 and all(filt not in root for filt in args.filter): continue
                root_files = [os.path.abspath(os.path.join(root, f)) for f in files if 'root' in f and (not f.startswith('log'))]
                limit = int(args.limit)
                groups = group_files_by_size(root_files, limit)
                for group in groups:
                    name = root.split("/")[-1]
                    write_ma_cmd(analysis, ','.join(group), name, save_path, args.input != root)

    subprocess.run('chmod u+x sub.sh.*', shell=True, cwd=save_path)
    return save_path

def submit(path):
    gindex = max(int(f.split('.')[-1]) for f in os.listdir(path) if f.startswith('sub.sh.'))+1
    # subprocess.run(['hep_sub','sub.sh."%{ProcId}"','-g','atlas','-n',str(gindex)],cwd=path)
    subprocess.run(f'hep_sub sub.sh."%{{ProcId}}" -g atlas -n {gindex}', shell=True, cwd=path)

def run_command(command, executable, cwd):
    subprocess.run(command, shell=True, executable=executable, cwd=cwd)
def run(path):
    commands = ['pwd','ls']
    for f in os.listdir(path):
        if f.startswith('sub.sh.'):
            commands.append("source "+f)
            # subprocess.Popen("source "+f, shell=True, executable="/usr/bin/bash", cwd=path)
    args = [(cmd, "/usr/bin/bash", path) for cmd in commands]
    with multiprocessing.Pool(processes=30) as pool:
        pool.starmap(run_command, args)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--old_build", help="Use old excutable file", default=False, action='store_true')
    parser.add_argument("-a", "--analysis", help="The name(s) of analysis algorithm(s)", nargs='+', required=True)
    parser.add_argument("-n", "--names", help="The name(s) of output folder(s)", nargs='+', default=[])
    parser.add_argument("-i", "--input", help="The input ntuples position", required=True)
    parser.add_argument("-f", "--filter", help="filter files and folders", nargs='+', default=["root"]) 
    #parser.add_argument("-f", "--filter", help="filter files and folders", default='') 
    parser.add_argument("-l", "--limit", help="More than one file for one job", default=10)
    parser.add_argument("-s", "--submit", help="submit condor jobs", default=False, action='store_true')
    parser.add_argument("-r", "--run", help="run bg jobs", default=False, action='store_true')
    args = parser.parse_args()

    if not args.old_build:
        logging.info("Compile")
        subprocess.run(['rm','-rf','build'])
        subprocess.run(['mkdir','-p','build'])
        subprocess.run(['cmake','..'],cwd='./build')
        subprocess.run(['make','-j','16'],cwd='./build')
    else:
        logging.info("Use previous compilation.")
    
    if args.names==[]:
        logging.info("Use default directory name: run")
        names = ["run"]*len(args.analysis)
    else:
        logging.info("Set directory name: "+", ".join(args.names))
        names = args.names

    for ana, name in zip(args.analysis, names):
        path = prepare_folder(ana, name)
        if args.submit:
            logging.info("Submit to condor.")
            submit(path)
        elif args.run:
            logging.info("Run by terminal background.")
            run(path)
        else:
            gindex = max(int(f.split('.')[-1]) for f in os.listdir(path) if f.startswith('sub.sh.'))+1
            logging.info("Submit command: "+" ".join(['hep_sub','sub.sh."%{ProcId}"','-g','atlas','-n',str(gindex)]))
            logging.info(str(gindex)+" files. "+"No submission.")
