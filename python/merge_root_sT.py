from collections import defaultdict
import subprocess, os, re
import time
from merge_csv_stop import to_period, to_target
import multiprocessing

def run_command(cmd):
    return subprocess.run(cmd, shell=True)

def to_year(name):
    if 'data15' in name:
        return '15'
    if 'data16' in name:
        return '16'
    if 'r13167' in name or 'r14859' in name or '20a' in name: 
        return '1516'
    if 'r13144' in name or 'r14860' in name or 'data17' in name or '20d' in name: 
        return '17'
    if 'r13145' in name or 'r14861' in name or 'data18' in name or '20e' in name: 
        return '18'
    if 'r14622' in name or 'r14932' in name or 'data22' in name or '23a' in name: 
        return '22'
    if 'r15224' in name or 'data23' in name or '23d' in name: 
        return '23'
    raise Exception("Sorry, failed to know which year")

def to_reg(file):
    return ".".join(file.split('.')[:-2])+".*.root"

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("inputs", help="The name(s) of input folder(s)", nargs='+', default=[])
    parser.add_argument("-o","--outputs", help="The name(s) of output file(s)", default='output')
    args = parser.parse_args()

    if os.path.exists(args.outputs):
        raise Exception("The folder "+args.outputs+" already exists.")
    else:
        os.makedirs(args.outputs)

    root_files_dict = defaultdict(list)
    root_files_dict_year = defaultdict(list)
    root_files_dict_run = defaultdict(list)
    for input in args.inputs:
        for file in os.listdir(input):
            if not file.endswith('.root'): continue
            target = to_target(file)
            if target:
                year = to_year(file)
                period = to_period(file)
                root_files_dict[target].append(os.path.join(input,to_reg(file)))
                root_files_dict_year[target+"_"+year].append(os.path.join(input,to_reg(file)))
                root_files_dict_run[target+"_"+period].append(os.path.join(input,to_reg(file)))
            else:
                print("missing: "+file)

    commands = []
    for key, values in root_files_dict_year.items():
        if key.startswith("dsid_"): continue
        commands.append('hadd '+ os.path.join(args.outputs, key+'.root')+' '+' '.join(set(values)))

    # for key, values in root_files_dict.items():
    #     if key.startswith("dsid_"): continue
    #     commands.append('hadd '+ os.path.join(args.outputs, key+'.root')+' '+' '.join(set(values)))               
    # for key, values in root_files_dict_run.items():
    #     if key.startswith("dsid_"): continue
    #     commands.append('hadd '+ os.path.join(args.outputs, key+'.root') + ' ' + ' '.join(set(values)))

    with multiprocessing.Pool(processes=8) as pool:
        pool.map(run_command, commands)
