import sys
from collections import defaultdict
import subprocess, os
import time
from merge_csv import to_period, to_year, to_target

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("inputs", help="The name(s) of input folder(s)", nargs='+', default=[])
    parser.add_argument("-o","--outputs", help="The name(s) of output file(s)", default='output')
    args = parser.parse_args()

    if os.path.exists(args.outputs):
        raise Exception("The folder "+args.outputs+" already exists.")
    else:
        os.makedirs(args.outputs)

    root_files_dict = defaultdict(list)

    commands = []
    for input in args.inputs:
        for file in os.listdir(input):
            if not file.endswith('.root'): continue            
            target = to_target(file, ("mc20" in file) or ("data1"  in file) )
            year = to_year(file)
            period = to_period(file)
            newname = '.'.join(file.split('.')[0:4]+[target]+file.split('.')[5:])
            commands.append('ln -s '+os.path.relpath(os.path.join(input,file),args.outputs)+' '+os.path.join(args.outputs, newname))
    for cmd in commands:
        subprocess.run(cmd, shell=True)