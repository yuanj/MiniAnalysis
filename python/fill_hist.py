import ROOT
import os
from multiprocessing import Process, Pool
from collections.abc import Iterable

def get_filtered_vars(filter):
    pp = filter.split('&&')
    result = []
    for p in pp:
        if ">" in p:
            result.append( p.split(">")[0].strip() )
        elif "<" in p:
            result.append( p.split("<")[0].strip() )
        elif "=" in p:
            result.append( p.split("=")[0].strip() )
    return result

def remove_cuts(filter, x_branch):
    # remove cuts related to a branch
    cuts = filter.split('&&')
    out_cuts = []
    for cut in cuts:
        if ">" in cut:
            cut_branch = cut.split(">")[0].strip()
        elif "<" in cut:
            cut_branch = cut.split("<")[0].strip()
        elif "=" in cut:
            cut_branch = cut.split("=")[0].strip()
        if cut_branch != x_branch: out_cuts.append(cut)

    if len(out_cuts)>0:
        return ' && '.join(out_cuts)
    else:
        return None

def fill_hist(path, file_name, tree_name, hist_configs,  weight_name, filter=None, newcols={}, Nminus1=True):
    if os.path.exists(file_name):
        raise IOError("The file %s already exists." % file_name)

    data = ROOT.RDataFrame(tree_name, os.path.join(path,file_name) )
    for cols, exp in newcols.items():
        data = data.Define(cols, exp)
    
    data_filtered = data.Filter(filter) if filter else data
    filtered_brs = get_filtered_vars(filter)
    hist_cache = []
    cols = data.GetColumnNames()
    for x_branch, binning in hist_configs.items():
        if x_branch not in cols: continue
        if x_branch in filtered_brs and Nminus1 : # draw N-1 plot
            new_filter = remove_cuts(filter, x_branch)
            tmp_data = data.Filter(new_filter) if new_filter else data
            hist_cache.append( tmp_data.Histo1D((x_branch,x_branch,binning[0],binning[1],binning[2]), x_branch, weight_name) )
        else: # draw filter-region plot
            hist_cache.append( data_filtered.Histo1D((x_branch,x_branch,binning[0],binning[1],binning[2]), x_branch, weight_name) )
        nbins = hist_cache[-1].GetNbinsX()
        hist_cache[-1].SetBinContent(nbins, hist_cache[-1].GetBinContent(nbins) + hist_cache[-1].GetBinContent(nbins+1))
    #data_filtered.Snapshot("myTree",file_name)
    output_file = ROOT.TFile.Open(file_name,"UPDATE")
    #output_file = ROOT.TFile.Open(file_name,"RECREATE")
    for hist in hist_cache:
        hist.Write()

    #input_file =  ROOT.TFile(os.path.join(path, file_name))
    #for key in input_file.GetListOfKeys():
    #    obj = key.ReadObj()
    #    if isinstance(obj, ROOT.TH1F):
    #        output_file.cd()
    #        obj.Write()
    #input_file.Close()

    output_file.Close()

def ManagedRun(output_path, path, input_files, tree, hist_config, weight, filter, new_cols,Nminus1):
    # if tree is a string, read the same tree for all files
    # if tree is a Iterable instance, read different tree for the files
    path = os.path.join('../', path)
    owd = os.getcwd()
    os.mkdir(output_path)
    os.chdir(output_path)
    if isinstance(tree, Iterable) and not isinstance(tree, (str, bytes)):
        arguments = [(path, file, t, hist_config, weight, filter, new_cols, Nminus1) 
                     for file, t in zip(input_files, tree)]
    else:
        arguments = [(path, file, tree, hist_config, weight, filter, new_cols, Nminus1) 
                     for file in input_files]
    with Pool(processes=10) as pool:
        pool.starmap(fill_hist, arguments)
    os.chdir(owd)

if __name__ == "__main__":
    input_files = [
        "data.root",
        "dijet.root",
        "ggH.root",
        "SingleTop.root",
        "topOther.root",
        "TTbar.root",
        "ttV.root",
        "VBFH.root",
        "VH.root",
        "VV.root",
        "VVV.root",
        "Wlvjets.root",
        "Wtvjets.root",
        "Zlljets.root",
        "Zttjets.root",
    ]
    input_files_run3 = [
        'data.root',
        "top.root",
        "VV.root",
        "VVV.root",
        "Wlvjets.root",
        "Wtvjets.root",
        "Zlljets.root",
        "Zttjets.root",
    ]
    hist_config = {
            "ana_pt_tau":[20,0,200],
            "ana_pt_lep":[20,0,200],
            "ana_MET":[25,0,500],
            "ana_Mll":[30,0,300],
            "ana_dRtt":[16,0,8],
            "ana_dPhitt":[16,0,3.2],
            "ana_MT2":[30,0,300],
            "ana_bNumber": [4,0,4],
            "ana_OS":[2,0,2],
            "ana_nLeps2":[4,0,4],
            "ana_nEles2":[4,0,4],
            "ana_nMuons2":[4,0,4],
            "ana_nLeps":[4,0,4],
            "ana_nEles":[4,0,4],
            "ana_nMuons":[4,0,4],
            "ana_nTaus":[4,0,4]
    }
    # weight = "ana_totalWeight"

    # path = '../../merge_1lep/'
    # path = '../../merge_met/'
    # filter = "ana_pt_tau>20"
    # filter = "ana_pt_tau>20 && ana_nEles2 == 1 && ana_nMuons2 == 0 && ana_nLeps2 == 1"
    # filter = "ana_pt_tau>20 && ana_nEles2 == 0 && ana_nMuons2 == 1 && ana_nLeps2 == 1"
    # filter = "ana_pt_tau>20 && ana_MET>50"
    # filter = "ana_pt_tau>20 && ana_MET>50 && ana_nEles2 == 1 && ana_nMuons2 == 0 && ana_nLeps2 == 1"
    # filter = "ana_pt_tau>20 && ana_MET>50 && ana_nEles2 == 0 && ana_nMuons2 == 1 && ana_nLeps2 == 1"
    # filter = "ana_nEles == 1 && ana_nMuons == 0 && ana_nLeps == 1"
    filter_l = "ana_nLeps == 1"
    filter_mu = "ana_nEles == 0 && ana_nMuons == 1 && ana_nLeps == 1"
    filter_e = "ana_nEles == 1 && ana_nMuons == 0 && ana_nLeps == 1"

    # filter = "EtMiss_tst_Nominal>50e3"# && ana_OS"
    # new_cols = {}
    #new_cols = {"EtMiss_tst":"EtMiss_tst_Nominal/1e3"}


    # MultiProcSubmit('plot_met', '../../merge_met/', input_files, "myTree", hist_config, "ana_totalWeight", filter_l, {})
    # MultiProcSubmit('plot_1lep', '../../merge_1lep/', input_files, "myTree", hist_config, "ana_totalWeight", filter_l, {})

    # MultiProcSubmit('plot_met_mu', '../../merge_met/', input_files, "myTree", hist_config, "ana_totalWeight", filter_mu, {})
    # MultiProcSubmit('plot_1lep_mu', '../../merge_1lep/', input_files, "myTree", hist_config, "ana_totalWeight", filter_mu, {})

    # MultiProcSubmit('plot_met_e', '../../merge_met/', input_files, "myTree", hist_config, "ana_totalWeight", filter_e, {})
    # MultiProcSubmit('plot_1lep_e', '../../merge_1lep/', input_files, "myTree", hist_config, "ana_totalWeight", filter_e, {})

    # MultiProcSubmit('plot3_met', '../../merge_met_run3/', input_files_run3, "myTree", hist_config, "ana_totalWeight", filter_l, {})
    # MultiProcSubmit('plot3_1lep', '../../merge_1lep_run3/', input_files_run3, "myTree", hist_config, "ana_totalWeight", filter_l, {})

    # MultiProcSubmit('plot3_met_mu', '../../merge_met_run3/', input_files_run3, "myTree", hist_config, "ana_totalWeight", filter_mu, {})
    # MultiProcSubmit('plot3_1lep_mu', '../../merge_1lep_run3/', input_files_run3, "myTree", hist_config, "ana_totalWeight", filter_mu, {})

    # MultiProcSubmit('plot3_met_e', '../../merge_met_run3/', input_files_run3, "myTree", hist_config, "ana_totalWeight", filter_e, {})
    # MultiProcSubmit('plot3_1lep_e', '../../merge_1lep_run3/', input_files_run3, "myTree", hist_config, "ana_totalWeight", filter_e, {})

    MultiProcSubmit('plot_hh', '../../merge_hh/', input_files, "myTree", hist_config, "ana_totalWeight", "", {})
    MultiProcSubmit('plot3_hh', '../../merge_hh_run3/', input_files_run3, "myTree", hist_config, "ana_totalWeight", "", {})
