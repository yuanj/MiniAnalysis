import re
from collections import OrderedDict
import subprocess

def intrange(string_range): 
    start, end = map(int, string_range.strip().split('-'))
    return range(start, end + 1)

def clean_lines(input_string):
    # remove # lines
    lines = input_string.split('\n')
    processed_lines = []
    for line in lines:
        if line.startswith('#'):
            continue
        line = line.split('#')[0].strip()
        processed_lines.append(line)
    result_string = '\n'.join(processed_lines)
    return result_string

def parse_input_string(input_string):
    results = []

    # remove
    input_string = clean_lines(input_string) 

    # remove alphabeta 
    pp = re.split(r'\s+', input_string)
    input_string = ','.join(p for p in pp if not any(c.isalpha() for c in p))
    parts = input_string.split(',')

    for part in parts:
        if part == '': continue
        if '-' in part:
            results.extend(intrange(part))
        else:
            results.append(int(part))
    return results

def rucio_ls(run=2):
    # get a list of mc dids
    if run==2:
        mc_lines = subprocess.run("rucio list-dids mc20_13TeV.%DAOD_PHYS.%p6266  --filter type=container --short", shell=True, stdout=subprocess.PIPE).stdout.splitlines()
    else:
        mc_lines = subprocess.run("rucio list-dids mc23_13p6TeV.%DAOD_PHYS.%p6266  --filter type=container --short", shell=True, stdout=subprocess.PIPE).stdout.splitlines()
    mc_lines = [ line.decode('utf-8') for line in mc_lines if re.search(r"e\d{4}_s\d{4}_r\d{5}_p", line.decode('utf-8')) ]
    return mc_lines

def filter_by_dsid(input_lines, filter_nums):
    # filter container list by dsid number
    result_lines = []
    for line_number, line in enumerate(input_lines):
        current_number = int(line.split('.')[1].strip())
        if current_number in filter_nums:
            result_lines.append(line.strip())
    return result_lines

sample_map = {
     "r13167": "mc20a", "r14859": "mc20a",
     "r14860": "mc20d", "r13144": "mc20d",
     "r13145": "mc20e", "r14861": "mc20e",
     "r14622": "mc23a", "r15540": "mc23a", "r14932": "mc23a",
     "r15224": "mc23d", "r15515": "mc23d", "r15530": "mc23d",
}
def get_rtag(line_str):
    match = re.search(r"r\d{5}", line_str)
    return int(match.group(0)[1:])
def get_rtag_camp(line_str):
    match = re.search(r"r\d{5}", line_str)
    return sample_map[match.group(0)] if match.group(0) in sample_map else match.group(0)
def get_etag(line_str):
    match = re.search(r"e\d{4}", line_str)
    return int(match.group(0)[1:])
def get_stag(line_str):
    match = re.search(r"s\d{4}", line_str)
    return int(match.group(0)[1:])
def get_ptag(line_str):
    match = re.search(r"p\d{4}", line_str)
    return int(match.group(0)[1:])

def get_rtag_set(mc_lines):
    unique_r_values = set()
    for line in mc_lines:
        rtag = get_rtag_camp(line)
        unique_r_values.add(rtag)  
    print(unique_r_values)
    return unique_r_values

def remove_duplicate(list_of_dsid):
    lines_dict = []  # This will store the filtered dsids
    
    for dsid in list_of_dsid:
        six_digit = dsid.split('.')[1]
        etag = get_etag(dsid)
        stag = get_stag(dsid)
        rtag = get_rtag(dsid)
        rcamp = get_rtag_camp(dsid)
        ptag = get_ptag(dsid) 

        matched = False  # To check if a replacement was made
        for ind, old in enumerate(lines_dict):
            old_six_digit = old.split('.')[1]
            old_etag = get_etag(old)
            old_stag = get_stag(old)
            old_rtag = get_rtag(old)
            old_rcamp = get_rtag_camp(old)
            old_ptag = get_ptag(old)

            # Check for duplicates with the same `six_digit` and `rtag`
            if old_six_digit == six_digit and rcamp == old_rcamp:
                print(f"Matched: new:{dsid}, old:{old}")
                if ptag > old_ptag or etag > old_etag or stag > old_stag or rtag > old_rtag:
                    lines_dict[ind] = dsid  # Replace with the better dsid
                    print(f"Update: {dsid}")
                else:
                    print(f"Drop: {dsid}")
                matched = True  # Mark as replaced
                break

        if not matched:  # If no replacement happened, add the new dsid
            lines_dict.append(dsid)
    
    return lines_dict


def printt(bkgs, run, output, remove_dup=True):
    # rucio list-dids 
    input_lines = rucio_ls(run)
    
    # filter all dids by dsid range
    results = []
    for key, dsid_range_str in bkgs.items():
        results += filter_by_dsid(input_lines, parse_input_string(dsid_range_str))
    
    # remove duplicate lines
    if remove_dup:
        results = remove_duplicate(results)
    
    # get rtag campaigns for output results
    rtag_camp_set = get_rtag_set(results)
    
    # write a file for one campaign
    for rtag in rtag_camp_set:
        with open(output + "_" + rtag + ".txt", 'w') as file:
            for line in results:
                line_rtag = get_rtag_camp(line)
                if rtag == line_rtag:
                    file.write(line + '\n')

#def split_dict(input_dict, keys_list1, keys_list2):
#    dict1 = {}
#    dict2 = {}
#    dict3 = {}
#    for key, value in input_dict.items():
#        if key in keys_list1:
#            dict1[key] = value
#        elif key in keys_list2:
#            dict2[key] = value
#        else:
#            dict3[key] = value
#    return dict1, dict2, dict3

bkgs= OrderedDict()

bkgs["dijet"]="Pythia8_dijet	364700-364712"

bkgs["Zjets"] = """
Zjets_ll
Sherpa_2211_Z	700320-700325 #,700335-700337 
Sherpa_2211_Z_EWK	700358-700360, #700361
Sherpa_2211_Z_mll_10_40	700467-700472
Sherpa_2214_Z	700792-700794
Sherpa_2214_Z_mll_10_40	700901-700903
"""

bkgs["Wjets"]="""
Wjets_lv
Sherpa_2211_W	700338-700349
Sherpa_2211_W_EWK	700362-700364
"""

bkgs["VV"]="""
Diboson
Sherpa2214_VV_EWK 701000,701005,701010,701015,701020,701025,701030,701035
Sherpa2214_VV_QCD 
Sh2214 leptonic 701055,701040,701045,701050,701060,701065
Sh2212 leptonic 700600,700601,700602,700603,700604,700605
semi-leptonic 701085,701090,701095,701100,701105,701110,701115,701120,701125
Sherpa222_VV_QCD	
gg-loop 345705-345706,345718,345723, 364302-364305,
lowMllPt 364288-364290,
"""

bkgs["VVV"]="""
Triboson
Sherpa_VVV	364242-364249 #  WWW->lvlvjj samples are dropped 364336-364339
"""

bkgs["MainTop"]="""
TopPair
PowhegPythia	410470,410471
SingleTop
PowhegPythia-schan	410644-410645
PowhegPythia-tchan	410658-410659,600027-600028
PowhegPythia8-DynScale-Wt	601352,601355
"""

bkgs["topOther"]="""
## rare
#aMcAtNloPythia-tHjb	346229-346234
aMcAtNloPythia-tllq	545027-545028
aMcAtNloPythia-ttll_mll_1_5	410276
aMcAtNloPythia-ttll_mll_1_6	410277
aMcAtNloPythia-ttll_mll_1_7	410278
aMcAtNloPythia-ttt	304014
#aMcAtNloPythia-tWZ	525955
#MadgraphPythia-tttj	525663
#MadGraphPythia-tttt	412043-412044,500326
#MadgraphPythia-tttW	525662
## ttbar
aMcAtNloPythia-ttll	504330,504334,504342
aMcAtNloPythia-ttV	410081
aMcAtNloPythia-ttZ	504338,504346
#MadGraphPythia-ttWZ	500463
#MadGraphPythia-ttZZ	500462
PowhegPythia8_ttH	346343-346345
"""

bkgs["ggH"]="""
Baseline
#H4l	345060,346695,346701
#Het	345125
# Hmt	345124
#HWW	345324
#Hyy	343981
#HZy	345316,345961
Hmm	345097
Htt	345120-345123,# 346531-346532,346564
"""

bkgs["VBFH"]="""
Baseline
#H4l	346228,346317,346696,346702
#Het	346194
#Hmt	346195
#HWW	345948
#Hyy	346214
#HZy	345833-345834
Hbb	345949
Htt	346190-346193, #346569-346570,346573
"""

bkgs["VH"]="""
VH
Description	DSID range
Baseline
#H4l	345066,346645-346646,346697-346700,346703-346706
#Het	345213-345214,345218
#Hincl	346310-346312
#Hinv	345596,346693-346694
#Hmt	345215-345216,345219
#HWW	341450,341452,341454,341456,341458,341460,345325-345327,345336-345337,345433,345445-345446,346524,346560-346561
#Hyy	345061,345317-345319
#HZy	345320-345322,345963-345965
Hbb	345053-345057
Hcc	345109-345113
Hmm	345098,345103-345105
Htt	345211-345212,345217,346329
"""
if __name__ == "__main__":
    printt(bkgs, 2, 'MCRun2')


bkgs23= OrderedDict()

bkgs23["Dijet"]="""
Pythia8	801165-801174
"""
bkgs23["top"]="""
TTbar 601229-601230,601237
SingleTop tW 601352,601355
s,t-chan 601348-601351
"""

bkgs23["topOther"]="""
aMcAtNloPythia-ttll	522024,522028,522032
aMcAtNloPythia-ttZ	522036,522040
PowhegPythia-ttH	602637-602638
Sherpa2214-ttW	700995-700997
aMcAtNloPythia-tllq	545027-545028
aMcAtNloPythia-tttt	523243
aMcAtNloPythia-tWZ	525955
MadgraphPythia-ttHH	525359
MadgraphPythia-tttj	525663
MadgraphPythia-tttW	525662
MadgraphPythia-ttWH	525360
MadgraphPythia-ttWW	525357
MadgraphPythia-ttWZ	525358
MadgraphPythia-ttZZ	525361
"""

bkgs23["VV"]="""
Sherpa_2214_VV 701000,701005,701010,701015,701020,701025,701030,701035,701085,701090,701095,701100,701105,701110,701115,701120,701125
Sh2214 QCD leptonic 701055, 701040,701045,701050,701060,701065
Sh2212 QCD leptonic 700600,700601,700602,700604,700605 # 700603
Sherpa_2214_VVV	700584,700866-700868 # WWW samples are dropped 700864-700865
"""

bkgs23["Vjets"]="""
Sherpa_2214	700777-700794, 700895-700903
"""

if __name__ == "__main__":
    printt(bkgs23, 3, 'MCRun3')
