from setuptools import setup
from Cython.Build import cythonize

setup(
   name='ZnC app',
   ext_modules=cythonize("CalZn.pyx", compiler_directives={'language_level': 3}),
   zip_safe=False,
)

