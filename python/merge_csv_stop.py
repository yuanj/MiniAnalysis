import sys,math,re
import dsid2sample_p6490 as dsid2sample
import logging
from collections import defaultdict

bkgs = {}
bkgs23 = {}

bkgs["dijet"] = """ 
364700-364712
"""
bkgs["Zjets"] ="""
Sherpa_2211_Z 700320-700337
Sherpa_2211_Z2jets 700358-700361
Sherpa_2211_Z_m10_40 700467-700472
Sherpa_2214_Ztautau 700792-700794
Sherpa_2214_Ztt_Mll10_40 700901-700903
"""
bkgs["Wjets"] ="""
700362-700364
Sherpa_2211_W 700338-700349
"""
bkgs["VV"] ="""
Sherpa_ggVV 364302, 364304-364305
Sherpa_WZ/ZZ 700489-700496
Sherpa_VVV 700587-700605
"""
bkgs["VVV"] ="""
Sherpa_WWW (same-sign/opposite-sign) 364336-364339
Sherpa_WWW 364242
Sherpa_WWZ 364243-364244
Sherpa_WZZ 364245-364246
Sherpa_ZZZ 364247-364249
"""
bkgs["SingleTop"] ="""
PowhegPythia_schan 410644-410645
PowhegPythia_tchan 410658-410659
PowhegPythia_tW 601352,601355 # 601353,601354
"""
bkgs["RareTop"] ="""
MP8_3top 304014
aMCPy8EG_tWZ 525955
aMCPy8EG_tllq 545027
"""
bkgs["ttX"] ="""
aMCPy8EG_ttll 504330,504342,504334
MP8_ttbarWW 410081
MGPy8EG_ttV 500462-500463
aMCPy8EG_ttZ 504338, 504346
Sh_2210_ttW 700168
PhPy8EG_ttH125 346343-346345
"""
bkgs["ttbar"] ="""
PhPy8EG_ttbar_hdamp 410470-410471 #410472
"""
# bkgs["Sh_ttbar"] ="""
# Sh_2212_ttbar_maxHTavrgTopPT 700659-700662
# """
# bkgs["HT_ttbar_nonallhad"] ="""
# PhPy8EG_ttbar_HT/MET 407342-407344
# """
# bkgs["MET_ttbar_nonallhad"] ="""
# PhPy8EG_ttbar_HT/MET 407345-407347
# """


bkgs23["dijet"] = """801165-801174"""
bkgs23["Zjets"] ="""700786-700797, 700895-700903, 700849, 700855"""
bkgs23["Wjets"] ="""700777-700785, 700843"""
bkgs23["VV"] ="""
701000, 701005, 701010, 701015, 701020, 701025, 701030, 701035, 701040, 701045, 701050, 701055, 701060,
701065, 701085, 701090, 701095, 701100, 701105, 701110, 701115, 701120, 701125
"""
bkgs23["VVV"] ="""
700864, 700865, 700866, 700867, 700868, 700584
"""
bkgs23["SingleTop"] ="""
601348-601352, 601355 # 601353,601354
"""
bkgs23["RareTop"] ="""
tttt 523243
tWZ 525955
"""
bkgs23["ttbar"] ="""
601229,601230,601237
"""
bkgs23["Sh_ttbar"] ="""700807-700810"""
bkgs23["ttX"] ="""
ttll 522024,522028,522032
ttZ 522036,522040,
ttW 700995-700997
"""
# bkgs23["PhPy_Zjets"] ="""601189-601191"""
# bkgs23["PhPy_Wjets"] ="""601183-601188"""
# bkgs23["MGPy_Zjets"] ="""513105-513116, 513123-513131"""
# bkgs23["MGPy_Wjets"] ="""513093-513104"""

def to_period(name):
    if 'mc20' in name or 'data1' in name:
        return 'run2'
    elif 'mc23' in name or 'data2' in name:
        return 'run3'
    else:
        raise Exception("Sorry, seems neither run2 nor run3")

def to_year(name):
    if 'r13167' in name or 'r14859' in name or 'data15' in name or 'data16' in name or '20a' in name: 
        return '1516'
    if 'r13144' in name or 'r14860' in name or 'data17' in name or '20d' in name: 
        return '17'
    if 'r13145' in name or 'r14861' in name or 'data18' in name or '20e' in name: 
        return '18'
    if 'r14622' in name or 'r14932' in name or 'data22' in name or '23a' in name: 
        return '22'
    if 'r15224' in name or 'data23' in name or '23d' in name: 
        return '23'
    raise Exception("Sorry, failed to know which year")

def to_target(name, run2 = True):
    if 'data' in name: 
        return 'data'
    elif 'ISR' in name:
        match = re.search(r"(StauStauISR_\d+_\d+)_", name)
        if match:
            return match.group(1)
    pattern = r"MGPy8EG_sT_(?P<mode>\w+)_(?P<M1>\d+)_(?P<M2>\d+)_(?P<M3>\d+)_(?P<sub1>\w+)_(?P<sub2_MS>\w+_MS)"
    match = re.search(pattern, name)
    if match:
        return f"sT_{match.group('mode')}_{match.group('M1')}_{match.group('M3')}"
    
    #match = re.search(r"user\.\w+\.(\d{6})", name)
    match = re.search(r"(\d{6})", name)
    if match:
        dsid = int(match.group(1))
        
    if run2 :
        bkg_dict = bkgs
    else:
        bkg_dict = bkgs23
    for process, text in bkg_dict.items():
        nums = dsid2sample.parse_input_string(text)
        if dsid in nums:
            return process
    # print ('dsid_'+str(dsid), ':', name)
    return 'dsid_'+str(dsid)

def read_line(line):
    components = line.split(',')
    name = components[0]
    values = [float(component.split('+-')[0].strip()) for component in components[1:]]
    errors = [float(component.split('+-')[1].strip()) for component in components[1:]]
    return name, values, errors

def sum_lines (values_accumulated, error2s_accumulated, keys):
    lens = len(list(values_accumulated.values())[0])
    if len(keys)==0:
        print("DEBUG: sum empty set")
        return [0]*lens, [0]*lens
    val_list = [ sum([ vals[i] for key, vals in values_accumulated.items() if key in keys]) for i in range(lens) ]
    err_list = [ math.sqrt( sum([ vals[i] for key, vals in error2s_accumulated.items() if key in keys]) ) for i in range(lens) ]
    return val_list, err_list

def get_table_head(inputs):
    Table_title = 'Process, Yields'
    f = inputs[0]
    with open(f, 'r') as file:
        for line in file:
            if line.startswith('Cut name'): 
                Table_title = line
                break
    column_num = len(Table_title.split(',')) - 1
    return Table_title, column_num

def myKey(line):
    switcher = {
        "Wjets"   : 1,
        "Zjets"   : 2,
        "Vjets"   : 3,
        "VV"      : 4,
        "Top"     : 5,
        "Higgs"   : 6,
        "dijet"   : 7,
    }
    if 'Stau' in line: 
        return 1000,line
    elif 'data' in line:
        return 900,line
    elif 'bkg wo dijet' in line:
        return 801, line
    elif 'bkg' in line:
        return 800,line
    elif line.split(',')[0] in switcher:
        return switcher[line.split(',')[0]], line
    else:
        return 799,line

def print_onepart(values_accumulated, error2s_accumulated, Table_title, time_tag, out_prefix):
    if time_tag not in ['1516','17','18','22','23','run2','run3','all']:
        raise Exception("Sorry, wrong tag")
    filtered_keys = [key for key in values_accumulated.keys() if time_tag in key ]

    mc_keys = [key for key in filtered_keys if 'data' not in key and 'ISR' not in key ]
    if len(mc_keys)==0: 
        print(f'skip {time_tag}')
        return
    vals_bkg,errs_bkg = sum_lines(values_accumulated, error2s_accumulated, mc_keys)
    mc_wojj_keys = [key for key in mc_keys if 'dijet' not in key]
    vals_bkg_wojj,errs_bkg_wojj = sum_lines(values_accumulated, error2s_accumulated, mc_wojj_keys)
    
    errors_accumulated = { target : [math.sqrt(error) for error in errors] for target, errors in error2s_accumulated.items()}
    lines = [ target.split('_',1)[1] +', '+ ' , '.join([ '%s +- %s' % (round(value,2),round(error,2)) for value, error in zip ( values_accumulated[target] , errors_accumulated[target]) ])  for target in filtered_keys ]
    lines.append( 'bkg' +', '+ ' , '.join([ '%s +- %s' % (round(value,2),round(error,2)) for value, error in zip (vals_bkg , errs_bkg) ]) )
    lines.append( 'bkg wo dijet' +', '+ ' , '.join([ '%s +- %s' % (round(value,2),round(error,2)) for value, error in zip (vals_bkg_wojj , errs_bkg_wojj) ]) )
    
    sorted_lines = sorted(lines, key = myKey)
    with open(out_prefix+time_tag+'.csv', 'w') as file:
        file.write(Table_title.strip()+'\n')
        for line in sorted_lines:
            file.write(line+'\n')  

def print_parts(inputs, mode, out_prefix):
    if mode == 'y':
        to_timetag = lambda x : to_year(x)+"_"
        time_tags = ['1516','17','18','22','23']
    elif mode == 'r':
        to_timetag = lambda x : to_period(x)+"_"
        time_tags = ['run2','run3']
    elif mode == 'a':
        to_timetag = lambda x : "all_"
        time_tags = ['all']
    else:
        raise Exception("Sorry, unexpected mode: "+mode)

    Table_title, column_num = get_table_head(inputs)
    values_accumulated = defaultdict(lambda : [0]*column_num)
    error2s_accumulated = defaultdict(lambda : [0]*column_num)
    for f in inputs:
        with open(f,'r') as file:
            for line in file:
                if line.startswith('Cut name'): continue 
                phys_short, values, errors = read_line(line)
                merged_name = to_timetag(phys_short) + to_target(phys_short, ("mc20" in phys_short) or ("data1"  in phys_short) )
                for i in range(len(values)):
                    values_accumulated[merged_name][i] += values[i]
                    error2s_accumulated[merged_name][i] += errors[i] ** 2
    for tag in time_tags:
        #try:
        print_onepart(values_accumulated, error2s_accumulated, Table_title, tag, out_prefix)
        #except:
        #    print(f"error: {tag}")

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("inputs", help="The name(s) of input file(s)", nargs='+', default=[])
    parser.add_argument("-o","--outputs", help="The name(s) of output file(s)", default='output')
    args = parser.parse_args()
 
    print_parts(args.inputs,'y',args.outputs) # year
    print_parts(args.inputs,'r',args.outputs) # run
    print_parts(args.inputs,'a',args.outputs) # all
