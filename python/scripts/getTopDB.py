import json
import re

JsonFile="/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/TheoreticalUncer/merge.json"
TopDBFile="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC16-13TeV.data"

Top_kFactor={}

with open(JsonFile) as f:
    OriginDB = json.load(f)

with open(TopDBFile) as t:
    for line in t:
        oneline = line.split()
        if len(oneline)!=0 and re.match('[0-9]+',oneline[0]):
            Top_kFactor[str(oneline[0])] = float(oneline[2])
            

kFactor = OriginDB["kFactor"]
for key in kFactor:
    if key in Top_kFactor:
        kFactor[key] = Top_kFactor[key]

#print(kFactor)

OriginDB["kFactor"] = kFactor

with open('yields.json', 'w') as outfile:
    json.dump(OriginDB, outfile,indent=4)
