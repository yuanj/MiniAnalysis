# 2022 Oct 11, Jiarong Yuan
# Merge metadata json file for MiniAnalysis framework
import json
import os

path='./run/'

files=[]
for file in os.listdir("./run"):
    if file.endswith(".json"):
        #print(os.path.join("./run", file))
        files+=[file]

rangeID = lambda start, end: range(start, end + 1)

Signal=[
        (rangeID(516006,516007),206,5.4004E-02),
        (rangeID(516006,516007),207,1.9924E-02),
        (rangeID(516008,516009),206,1.1802E-02),
        (rangeID(516008,516009),207,4.4947E-03),
        ]


from collections import defaultdict
metadata = {'Nnorm':defaultdict(lambda: defaultdict(lambda: defaultdict(int))),'filterEff': defaultdict(lambda: -99),'kFactor': defaultdict(lambda: -99),'xSection':defaultdict(lambda: defaultdict(lambda: -99)),'lumi':defaultdict(int)}
for afile in files:
    f = open(path+afile)
    data = json.load(f)
    f.close()
    for process in data['Nnorm']:
        for number in data['Nnorm'][process]:
            for MCcompaign in data['Nnorm'][process][number]:
                try:
                    metadata['Nnorm'][process][number][MCcompaign]+=data['Nnorm'][process][number][MCcompaign]
                except:
                    print("Error from Nnorm of p,n,y: ",process,number,MCcompaign)
    for number in data['filterEff']:
        if(metadata['filterEff'][number] == -99):
            metadata['filterEff'][number] = data['filterEff'][number]
        elif(metadata['filterEff'][number] != data['filterEff'][number]):
            print("warning for different filterEff!!")
    for number in data['kFactor']:
        if(metadata['kFactor'][number] == -99):
            metadata['kFactor'][number] = data['kFactor'][number]
        elif(metadata['kFactor'][number] != data['kFactor'][number]):
            print("warning for different kFactor!!")
    for process in data['xSection']:
        for number in data['xSection'][process]:
            if(metadata['xSection'][process][number] == -99):
                metadata['xSection'][process][number] = data['xSection'][process][number]
            elif(metadata['xSection'][process][number] != data['xSection'][process][number]):
                print("warning for different Xsec!!")

metadata['lumi']['18'] = 58450.120704
metadata['lumi']['17'] = 44307.39456
metadata['lumi']['16'] = 36207.680256

for xsec in Signal:
    for number in xsec[0]:
        metadata['xSection'][str(xsec[1])][str(number)] = xsec[2]

with open("MetaDB.json", "w") as outfile:
    json.dump(metadata, outfile, indent = 4)
